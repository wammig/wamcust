package lexis.wamcust.service.java8;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.service.java8.AccountWamServiceJava8;

import lexis.wamcust.WamcustApplication;
import lexis.wamcust.base.BaseTest;
import lexis.wamcust.context.DataSourceConfig;
import lexis.wamcust.context.WebConfig;
import reactor.core.publisher.Flux;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { WamcustApplication.class,
		DataSourceConfig.class , WebConfig.class})
public class AccountWamServiceJava8Tests extends BaseTest{

	public final static Logger logger = LoggerFactory.getLogger(AccountWamServiceJava8Tests.class);
	
	Predicate accountNameValue = (v) -> ((String) v).contains("005");
	
	@Autowired
	private WebApplicationContext ctx;
	
	@Autowired
	private AccountWamServiceJava8 accountWamServiceJava8;
	
	private MockMvc mockMvc;
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
		.build();
	}
	
	
	@Test
	public void testSearchLbuAccount2Java8()
	{
		Subscriber<AdminDataTables> mySubscriber = new Subscriber<AdminDataTables>() {

			@Override
			public void onComplete() {
				output.accept(" testSearchLbuAccount2Java8 complete ");
				
			}

			@Override
			public void onError(Throwable arg0) {
				output.accept(" testSearchLbuAccount2Java8 error " + arg0.getMessage());
				
			}

			@Override
			public void onNext(AdminDataTables arg0) {
				output.accept("------------onNext AdminDataTables-----------------");
				output.accept(arg0.getData().toString());
				
				Consumer<LbuAccount> onNext = (v) -> output.accept("**************** lbuAccount " + v.getLbuAccountId());
				
				ArrayList<LbuAccount> list = (ArrayList<LbuAccount>)arg0.getData();
				output.accept("list size " + list.size());
				
				/*
				Boolean allAccountNameValueValid = list
				.stream()
				.allMatch(accountNameValue)
				;
				assertEquals(allAccountNameValueValid, true);
				*/
				//output.accept("allAccountNameValueValid.toString()");
			
			}

			@Override
			public void onSubscribe(Subscription subs) {
				subs.request(Long.MAX_VALUE);
				
			}
			
		};
		
		try {
			accountWamServiceJava8
			.searchLbuAccount2Java8(null, "005", null, null, null, null, null, null, null, null, null, null, null)
			.subscribe(mySubscriber)
			;
			
			showThreads();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
