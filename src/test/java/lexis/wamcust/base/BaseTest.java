package lexis.wamcust.base;

import java.util.function.Consumer;

import org.reactivestreams.Publisher;

import reactor.core.publisher.Mono;

public class BaseTest {

	public Consumer<String> output = (v) -> System.out.println(v);
	
	Runnable taskCurrentThread = () ->{
		output.accept("----------taskCurrentThread-----------");
		output.accept(Thread.currentThread().getName());
	};
	
	public Publisher<Void> showThreads()
	{
		Mono<Void> execThread = Mono.fromRunnable(taskCurrentThread);
		
		return execThread;
	}
	
}
