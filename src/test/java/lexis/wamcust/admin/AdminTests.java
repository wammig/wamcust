package lexis.wamcust.admin;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;

import lexis.wamcust.WamcustApplication;
import lexis.wamcust.base.BaseTest;
import lexis.wamcust.context.DataSourceConfig;
import lexis.wamcust.context.WebConfig;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { WamcustApplication.class,
		DataSourceConfig.class , WebConfig.class})
public class AdminTests extends BaseTest{

	public final static Logger logger = LoggerFactory.getLogger(AdminTests.class);
	
	@Autowired
	private WebApplicationContext ctx;
	
	private MockMvc mockMvc;
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
		.build();
	}
	
	@Test
	public void testAdminHomePage() throws Exception
	{
		mockMvc.perform(get("/admin"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(view().name("admin/default/index"))
		;
	}
}
