package lexis.wamcust.admin;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;

import lexis.wamcust.WamcustApplication;
import lexis.wamcust.base.BaseTest;
import lexis.wamcust.context.DataSourceConfig;
import lexis.wamcust.context.WebConfig;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { WamcustApplication.class,
		DataSourceConfig.class , WebConfig.class})
public class ApiTests extends BaseTest{

public final static Logger logger = LoggerFactory.getLogger(ApiTests.class);
	
	@Autowired
	private WebApplicationContext ctx;
	
	private MockMvc mockMvc;
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
		.build();
	}
	
	@Test
	public void testApiHomePage() throws Exception
	{
		MvcResult result = mockMvc.perform(get("/api/index.json"))
		.andDo(print())
		.andExpect(status().isOk())
		.andReturn()
		;
		
		logger.info("----------------------testApiHomePage------------------------");
		
		
		String resultString = result.getResponse().getContentAsString();
		output.accept(resultString);
		
		/**
		 * first way to get length of json
		 */
		int length = JsonPath
		        .parse(resultString)
		        .read("$.length()");
		
		assertThat(length).isEqualTo(8);
		
		/**
		 * second way to get length of json
		 */
		JSONObject jsonObject = (JSONObject) JSONValue.parse(resultString);
		
		assertThat(jsonObject.size()).isEqualTo(8);
		
		logger.info("----------------------jsonObject.entrySet().stream()-----------------------------");
		jsonObject.entrySet().stream()
		.map( v -> v )
		.forEach((v) -> {
			Entry entry = v; 
			output.accept(entry.getKey().toString()); 
			output.accept(null != entry.getValue() ? entry.getValue().toString() : null);
		})
		;
		logger.info("----------------------end testApiHomePage------------------------");
	}
	
}
