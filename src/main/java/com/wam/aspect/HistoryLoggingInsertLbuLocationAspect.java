package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.BaseService;
import com.wam.service.LocationWamService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;

@Component
@Aspect
public class HistoryLoggingInsertLbuLocationAspect {

	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingInsertLbuLocationAspect.class);
	
	public Map<String,Object> historyLoggingInsertLbuLocationAround(Map<String,Object> outMap) throws Exception 
	{
		LbuLocation lbuLocation = (LbuLocation)outMap.get("lbuLocation");
		
		SysHistoryCreateStoredProcedure shsp = new SysHistoryCreateStoredProcedure(dataSource);
		
		SysHistory sysHistory = new SysHistory();
		sysHistory.setHistoryId(null);
		sysHistory.setProcessType(Definition.LOCATION);
		sysHistory.setProcessSubType(Definition.CREATE);
		sysHistory.setEntityId(lbuLocation.getLbuSetId() + " "  + lbuLocation.getLbuAccountId() + " " + lbuLocation.getLbuLocationId());
		sysHistory.setSendStatus(null);
		sysHistory.setReplyStatus(null);
		sysHistory.setErrorCode(null);
		sysHistory.setErrorMessage(null);
		sysHistory.setTransId(BaseService.generateTransIdEcm());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(null);
		sysHistory.setCreatedBy(Definition.PLSQL_LOCATION);
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
				sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
				sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
				sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
				);
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			LocationWamService.logger.info("Create Location Writed into SysHistory successfully : " + shspOutmap.toString());
		} else {
			LocationWamService.logger.error("returned error from SysHistoryCreateStoredProcedure insertLbuLocation: " + shspOutmap.toString());
		}
		
		return outMap;
	}
	
}
