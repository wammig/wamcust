package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateStoredProcedure;
import com.wam.utility.definition.Definition;

@Component
@Aspect
public class HistoryLoggingSyncOrderReplyAspect {
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingSyncOrderReplyAspect.class);
	
	public Map<String,Object> historyLoggingSyncOrderReplyAround(SysHistory sysHistory, DataSource dataSource) throws Exception 
	{
		//out.println("HistoryLoggingSyncOrderReplyAspect historyLoggingSyncOrderReplyAround detected");
		
		SysHistoryUpdateStoredProcedure shsp = new SysHistoryUpdateStoredProcedure(dataSource);
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
							sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
							sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
							sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getUpdatedBy(),
							sysHistory.getReplyMessage()
						 );
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			logger.info("SyncOrderReply Update SysHistory successfully : " + shspOutmap.toString());
		} else {
			logger.error("returned error from historyLoggingSyncOrderReplyAround : " + shspOutmap.toString());
		}
			
		return shspOutmap;
	}
	
}
