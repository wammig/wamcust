package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.BaseService;
import com.wam.service.LocationWamService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;

@Component
@Aspect
public class HistoryLoggingUpdateLbuLocationAspect {

	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingUpdateLbuLocationAspect.class);
	
	public Map<String,Object> historyLoggingUpdateLbuLocationAround(Map<String,Object> outMap) throws Exception 
	{
		
		LbuLocation lbuLocation = (LbuLocation)outMap.get(Definition.LBU_LOCATION);
		SysHistoryCreateStoredProcedure shsp = new SysHistoryCreateStoredProcedure(dataSource);
		
		SysHistory sysHistory = new SysHistory();
		sysHistory.setHistoryId(null);
		sysHistory.setProcessType(Definition.LOCATION);
		sysHistory.setProcessSubType(Definition.UPDATE);
		sysHistory.setEntityId(lbuLocation.getLbuSetId() + " " + lbuLocation.getLbuAccountId() + " " + lbuLocation.getLbuLocationId());
		
		if(outMap.containsKey("O_ROWCOUNT"))
		{
			if( ((BigDecimal)outMap.get("O_ROWCOUNT")).intValue() > 0)
			{
				if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() == 0)
				{
					sysHistory.setSendStatus(Definition.SUCCESS);
				} else {
					sysHistory.setSendStatus(Definition.FAILURE);
				}
			}
		}
		sysHistory.setReplyStatus(null);
		
		sysHistory.setErrorCode(outMap.get("ERROR_CODE").toString());
		if(outMap.containsKey("ERROR_DESC"))
		{
			if(outMap.get("ERROR_DESC") == null)
			{
				sysHistory.setErrorMessage(null);
			} else {
				sysHistory.setErrorMessage(outMap.get("ERROR_DESC").toString());
			}
		}
		
		sysHistory.setTransId(BaseService.generateTransIdSysHistory());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(null);
		sysHistory.setCreatedBy(Definition.PLSQL_LOCATION);
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
				sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
				sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
				sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
				);
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			logger.info("Create Location Writed into SysHistory successfully : " + shspOutmap.toString());
		} else {
			logger.error("returned error from SysHistoryCreateStoredProcedure updateLbuLocation: " + shspOutmap.toString());
		}
		
		return outMap;
	}
	
}
