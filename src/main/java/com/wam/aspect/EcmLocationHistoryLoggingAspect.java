package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.BaseService;
import com.wam.service.LocationWamService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryUpdateStoredProcedure;
import com.wam.utility.definition.Definition;
import com.wam.utility.net.HttpUtility;

@Component("EcmLocationHistoryLoggingAspect")
@Aspect
public class EcmLocationHistoryLoggingAspect {

	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;

	public final static Logger logger = LoggerFactory.getLogger(EcmLocationHistoryLoggingAspect.class);
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public void setHistoryPackage(HistoryPackage historyPackage)
	{
		this.historyPackage = historyPackage;
	}
	
	/**
	 * 
	 * @param outMap
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> historyLoggingLocationEcmWsAround(Map<String, Object> outMap) throws Exception 
	{
		
		//out.println("historyLoggingLocationEcmWsAround detected");
		
		LbuLocation lbuLocation = (LbuLocation)outMap.get(Definition.LBU_LOCATION);
		Map<String, String> returnedMapFromEcm = (Map<String, String>) outMap.get(Definition.RETURNED_MAP_FROM_ECM);
		
		if(outMap.size() == 0)
		{
			//throw new Exception(" returnMap in createPlaceOfBusinessEcmWs is empty ");
			return new HashMap<String,Object>();
		}
		
		/**
		 * when ecm returned 
		 * log in sysHistory
		 */
		SysHistoryUpdateStoredProcedure shsp = new SysHistoryUpdateStoredProcedure(dataSource);
		SysHistory sysHistory = historyPackage.findSysHistorySearchLast(null, Definition.LOCATION, null, null, null, lbuLocation.getLbuAccountId(), lbuLocation.getLbuLocationId(), null, null, null, null, null, null, null);
		
		sysHistory.setProcessType(Definition.LOCATION);
		sysHistory.setProcessSubType(outMap.get(Definition.ACTION).toString());
		sysHistory.setEntityId(lbuLocation.getLbuSetId() + " " + lbuLocation.getLbuAccountId() + " " + lbuLocation.getLbuLocationId());
		sysHistory.setSendStatus(Definition.SUCCESS);
		sysHistory.setReplyStatus(outMap.get(Definition.REPLY_STATUS).toString());
		if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) {
			sysHistory.setErrorCode(returnedMapFromEcm.get(Definition.RESPONSE_CODE).toString());
		} else {
			sysHistory.setErrorCode(null);
		}
		if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) {
			sysHistory.setErrorMessage(returnedMapFromEcm.get(Definition.RESPONSE).toString());
		} else {
			sysHistory.setErrorMessage(null);
		}
		sysHistory.setTransId(BaseService.generateTransIdEcm());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(outMap.get(Definition.PARAMS).toString());
		sysHistory.setCreatedBy(Definition.ECM);
		sysHistory.setReplyMessage(outMap.get(Definition.RESPONSE).toString());
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
				sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
				sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
				sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy(),
				sysHistory.getReplyMessage()
				);
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			/**
			 *  here is always UPDATE. Because it's update the returned data from ecm after insert db
			 */
			logger.info("Update Location Writed into SysHistory successfully : " + shspOutmap.toString());
		} else {
			logger.error("returned error from SysHistoryUpdateStoredProcedure : " + shspOutmap.toString());
		}
		/**
		 * end log in sysHistory
		 */
		
		return outMap;
	}
}
