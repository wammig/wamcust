package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.service.AccountWamService;
import com.wam.service.LocationWamService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateStoredProcedure;
import com.wam.utility.definition.Definition;
import com.wam.utility.net.HttpUtility;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;

@Component
@Aspect
public class HistoryLoggingAspect {
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingAspect.class);
}
