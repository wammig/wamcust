package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.BaseService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;

@Component
@Aspect
public class HistoryLoggingUpdateLbuAccountAspect {

	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingUpdateLbuAccountAspect.class);
	
	//@Around("execution(* com.wam.stored.packages.LbuAccountPackage.updateLbuAccount(..) )")
	public Map<String,Object> historyLoggingUpdateLbuAccountAround(Map<String,Object> outMap /*ProceedingJoinPoint joinPoint*/) throws Exception 
	{
		//out.println("historyLoggingUpdateLbuAccountAround detected ");	
		//Map<String,Object> outMap = (Map<String,Object>)joinPoint.proceed();
		LbuAccount lbuAccount = (LbuAccount)outMap.get("lbuAccount");
		
		SysHistoryCreateStoredProcedure shsp = new SysHistoryCreateStoredProcedure(dataSource);
		SysHistory sysHistory = new SysHistory();

		if(outMap.containsKey("O_ROWCOUNT"))
		{
			if( ((BigDecimal)outMap.get("O_ROWCOUNT")).intValue() > 0)
			{
				if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() == 0)
				{
					sysHistory.setSendStatus(Definition.SUCCESS);
				} else {
					sysHistory.setSendStatus(Definition.FAILURE);
				}
			}
		}

		sysHistory.setHistoryId(null);
		sysHistory.setProcessType(Definition.ACCOUNT);
		sysHistory.setProcessSubType(Definition.UPDATE);
		sysHistory.setEntityId(lbuAccount.getLbuSetId() + " " + lbuAccount.getLbuAccountId());
		
		sysHistory.setReplyStatus(null);
		sysHistory.setErrorCode(null);

		if(outMap.containsKey("ERROR_DESC"))
		{
			if(outMap.get("ERROR_DESC") == null)
			{
				sysHistory.setErrorMessage(null);
			} else {
				sysHistory.setErrorMessage(outMap.get("ERROR_DESC").toString());
			}
		}
		

		sysHistory.setTransId(BaseService.generateTransIdEcm());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(null);
		sysHistory.setCreatedBy(Definition.PLSQL_ACCOUNT);
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
				sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
				sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
				sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
				);
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			//out.println("Update Account Writed into SysHistory successfully : " + shspOutmap.toString());
		} else {
			//out.println("returned error from SysHistoryCreateStoredProcedure updateLbuAccount : " + shspOutmap.toString());
		}
		
		return outMap;
	}
	
}
