package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.BaseService;
import com.wam.service.ContentSubWamService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;

@Component
@Aspect
public class HistoryLoggingInsertLbuContentSubAspect {
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingInsertLbuContentSubAspect.class);
	
	public Map<String,Object> historyLoggingInsertLbuContentSubAround(Map<String,Object> outMap) throws Exception 
	{
		//out.println("HistoryLoggingInsertLbuContentSubAspect historyLoggingInsertLbuContentSubAround detected");
		
		LbuContentSub lbuContentSub = (LbuContentSub)outMap.get("lbuContentSub"); 
		
		SysHistoryCreateStoredProcedure shsp = new SysHistoryCreateStoredProcedure(dataSource);
		
		SysHistory sysHistory = new SysHistory();
		sysHistory.setHistoryId(null);
		sysHistory.setProcessType(Definition.CONTENTSUB);
		sysHistory.setProcessSubType(Definition.CREATE);
		sysHistory.setEntityId(lbuContentSub.getLbuSetId() + " " + lbuContentSub.getLbuAccountId() + " " + lbuContentSub.getLbuContentSubId());
		/**
		 * we never send contentSub to order ws.
		 * the send status and reply status is always null 
		 */
		sysHistory.setSendStatus(null);
		sysHistory.setReplyStatus(null);
		sysHistory.setErrorCode(null);
		sysHistory.setErrorMessage(null);
		sysHistory.setTransId(BaseService.generateTransIdEcm());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(null);
		sysHistory.setCreatedBy(Definition.PLSQL_CONTENTSUB);
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
							sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
							sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
							sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
						 );
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			ContentSubWamService.logger.info("Create ContentSub Writed into SysHistory successfully : " + shspOutmap.toString());
		} else {
			ContentSubWamService.logger.error("returned error from SysHistoryCreateStoredProcedure insertLbuContentSub: " + shspOutmap.toString());
		}
			
			
		return outMap;
	}
	
}
