package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;

@Component
@Aspect
public class HistoryLoggingInsertLbuAgreementAspect {
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingInsertLbuAgreementAspect.class);
	
	public Map<String,Object> historyLoggingInsertLbuAgreementAround(Map<String,Object> outMap) throws Exception 
	{
		//out.println("HistoryLoggingInsertLbuAgreementAspect historyLoggingInsertLbuAgreementAround detected");
		
		LbuContentSub lbuContentSub = (LbuContentSub)outMap.get(Definition.CONTENTSUB); 
		
		SysHistoryCreateStoredProcedure shsp = new SysHistoryCreateStoredProcedure(dataSource);
		
		SysHistory sysHistory = new SysHistory();
		sysHistory.setHistoryId(null);
		sysHistory.setProcessType(Definition.AGREEMENT);
		sysHistory.setProcessSubType(Definition.CREATE);
		sysHistory.setEntityId(lbuContentSub.getLbuSetId() + Definition.SPACE + lbuContentSub.getLbuAccountId() + Definition.SPACE + outMap.get("OUT_AGREEMENT_ID").toString());
		sysHistory.setSendStatus(null);
		sysHistory.setReplyStatus(null);
		sysHistory.setErrorCode(null);
		sysHistory.setErrorMessage(null);
		sysHistory.setTransId(outMap.get(Definition.GENERATED_TRANS_ID).toString());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(null);
		sysHistory.setCreatedBy(Definition.PLSQL_AGREEMENT);
		
		Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
							sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
							sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
							sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
						 );
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			logger.info("Create Agreement Writed into SysHistory successfully : " + shspOutmap.toString());
		} else {
			logger.error("returned error from historyLoggingInsertLbuAgreementAround: " + shspOutmap.toString());
		}
			
			
		return outMap;
	}
	
}
