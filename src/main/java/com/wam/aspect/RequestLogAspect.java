package com.wam.aspect;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.wam.model.ldap.AdminUser;
import com.wam.service.AdminService;

import static java.lang.System.out;

import java.util.Date;

@Aspect
@Component
public class RequestLogAspect {
	public final static Logger logger = LoggerFactory.getLogger(RequestLogAspect.class);
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	@Resource(name = "adminUser")
	public AdminUser adminUser;
	
    @Around("@annotation(org.springframework.web.bind.annotation.RequestMapping) && execution(public * *(..))")
    public Object log(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .currentRequestAttributes())
                .getRequest();

        Object value;

        try {
            value = proceedingJoinPoint.proceed();
            
            
        } catch (Throwable throwable) {
            throw throwable;
        } finally {
        	
        	/**
        	 * save ps history in logMessage
        	 */
        	if(request.getQueryString() != null) 
        	{
        		if(request.getQueryString().length() != 0)
            	{
            		if(request.getQueryString().contains("prc=ord&") && !request.getRequestURI().contains("/ajax"))
            		{
            			String  url = request.getRequestURL() + "?" + request.getQueryString();
            			createMessagePsOrderUrl(url);
            		}
            	}
        	}
        	
        	/**
        	 * protect bowam
        	 */
        	if((request.getRequestURI().contains("/api") || request.getRequestURI().contains("/admin")) && !request.getRequestURI().contains("/security"))
        	{
        		if(null == adminUser.getWindowsId())
        		{
        			return "redirect:" + "/security/login";
        		}
        	}
        	
        	logger.info(
        			new Date().toString() + " " +
                    request.getMethod() + " " +
                    request.getRequestURI() + " " +
                    request.getRemoteAddr() + " " +
                    request.getRequestedSessionId() );
        }

        return value;
    }
    
    /**
     * save ps visit in log message
     * @param url
     * @return
     * @throws Exception
     */
    public Boolean createMessagePsOrderUrl(String url) throws Exception
    {
    	return adminService.logMessageCreateForPSOrderUrl(url);
    }
}