package com.wam.aspect;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.BaseService;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;


@Component
@Aspect
public class HistoryLoggingInsertLbuAccountAspect {
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryPackage historyPackage;
	
	public final static Logger logger = LoggerFactory.getLogger(HistoryLoggingInsertLbuAccountAspect.class);
	
	//@Around("execution(* com.wam.stored.packages.LbuAccountPackage.insertLbuAccount(..) )")
	public Map<String,Object> historyLoggingInsertLbuAccountAround(Map<String,Object> outMap /*ProceedingJoinPoint joinPoint*/) throws Exception 
	{
		//out.println("HistoryLoggingInsertLbuAccountAspect historyLoggingInsertLbuAccountAround detected");
		
		//Map<String,Object> outMap = (Map<String,Object>)joinPoint.proceed();
		LbuAccount lbuAccount = (LbuAccount)outMap.get("lbuAccount");
		
		SysHistory sysHistory = new SysHistory();		
		sysHistory.setHistoryId(null);
		sysHistory.setProcessType(Definition.ACCOUNT);
		sysHistory.setProcessSubType(Definition.CREATE);
		sysHistory.setEntityId(lbuAccount.getLbuSetId() + " " + outMap.get("OUTPUT_LBU_ACCOUNT_ID").toString());
		sysHistory.setSendStatus(null);
		sysHistory.setReplyStatus(null);
		sysHistory.setErrorCode(null);
		
		if(outMap.get("ERROR_MESSAGE") == null)
		{
			sysHistory.setErrorMessage(null);
		} else {
			sysHistory.setErrorMessage(outMap.get("ERROR_MESSAGE").toString());
		}

		sysHistory.setTransId(BaseService.generateTransIdEcm());
		sysHistory.setMessageId(null);
		sysHistory.setMessage(null);
		sysHistory.setCreatedBy(Definition.PLSQL_ACCOUNT);
		
		Map shspOutmap = historyPackage.sysHistoryCreate(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
							sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
							sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
							sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
						 );
		
		if( ((BigDecimal)shspOutmap.get("ERROR_CODE")).intValue() == 0)
		{
			if(!shspOutmap.containsKey("O_HISTORY_ID"))
			{
				throw new Exception("no O_HISTORY_ID in historyLoggingInsertLbuAccountAround");
			}
				
			//out.println("Create Account Writed into SysHistory successfully historyLoggingInsertLbuAccountAround: " + shspOutmap.toString());
		} else {
			//out.println("returned error from SysHistoryCreateStoredProcedure historyLoggingInsertLbuAccountAround : " + shspOutmap.toString());
		}

		return outMap;

	}
}
