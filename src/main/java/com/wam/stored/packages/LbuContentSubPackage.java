package com.wam.stored.packages;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wam.aspect.HistoryLoggingInsertLbuContentSubAspect;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.service.BaseService;
import com.wam.service.ContentSubWamService;
import com.wam.service.WamService;
import com.wam.stored.procedure.LbuContentSubCreateStoredProcedure;
import com.wam.stored.procedure.LbuContentSubSearchStoredProcedure;
import com.wam.stored.procedure.LbuContentSubUpdateAgreementIdStoredProcedure;
import com.wam.stored.procedure.LbuContentSubUpdateAgreementPguidStoredProcedure;
import com.wam.stored.procedure.LbuContentSubUpdateAssetAgreementIdStoredProcedure;
import com.wam.stored.procedure.LbuContentSubUpdateStoredProcedure;
import com.wam.stored.procedure.LbuContentSubSelectByIdStoredProcedure;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.test.db.BaseTest;
import com.wam.utility.definition.Definition;

import static java.lang.System.out;

/**
 * This is a class which contains all the methods to be called in ContentSub Package in oracle
 * extends BaseTest is for the main function to connect to get the application context
 * 
 * the dataSource in BaseTest will be surcharged by the autowaired dataSource 
 * in web application
 * 
 * But the main function will use the dataSource defined in BaseTest to do all the test cases
 * 
 * @author QINC1
 *
 */

@Service("LbuContentSubPackage")
public class LbuContentSubPackage extends BaseTest{
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryLoggingInsertLbuContentSubAspect historyLoggingInsertLbuContentSubAspect;
	
	public LbuContentSubPackage()
	{
		/**
		 * if the autowired dataSource is null.
		 * So it means it's executed by the main function.
		 * We use the dataSource defined in BaseTest
		 */
		if(dataSource == null) {
			dataSource = super.dataSource;
		}
	}

	/**
	 * insert contentSub into table LBU_CONTENTSUB
	 * 
	 * @param lbuContentSub
	 * @return
	 * @throws Exception
	 */
	public LbuContentSub insertLbuContentSub(LbuContentSub lbuContentSub) throws Exception
	{
		LbuContentSubCreateStoredProcedure sp = new LbuContentSubCreateStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				lbuContentSub.getLbuContentSubId(),
				lbuContentSub.getContractNum(),
				lbuContentSub.getContractLineNum(),
				lbuContentSub.getContractLineNumOrigin(),
				lbuContentSub.getContentSubName(),
				lbuContentSub.getLbuAccountId(),
				lbuContentSub.getLbuContentSubId(),
				lbuContentSub.getBeginDate(),
				lbuContentSub.getEndDate(),
				lbuContentSub.getAuditUserId(),
				lbuContentSub.getMaxUsers(),
				lbuContentSub.getMonthlyCommitment(),
				lbuContentSub.getMonthlyCap(),
				lbuContentSub.getLbuSourcePackageId(),
				lbuContentSub.getUpdatedDatetime(),
				lbuContentSub.getCreatedDatetime(),
				lbuContentSub.getLbuSetId(),
				lbuContentSub.getLbuLocationId(),
				lbuContentSub.getAddressSeq(),
				lbuContentSub.getAgreementPguid(),
				lbuContentSub.getAssetAgreementId(),
				lbuContentSub.getNoReNew()
		);

		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true || outMap.containsKey("OUTPUT_LBU_CONTENT_SUB_ID") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=\"\", OUTPUT_LBU_CONTENT_SUB_ID=9900012, ERROR_CODE=0}");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		outMap.put("lbuContentSub", lbuContentSub);
				
		//historyLoggingInsertLbuContentSubAspect.historyLoggingInsertLbuContentSubAround(outMap);
		
		return lbuContentSub;
	}
	
	
	/**
	 * call stored procedure to update agreementId
	 * @param contentSubId
	 * @param AGREEMENT_PGUID
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> updateLbuContentSubAgreementId(String contentSubId, String agreementPguId) throws Exception
	{		
		LbuContentSubUpdateAgreementIdStoredProcedure sp = new LbuContentSubUpdateAgreementIdStoredProcedure(dataSource);
		Map outMap = sp.execute(
							contentSubId,
							agreementPguId
						 );
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=\"\", ERROR_CODE=0}");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		// todo 
		// write into SysHistory 
		
		ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("lbuContentSubId " + contentSubId + " agreementPguid " + agreementPguId + " updated ", ResponseMessage.SUCCESS));
		
		return outMap;
	}
	
	/**
	 * 
	 * @param contentSubId
	 * @param assetAgreementId
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> updateLbuContentSubAssetAgreementId(String contentSubId, String assetAgreementId) throws Exception
	{	
		LbuContentSubUpdateAssetAgreementIdStoredProcedure sp = new LbuContentSubUpdateAssetAgreementIdStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						contentSubId,
						assetAgreementId
					 );
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=\"\", ERROR_CODE=0}");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		return outMap;
	}
	
	/**
	 * 
	 * @param contentSubId
	 * @param agreementPguid
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> updateLbuContentSubAgreementPguid(String contentSubId, String agreementPguid) throws Exception
	{	
		LbuContentSubUpdateAgreementPguidStoredProcedure sp = new LbuContentSubUpdateAgreementPguidStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						contentSubId,
						agreementPguid
					 );
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=\"\", ERROR_CODE=0}");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		return outMap;
	}
	
	/**
	 * 
	 * @param lbuContentSub
	 * @param sessionId
	 * @return
	 * @throws Exception
	 */
	public LbuContentSub updateLbuContentSub(LbuContentSub lbuContentSub, String sessionId) throws Exception
	{
		LbuContentSubUpdateStoredProcedure sp = new LbuContentSubUpdateStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				        lbuContentSub.getLbuContentSubId(),
						lbuContentSub.getLbuSetId(),
						lbuContentSub.getContractNum(),
						lbuContentSub.getContractLineNum(),
						lbuContentSub.getContractLineNumOrigin(),
						lbuContentSub.getLbuAccountId(),
						lbuContentSub.getLbuLocationId(),
						lbuContentSub.getContentSubName(),
						lbuContentSub.getBeginDate(),
						lbuContentSub.getEndDate(),
						lbuContentSub.getMaxUsers(),
						(float)lbuContentSub.getMonthlyCommitment(),
						(float)lbuContentSub.getMonthlyCap(),
						lbuContentSub.getLbuSourcePackageId(),
						lbuContentSub.getAuditUserId(),
						lbuContentSub.getNoReNew(),
						sessionId,
						lbuContentSub.getAddressSeq()
					 );
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_DESC") != true || outMap.containsKey("O_ROWCOUNT") != true)
		{
			throw new Exception("Oracle stocked procedure error.");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
			String className = this.getClass().toString();
			
			throw new Exception(className + " " + methodName + " LbuContentSubUpdateStoredProcedure code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_DESC"));
		}
		
		if(outMap.containsKey("O_ROWCOUNT"))
		{
			if( ((BigDecimal)outMap.get("O_ROWCOUNT")).intValue() > 0)
			{
				lbuContentSub.setIsChanged(true);
			} else {
				lbuContentSub.setIsChanged(false);
			}
		}
		
		return lbuContentSub;
	}
	/**
	 * 
	 * @param setId
	 * @param contentSubId
	 * @return
	 * @throws Exception
	 */
	public LbuContentSub LbuContentSubSelectById(
			 String accountId,
			 String contractNum,
			 Integer contractLineNum,
			 String sourcePackageId
			 ) throws Exception
	{
		LbuContentSubSelectByIdStoredProcedure sp = new LbuContentSubSelectByIdStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				  accountId,
				  contractNum,
				  contractLineNum,
				  sourcePackageId
				);

		LbuContentSub lbuContentSub = (LbuContentSub)sp.transformContentSubCursorToObject(outMap);
		
		return lbuContentSub;
	}
	
	/**
	 * lbuContentSubSearch
	 * @param setId
	 * @param accountId
	 * @param contractNum
	 * @param contractLineNum
	 * @param lbuContentSubId
	 * @param gbsContentSubId
	 * @param packageId
	 * @param contentSubName
	 * @param salesRepId
	 * @param auditUserId
	 * @param beginDate
	 * @param endDate
	 * @param sourcePackageId
	 * @return
	 * @throws Exception
	 */
	public LbuContentSubs lbuContentSubSearch(
			 String setId,
			 String accountId,
			 String contractNum,
			 String contractLineNum,
			 String lbuContentSubId,
			 String gbsContentSubId,
			 String packageId,
			 String contentSubName,
			 String salesRepId,
			 String auditUserId,
			 String beginDate,
			 String endDate,
			 String sourcePackageId,
			 String assetAgreement
			) throws Exception
	{
		LbuContentSubSearchStoredProcedure sp = new LbuContentSubSearchStoredProcedure(dataSource);

		Map outMap = sp.execute(
					  setId,
					  accountId,
					  contractNum,
					  contractLineNum,
					  lbuContentSubId,
					  gbsContentSubId,
					  packageId,
					  contentSubName,
					  salesRepId,
					  auditUserId,
					  beginDate,
					  endDate,
					  sourcePackageId,
					  assetAgreement
					);

		return (LbuContentSubs)sp.transformContentSubCursorToObject(outMap);
	}
	
	
}
