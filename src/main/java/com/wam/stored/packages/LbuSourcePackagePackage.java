package com.wam.stored.packages;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.LbuSourcePackage;
import com.wam.stored.procedure.LbuAgreementSelectByAssetStoredProcedure;
import com.wam.stored.procedure.LbuSourcePackageSelectStoredProcedure;
import com.wam.test.db.BaseTest;

@Service("LbuSourcePackagePackage")
public class LbuSourcePackagePackage extends BaseTest{

	@Autowired
    DataSource dataSource;
	
	public LbuSourcePackagePackage()
	{
		
	}
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public String lbuSourcePackageSelectAssemblyId(String setId, String sourcePackageId) throws Exception
	{
		LbuSourcePackageSelectStoredProcedure sp = new LbuSourcePackageSelectStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						setId,
						sourcePackageId
					 );
		
		LbuSourcePackage lbuSourcePackage = sp.transformSourcePackageCursorToObject(outMap);

		if(lbuSourcePackage == null)
		{
			return null;
		}
		
		return lbuSourcePackage.getAssemblyId();
	}
	
}
