package com.wam.stored.packages;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.helper.LogMessages;
import com.wam.stored.procedure.LogMessageSearch2StoredProcedure;
import com.wam.stored.procedure.LogMessageSearchStoredProcedure;
import com.wam.stored.procedure.LogMessageUpdateStatusStoredProcedure;
import com.wam.stored.procedure.LogMessageUpdateStatusStoredProcedure2;
import com.wam.stored.procedure.LogMessageCreateStoredProcedure;

import static java.lang.System.out;

@Service("LogMessagePackage")
public class LogMessagePackage {
	
	@Autowired
	@Qualifier("dataSource")
    public DataSource dataSource;
	
	
	public LogMessagePackage()
	{	/*
		if(dataSource == null) {
			dataSource = super.dataSource;
		}
		*/			
	}
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSource()
	{
		return this.dataSource;
	}
	
	/**
	 * 
	 * @param entityId
	 * @param processType
	 * @param processSubType  ==> CREATE OR UPDATE
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public LogMessages LogMessageSearch(String entityId, String processType, String processSubType, String actionId, String status, Integer batchFrom, Integer batchMax) throws Exception 
	{
		LogMessageSearchStoredProcedure sp = new LogMessageSearchStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						 entityId,  processType, processSubType, actionId, status, batchFrom, batchMax
					 );
		
		return sp.transformLogMessageCursorToObject(outMap);
	}
	
	/**
	 * 
	 * @param entityId
	 * @param processType
	 * @param transId
	 * @param messageId
	 * @param fromNumber
	 * @param numberPerPage
	 * @return
	 * @throws Exception
	 */
	public AdminDataTables LogMessageSearch2(String messageId, String processType, 
										 String message, String transId,
										 String actionId, String status,
			Integer fromNumber,Integer numberPerPage) throws Exception 
	{
		LogMessageSearch2StoredProcedure sp = new LogMessageSearch2StoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						messageId,  processType, message, transId, actionId, status, fromNumber, numberPerPage
					 );
		
		return sp.transformLogMessageCursorToObject(outMap);
	}
	
	/**
	 * 
	 * @param messageId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public Boolean LogMessageUpdate(Integer messageId, String status, Integer actionId) throws Exception 
	{
		LogMessageUpdateStatusStoredProcedure sp = new LogMessageUpdateStatusStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						messageId, status, actionId
					 );
		
		if(outMap.containsKey("ERROR_CODE"))
		{
			if(Integer.parseInt(outMap.get("ERROR_CODE").toString()) != 0) {
				throw new Exception(outMap.get("ERROR_DESC").toString());
			}
		}
		
		return true;
		
	}
	
	/**
	 * 
	 * @param messageId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public Boolean LogMessageUpdate(Integer messageId, String status) throws Exception 
	{
		LogMessageUpdateStatusStoredProcedure2 sp = new LogMessageUpdateStatusStoredProcedure2(dataSource);
		
		Map outMap = sp.execute(
						messageId, status
					 );
		
		if(outMap.containsKey("ERROR_CODE"))
		{
			if(Integer.parseInt(outMap.get("ERROR_CODE").toString()) != 0) {
				throw new Exception(outMap.get("ERROR_DESC").toString());
			}
		}
		
		return true;
	}
	
	public Boolean LogMessageCreateStoredProcedure(
			Integer messageId, 
			String processType,
			String processSubType,
			Integer actionId,
			String status,
			String transId,
			String groupId,
			String message,
			String application,
			String adminUser
			) throws Exception 
	{
		LogMessageCreateStoredProcedure sp = new LogMessageCreateStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				 messageId, 
				 processType,
				 processSubType,
				 actionId,
				 status,
				 transId,
				 groupId,
				 message,
				 application,
				 adminUser
			 );
		
		if(outMap.containsKey("ERROR_CODE"))
		{
			if(Integer.parseInt(outMap.get("ERROR_CODE").toString()) != 0) {
				throw new Exception(outMap.get("ERROR_DESC").toString());
			}
		}
		
		return true;
	}
}
