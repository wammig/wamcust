package com.wam.stored.packages;

import java.util.Arrays;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wam.stored.procedure.WindowsAccountRoleSearchStoredProcedure;
import com.wam.utility.definition.Definition;

@Service("WindowsPackage")
public class WindowsPackage {

	@Autowired
	@Qualifier("dataSource")
    public DataSource dataSource;
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSource()
	{
		return this.dataSource;
	}
	
	public String WindowsAccountRoleSearchStoredProcedure(String windowsId) throws Exception 
	{
		try {
			WindowsAccountRoleSearchStoredProcedure sp = new WindowsAccountRoleSearchStoredProcedure(dataSource);
			Map<String, Object> map = sp.execute(windowsId);
			
			if(Arrays.asList(Definition.ROLE_ALL, Definition.ROLE_READ_ONLY, Definition.ROLE_SEND).contains((String) map.get("O_WINDOWS_ROLE")))
			{
				return (String) map.get("O_WINDOWS_ROLE");
			}
			
			return Definition.ROLE_READ_ONLY;
		} catch(Exception e) {
			return Definition.ROLE_READ_ONLY;
		}
	}
	
}
