package com.wam.stored.packages;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wam.aspect.HistoryLoggingInsertLbuAgreementAspect;
import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.model.db.gbsd.helper.LbuFeatures;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.service.BaseService;
import com.wam.service.WamService;
import com.wam.stored.procedure.LbuAgreementCreateStoredProcedure;
import com.wam.stored.procedure.LbuAgreementIDSelectAllStoredProcedure;
import com.wam.stored.procedure.LbuAgreementPopulateStoredProcedure;
import com.wam.stored.procedure.LbuAgreementSearchStoredProcedure;
import com.wam.stored.procedure.LbuAgreementSelectByAssetStoredProcedure;
import com.wam.stored.procedure.LbuAgreementSelectByCustomerStoredProcedure;
import com.wam.stored.procedure.LbuAgreementSelectByIdStoredProcedure;
import com.wam.stored.procedure.LbuAgreementUpdateCustomerPGUIDStoredProcedure;
import com.wam.stored.procedure.LbuAgreementUpdateStoredProcedure;
import com.wam.stored.procedure.LbuFeatureSelectAllStoredProcedure;
import com.wam.utility.definition.Definition;

import static java.lang.System.out;

import java.math.BigDecimal;

@Service("LbuAgreementPackage")
public class LbuAgreementPackage {

	@Autowired
    DataSource dataSource;
	
	
	public LbuAgreementPackage()
	{
		
	}
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	/**
	 * 
	 * @param assetAgreementId
	 * @return
	 * @throws Exception
	 */
	public LbuAgreement LbuAgreementSelectByAsset(String assetAgreementId) throws Exception
	{
		LbuAgreementSelectByAssetStoredProcedure sp = new LbuAgreementSelectByAssetStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						assetAgreementId
					 );

		
		LbuAgreement lbuAgreement = (LbuAgreement)sp.transformAgreementCursorToObject(outMap);
		
		return lbuAgreement;
	}
	
	public AdminDataTables LbuAgreementSearch(
			String agreementId,
			String assetAgreementId,
			String productId,
			String customerPguid,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception
	{
		LbuAgreementSearchStoredProcedure sp = new LbuAgreementSearchStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						 agreementId,
						 assetAgreementId,
						 productId,
						 customerPguid,
						 fromNumber,
						 numberPerPage
					 );

		
		AdminDataTables adminDataTables = (AdminDataTables)sp.transformAgreementCursorToObject(outMap);
		
		return adminDataTables;
	}
	
	/**
	 * 
	 * @param agreementId
	 * @return
	 * @throws Exception
	 */
	public LbuAgreements LbuAgreementSelectById(String agreementId) throws Exception
	{
		LbuAgreements lbuAgreements;

		LbuAgreementSelectByIdStoredProcedure sp = new LbuAgreementSelectByIdStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						agreementId
					 );
		
		lbuAgreements = (LbuAgreements)sp.transformAgreementCursorToObject(outMap);
		
		return lbuAgreements;
	}
	
	/**
	 * 
	 * @param customerPguid
	 * @return
	 * @throws Exception
	 */
	public LbuAgreements LbuAgreementSelectByCustomer(String customerPguid) throws Exception
	{
		LbuAgreementSelectByCustomerStoredProcedure sp = new LbuAgreementSelectByCustomerStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						customerPguid
					 );

		LbuAgreements lbuAgreements = (LbuAgreements)sp.transformAgreementCursorToObject(outMap);
		
		return lbuAgreements;
	}
	
	public LbuAgreements LbuAgreementIdSelectAll() throws Exception
	{
		LbuAgreementIDSelectAllStoredProcedure sp = new LbuAgreementIDSelectAllStoredProcedure(dataSource);
		
		Map outMap = sp.execute();

		LbuAgreements lbuAgreements = (LbuAgreements)sp.transformAgreementCursorToObject(outMap);
		
		return lbuAgreements;
	}
	
	
	/**
	 * 
	 * @param lbuAgreement
	 * @param transId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> LbuAgreementCreate(LbuAgreement lbuAgreement, String transId) throws Exception
	{
		LbuAgreementCreateStoredProcedure sp = new LbuAgreementCreateStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				lbuAgreement.getAgreementId(),
				lbuAgreement.getAssetAgreementId(),
				lbuAgreement.getProductId(),
				lbuAgreement.getBeginDate(),
				lbuAgreement.getEndDate(),
				lbuAgreement.getLicenseCount(),
				lbuAgreement.getCustomerPguid(),
				transId
			 );

		
		return outMap;
	}
	
	/**
	 * 
	 * @param lbuAgreement
	 * @return
	 * @throws Exception
	 */
	public LbuAgreement LbuAgreementUpdate(LbuAgreement lbuAgreement) throws Exception
	{
		LbuAgreementUpdateStoredProcedure sp = new LbuAgreementUpdateStoredProcedure(dataSource);
		Map outMap = sp.execute(
				lbuAgreement.getAgreementId(),
				lbuAgreement.getAssetAgreementId(),
				lbuAgreement.getProductId(),
				lbuAgreement.getBeginDate(),
				lbuAgreement.getEndDate(),
				lbuAgreement.getLicenseCount(),
				lbuAgreement.getCustomerPguid()
			 );
		
		if(Integer.parseInt(outMap.get("HAS_UPDATED").toString()) > 0)
		{
			lbuAgreement.setIsChanged(true);
		} else {
			lbuAgreement.setIsChanged(false);
		}
		
		return lbuAgreement;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public LbuFeatures LbuFeatureSelectAll() throws Exception
	{
		LbuFeatureSelectAllStoredProcedure sp = new LbuFeatureSelectAllStoredProcedure(dataSource);
		Map outMap = sp.execute();
		LbuFeatures lbuFeatures = (LbuFeatures)sp.transformFeatureCursorToObject(outMap);
		
		return lbuFeatures;
	}
	
	/**
	 * 
	 * @param assetAgreementId
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> LbuAgreementPopulate(LbuContentSub lbuContentSub, String transIdSysHistory, String assemblyId) throws Exception
	{
		LbuAgreementPopulateStoredProcedure sp = new LbuAgreementPopulateStoredProcedure(dataSource);
		
		/**
		 * lbuContentSub.getMessageId() is just for batch
		 * not for url
		 */
		if(lbuContentSub.getMessageId() == null)
		{
			lbuContentSub.setMessageId(111);
		}

		Map outMap = sp.execute(lbuContentSub.getAssetAgreementId(), lbuContentSub.getLbuAccountId(), transIdSysHistory, lbuContentSub.getMessageId());
		
		/**
		 * if OUT_AGREEMENT_ID is null
		 * it means agreement not found by AssetAgreementId combination
		 * we need to search for agreement with origin AssetAgreementId from db
		 */
		if(outMap.get("OUT_AGREEMENT_ID") == null)
		{
			outMap = sp.execute(lbuContentSub.getAssetAgreementIdFromDb(), lbuContentSub.getLbuAccountId(), transIdSysHistory, lbuContentSub.getMessageId());
		}
		
		if(!outMap.containsKey("AGREEMENT_IS_CREATED") || !outMap.containsKey("AGREEMENT_HAS_UPDATED"))
		{
			throw new Exception("no AGREEMENT_IS_CREATED or AGREEMENT_HAS_UPDATED in LbuAgreementPopulate");
		}
		
		if(outMap.containsKey("AGREEMENT_IS_CREATED"))
		{
			if( ((BigDecimal)outMap.get("AGREEMENT_IS_CREATED")).intValue() > 0)
			{
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The lbuAgreement " + lbuContentSub.getAssetAgreementId()+"P"+assemblyId + " is created. ", ResponseMessage.SUCCESS));
				
				/**
				 * if AGREEMENT_IS_CREATED == 1
				 * return directly
				 */
				return outMap;
			}
		}
		
		if(outMap.containsKey("AGREEMENT_HAS_UPDATED"))
		{
			if( ((BigDecimal)outMap.get("AGREEMENT_HAS_UPDATED")).intValue() > 0)
			{
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The lbuAgreement " + lbuContentSub.getAssetAgreementId()+"P"+assemblyId + " modification is saved. ", ResponseMessage.SUCCESS));
			} else {
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The lbuAgreement " + lbuContentSub.getAssetAgreementId()+"P"+assemblyId + " is already saved. Nothing changed. ", ResponseMessage.SUCCESS));
			}
		}
		
		return outMap;
	}
	
	/**
	 * 
	 * @param assetAgreementId
	 * @return
	 * @throws Exception
	 */
	public String LbuAgreementUpdateCustomerPGUID(String assetAgreementId) throws Exception
	{
		LbuAgreementUpdateCustomerPGUIDStoredProcedure sp =new LbuAgreementUpdateCustomerPGUIDStoredProcedure(dataSource);
		Map outMap = sp.execute(assetAgreementId);
		
		if(!outMap.containsKey("P_CUSTOMERPGUID"))
		{
			throw new Exception("no P_CUSTOMERPGUID in LbuAgreementUpdateCustomerPGUID");
		}
		
		return outMap.get("P_CUSTOMERPGUID").toString();
	}
}
