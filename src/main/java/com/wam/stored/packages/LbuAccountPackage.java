package com.wam.stored.packages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.wam.aspect.HistoryLoggingInsertLbuAccountAspect;
import com.wam.aspect.HistoryLoggingUpdateLbuAccountAspect;
import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.service.AccountWamService;
import com.wam.service.BaseService;
import com.wam.service.WamService;
import com.wam.stored.procedure.AdminDBStaticStoredProcedure;
import com.wam.stored.procedure.LbuAccountCreateStoredProcedure;
import com.wam.stored.procedure.LbuAccountSearch2StoredProcedure;
import com.wam.stored.procedure.LbuAccountSearchStoredProcedure;
import com.wam.stored.procedure.LbuAccountSelectByAssetStoredProcedure;
import com.wam.stored.procedure.LbuAccountSelectByCustomerPGUIDStoredProcedure;
import com.wam.stored.procedure.LbuAccountSelectByIdStoredProcedure;
import com.wam.stored.procedure.LbuAccountUpdateAllStoredProcedure;
import com.wam.stored.procedure.LbuAccountUpdateEcmStoredProcedure;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateStoredProcedure;
import com.wam.test.db.BaseTest;
import com.wam.utility.definition.Definition;

import static java.lang.System.out;

import java.math.BigDecimal;

/**
 * This is a class which contains all the methods to be called in Account Package in oracle
 * extends BaseTest is for the main function to connect to get the application context
 * 
 * the dataSource in BaseTest will be surcharged by the autowaired dataSource 
 * in web application
 * 
 * But the main function will use the dataSource defined in BaseTest to do all the test cases
 * 
 * @author QINC1
 *
 */

@Service("LbuAccountPackage")
public class LbuAccountPackage extends BaseTest{
	
	@Autowired
    DataSource dataSource;

	@Autowired
	HistoryLoggingInsertLbuAccountAspect historyLoggingInsertLbuAccountAspect;
	
	@Autowired
	HistoryLoggingUpdateLbuAccountAspect historyLoggingUpdateLbuAccountAspect;
	/**
	 * test all the functions in the account package
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		LbuAccountPackage lbuAccountPackage = new LbuAccountPackage();
		
		Map<String, String> mapTestCaseAccount = lbuAccountPackage.getTestCaseAccount();
		
		if(mapTestCaseAccount.containsKey("LBU_ACCOUNT_ID"))
		{
			LbuAccount lbuAccount = (LbuAccount)lbuAccountPackage.selectLbuAccountById(mapTestCaseAccount.get("LBU_ACCOUNT_ID").toString(), mapTestCaseAccount.get("LBU_SET_ID").toString());
			out.println("---------------Begin selectLbuAccountById-----------------");
			out.println(lbuAccount.toString());
			out.println("---------------End selectLbuAccountById-----------------");
		} else {
			out.println("no getTestCaseAccount");
		}
		
		
		LbuAccounts lbuAccounts =(LbuAccounts)lbuAccountPackage.searchLbuAccount(
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null);
		out.println("---------------Begin searchLbuAccount-----------------");
		out.println(lbuAccounts.toString());
		out.println("---------------End searchLbuAccount-----------------");
	}
	
	public LbuAccountPackage()
	{
		/**
		 * if the autowired dataSource is null.
		 * So it means it's executed by the main function.
		 * We use the dataSource defined in BaseTest
		 */
		if(dataSource == null) {
			dataSource = super.dataSource;
		}
	}
	/**
	 * fetch a LbuAccount object by id
	 * @param lbuAccountId
	 * @param lbuSetId
	 * @return
	 * @throws Exception
	 */
	public LbuAccount selectLbuAccountById(String lbuAccountId,String lbuSetId) throws Exception
	{
		LbuAccountSelectByIdStoredProcedure lbuAccountSelectByIdStoredProcedure = new LbuAccountSelectByIdStoredProcedure(dataSource);

		Map outMap = lbuAccountSelectByIdStoredProcedure.execute(
				lbuSetId,
				lbuAccountId
				);
		
		LbuAccount lbuAccount = (LbuAccount)LbuAccountSelectByIdStoredProcedure.transformAccountCursorToObject(outMap);
		
		return lbuAccount;
	}
	
	/**
	 * 
	 * @param customerPGUID
	 * @return
	 * @throws Exception
	 */
	public LbuAccount selectLbuAccountByCustomerPGUID(String customerPGUID) throws Exception
	{
		LbuAccountSelectByCustomerPGUIDStoredProcedure lbuAccountSelectByCustomerPGUIDStoredProcedure = new LbuAccountSelectByCustomerPGUIDStoredProcedure(dataSource);

		Map outMap = lbuAccountSelectByCustomerPGUIDStoredProcedure.execute(
						customerPGUID
					 );
		
		LbuAccount lbuAccount = (LbuAccount)LbuAccountSelectByIdStoredProcedure.transformAccountCursorToObject(outMap);
		
		return lbuAccount;
	}
	
	/**
	 * fetch a list of LbuAccount by criteras
	 * @param lbuSetId
	 * @param lbuAccountId
	 * @param gbsAccountId
	 * @param glpAccountId
	 * @param accountName1
	 * @param accountName2
	 * @param salesRepId
	 * @param accountType
	 * @param zipCode
	 * @param countryCode
	 * @param billableStatus
	 * @param addressLine1
	 * @return
	 * @throws Exception
	 */
	public LbuAccounts searchLbuAccount(
			String lbuSetId, 
			String lbuAccountId, 
			String gbsAccountId,
			String glpAccountId, 
			String accountName1,
			String accountName2,
			String salesRepId,
			String accountType,
			String zipCode,
			String countryCode,
			String billableStatus,
			String addressLine1
			) throws Exception
	{
		LbuAccountSearchStoredProcedure sp = new LbuAccountSearchStoredProcedure(dataSource);
		Map outMap = sp.execute(
				 lbuSetId, 
				 lbuAccountId, 
				 gbsAccountId,
				 glpAccountId, 
				 accountName1,
				 accountName2,
				 salesRepId,
				 accountType,
				 zipCode,
				 countryCode,
				 billableStatus,
				 addressLine1
				);
		
		LbuAccounts lbuAccounts = (LbuAccounts)sp.transformAccountCursorToObject(outMap);
		
		return lbuAccounts;
	}
	
	
	public AdminDataTables searchLbuAccount2(
			String lbuSetId, 
			String lbuAccountId, 
			String gbsAccountId,
			String accountName1,
			String accountName2,
			String city,
			String zipCode,
			String countryCode,
			String billableStatus,
			String addressLine1,
			String customerPguid,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception
	{
		LbuAccountSearch2StoredProcedure sp = new LbuAccountSearch2StoredProcedure(dataSource);
		Map outMap = sp.execute(
				 lbuSetId, 
				 lbuAccountId, 
				 gbsAccountId,
				 accountName1,
				 accountName2,
				 city,
				 zipCode,
				 countryCode,
				 billableStatus,
				 addressLine1,
				 customerPguid,
				 fromNumber,
			     numberPerPage
				);
		
		AdminDataTables adminLbuAccounts = (AdminDataTables)sp.transformAccountCursorToObject(outMap);
		
		return adminLbuAccounts;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> adminDBStatic() throws Exception
	{
		AdminDBStaticStoredProcedure sp = new AdminDBStaticStoredProcedure(dataSource);
		Map outMap = sp.execute();
		
		return outMap;
	}
	
	
	/**
	 * 
	 * @param assetAgreementId
	 * @return
	 * @throws Exception
	 */
	public String selectCustomerPguidByAsset(String assetAgreementId) throws Exception
	{
		LbuAccountSelectByAssetStoredProcedure sp = new LbuAccountSelectByAssetStoredProcedure(this.dataSource);
		Map outMap = sp.execute(assetAgreementId);
		
		if(outMap.get("CUSTOMERPGUID") == null)
		{
			return null;
		}
		
		return outMap.get("CUSTOMERPGUID").toString();
	}
	
	/**
	 * call the stocked procedure defined in oracle to create the account
	 * @param lbuAccount
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> insertLbuAccount(LbuAccount lbuAccount) throws Exception
	{
		LbuAccountCreateStoredProcedure sp = new LbuAccountCreateStoredProcedure(dataSource);
		
		Map<String, Object> outMap = sp.execute(lbuAccount.getLbuAccountId(), 
				lbuAccount.getGbsAccountId(), 
				lbuAccount.getGlpAccountId(), 
				lbuAccount.getAccountName1(), 
				lbuAccount.getAccountName2(), 
				lbuAccount.getAuditUserId(), 
				lbuAccount.getSalesRepId(), 
				lbuAccount.getExtOrderNo(), 
				lbuAccount.getMaxUsers(), 
				lbuAccount.getBillableStatus(), 
				lbuAccount.getAccountType(), 
				lbuAccount.getAddressLine1(), 
				lbuAccount.getAddressLine2(), 
				lbuAccount.getCityName(), 
				lbuAccount.getCountryName(), 
				lbuAccount.getZipCode(), 
				lbuAccount.getStateOrProvinceCode(), 
				lbuAccount.getCountryCode(), 
				lbuAccount.getUpdatedDatetime(), 
				lbuAccount.getCreatedDatetime(), 
				lbuAccount.getDataMigration(), 
				lbuAccount.getLbuSetId());
		
		AccountWamService.logger.info("Insert Object lbuAccount : " + lbuAccount.toString());
		
		/**
		 *  check the returned data from oracle stocked procedure
		 */
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true || outMap.containsKey("OUTPUT_LBU_ACCOUNT_ID") != true)
		{
			AccountWamService.logger.error("returned error from LbuAccountCreateStoredProcedure : " + outMap.toString());
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=LBU_ACCOUNT_ID: 9900012 already exist, OUTPUT_LBU_ACCOUNT_ID=9900012, ERROR_CODE=10014}");
		}
		
		/**
		 *  transform BigDecimal to Int
		 */
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			AccountWamService.logger.error("returned error from LbuAccountCreateStoredProcedure : " + outMap.toString());
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		outMap.put("lbuAccount", lbuAccount);
		
		//historyLoggingInsertLbuAccountAspect.historyLoggingInsertLbuAccountAround(outMap);
		
		return outMap;
	}
	
	/**
	 * call the stocked procedure defined in oracle to update the account
	 * @param lbuAccount
	 * @throws Exception 
	 */
	public Map<String, Object> updateLbuAccount(LbuAccount lbuAccount) throws Exception
	{
		
		LbuAccountUpdateAllStoredProcedure sp = new LbuAccountUpdateAllStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						lbuAccount.getLbuSetId(),
						lbuAccount.getLbuAccountId(), 
						lbuAccount.getAccountName1(), 
						lbuAccount.getAccountName2(), 
						lbuAccount.getAuditUserId(), 
						lbuAccount.getSalesRepId(), 
						lbuAccount.getExtOrderNo(), 
						lbuAccount.getMaxUsers(), 
						lbuAccount.getBillableStatus(), 
						lbuAccount.getAccountType(), 
						lbuAccount.getAddressLine1(), 
						lbuAccount.getAddressLine2(), 
						lbuAccount.getCityName(), 
						lbuAccount.getCountryName(), 
						lbuAccount.getZipCode(), 
						lbuAccount.getStateOrProvinceCode(), 
						lbuAccount.getCountryCode()
					);
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_DESC") != true || outMap.containsKey("O_ROWCOUNT") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_DESC=\"\", ERROR_CODE=0, COUNT_UPDATED=1}");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			if(((BigDecimal)outMap.get("ERROR_CODE")).intValue() == 10110 )
			{
				throw new Exception(lbuAccount.getLbuAccountId() + " Account Id not found in updateLbuAccount");
			}
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_DESC"));
		}
		
		/**
		 * fetch from db for the refreshed CUSTOMERPGUID and POBPGUID
		 * 
		 */
		lbuAccount = (LbuAccount)this.selectLbuAccountById(lbuAccount.getLbuAccountId(), lbuAccount.getLbuSetId());
		
		if(outMap.containsKey("O_ROWCOUNT"))
		{
			if( ((BigDecimal)outMap.get("O_ROWCOUNT")).intValue() > 0)
			{
				lbuAccount.setIsChanged(true);
			} else {
				lbuAccount.setIsChanged(false);
			}
		}
		
		outMap.put("lbuAccount", lbuAccount);
		
		//historyLoggingUpdateLbuAccountAspect.historyLoggingUpdateLbuAccountAround(outMap);
		
		return outMap;
	}
	
	/**
	 * After calling the remote ws orgCustomer (creation customer)
	 * We need to update the account with the returned OrgCustomerPGUID and the returned POBPGUID
	 * 
	 * @param accountId
	 * @param customerPGUID
	 * @param pOBPGUID
	 * @throws Exception
	 */
	public List<ResponseMessage> updateAccountCustomerPGUIDId(String accountId, String customerPGUID, String pOBPGUID, String addressPGUID) throws Exception
	{
		LbuAccountUpdateEcmStoredProcedure sp = new LbuAccountUpdateEcmStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						accountId,
						customerPGUID,
						pOBPGUID,
						addressPGUID
					);
			
		/**
		 *  check the returned data from oracle stocked procedure
		 */
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=9900012 LBU_ACCOUNT_ID not found, ERROR_CODE=10210}");
		}
		
		/**
		 *  transform BigDecimal to Int
		 */
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		/*
		if(!outMap.containsKey("O_ROWCOUNT"))
		{
			outMap.put("O_ROWCOUNT", 1);
		}
		
		if(!outMap.containsKey("OUTPUT_LBU_ACCOUNT_ID"))
		{
			outMap.put("OUTPUT_LBU_ACCOUNT_ID", accountId);
		}
		*/
		
		String message = " Updated account " + accountId + " with customerPGUID: " + customerPGUID + " and pOBPGUID: " + pOBPGUID + " successfully.";
		ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(message, ResponseMessage.SUCCESS));
		
		//historyLoggingUpdateLbuAccountAspect.historyLoggingUpdateLbuAccountAround(outMap);
		
		return ResponseHelper.getMessageList();
	}
	
}
