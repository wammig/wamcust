package com.wam.stored.packages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.stored.procedure.LbuAccountSysSelectStoredProcedure;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.stored.procedure.SysHistorySearch2StoredProcedure;
import com.wam.stored.procedure.SysHistorySearchLastStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateMessageIdStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateMessageStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateSendStatusStoredProcedure;
import com.wam.utility.definition.Definition;

@Service("HistoryPackage")
public class HistoryPackage {

	@Autowired
    DataSource dataSource;
	
	public void setDataSrouce(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSrouce()
	{
		return this.dataSource;
	}
	
	
	/**
	 * 
	 * @param historyId
	 * @param processType
	 * @param processSubType
	 * @param entityId
	 * @param sendStatus
	 * @param replyStatus
	 * @param errorCode
	 * @param errorMessage
	 * @param transId
	 * @param messageId
	 * @param message
	 * @param adminUser
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> sysHistoryCreate(Integer historyId, String processType, String processSubType, String entityId, String sendStatus,
			String replyStatus, String errorCode, String errorMessage, String transId, Integer messageId, String message, 
			String adminUser) throws Exception
	{
		SysHistoryCreateStoredProcedure sp = new SysHistoryCreateStoredProcedure(dataSource);
		Map outMap = sp.execute(historyId, processType, processSubType, entityId, sendStatus,
				 replyStatus, errorCode, errorMessage, transId, messageId, message, adminUser);
		
		return outMap;
	}
	
	/**
	 * 
	 * @param transId
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public Map<?, ?> sysHistoryUpdateMessage(String transId, String params) throws Exception
	{
		SysHistoryUpdateMessageStoredProcedure sp = new SysHistoryUpdateMessageStoredProcedure(dataSource);
		return sp.execute(transId, params);
	}
	
	/**
	 * 
	 * @param transId
	 * @param sendStatus
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> sysHistoryUpdateSendStatus(String transId, String sendStatus) throws Exception
	{
		SysHistoryUpdateSendStatusStoredProcedure sp = new SysHistoryUpdateSendStatusStoredProcedure(dataSource);
		
		return sp.execute(transId, sendStatus);
	}
	
	/**
	 * 
	 * @param transId
	 * @param messageId
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> sysHistoryUpdateMessageid(String transId, String messageId) throws Exception
	{
		SysHistoryUpdateMessageIdStoredProcedure sp = new SysHistoryUpdateMessageIdStoredProcedure(dataSource);
		
		return sp.execute(transId, messageId);
	}
	
	/**
	 * 
	 * @param historyId
	 * @param processType
	 * @param processSubType
	 * @param entityId
	 * @param entitySeq0
	 * @param entitySeq1
	 * @param entitySeq2
	 * @param entitySeq3
	 * @param entitySeq4
	 * @param entitySeq5
	 * @param transId
	 * @param messageId
	 * @param sendStatus
	 * @param replyStatus
	 * @return
	 * @throws Exception
	 */
	public SysHistory findSysHistorySearchLast(
			 Integer historyId, String processType, String processSubType, String entityId, 
			 String entitySeq0, String entitySeq1, String entitySeq2, String entitySeq3, String entitySeq4, String entitySeq5,
			 String transId, Integer messageId, String sendStatus, String replyStatus
			) throws Exception
	{
		SysHistorySearchLastStoredProcedure sp = new SysHistorySearchLastStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				  historyId,  processType,  processSubType,  entityId, 
				  entitySeq0,  entitySeq1,  entitySeq2,  entitySeq3,  entitySeq4,  entitySeq5,
				  transId,  messageId,  sendStatus,  replyStatus
				);
		
		
		return sp.transformHistoryCursorToObject(outMap, Definition.DATA_SOURCE);
	}
	
	public AdminDataTables findSysHistorySearch2(
			String historyId,  String entityId, String transId, String messageId,
			 Integer fromNumber,Integer numberPerPage
			) throws Exception
	{
		SysHistorySearch2StoredProcedure sp = new SysHistorySearch2StoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				  historyId,  entityId, 
				  transId,  messageId,  fromNumber, numberPerPage
				);
		
		
		return sp.transformHistoryCursorToObject(outMap);
	}
	
	
	public List<?> findAccountSys(String fromDate, String toDate, String cohorte) throws Exception
	{
		LbuAccountSysSelectStoredProcedure sp = new LbuAccountSysSelectStoredProcedure(this.dataSource);
		Map<?, ?> outMap = sp.execute(fromDate, toDate, cohorte);

		return (ArrayList)outMap.get("P_ACCOUNT_SYS_CURSOR");
	}
	
	/**
	 * 
	 * @param transId
	 * @return
	 * @throws Exception
	 */
	public SysHistory findSysHistoryByTransId(String transId) throws Exception
	{
		SysHistorySearchLastStoredProcedure sp = new SysHistorySearchLastStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				  null,  null,  null,  null, 
				  null,  null,  null,  null,  null,  null,
				  transId,  null,  null,  null
				);
		
		return sp.transformHistoryCursorToObject(outMap, Definition.DATA_SOURCE);
	}
	
}
