package com.wam.stored.packages;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wam.aspect.HistoryLoggingInsertLbuLocationAspect;
import com.wam.aspect.HistoryLoggingUpdateLbuLocationAspect;
import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.service.BaseService;
import com.wam.service.LocationWamService;
import com.wam.service.WamService;
import com.wam.stored.procedure.LbuLocationCreateStoredProcedure;
import com.wam.stored.procedure.LbuLocationSearch2StoredProcedure;
import com.wam.stored.procedure.LbuLocationSearchStoredProcedure;
import com.wam.stored.procedure.LbuLocationSelectByIdStoredProcedure;
import com.wam.stored.procedure.LbuLocationUpdateAddressPGUIDStoredProcedure;
import com.wam.stored.procedure.LbuLocationUpdateStoredProcedure;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.stored.procedure.SysHistoryUpdateStoredProcedure;
import com.wam.test.db.BaseTest;
import com.wam.utility.definition.Definition;

import static java.lang.System.out;

import java.math.BigDecimal;

/**
 * This is a class which contains all the methods to be called in Location Package in oracle
 * extends BaseTest is for the main function to connect to get the application context
 * 
 * the dataSource in BaseTest will be surcharged by the autowaired dataSource 
 * in web application
 * 
 * But the main function will use the dataSource defined in BaseTest to do all the test cases
 * 
 * @author QINC1
 *
 */

@Service("LbuLocationPackage")
public class LbuLocationPackage extends BaseTest{
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	HistoryLoggingInsertLbuLocationAspect historyLoggingInsertLbuLocationAspect;
	
	@Autowired
	HistoryLoggingUpdateLbuLocationAspect historyLoggingUpdateLbuLocationAspect;
	
	public static void main(String[] args) throws Exception
	{
		LbuLocationPackage lbuLocationPackage = new LbuLocationPackage();
		//LbuLocations lbuLocations = (LbuLocations)lbuLocationPackage.searchLbuLocation("FR", null, null, null, null, null, null, null, null);
	
		out.println("---------------Begin searchLbuLocation-----------------");
		//out.println(lbuLocations);
		out.println("---------------End searchLbuLocation-----------------");
		
		LbuLocation lbuLocationSelect = lbuLocationPackage.selectLbuLocation("FR", "608984111797810", "608984111797810S01");
		
		out.println("---------------Begin selectLbuLocation-----------------");
		out.println(lbuLocationSelect);
		out.println("---------------End selectLbuLocation-----------------");
		
		lbuLocationSelect.setAddressLine2("address 2");
		LbuLocation lbuLocationSelectAfterUpdate = lbuLocationPackage.updateLbuLocation(lbuLocationSelect);
		
		out.println("---------------Begin updateLbuLocation-----------------");
		out.println(lbuLocationSelectAfterUpdate);
		out.println("---------------End updateLbuLocation-----------------");
	}
	
	public LbuLocationPackage()
	{
		/**
		 * if the autowired dataSource is null.
		 * So it means it's executed by the main function.
		 * We use the dataSource defined in BaseTest
		 */
		if(dataSource == null) {
			dataSource = super.dataSource;
		}
	}

	/**
	 * 
	 * @param lbuSetId
	 * @param lbuAccountId
	 * @param lbuLocationId
	 * @param gbsLocationId
	 * @param glpLocationId
	 * @param zipCode
	 * @param countryCode
	 * @param addressLine1
	 * @param addressLine2
	 * @return
	 * @throws Exception
	 */
	public LbuLocations searchLbuLocation(
			String lbuSetId, 
			String lbuAccountId,
			String lbuLocationId,
			String gbsLocationId,
			String glpLocationId,
			String zipCode,
			String countryCode,
			String addressLine1,
			String addressLine2
			) throws Exception
	{
		LbuLocationSearchStoredProcedure sp = new LbuLocationSearchStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				 lbuSetId, 
				 lbuAccountId,
				 lbuLocationId,
				 gbsLocationId,
				 glpLocationId,
				 zipCode,
				 countryCode,
				 addressLine1,
				 addressLine2
				);
		
		LbuLocations lbuLocations = (LbuLocations)sp.transformLocationCursorToObject(outMap);
		
		return lbuLocations;
	}
	
	/**
	 * 
	 * @param lbuAccountId
	 * @param lbuLocationId
	 * @param zipCode
	 * @param countryCode
	 * @param addressLine1
	 * @param addressLine2
	 * @return
	 * @throws Exception
	 */
	public AdminDataTables searchLbuLocation2(
			String lbuAccountId,
			String lbuLocationId,
			String accountName1,
			String cityName,
			String zipCode,
			String countryCode,
			String addressLine1,
			String addressLine2,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception
	{
		LbuLocationSearch2StoredProcedure sp = new LbuLocationSearch2StoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				 lbuAccountId,
				 lbuLocationId,
				 accountName1,
			     cityName,
				 zipCode,
				 countryCode,
				 addressLine1,
				 addressLine2,
				 fromNumber,
			     numberPerPage
				);
		
		AdminDataTables lbuLocations = (AdminDataTables)sp.transformLocationCursorToObject(outMap);
		
		return lbuLocations;
	}
	
	/**
	 * 
	 * @param lbuSetId
	 * @param lbuAccountId
	 * @param lbuLocationId
	 * @return
	 * @throws Exception
	 */
	public LbuLocation selectLbuLocation(
			String lbuSetId, 
			String lbuAccountId,
			String lbuLocationId
			) throws Exception
	{
		LbuLocationSelectByIdStoredProcedure sp = new LbuLocationSelectByIdStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
						 lbuSetId, 
						 lbuAccountId,
						 lbuLocationId
					  );
		
		LbuLocation lbuLocation = (LbuLocation)sp.transformLocationCursorToObject(outMap);

		return lbuLocation;
		
	}
	
	/**
	 * call the stocked procedure defined in oracle
	 * @param lbuLocation
	 * @throws Exception 
	 */
	public LbuLocation insertLbuLocation(LbuLocation lbuLocation) throws Exception
	{
		LbuLocationCreateStoredProcedure sp = new LbuLocationCreateStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				lbuLocation.getLbuAccountId(),
				lbuLocation.getLbuLocationId(),
				lbuLocation.getGbsLocationId(),
				lbuLocation.getGlpLocationId(),
				lbuLocation.getLbuAddressId(),
				lbuLocation.getGbsAddressId(),
				lbuLocation.getAddressSeq(),
				lbuLocation.getAddressLine1(),
				lbuLocation.getAddressLine2(),
				lbuLocation.getCityName(),
				lbuLocation.getAuditUserId(),
				lbuLocation.getSalesRepId(),
				lbuLocation.getExtOrderNo(),
				lbuLocation.getCountryName(),
				lbuLocation.getZipCode(),
				lbuLocation.getStateOrProvinceCode(),
				lbuLocation.getCountryCode(),
				lbuLocation.getUpdatedDatetime(),
				lbuLocation.getCreatedDatetime(),
				lbuLocation.getDataMigration(),
				lbuLocation.getLbuSetId(),
				lbuLocation.getAddressPGUID(),
				lbuLocation.getPobPGUID()
				);
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_DESC") != true || outMap.containsKey("P_LOCATION_ID") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_MESSAGE=\"\", OUTPUT_LBU_LOCATION_ID=9900012, ERROR_CODE=0}");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_DESC"));
		}
		
		// set the new locationId generated from db
		lbuLocation.setLbuLocationId(outMap.get("P_LOCATION_ID").toString());  
		
		//outMap.put("lbuLocation", lbuLocation);
		
		//historyLoggingInsertLbuLocationAspect.historyLoggingInsertLbuLocationAround(outMap);
		
		return lbuLocation;
	}
	
	/**
	 * 
	 * @param lbuLocation
	 * @return
	 * @throws Exception
	 */
	public LbuLocation updateLbuLocation(LbuLocation lbuLocation) throws Exception
	{
		LbuLocationUpdateStoredProcedure sp = new LbuLocationUpdateStoredProcedure(dataSource);

		Map outMap = sp.execute(
						lbuLocation.getLbuSetId(),
						lbuLocation.getLbuAccountId(),
						lbuLocation.getLbuAddressId(),
						lbuLocation.getAddressSeq(),
						lbuLocation.getSalesRepId(),
						lbuLocation.getExtOrderNo(),
						lbuLocation.getAddressLine1(),
						lbuLocation.getAddressLine2(),
						lbuLocation.getCityName(),
						lbuLocation.getCountryName(),
						lbuLocation.getZipCode(),
						lbuLocation.getStateOrProvinceCode(),
						lbuLocation.getCountryCode(),
						lbuLocation.getAuditUserId(),
						BaseService.generateTransIdSysHistory()
					);
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true || outMap.containsKey("O_ROWCOUNT") != true)
		{
			throw new Exception("Oracle stocked procedure error.");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		if(outMap.containsKey("O_ROWCOUNT"))
		{
			if( ((BigDecimal)outMap.get("O_ROWCOUNT")).intValue() > 0)
			{
				lbuLocation.setIsChanged(true);
			} else {
				lbuLocation.setIsChanged(false);
			}
		}
		
		return lbuLocation;	
	}
	
	
	/**
	 * update addressPguid for location after calling ecm ws
	 * @param accountId
	 * @param addressPguid
	 * @return
	 * @throws Exception
	 */
	public List<ResponseMessage> updateLocationAddressPGUID(String accountId, String locationId, String addressPguid, String pobPGUID) throws Exception
	{
		LbuLocationUpdateAddressPGUIDStoredProcedure sp = new LbuLocationUpdateAddressPGUIDStoredProcedure(dataSource);

		Map outMap = sp.execute(
						accountId,
						locationId,
						addressPguid,
						pobPGUID
					);
		
		/**
		 *  check the returned data from oracle stocked procedure
		 */
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_MESSAGE") != true)
		{
			throw new Exception("Oracle stocked procedure error. return example {ERROR_DESC=ADDRESSPGUID not found, ERROR_CODE=10211}");
		}
		
		/**
		 *  transform BigDecimal to Int
		 */
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_MESSAGE"));
		}
		
		String message = " Updated location with ADDRESSPGUID: " + addressPguid + " successfully.";

		ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(message, ResponseMessage.SUCCESS));
		
		return ResponseHelper.getMessageList();
	}
	
}
