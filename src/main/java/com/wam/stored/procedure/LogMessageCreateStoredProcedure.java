package com.wam.stored.procedure;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import static java.lang.System.out;

public class LogMessageCreateStoredProcedure extends StoredProcedure{

	private static final String P_MESSAGE_CREATE = "PKG_LOG_MESSAGE.P_MESSAGE_CREATE";
	
	public LogMessageCreateStoredProcedure(DataSource ds){
        super(ds,P_MESSAGE_CREATE);
        
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_PROCESS_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PROCESS_SUBTYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACTIONID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_GROUP_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_APPLICATION",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADMIN_USER",Types.VARCHAR));
        declareParameter(new SqlOutParameter("O_MESSAGE_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(
				Integer messageId, 
				String processType,
				String processSubType,
				Integer actionId,
				String status,
				String transId,
				String groupId,
				String message,
				String application,
				String adminUser
			) throws Exception{

			Map in = new HashMap();
		
			in.put("P_MESSAGE_ID", messageId);
			in.put("P_PROCESS_TYPE", processType);
			in.put("P_PROCESS_SUBTYPE", processSubType);
			in.put("P_ACTIONID", actionId);
			in.put("P_STATUS", status);
			in.put("P_TRANS_ID", transId);
			in.put("P_GROUP_ID", groupId);
			in.put("P_MESSAGE", message);
			in.put("P_APPLICATION", application);
			in.put("P_ADMIN_USER", adminUser);
			
		 	try {
	        	Map outMap = execute(in);
	        	
	        	return outMap;
	        } catch(Exception ex)
	        {
	        	System.out.println(ex.toString());
	        	
	        	return new HashMap();
	        }
	}
	
}
