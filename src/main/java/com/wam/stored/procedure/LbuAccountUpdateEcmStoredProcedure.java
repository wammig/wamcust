package com.wam.stored.procedure;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class LbuAccountUpdateEcmStoredProcedure extends StoredProcedure{

	private static final String P_ACCOUNT_UPDATE = "PKG_LBU_ACCOUNT.P_ACCOUNT_UPDATE";
	
	public LbuAccountUpdateEcmStoredProcedure(DataSource ds){
        super(ds,P_ACCOUNT_UPDATE);
        
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CUSTOMERPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_POBPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESSPGUID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_MESSAGE",Types.VARCHAR));
        
        compile();
	}
	
	public Map execute(String lbuAccountId, String customerPGUID, String pOBPGUID, String addressPGUID) throws Exception{
		
		
		
		Map in = new HashMap();
       
        in.put("P_ACCOUNT_ID",lbuAccountId);
        in.put("P_CUSTOMERPGUID",customerPGUID);        
        in.put("P_POBPGUID",pOBPGUID);  
        in.put("P_ADDRESSPGUID",addressPGUID);  
        
        try {
        	Map out = execute(in);
        	
        	return out;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
    }
	
}
