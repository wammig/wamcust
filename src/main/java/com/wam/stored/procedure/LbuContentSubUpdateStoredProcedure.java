package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wam.service.BaseService;

public class LbuContentSubUpdateStoredProcedure extends StoredProcedure{
	
	private static final String CONTENTSUB_UPDATE = "PKG_LBU_CONTENTSUB.P_CONTENTSUB_UPDATE";
	
	public LbuContentSubUpdateStoredProcedure(DataSource ds){
        super(ds,CONTENTSUB_UPDATE);
        
        declareParameter(new SqlParameter("P_CONTENT_SUB_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTRACT_NUM",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTRACT_LINE_NUM",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTRACT_LINE_NUM_ORIGIN",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_LOCATION_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTENT_SUB_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_BEGIN_DATE",Types.DATE));
        declareParameter(new SqlParameter("P_END_DATE",Types.DATE));
        declareParameter(new SqlParameter("P_MAX_USERS",Types.NUMERIC));
        declareParameter(new SqlParameter("P_MONTHLY_COMMITMENT",Types.FLOAT));
        declareParameter(new SqlParameter("P_MONTHLY_CAP",Types.FLOAT));
        declareParameter(new SqlParameter("P_SOURCE_PACKAGE_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADMIN_USER",Types.VARCHAR));
        declareParameter(new SqlParameter("P_NO_RENEW",Types.VARCHAR));
        declareParameter(new SqlParameter("P_SESSION_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_SEQ",Types.NUMERIC));
        
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("O_ROWCOUNT",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map execute(
			String contentSubId,
			String setId, 
			String contractNum,
			Integer contractLineNum,
			Integer contractLineNumOrigin,
			String accountId,
			String locationId,
			String contentSubName,
			String beginDate,
			String endDate,
			Integer maxUsers,
			Float monthlyCommitment,
			Float monthlyCap,
			String sourcePackageId,
			String adminUser,
			String noRenew,
			String sessionId,
			Integer addressSeq
			) throws Exception{
		Map in = new HashMap();

		in.put("P_CONTENT_SUB_ID",contentSubId);
        in.put("P_SET_ID",setId);
        in.put("P_CONTRACT_NUM",contractNum);         
        in.put("P_CONTRACT_LINE_NUM",contractLineNum); 
        in.put("P_CONTRACT_LINE_NUM_ORIGIN",contractLineNumOrigin); 
        in.put("P_ACCOUNT_ID",accountId); 
        in.put("P_LOCATION_ID",locationId); 
        in.put("P_CONTENT_SUB_NAME",contentSubName); 
        in.put("P_MAX_USERS",maxUsers); 
        in.put("P_MONTHLY_COMMITMENT",monthlyCommitment); 
        in.put("P_MONTHLY_CAP",monthlyCap); 
        in.put("P_SOURCE_PACKAGE_ID",sourcePackageId); 
        in.put("P_ADMIN_USER",adminUser); 
        in.put("P_NO_RENEW",noRenew);
        in.put("P_SESSION_ID",sessionId); 
        in.put("P_TRANS_ID",BaseService.generateTransIdSysHistory()); 
        in.put("P_ADDRESS_SEQ",addressSeq); 
        
        try {
        	/**
        	 * receive format
        	 */
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			
			/**
			 * target format
			 */
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			
			Date begin = formatter.parse(beginDate);
			in.put("P_BEGIN_DATE", formatter2.format(begin).toString());

			Date end = formatter.parse(endDate);
			in.put("P_END_DATE", formatter2.format(end).toString());
			
		} catch(Exception ex) {
			throw new Exception(" Date Format of P_BEGIN_DATE, P_END_DATE");
		}
        
        try {
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
    }

}
