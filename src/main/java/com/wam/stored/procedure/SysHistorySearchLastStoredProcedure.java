package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.mvc.response.helper.ErrorObject;
import com.wam.utility.definition.Definition;

import oracle.jdbc.OracleTypes;

public class SysHistorySearchLastStoredProcedure extends StoredProcedure{

	private static final String SYS_HISTORY_SEARCH_LAST = "PKG_SYS_HISTORY.P_HISTORY_SEARCH_LAST";
	
	public SysHistorySearchLastStoredProcedure(DataSource ds){
        super(ds,SYS_HISTORY_SEARCH_LAST);
        
        declareParameter(new SqlParameter("P_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_PROCESS_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PROCESS_SUBTYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_SEQ0",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_SEQ1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_SEQ2",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_SEQ3",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_SEQ4",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_SEQ5",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_SEND_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_REPLY_STATUS",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_HISTORY_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(Integer historyId, String processType, String processSubType, String entityId, 
			 String entitySeq0, String entitySeq1, String entitySeq2, String entitySeq3, String entitySeq4, String entitySeq5,
			 String transId, Integer messageId, String sendStatus, String replyStatus) throws Exception{

			Map in = new HashMap();
		
			in.put("P_HISTORY_ID", historyId);
			in.put("P_PROCESS_TYPE", processType);
			in.put("P_PROCESS_SUBTYPE", processSubType);
			in.put("P_ENTITY_ID", entityId);
			in.put("P_ENTITY_SEQ0", entitySeq0);
			in.put("P_ENTITY_SEQ1", entitySeq1);
			in.put("P_ENTITY_SEQ2", entitySeq2);
			in.put("P_ENTITY_SEQ3", entitySeq3);
			in.put("P_ENTITY_SEQ4", entitySeq4);
			in.put("P_ENTITY_SEQ5", entitySeq5);
			in.put("P_SEND_STATUS", sendStatus);
			in.put("P_REPLY_STATUS", replyStatus);
			in.put("P_TRANS_ID", transId);
			in.put("P_MESSAGE_ID", messageId);

			
		 	try {
	        	Map outMap = execute(in);
	        	
	        	return outMap;
	        } 
		 	catch(Exception ex)
	        {
	        	System.out.println(ex.toString());
	        	
	        	return new HashMap();
	        }
	}
	
	
	public SysHistory transformHistoryCursorToObject(Map<String, Object> map, String dataSourceName) throws Exception
	{
		SysHistory sysHistory = new SysHistory();
		
		if(!map.containsKey("P_HISTORY_CURSOR")) 
		{
			return null;
		}
		
		List  historyList = (ArrayList)map.get("P_HISTORY_CURSOR");
		
		if(historyList.size() != 1)
		{
			return null;
		}
		
		Map mapHistory = (LinkedCaseInsensitiveMap)historyList.get(0);
		
		String historyString = mapHistory.toString();
		
		if(mapHistory.containsKey("HISTORYID"))
		{
			if(mapHistory.get("HISTORYID") != null)
			{
				sysHistory.setHistoryId(Integer.parseInt(mapHistory.get("HISTORYID").toString()));
			}
		}
		
		if(mapHistory.containsKey("PROCESS_TYPE"))
		{
			if(mapHistory.get("PROCESS_TYPE") != null)
			{
				sysHistory.setProcessType(mapHistory.get("PROCESS_TYPE").toString());
			}
		}
		
		if(mapHistory.containsKey("PROCESS_SUBTYPE"))
		{
			if(mapHistory.get("PROCESS_SUBTYPE") != null)
			{
				sysHistory.setProcessSubType(mapHistory.get("PROCESS_SUBTYPE").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITYID"))
		{
			if(mapHistory.get("ENTITYID") != null)
			{
				sysHistory.setEntityId(mapHistory.get("ENTITYID").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITY_SEQ0"))
		{
			if(mapHistory.get("ENTITY_SEQ0") != null)
			{
				sysHistory.setEntitySeq0(mapHistory.get("ENTITY_SEQ0").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITY_SEQ1"))
		{
			if(mapHistory.get("ENTITY_SEQ1") != null)
			{
				sysHistory.setEntitySeq1(mapHistory.get("ENTITY_SEQ1").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITY_SEQ2"))
		{
			if(mapHistory.get("ENTITY_SEQ2") != null)
			{
				sysHistory.setEntitySeq2(mapHistory.get("ENTITY_SEQ2").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITY_SEQ3"))
		{
			if(mapHistory.get("ENTITY_SEQ3") != null)
			{
				sysHistory.setEntitySeq3(mapHistory.get("ENTITY_SEQ3").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITY_SEQ4"))
		{
			if(mapHistory.get("ENTITY_SEQ4") != null)
			{
				sysHistory.setEntitySeq4(mapHistory.get("ENTITY_SEQ4").toString());
			}
		}
		
		if(mapHistory.containsKey("ENTITY_SEQ5"))
		{
			if(mapHistory.get("ENTITY_SEQ5") != null)
			{
				sysHistory.setEntitySeq5(mapHistory.get("ENTITY_SEQ5").toString());
			}
		}
		
		if(mapHistory.containsKey("SEND_STATUS"))
		{
			if(mapHistory.get("SEND_STATUS") != null)
			{
				sysHistory.setSendStatus(mapHistory.get("SEND_STATUS").toString());
			}
		}
		
		if(mapHistory.containsKey("REPLY_STATUS"))
		{
			if(mapHistory.get("REPLY_STATUS") != null)
			{
				sysHistory.setReplyStatus(mapHistory.get("REPLY_STATUS").toString());
			}
		}
		
		if(mapHistory.containsKey("ERROR_CODE"))
		{
			if(mapHistory.get("ERROR_CODE") != null)
			{
				sysHistory.setErrorCode(mapHistory.get("ERROR_CODE").toString());
			}
		}
		
		if(mapHistory.containsKey("ERROR_MESSAGE"))
		{
			if(mapHistory.get("ERROR_MESSAGE") != null)
			{
				sysHistory.setErrorMessage(mapHistory.get("ERROR_MESSAGE").toString());
			}
		}
		
		if(mapHistory.containsKey("TRANSID"))
		{
			if(mapHistory.get("TRANSID") != null)
			{
				sysHistory.setTransId(mapHistory.get("TRANSID").toString());
			}
		}
		
		if(mapHistory.containsKey("MESSAGE"))
		{
			if(mapHistory.get("MESSAGE") != null)
			{
				sysHistory.setMessage(mapHistory.get("MESSAGE").toString());
			}
		}
		
		if(mapHistory.containsKey("MESSAGEID"))
		{
			if(mapHistory.get("MESSAGEID") != null)
			{
				sysHistory.setMessageId(Integer.parseInt(mapHistory.get("MESSAGEID").toString()));
			}
		}
		
		if(mapHistory.containsKey("REPLY_MESSAGE"))
		{
			if(mapHistory.get("REPLY_MESSAGE") != null)
			{
				sysHistory.setReplyMessage(mapHistory.get("REPLY_MESSAGE").toString());
			}
		}
		
		if(mapHistory.containsKey("CREATED_BY"))
		{
			if(mapHistory.get("CREATED_BY") != null)
			{
				sysHistory.setCreatedBy(mapHistory.get("CREATED_BY").toString());
			}
		}
		
		if(mapHistory.containsKey("UPDATED_BY"))
		{
			if(mapHistory.get("UPDATED_BY") != null)
			{
				sysHistory.setUpdatedBy(mapHistory.get("UPDATED_BY").toString());
			}
		}
		
		/**
		 * extract the created date
		 */
		Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
		Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(historyString);
		
		if(createdDateTimeMatcher.find())
		{ 
			Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
			Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
			
			if(createdDateTimeTrimMatcher.find()) 
			{
				String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
				sysHistory.setCreatedDateTime(createdDateTime);
			}
		}
		
		/**
		 * extract the updated date
		 */
		Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
		Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(historyString);
		
		if(updatedDateTimeMatcher.find())
		{ 
			Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
			Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
			
			if(updateDateTimeTrimMatcher.find()) 
			{
				String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
				sysHistory.setUpdatedDatetime(updateDateTime);
			}
		}
		
		sysHistory.setDataSourceName(dataSourceName);
		
		return sysHistory;
	}
	
}
