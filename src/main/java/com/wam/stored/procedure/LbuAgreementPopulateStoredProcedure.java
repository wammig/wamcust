package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;

import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;

import oracle.jdbc.internal.OracleTypes;


@Service("LbuAgreementPopulateStoredProcedure")
public class LbuAgreementPopulateStoredProcedure extends StoredProcedure{
	
	private static final String P_AGREEMENT_POPULATE = "PKG_LBU_AGREEMENT.P_AGREEMENT_POPULATE";
	
	public LbuAgreementPopulateStoredProcedure(DataSource ds){
        super(ds,P_AGREEMENT_POPULATE);
        
        declareParameter(new SqlParameter("P_ASSET_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("OUT_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("AGREEMENT_HAS_UPDATED",Types.NUMERIC));
        declareParameter(new SqlOutParameter("AGREEMENT_IS_CREATED",Types.NUMERIC));
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute(
				String assetAgreementId,
				String accountId,
				String transId,
				int messageId
			) throws Exception{
		Map in = new HashMap();
		try {
			in.put("P_ASSET_AGREEMENT_ID", assetAgreementId);
			in.put("P_ACCOUNT_ID", accountId);
			in.put("P_TRANS_ID", transId);
			in.put("P_MESSAGE_ID", messageId);

        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
		
	}
	
}
