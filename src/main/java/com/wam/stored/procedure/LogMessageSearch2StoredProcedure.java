package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LogMessages;

import oracle.jdbc.OracleTypes;

import static java.lang.System.out;

public class LogMessageSearch2StoredProcedure extends StoredProcedure{

	private static final String P_MESSAGE_SEARCH = "PKG_LOG_MESSAGE.P_MESSAGE_SEARCH_2";
	
	public LogMessageSearch2StoredProcedure(DataSource ds){
        super(ds,P_MESSAGE_SEARCH);
        
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PROCESS_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANSID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACTIONID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_FROM_NUMBER",Types.NUMERIC));
        declareParameter(new SqlParameter("P_NUMBER_PER_PAGE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL",Types.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL_FILTERED",Types.NUMERIC));
        declareParameter(new SqlOutParameter("P_MESSAGE_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(String messageId, String processType, String message, String transId, 
			String actionId, String status,
			Integer fromNumber,Integer numberPerPage) throws Exception{

			Map in = new HashMap();
			
			in.put("P_MESSAGE_ID", messageId);
			in.put("P_PROCESS_TYPE", processType);
			in.put("P_MESSAGE", message);
			in.put("P_TRANSID", transId);
			in.put("P_FROM_NUMBER", fromNumber);
			in.put("P_NUMBER_PER_PAGE", numberPerPage);
			in.put("P_STATUS", status);
			in.put("P_ACTIONID", actionId);
			
		 	try {
	        	Map outMap = execute(in);
	        	
	        	return outMap;
	        } catch(Exception ex)
	        {
	        	System.out.println(ex.toString());
	        	
	        	return new HashMap();
	        }
	}
	
	
	public AdminDataTables transformLogMessageCursorToObject(Map<String, Object> map) throws Exception
	{
		AdminDataTables adminLogMessages = new AdminDataTables();
		LogMessages logMessages= new LogMessages();
		logMessages.setList(new ArrayList<LogMessage>());
		
		if(!map.containsKey("P_MESSAGE_CURSOR")) {
			return null;
		}
		
		List  messageList = (ArrayList)map.get("P_MESSAGE_CURSOR");
		
		Integer total = Integer.parseInt(map.get("P_TOTAL").toString());
		Integer filteredTotal = Integer.parseInt(map.get("P_TOTAL_FILTERED").toString());
		
		for(Object message : messageList)
		{
			Map mapMessage = (LinkedCaseInsensitiveMap)message;
			
			LogMessage logMessage = new LogMessage();
			
			if(mapMessage.containsKey("MESSAGEID"))
			{
				if(mapMessage.get("MESSAGEID") != null)
				{
					logMessage.setMessageId(Integer.parseInt(mapMessage.get("MESSAGEID").toString()));
				}
			}
			
			if(mapMessage.containsKey("STATUS"))
			{
				if(mapMessage.get("STATUS") != null)
				{
					logMessage.setStatus(mapMessage.get("STATUS").toString());
				}
			}
			
			if(mapMessage.containsKey("TRANSID"))
			{
				if(mapMessage.get("TRANSID") != null)
				{
					logMessage.setTransId(mapMessage.get("TRANSID").toString());
				}
			}
			
			if(mapMessage.containsKey("GROUPID"))
			{
				if(mapMessage.get("GROUPID") != null)
				{
					logMessage.setGroupId(mapMessage.get("GROUPID").toString());
				}
			}
			
			if(mapMessage.containsKey("MESSAGE"))
			{
				if(mapMessage.get("MESSAGE") != null)
				{
					logMessage.setMessage(mapMessage.get("MESSAGE").toString());
				}
			}
			
			if(mapMessage.containsKey("APPLICATION"))
			{
				if(mapMessage.get("APPLICATION") != null)
				{
					logMessage.setApplication(mapMessage.get("APPLICATION").toString());
				}
			}
			
			if(mapMessage.containsKey("ACTIONID"))
			{
				if(mapMessage.get("ACTIONID") != null)
				{
					logMessage.setActionId(Integer.parseInt(mapMessage.get("ACTIONID").toString()));
				}
			}
			
			if(mapMessage.containsKey("PROCESS_TYPE"))
			{
				if(mapMessage.get("PROCESS_TYPE") != null)
				{
					logMessage.setProcessType(mapMessage.get("PROCESS_TYPE").toString());
				}
			}
			
			if(mapMessage.containsKey("PROCESS_SUBTYPE"))
			{
				if(mapMessage.get("PROCESS_SUBTYPE") != null)
				{
					logMessage.setProcessSubType(mapMessage.get("PROCESS_SUBTYPE").toString());
				}
			}
			
			if(mapMessage.containsKey("CREATED_BY"))
			{
				if(mapMessage.get("CREATED_BY") != null)
				{
					logMessage.setCreatedBy(mapMessage.get("CREATED_BY").toString());
				}
			}
			
			if(mapMessage.containsKey("UPDATED_BY"))
			{
				if(mapMessage.get("UPDATED_BY") != null)
				{
					logMessage.setUpdatedBy(mapMessage.get("UPDATED_BY").toString());
				}
			}
			
			if(mapMessage.containsKey("CREATED_DATETIME"))
			{
				if(mapMessage.get("CREATED_DATETIME") != null)
				{
					logMessage.setCreatedDatetime(mapMessage.get("CREATED_DATETIME").toString());
				}
			}
			
			if(mapMessage.containsKey("UPDATED_DATETIME"))
			{
				if(mapMessage.get("UPDATED_DATETIME") != null)
				{
					logMessage.setUpdatedDatetime(mapMessage.get("UPDATED_DATETIME").toString());
				}
			}
			
			logMessages.getList().add(logMessage);
		}
		
		adminLogMessages.setData(logMessages.getList());
		adminLogMessages.setRecordsTotal(total);
		adminLogMessages.setRecordsFiltered(filteredTotal);
		
		return adminLogMessages;
	}
	
}
