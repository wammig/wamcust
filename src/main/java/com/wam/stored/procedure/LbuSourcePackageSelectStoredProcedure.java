package com.wam.stored.procedure;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuSourcePackage;

import oracle.jdbc.OracleTypes;

import static java.lang.System.out;

public class LbuSourcePackageSelectStoredProcedure extends StoredProcedure{

	private static final String SOURCEPACKAGE_SELECT = "PKG_LBU_SOURCEPACKAGE.P_SOURCEPACKAGE_SELECT";
	
	private String sourcePackageId;
	private String setId;
	
	public LbuSourcePackageSelectStoredProcedure(DataSource ds){
        super(ds,SOURCEPACKAGE_SELECT);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_SOURCE_PACKAGE_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_SOURCE_PACKAGE_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map<String, Object> execute(String setId, String sourcePackageId)
	{
		Map<String, Object> in = new HashMap<String, Object>();
	       
        in.put("P_SET_ID",setId);
        in.put("P_SOURCE_PACKAGE_ID",sourcePackageId);
        
        this.sourcePackageId = sourcePackageId;
        this.setId = setId;
        
        try {
        	Map<String, Object> outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap<String, Object>();
        }
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public LbuSourcePackage transformSourcePackageCursorToObject(Map<String, Object> map) throws Exception
	{
		LbuSourcePackage lbuSourcePackage = new LbuSourcePackage();
		
		if(!map.containsKey("P_SOURCE_PACKAGE_CURSOR")) {
			return null;
		}
		
		List sourcePackagetList = (ArrayList)map.get("P_SOURCE_PACKAGE_CURSOR");
		
		if(sourcePackagetList.isEmpty())
		{
			//throw new Exception("SOURCE_PACKAGE: " + this.sourcePackageId + " not found in LBU_SOURCEPACKAGE");
			return null;
		}
		
		if(sourcePackagetList.size() != 1)
		{
			throw new Exception("P_SOURCE_PACKAGE_CURSOR size is " + sourcePackagetList.size());
		}
		
		Map mapSourcePackage = (LinkedCaseInsensitiveMap)sourcePackagetList.get(0);
		
		String sourcePackageString = sourcePackagetList.get(0).toString();
		
		if(mapSourcePackage.containsKey("LBU_SOURCE_PACKAGE_ID"))
		{
			if(mapSourcePackage.get("LBU_SOURCE_PACKAGE_ID") != null)
			{
				lbuSourcePackage.setLbuSourcePackageId(mapSourcePackage.get("LBU_SOURCE_PACKAGE_ID").toString());
			}
		}
		
		if(mapSourcePackage.containsKey("SOURCE_PACKAGE_NAME"))
		{
			if(mapSourcePackage.get("SOURCE_PACKAGE_NAME") != null)
			{
				lbuSourcePackage.setSourcePackageName(mapSourcePackage.get("SOURCE_PACKAGE_NAME").toString());
			}
		}
		
		if(mapSourcePackage.containsKey("LBU_SET_ID"))
		{
			if(mapSourcePackage.get("LBU_SET_ID") != null)
			{
				lbuSourcePackage.setLbuSetId(mapSourcePackage.get("LBU_SET_ID").toString());
			}
		}
		
		if(mapSourcePackage.containsKey("ASSEMBLY_ID"))
		{
			if(mapSourcePackage.get("ASSEMBLY_ID") != null)
			{
				lbuSourcePackage.setAssemblyId(mapSourcePackage.get("ASSEMBLY_ID").toString());
			}
		}
		
		/**
		 * extract the created date
		 */
		Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
		Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(sourcePackageString);
		
		if(createdDateTimeMatcher.find())
		{ 
			Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
			Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
			
			if(createdDateTimeTrimMatcher.find()) 
			{
				String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
				lbuSourcePackage.setCreatedDateTime(createdDateTime);
			}
		}
		
		
		/**
		 * extract the updated date
		 */
		Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
		Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(sourcePackageString);
		
		if(updatedDateTimeMatcher.find())
		{ 
			Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
			Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
			
			if(updateDateTimeTrimMatcher.find()) 
			{
				String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
				lbuSourcePackage.setUpdatedDateTime(updateDateTime);
			}
		}
		
		return lbuSourcePackage;
	}
	
	
}
