package com.wam.stored.procedure;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wam.service.BaseService;

public class LbuAccountUpdateAllStoredProcedure extends StoredProcedure{

	private static final String P_ACCOUNT_UPDATE = "PKG_LBU_ACCOUNT.P_ACCOUNT_UPDATE";
	
	public LbuAccountUpdateAllStoredProcedure(DataSource ds){
        super(ds,P_ACCOUNT_UPDATE);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_NAME1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_NAME2",Types.VARCHAR));
        declareParameter(new SqlParameter("P_SALES_REP_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_EXT_ORDER_NO",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MAX_USERS",Types.NUMERIC));
        declareParameter(new SqlParameter("P_BILLABLE_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE2",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CITY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COUNTY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ZIP_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_STATE_OR_PROVINCE_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COUNTRY_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADMIN_USER",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("O_ROWCOUNT",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}
	
	public Map execute(String lbuSetId, String lbuAccountId, String accountName1, String accountName2, 
			String auditUserId, String salesRepId, String extOrderNo, Integer maxUsers, String billableStatus, String accountType,
			String addressLine1, String addressLine2, String cityName, String countryName, String zipCode, String stateOrProvinceCode,
			String countryCode) throws Exception{
		Map in = new HashMap();
		
		in.put("P_SET_ID", lbuSetId);
		in.put("P_ACCOUNT_ID",lbuAccountId);
		in.put("P_ACCOUNT_NAME1",accountName1);
		in.put("P_ACCOUNT_NAME2",accountName2);
		in.put("P_SALES_REP_ID",salesRepId);
		in.put("P_EXT_ORDER_NO",extOrderNo);
		in.put("P_MAX_USERS",maxUsers);
		in.put("P_BILLABLE_STATUS",billableStatus);
		in.put("P_ACCOUNT_TYPE",accountType);
		in.put("P_ADDRESS_LINE1",addressLine1);
		in.put("P_ADDRESS_LINE2",addressLine2);
		in.put("P_CITY_NAME",cityName);
		in.put("P_COUNTY_NAME",countryName);
		in.put("P_ZIP_CODE",zipCode);
		in.put("P_STATE_OR_PROVINCE_CODE",stateOrProvinceCode);
		in.put("P_COUNTRY_CODE",countryCode);
		in.put("P_ADMIN_USER",auditUserId);
		in.put("P_TRANS_ID", BaseService.generateTransIdSysHistory());
		
		
		try {
        	return execute(in);
        } 
		catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
	}
}
