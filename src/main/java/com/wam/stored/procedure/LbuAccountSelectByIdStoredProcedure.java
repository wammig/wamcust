package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.service.AccountWamService;
import com.wam.service.ContentSubWamService;
import com.wam.test.db.BaseTest;
import com.wam.test.db.TestLbuAccountSelectByIdStoredProcedure;

import oracle.jdbc.OracleTypes;

public class LbuAccountSelectByIdStoredProcedure extends StoredProcedure{
	
	public final static Logger logger = LoggerFactory.getLogger(LbuAccountSelectByIdStoredProcedure.class);
	
	private static final String ACCOUNT_SELECT = "PKG_LBU_ACCOUNT.P_ACCOUNT_SELECT";
	
	/**
	 * regexUpdatedDateTime is shared in many other classes
	 * don't change it
	 */
	public static String regexUpdatedDateTime = "(UPDATED_DATETIME){1}([^=]*[=])([^,]*[,]|[^}]*[}])";
	
	/**
	 * regexDateTimeTrim is shared in many other classes
	 * don't change it
	 */
	public static String regexDateTimeTrim = "[0-9-//s]{8,}";
	
	/**
	 * regexCreatedDateTime is shared in many other classes
	 * don't change it
	 */
	public static String regexCreatedDateTime = "(CREATED_DATETIME){1}([^=]*[=])([^,]*[,]|[^}]*[}])";
	
	public LbuAccountSelectByIdStoredProcedure(DataSource ds){
        super(ds,ACCOUNT_SELECT);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_ACCOUNT_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute(String lbuSetId, String lbuAccountId) throws Exception
	{
		try {
			Map in = new HashMap();
			in.put("P_SET_ID", lbuSetId);
			in.put("P_ACCOUNT_ID", lbuAccountId);
        	Map outMap = execute(in);

        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	//throw new Exception(ex.getMessage());
        	
        	return new HashMap();
        }
		
	}
	
	public static LbuAccount transformAccountCursorToObject(Map<String, Object> map) throws Exception
	{
		LbuAccount lbuAccountDb = new LbuAccount();
		
		if(!map.containsKey("P_ACCOUNT_CURSOR")) {
			logger.error("No key found P_ACCOUNT_CURSOR ");
			return null;
		}
		
		List  accountList = (ArrayList)map.get("P_ACCOUNT_CURSOR");

		if(accountList.size() != 1)
		{
			logger.error("P_ACCOUNT_CURSOR size is " + accountList.size());
			return null;
			//throw new Exception("P_ACCOUNT_CURSOR size is " + accountList.size());
		}
		Map mapAccount = (LinkedCaseInsensitiveMap)accountList.get(0);
		
		String accountString = accountList.get(0).toString();
		
		if(mapAccount.containsKey("LBU_SET_ID"))
		{
			if(mapAccount.get("LBU_SET_ID") != null)
			{
				lbuAccountDb.setLbuSetId(mapAccount.get("LBU_SET_ID").toString());
			}
		}
		
		if(mapAccount.containsKey("LBU_ACCOUNT_ID"))
		{
			if(mapAccount.get("LBU_ACCOUNT_ID") != null)
			{
				lbuAccountDb.setLbuAccountId(mapAccount.get("LBU_ACCOUNT_ID").toString());
			}
		}
		
		if(mapAccount.containsKey("GBS_ACCOUNT_ID"))
		{
			if(mapAccount.get("GBS_ACCOUNT_ID") != null)
			{
				lbuAccountDb.setGbsAccountId(mapAccount.get("GBS_ACCOUNT_ID").toString());
			}
		}
		
		if(mapAccount.containsKey("GLP_ACCOUNT_ID"))
		{
			if(mapAccount.get("GLP_ACCOUNT_ID") != null)
			{
				lbuAccountDb.setGlpAccountId(mapAccount.get("GLP_ACCOUNT_ID").toString());
			}
		}
		
		if(mapAccount.containsKey("ACCOUNT_NAME1"))
		{
			if(mapAccount.get("ACCOUNT_NAME1") != null)
			{
				lbuAccountDb.setAccountName1(mapAccount.get("ACCOUNT_NAME1").toString());
			}
		}
		
		if(mapAccount.containsKey("ACCOUNT_NAME2"))
		{
			if(mapAccount.get("ACCOUNT_NAME2") != null)
			{
				lbuAccountDb.setAccountName2(mapAccount.get("ACCOUNT_NAME2").toString());
			}
		}
		
		if(mapAccount.containsKey("AUDIT_USER_ID"))
		{
			if(mapAccount.get("AUDIT_USER_ID") != null)
			{
				lbuAccountDb.setAuditUserId(mapAccount.get("AUDIT_USER_ID").toString());
			}
		}
		
		if(mapAccount.containsKey("SALES_REP_ID"))
		{
			if(mapAccount.get("SALES_REP_ID") != null)
			{
				lbuAccountDb.setSalesRepId(mapAccount.get("SALES_REP_ID").toString());
			}
		}
		
		if(mapAccount.containsKey("EXT_ORDER_NO"))
		{
			if(mapAccount.get("EXT_ORDER_NO") != null)
			{
				lbuAccountDb.setExtOrderNo(mapAccount.get("EXT_ORDER_NO").toString());
			}
		}
		
		if(mapAccount.containsKey("MAX_USERS"))
		{
			if(mapAccount.get("MAX_USERS") != null)
			{
				lbuAccountDb.setMaxUsers(Integer.parseInt(mapAccount.get("MAX_USERS").toString()));
			}
		}
		
		if(mapAccount.containsKey("BILLABLE_STATUS"))
		{
			if(mapAccount.get("BILLABLE_STATUS") != null)
			{
				lbuAccountDb.setBillableStatus(mapAccount.get("BILLABLE_STATUS").toString());
			}
		}
		
		if(mapAccount.containsKey("ACCOUNT_TYPE"))
		{
			if(mapAccount.get("ACCOUNT_TYPE") != null)
			{
				lbuAccountDb.setAccountType(mapAccount.get("ACCOUNT_TYPE").toString());
			}
		}
		
		if(mapAccount.containsKey("ADDRESS_LINE1"))
		{
			if(mapAccount.get("ADDRESS_LINE1") != null)
			{
				lbuAccountDb.setAddressLine1(mapAccount.get("ADDRESS_LINE1").toString());
			}
		}
		
		if(mapAccount.containsKey("ADDRESS_LINE2"))
		{
			if(mapAccount.get("ADDRESS_LINE2") != null)
			{
				lbuAccountDb.setAddressLine2(mapAccount.get("ADDRESS_LINE2").toString());
			}
		}
		
		if(mapAccount.containsKey("CITY_NAME"))
		{
			if(mapAccount.get("CITY_NAME") != null)
			{
				lbuAccountDb.setCityName(mapAccount.get("CITY_NAME").toString());
			}
		}
		
		if(mapAccount.containsKey("COUNTY_NAME"))
		{
			if(mapAccount.get("COUNTY_NAME") != null)
			{
				lbuAccountDb.setCountryName(mapAccount.get("COUNTY_NAME").toString());
			}
		}

		if(mapAccount.containsKey("ZIP_CODE"))
		{
			if(mapAccount.get("ZIP_CODE") != null)
			{
				lbuAccountDb.setZipCode(mapAccount.get("ZIP_CODE").toString());
			}
		}
		
		if(mapAccount.containsKey("STATE_OR_PROVINCE_CODE"))
		{
			if(mapAccount.get("STATE_OR_PROVINCE_CODE") != null)
			{
				lbuAccountDb.setStateOrProvinceCode(mapAccount.get("STATE_OR_PROVINCE_CODE").toString());
			}
		}
		
		if(mapAccount.containsKey("COUNTRY_CODE"))
		{
			if(mapAccount.get("COUNTRY_CODE") != null)
			{
				lbuAccountDb.setCountryCode(mapAccount.get("COUNTRY_CODE").toString());
			}
		}
		
		if(mapAccount.containsKey("DATA_MIGRATION"))
		{
			if(mapAccount.get("DATA_MIGRATION") != null)
			{
				lbuAccountDb.setDataMigration(mapAccount.get("DATA_MIGRATION").toString());
			}
		}
		
		if(mapAccount.containsKey("CUSTOMERPGUID"))
		{
			if(mapAccount.get("CUSTOMERPGUID") != null)
			{
				lbuAccountDb.setCustomerPguid(mapAccount.get("CUSTOMERPGUID").toString());
			}
		}
		
		if(mapAccount.containsKey("POBPGUID"))
		{
			if(mapAccount.get("POBPGUID") != null)
			{
				lbuAccountDb.setPobpguid(mapAccount.get("POBPGUID").toString());
			}
		}
		
		if(mapAccount.containsKey("ADDRESSPGUID"))
		{
			if(mapAccount.get("ADDRESSPGUID") != null)
			{
				lbuAccountDb.setAddressPGUID(mapAccount.get("ADDRESSPGUID").toString());
			}
		}
		
		/**
		 * extract the created date
		 */
		Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
		Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(accountString);
		
		if(createdDateTimeMatcher.find())
		{ 
			Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
			Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
			
			if(createdDateTimeTrimMatcher.find()) 
			{
				String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
				lbuAccountDb.setCreatedDatetime(createdDateTime);
			}
		}
		
		/**
		 * extract the updated date
		 */
		Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
		Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(accountString);
		
		if(updatedDateTimeMatcher.find())
		{ 
			Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
			Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
			
			if(updateDateTimeTrimMatcher.find()) 
			{
				String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
				lbuAccountDb.setUpdatedDatetime(updateDateTime);
			}
		}

		return lbuAccountDb;
	}
}
