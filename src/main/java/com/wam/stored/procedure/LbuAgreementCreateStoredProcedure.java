package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.service.AccountWamService;
import com.wam.service.ContentSubWamService;
import com.wam.test.db.BaseTest;
import com.wam.test.db.TestLbuAccountSelectByIdStoredProcedure;
import com.wam.utility.definition.Definition;

import oracle.jdbc.OracleTypes;

public class LbuAgreementCreateStoredProcedure extends StoredProcedure{
	
	private static final String P_AGREEMENT_CREATE = "PKG_LBU_AGREEMENT.P_AGREEMENT_CREATE";
	
	public LbuAgreementCreateStoredProcedure(DataSource ds){
        super(ds,P_AGREEMENT_CREATE);
        
        declareParameter(new SqlParameter("P_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ASSET_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PRODUCT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_BEGIN_DATE",Types.DATE));
        declareParameter(new SqlParameter("P_END_DATE",Types.DATE));
        declareParameter(new SqlParameter("P_LICENSE_COUNT",Types.NUMERIC));
        declareParameter(new SqlParameter("P_CUSTOMERPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute(
				String agreementId,
				String assetAgreementId,
				String productId,
				String beginDate,
				String endDate, 
				Integer licenseCount,
				String customerPguid,
				String transId
			) throws Exception{
		Map in = new HashMap();
		try {
			in.put("P_AGREEMENT_ID", agreementId);
			in.put("P_ASSET_AGREEMENT_ID", assetAgreementId);
			in.put("P_PRODUCT_ID", productId);
			in.put("P_LICENSE_COUNT", licenseCount);
			in.put("P_CUSTOMERPGUID", customerPguid);
			in.put("P_TRANS_ID", transId);
			
			
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_MMDDYYYYHHMMSS);
				SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_YYYY_MM_DD);

				Date begin = formatter.parse(beginDate);
				in.put("P_BEGIN_DATE", formatter2.format(begin).toString());

				Date end = formatter.parse(endDate);
				in.put("P_END_DATE", formatter2.format(end).toString());
		        
			} catch(Exception ex) {
				throw new Exception(" Date Format ");
			}
			
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
		
	}
	
}
