package com.wam.stored.procedure;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;

public class WindowsAccountRoleSearchStoredProcedure extends StoredProcedure{

	private static final String P_WINDOWS_ACCOUNT_ROLE_SEARCH = "PKG_WINDOWS.P_WINDOWS_ACCOUNT_ROLE_SEARCH";
	
	public WindowsAccountRoleSearchStoredProcedure(DataSource ds){
        super(ds,P_WINDOWS_ACCOUNT_ROLE_SEARCH);

        declareParameter(new SqlParameter("P_WINDOWS_ID",OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("O_WINDOWS_ROLE",OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}
	
	public Map<String, Object> execute(String windowsId) throws Exception{
		Map<String, Object> in = new HashMap<String, Object>();
		try {	
        	in.put("P_WINDOWS_ID", windowsId);
        	
			Map<String, Object> outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex) {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
		
	}
}
