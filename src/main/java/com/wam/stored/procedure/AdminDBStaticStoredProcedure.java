package com.wam.stored.procedure;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import oracle.jdbc.OracleTypes;

public class AdminDBStaticStoredProcedure extends StoredProcedure{

	private static final String P_DB_STATISTIC = "PKG_LBU_ACCOUNT.P_DB_STATISTIC";
	
	public AdminDBStaticStoredProcedure(DataSource ds){
        super(ds,P_DB_STATISTIC);

        declareParameter(new SqlOutParameter("P_ACCOUNT_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOCATION_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_AGREEMENT_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_CONTENTSUB_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_SYSHISTORY_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOGMESSAGE_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_SYS_HISTORY_AGREE_S_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_SYS_HISTORY_AGREE_F_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_SYS_HISTORY_ACCOUNT_S_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_SYS_HISTORY_ACCOUNT_F_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOGMESSAGE_C_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOGMESSAGE_B_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOGMESSAGE_E_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOGMESSAGE_R_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}
	
	public Map<String, Object> execute() throws Exception{
		Map<String, Object> in = new HashMap<String, Object>();
		try {	
        	Map<String, Object> outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex) {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
		
	}
}
