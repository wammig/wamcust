package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.out;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;

import oracle.jdbc.OracleTypes;

public class LbuAccountSearch2StoredProcedure extends StoredProcedure{

	private static final String ACCOUNT_SEARCH = "PKG_LBU_ACCOUNT.P_ACCOUNT_SEARCH2";
	
	public LbuAccountSearch2StoredProcedure(DataSource ds){
        super(ds,ACCOUNT_SEARCH);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_GBSACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_NAME1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_NAME2",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CITY",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ZIP_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COUNTRY_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_BILLABLE_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CUSTOMERPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_FROM_NUMBER",Types.NUMERIC));
        declareParameter(new SqlParameter("P_NUMBER_PER_PAGE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL_FILTERED",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_ACCOUNT_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}
	
	public Map execute(
			String lbuSetId, 
			String lbuAccountId, 
			String gbsAccountId,
			String accountName1,
			String accountName2,
			String city,
			String zipCode,
			String countryCode,
			String billableStatus,
			String addressLine1,
			String customerPguid,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception{
		Map in = new HashMap();
		try {
			in.put("P_SET_ID", lbuSetId);
			in.put("P_ACCOUNT_ID", lbuAccountId);
			in.put("P_GBSACCOUNT_ID", gbsAccountId);
			in.put("P_ACCOUNT_NAME1", accountName1);
			in.put("P_ACCOUNT_NAME2", accountName2);
			in.put("P_CITY", city);
			in.put("P_ZIP_CODE", zipCode);
			in.put("P_COUNTRY_CODE", countryCode);
			in.put("P_BILLABLE_STATUS", billableStatus);
			in.put("P_ADDRESS_LINE1", addressLine1);
			in.put("P_CUSTOMERPGUID", customerPguid);
			in.put("P_FROM_NUMBER", fromNumber);
			in.put("P_NUMBER_PER_PAGE", numberPerPage);
			
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex) {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
		
	}
	
	/**
	 * get a list of LbuAccount
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public AdminDataTables transformAccountCursorToObject(Map<String, Object> map) throws Exception
	{
		AdminDataTables adminLbuAccounts = new AdminDataTables(); 
		LbuAccounts lbuAccountDbs = new LbuAccounts();
		
		List  accountList = (ArrayList)map.get("P_ACCOUNT_CURSOR");

		Integer total = Integer.parseInt(map.get("P_TOTAL").toString());
		Integer filteredTotal = Integer.parseInt(map.get("P_TOTAL_FILTERED").toString());
		
		for(Object account : accountList) {
			Map mapAccount = (LinkedCaseInsensitiveMap)account;

			LbuAccount lbuAccountDb = new LbuAccount();
			
			String accountString = mapAccount.toString();
			if(mapAccount.containsKey("LBU_SET_ID"))
			{
				if(mapAccount.get("LBU_SET_ID") != null)
				{
					lbuAccountDb.setLbuSetId(mapAccount.get("LBU_SET_ID").toString());
				}
			}
			
			if(mapAccount.containsKey("LBU_ACCOUNT_ID"))
			{
				if(mapAccount.get("LBU_ACCOUNT_ID") != null)
				{
					lbuAccountDb.setLbuAccountId(mapAccount.get("LBU_ACCOUNT_ID").toString());
				}
			}
			
			if(mapAccount.containsKey("GBS_ACCOUNT_ID"))
			{
				if(mapAccount.get("GBS_ACCOUNT_ID") != null)
				{
					lbuAccountDb.setGbsAccountId(mapAccount.get("GBS_ACCOUNT_ID").toString());
				}
			}
			
			if(mapAccount.containsKey("GLP_ACCOUNT_ID"))
			{
				if(mapAccount.get("GLP_ACCOUNT_ID") != null)
				{
					lbuAccountDb.setGlpAccountId(mapAccount.get("GLP_ACCOUNT_ID").toString());
				}
			}
			
			if(mapAccount.containsKey("ACCOUNT_NAME1"))
			{
				if(mapAccount.get("ACCOUNT_NAME1") != null)
				{
					lbuAccountDb.setAccountName1(mapAccount.get("ACCOUNT_NAME1").toString());
				}
			}
			
			if(mapAccount.containsKey("ACCOUNT_NAME2"))
			{
				if(mapAccount.get("ACCOUNT_NAME2") != null)
				{
					lbuAccountDb.setAccountName2(mapAccount.get("ACCOUNT_NAME2").toString());
				}
			}
			
			if(mapAccount.containsKey("AUDIT_USER_ID"))
			{
				if(mapAccount.get("AUDIT_USER_ID") != null)
				{
					lbuAccountDb.setAuditUserId(mapAccount.get("AUDIT_USER_ID").toString());
				}
			}
			
			if(mapAccount.containsKey("SALES_REP_ID"))
			{
				if(mapAccount.get("SALES_REP_ID") != null)
				{
					lbuAccountDb.setSalesRepId(mapAccount.get("SALES_REP_ID").toString());
				}
			}
			
			if(mapAccount.containsKey("EXT_ORDER_NO"))
			{
				if(mapAccount.get("EXT_ORDER_NO") != null)
				{
					lbuAccountDb.setExtOrderNo(mapAccount.get("EXT_ORDER_NO").toString());
				}
			}
			
			if(mapAccount.containsKey("MAX_USERS"))
			{
				if(mapAccount.get("MAX_USERS") != null)
				{
					lbuAccountDb.setMaxUsers(Integer.parseInt(mapAccount.get("MAX_USERS").toString()));
				}
			}
			
			if(mapAccount.containsKey("BILLABLE_STATUS"))
			{
				if(mapAccount.get("BILLABLE_STATUS") != null)
				{
					lbuAccountDb.setBillableStatus(mapAccount.get("BILLABLE_STATUS").toString());
				}
			}
			
			if(mapAccount.containsKey("ACCOUNT_TYPE"))
			{
				if(mapAccount.get("ACCOUNT_TYPE") != null)
				{
					lbuAccountDb.setAccountType(mapAccount.get("ACCOUNT_TYPE").toString());
				}
			}
			
			if(mapAccount.containsKey("ADDRESS_LINE1"))
			{
				if(mapAccount.get("ADDRESS_LINE1") != null)
				{
					lbuAccountDb.setAddressLine1(mapAccount.get("ADDRESS_LINE1").toString());
				}
			}
			
			if(mapAccount.containsKey("ADDRESS_LINE2"))
			{
				if(mapAccount.get("ADDRESS_LINE2") != null)
				{
					lbuAccountDb.setAddressLine2(mapAccount.get("ADDRESS_LINE2").toString());
				}
			}
			
			if(mapAccount.containsKey("CITY_NAME"))
			{
				if(mapAccount.get("CITY_NAME") != null)
				{
					lbuAccountDb.setCityName(mapAccount.get("CITY_NAME").toString());
				}
			}
			
			if(mapAccount.containsKey("COUNTY_NAME"))
			{
				if(mapAccount.get("COUNTY_NAME") != null)
				{
					lbuAccountDb.setCountryName(mapAccount.get("COUNTY_NAME").toString());
				}
			}

			if(mapAccount.containsKey("ZIP_CODE"))
			{
				if(mapAccount.get("ZIP_CODE") != null)
				{
					lbuAccountDb.setZipCode(mapAccount.get("ZIP_CODE").toString());
				}
			}
			
			if(mapAccount.containsKey("STATE_OR_PROVINCE_CODE"))
			{
				if(mapAccount.get("STATE_OR_PROVINCE_CODE") != null)
				{
					lbuAccountDb.setStateOrProvinceCode(mapAccount.get("STATE_OR_PROVINCE_CODE").toString());
				}
			}
			
			if(mapAccount.containsKey("COUNTRY_CODE"))
			{
				if(mapAccount.get("COUNTRY_CODE") != null)
				{
					lbuAccountDb.setCountryCode(mapAccount.get("COUNTRY_CODE").toString());
				}
			}
			
			if(mapAccount.containsKey("DATA_MIGRATION"))
			{
				if(mapAccount.get("DATA_MIGRATION") != null)
				{
					lbuAccountDb.setDataMigration(mapAccount.get("DATA_MIGRATION").toString());
				}
			}
			
			if(mapAccount.containsKey("CUSTOMERPGUID"))
			{
				if(mapAccount.get("CUSTOMERPGUID") != null)
				{
					lbuAccountDb.setCustomerPguid(mapAccount.get("CUSTOMERPGUID").toString());
				}
			}
			
			if(mapAccount.containsKey("POBPGUID"))
			{
				if(mapAccount.get("POBPGUID") != null)
				{
					lbuAccountDb.setPobpguid(mapAccount.get("POBPGUID").toString());
				}
			}
			
			Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
			Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(accountString);
			
			if(createdDateTimeMatcher.find())
			{ 
				Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
				
				if(createdDateTimeTrimMatcher.find()) 
				{
					String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
					lbuAccountDb.setCreatedDatetime(createdDateTime);
				}
			}
			

			Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
			Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(accountString);
			
			if(updatedDateTimeMatcher.find())
			{ 
				Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
				
				if(updateDateTimeTrimMatcher.find()) 
				{
					String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
					lbuAccountDb.setUpdatedDatetime(updateDateTime);
				}
			}
			
			lbuAccountDbs.getList().add(lbuAccountDb);
		}
		
		adminLbuAccounts.setData(lbuAccountDbs.getList());
		adminLbuAccounts.setRecordsTotal(total);
		adminLbuAccounts.setRecordsFiltered(filteredTotal);
		
		return adminLbuAccounts;
	}
	
}
