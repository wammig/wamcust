package com.wam.stored.procedure;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * update the sysHistory when ecm returned
 * @author QINC1
 *
 */
public class SysHistoryUpdateStoredProcedure extends StoredProcedure{
	
	private static final String SYS_HISTORY_UPDATE = "PKG_SYS_HISTORY.P_HISTORY_UPDATE";

	public SysHistoryUpdateStoredProcedure(DataSource ds){
        super(ds,SYS_HISTORY_UPDATE);
        
        declareParameter(new SqlParameter("P_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_PROCESS_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PROCESS_SUBTYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_SEND_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_REPLY_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ERROR_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ERROR_MESSAGE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_MESSAGE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADMIN_USER",Types.VARCHAR));
        declareParameter(new SqlParameter("P_REPLY_MESSAGE",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("O_ROWCOUNT",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(Integer historyId, String processType, String processSubType, String entityId, String sendStatus,
			String replyStatus, String errorCode, String errorMessage, String transId, Integer messageId, String message, 
			String adminUser, String replyMessage) throws Exception{
		
		Map in = new HashMap();
		
		in.put("P_HISTORY_ID", historyId);
		in.put("P_PROCESS_TYPE", processType);
		in.put("P_PROCESS_SUBTYPE", processSubType);
		in.put("P_ENTITY_ID", entityId);
		in.put("P_SEND_STATUS", sendStatus);
		in.put("P_REPLY_STATUS", replyStatus);
		in.put("P_ERROR_CODE", errorCode);
		in.put("P_ERROR_MESSAGE", errorMessage);
		in.put("P_TRANS_ID", transId);
		in.put("P_MESSAGE_ID", messageId);
		in.put("P_MESSAGE", message);
		in.put("P_ADMIN_USER", adminUser);
		in.put("P_REPLY_MESSAGE", replyMessage);
		
	 	try {
        	Map out = execute(in);

        	return out;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
	 	
	}
}
