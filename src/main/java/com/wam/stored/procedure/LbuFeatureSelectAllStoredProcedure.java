package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuFeature;
import com.wam.model.db.gbsd.helper.LbuFeatures;

import oracle.jdbc.OracleTypes;

@Service("LbuFeatureSelectAllStoredProcedure")
public class LbuFeatureSelectAllStoredProcedure extends StoredProcedure{
	
	private static final String P_FEATURE_SELECT = "PKG_LBU_AGREEMENT.P_FEATURE_SELECT_ALL";
	
	public LbuFeatureSelectAllStoredProcedure(DataSource ds){
        super(ds,P_FEATURE_SELECT);
        
        declareParameter(new SqlOutParameter("P_FEATURE_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute() throws Exception{
		Map in = new HashMap();
		try {

        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
		
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public LbuFeatures transformFeatureCursorToObject(Map<String, Object> map) throws Exception
	{
		LbuFeatures lbuFeatures = new LbuFeatures();
		
		if(!map.containsKey("P_FEATURE_CURSOR")) {
			return null;
		}
		
		List  featureList = (ArrayList)map.get("P_FEATURE_CURSOR");

		for(Object feature : featureList) {
			LbuFeature lbuFeature = new LbuFeature();
			
			Map mapFeature = (LinkedCaseInsensitiveMap)feature;
			
			if(mapFeature.containsKey("ASSEMBLY_ID"))
			{
				if(mapFeature.get("ASSEMBLY_ID") != null)
				{
					lbuFeature.setAssemblyId(mapFeature.get("ASSEMBLY_ID").toString());
				}
			}
			
			if(mapFeature.containsKey("DESCRIPTION"))
			{
				if(mapFeature.get("DESCRIPTION") != null)
				{
					lbuFeature.setDescription(mapFeature.get("DESCRIPTION").toString());
				}
			}
			
			if(mapFeature.containsKey("STATUS"))
			{
				if(mapFeature.get("STATUS") != null)
				{
					lbuFeature.setStatus(mapFeature.get("STATUS").toString());
				}
			}
			
			lbuFeatures.getList().add(lbuFeature);
		}
		
		return lbuFeatures;
	}
}
