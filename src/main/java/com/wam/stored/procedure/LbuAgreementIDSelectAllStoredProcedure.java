package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.service.AccountWamService;
import com.wam.service.ContentSubWamService;
import com.wam.test.db.BaseTest;
import com.wam.test.db.TestLbuAccountSelectByIdStoredProcedure;

import oracle.jdbc.OracleTypes;

public class LbuAgreementIDSelectAllStoredProcedure extends StoredProcedure{
	
	private static final String P_AGREEMENT_ID_SELECT_ALL = "PKG_LBU_AGREEMENT.P_AGREEMENT_ID_SELECT_ALL";
	
	public LbuAgreementIDSelectAllStoredProcedure(DataSource ds){
        super(ds,P_AGREEMENT_ID_SELECT_ALL);
        
        declareParameter(new SqlOutParameter("P_AGREEMENT_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute() throws Exception{
		Map in = new HashMap();
		try {
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	out.println(ex.toString());
        	
        	return new HashMap();
        }
		
	}
	
	public LbuAgreements transformAgreementCursorToObject(Map<String, Object> map) throws Exception
	{
		LbuAgreements lbuAgreements = new LbuAgreements();
		lbuAgreements.setList(new ArrayList<LbuAgreement>());
		
		/**
		 * to fix the bud in dataTable when table is empty
		 */
		
		if(map.size() == 0)
		{
			return lbuAgreements;
		}

		if(!map.containsKey("P_AGREEMENT_CURSOR")) {
			return lbuAgreements;
		}
		
		List  agreementList = (ArrayList)map.get("P_AGREEMENT_CURSOR");

		for(Object agreement : agreementList) 
		{
			Map mapAgreement = (LinkedCaseInsensitiveMap)agreement;
			
			LbuAgreement lbuAgreement = new LbuAgreement();
			if(mapAgreement.containsKey("AGREEMENT_ID"))
			{
				if(mapAgreement.get("AGREEMENT_ID") != null)
				{
					lbuAgreement.setAgreementId(mapAgreement.get("AGREEMENT_ID").toString());
				}
			}
			
			if(mapAgreement.containsKey("ASSET_AGREEMENT_ID"))
			{
				if(mapAgreement.get("ASSET_AGREEMENT_ID") != null)
				{
					lbuAgreement.setAssetAgreementId(mapAgreement.get("ASSET_AGREEMENT_ID").toString());
				}
			}
		
			lbuAgreements.getList().add(lbuAgreement);
		}
		
	
		return lbuAgreements;
	}
}
