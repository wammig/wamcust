package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class LbuLocationUpdateAddressPGUIDStoredProcedure extends StoredProcedure{
	
	private static final String LOCATION_UPDATE = "PKG_LBU_LOCATION.P_LOCATION_UPDATE_ADDRESS";

	public LbuLocationUpdateAddressPGUIDStoredProcedure(DataSource ds){
        super(ds,LOCATION_UPDATE);
        
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_LOCATION_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESSPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_POBPGUID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_MESSAGE",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map execute(
			String lbuAccountId,
			String locationId,
			String addressPguid,
			String pobPguid
			) throws Exception{

		Map in = new HashMap();
		
        in.put("P_ACCOUNT_ID",lbuAccountId);
        in.put("P_LOCATION_ID",locationId);
        in.put("P_ADDRESSPGUID", addressPguid);
        in.put("P_POBPGUID", pobPguid);
        
        try {
        	Map out = execute(in);
        	
        	return out;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
    }

}
