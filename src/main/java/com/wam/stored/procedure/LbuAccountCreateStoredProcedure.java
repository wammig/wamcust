package com.wam.stored.procedure;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wam.service.BaseService;
import com.wam.utility.definition.Definition;

public class LbuAccountCreateStoredProcedure extends StoredProcedure{
	
	private static final String ACCOUNT_INSERT = "PKG_LBU_ACCOUNT.P_ACCOUNT_INSERT";
	
	public LbuAccountCreateStoredProcedure(DataSource ds){
        super(ds,ACCOUNT_INSERT);
        
        declareParameter(new SqlParameter("LBU_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("GBS_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("GLP_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("ACCOUNT_NAME1",Types.VARCHAR));
        declareParameter(new SqlParameter("ACCOUNT_NAME2",Types.VARCHAR));
        declareParameter(new SqlParameter("AUDIT_USER_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("SALES_REP_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("EXT_ORDER_NO",Types.VARCHAR));
        declareParameter(new SqlParameter("MAX_USERS",Types.NUMERIC));
        declareParameter(new SqlParameter("BILLABLE_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("ACCOUNT_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("ADDRESS_LINE1",Types.VARCHAR));
        declareParameter(new SqlParameter("ADDRESS_LINE2",Types.VARCHAR));
        declareParameter(new SqlParameter("CITY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("COUNTY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("ZIP_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("STATE_OR_PROVINCE_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("COUNTRY_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("UPDATED_DATETIME",Types.DATE));
        declareParameter(new SqlParameter("CREATED_DATETIME",Types.DATE));
        declareParameter(new SqlParameter("DATA_MIGRATION",Types.VARCHAR));
        declareParameter(new SqlParameter("LBU_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.NUMERIC));
        declareParameter(new SqlOutParameter("OUTPUT_LBU_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_MESSAGE",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map<String, Object> execute(String lbuAccountId, String gbsAccountId, String glpAccountId, String accountName1, String accountName2, 
			String auditUserId, String salesRepId, String extOrderNo, Integer maxUsers, String billableStatus, String accountType,
			String addressLine1, String addressLine2, String cityName, String countryName, String zipCode, String stateOrProvinceCode,
			String countryCode, String updatedDatetime, String createdDatetime, String dataMigration, String lbuSetId) throws Exception{
		
		
		
		Map<String, Object> in = new HashMap<String, Object>();
       
        in.put("LBU_ACCOUNT_ID",lbuAccountId);
        in.put("GBS_ACCOUNT_ID",gbsAccountId);
        in.put("GLP_ACCOUNT_ID",glpAccountId);
        in.put("ACCOUNT_NAME1",accountName1);
        in.put("ACCOUNT_NAME2",accountName2);
        in.put("AUDIT_USER_ID",auditUserId);
        in.put("SALES_REP_ID",salesRepId);
        in.put("EXT_ORDER_NO",extOrderNo);
        in.put("MAX_USERS",maxUsers);
        in.put("BILLABLE_STATUS",billableStatus);
        in.put("ACCOUNT_TYPE",accountType);
        in.put("ADDRESS_LINE1",addressLine1);
        in.put("ADDRESS_LINE2",addressLine2);
        in.put("CITY_NAME",cityName);
        in.put("COUNTY_NAME",countryName);
        in.put("ZIP_CODE",zipCode);
        in.put("STATE_OR_PROVINCE_CODE",stateOrProvinceCode);
        in.put("COUNTRY_CODE",countryCode);
        in.put("P_TRANS_ID",BaseService.generateTransIdSysHistory());
        
        try {
			SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_DDMMYYYY);
	        Date updated = formatter.parse(updatedDatetime);
	        in.put("UPDATED_DATETIME", updated);
	        
	        Date created = formatter.parse(createdDatetime);
	        in.put("CREATED_DATETIME",created);
	        
		} catch(Exception ex) {
			throw new Exception(" Date Format ");
		}
        
        
        
        in.put("DATA_MIGRATION",dataMigration);
        in.put("LBU_SET_ID",lbuSetId);
        
        
        try {
        	Map<String, Object> out = execute(in);
        	
        	return out;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap<String, Object>();
        }
    }
	
}
