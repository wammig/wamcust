package com.wam.stored.procedure;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class LbuAccountSelectByAssetStoredProcedure extends StoredProcedure{

	private static final String ACCOUNT_SELECT = "PKG_LBU_ACCOUNT.P_ACCOUNT_SELECT";
	
	public LbuAccountSelectByAssetStoredProcedure(DataSource ds){
        super(ds,ACCOUNT_SELECT);
        
        declareParameter(new SqlParameter("P_ASSET_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("CUSTOMERPGUID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map<String, Object> execute(String assetAgreementId)
	{
		Map<String, Object> in = new HashMap<String, Object>();
	       
        in.put("P_ASSET_AGREEMENT_ID",assetAgreementId);
        
        try {
        	Map<String, Object> outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap<String, Object>();
        }
	}
}
