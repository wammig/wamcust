package com.wam.stored.procedure;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * update the sysHistory when ecm returned
 * @author QINC1
 *
 */
public class SysHistoryUpdateMessageIdStoredProcedure extends StoredProcedure{
	
	private static final String SYS_HISTORY_UPDATE = "PKG_SYS_HISTORY.P_HISTORY_UPDATE_MESSAGE_ID";

	public SysHistoryUpdateMessageIdStoredProcedure(DataSource ds){
        super(ds,SYS_HISTORY_UPDATE);
        
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.VARCHAR));

        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map<?, ?> execute(String transId, String messageId) throws Exception{
		Map<String, String> in = new HashMap<String, String>();
		
		in.put("P_TRANS_ID", transId);
		in.put("P_MESSAGE_ID", messageId);
		
	 	try {
	 		Map<?, ?> outMap = execute(in);
	 		
	 		return outMap;

        } catch(Exception ex)
        {
        	return new HashMap();
        }
	 	
	}
}
