package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wam.service.BaseService;
import com.wam.utility.definition.Definition;

public class LbuContentSubCreateStoredProcedure extends StoredProcedure{
	
	private static final String CONTENTSUB_INSERT = "PKG_LBU_CONTENTSUB.P_CONTENTSUB_INSERT";
	
	public LbuContentSubCreateStoredProcedure(DataSource ds){
        super(ds,CONTENTSUB_INSERT);
        
        declareParameter(new SqlParameter("P_CONTENT_SUB_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("CONTRACT_NUM",Types.VARCHAR));
        declareParameter(new SqlParameter("CONTRACT_LINE_NUM1",Types.NUMERIC));
        declareParameter(new SqlParameter("CONTRACT_LINE_NUM2",Types.NUMERIC));
        declareParameter(new SqlParameter("CONTENT_SUB_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("LBU_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("LBU_CONTENT_SUB_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("BEGIN_DATE",Types.DATE));
        declareParameter(new SqlParameter("END_DATE",Types.DATE));
        declareParameter(new SqlParameter("AUDIT_USER_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("MAX_USERS",Types.NUMERIC));
        declareParameter(new SqlParameter("MONTHLY_COMMITMENT",Types.NUMERIC));
        declareParameter(new SqlParameter("MONTHLY_CAP",Types.NUMERIC));
        declareParameter(new SqlParameter("LBU_SOURCE_PACKAGE_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("LBU_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("LBU_LOCATION_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("ADDRESS_SEQ",Types.NUMERIC));
        declareParameter(new SqlParameter("AGREEMENT_PGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("ASSET_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("NO_RENEW",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("OUTPUT_LBU_CONTENT_SUB_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_MESSAGE",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map execute(
			 String contentSubId,
			 String contractNum,
			 Integer contractLineNum1,
			 Integer contractLineNum2,
			 String contentSubName,
			 String lbuAccountId,
			 String lbuContentSubId,
			 String beginDate,
			 String endDate,
			 String auditUserId,
			 Integer maxUsers,
			 Float monthlyCommitment,
			 Float monthlyCap,
			 String lbuSourcePackageId,
			 String updatedDatetime,
			 String createdDatetime,
			 String lbuSetId,
			 String lbuLocationId,
			 Integer addressSeq,
			 String agreementPguid,
			 String assetAgreementId,
			 String noRenew
			) throws Exception{
		
		Map in = new HashMap();
       
		in.put("P_CONTENT_SUB_ID", contentSubId);
        in.put("CONTRACT_NUM", contractNum);
        in.put("CONTRACT_LINE_NUM1", contractLineNum1);
        in.put("CONTRACT_LINE_NUM2", contractLineNum2);
        in.put("CONTENT_SUB_NAME", contentSubName);
        in.put("LBU_ACCOUNT_ID", lbuAccountId);
        in.put("LBU_CONTENT_SUB_ID", lbuContentSubId);
        in.put("AUDIT_USER_ID", auditUserId);
        in.put("MAX_USERS",maxUsers);
        in.put("MONTHLY_COMMITMENT", monthlyCommitment);
        in.put("MONTHLY_CAP", monthlyCap);
        in.put("LBU_SOURCE_PACKAGE_ID", lbuSourcePackageId);
        
        
        try {
			SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_YYYYMMDD);
			
			SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_YYYY_MM_DD);
			
			Date begin = formatter.parse(beginDate);
			in.put("BEGIN_DATE", formatter2.format(begin).toString());

			Date end = formatter.parse(endDate);
			in.put("END_DATE", formatter2.format(end).toString());
	        
		} catch(Exception ex) {
			throw new Exception(" Date Format ");
		}
		
        
        in.put("LBU_SET_ID", lbuSetId);
        in.put("LBU_LOCATION_ID",lbuLocationId);
        in.put("ADDRESS_SEQ", addressSeq);
        in.put("AGREEMENT_PGUID", agreementPguid);
        in.put("ASSET_AGREEMENT_ID", assetAgreementId);
        in.put("NO_RENEW", noRenew);
        in.put("P_TRANS_ID", BaseService.generateTransIdSysHistory());
        
        try {
        	Map out = execute(in);

        	return out;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
    }
	
	

}
