package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.helper.LbuContentSubs;

import oracle.jdbc.internal.OracleTypes;

public class LbuContentSubSearchStoredProcedure extends StoredProcedure{
	
	private static final String P_CONTENTSUB_SEARCH = "PKG_LBU_CONTENTSUB.P_CONTENTSUB_SEARCH";
	
	/**
	 * regexBeginDateTime is shared in many other classes
	 * don't change it
	 */
	public static String regexBeginDateTime = "(BEGIN_DATE){1}([^=]*[=])([^,]*[,])";
	
	/**
	 * regexDateTimeTrim is shared in many other classes
	 * don't change it
	 */
	public static String regexDateTimeTrim = "[0-9-//s]{8,}";
	
	
	/**
	 * regexEndDateTime is shared in many other classes
	 * don't change it
	 */
	public static String regexEndDateTime = "(END_DATE){1}([^=]*[=])([^,]*[,])";
	
	
	public LbuContentSubSearchStoredProcedure(DataSource ds){
        super(ds,P_CONTENTSUB_SEARCH);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTRACT_NUM",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTRACT_LINE_NUM",Types.VARCHAR));
        declareParameter(new SqlParameter("P_LBU_CONTENTSUB_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_GBS_CONTENTSUB_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PACKAGE_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CONTENT_SUB_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_SALES_REP_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_AUDIT_USER_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_BEGIN_DATE",Types.DATE));
        declareParameter(new SqlParameter("P_END_DATE",Types.DATE));
        declareParameter(new SqlParameter("P_SOURCE_PACKAGE_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ASSET_AGREEMENT_ID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("P_CONTENTSUB_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_MESSAGE",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map execute(
			 String setId,
			 String accountId,
			 String contractNum,
			 String contractLineNum,
			 String lbuContentSubId,
			 String gbsContentSubId,
			 String packageId,
			 String contentSubName,
			 String salesRepId,
			 String auditUserId,
			 String beginDate,
			 String endDate,
			 String sourcePackageId,
			 String assetAgreementId
			) throws Exception{

		Map in = new HashMap();
       
        in.put("P_SET_ID", setId);
        in.put("P_ACCOUNT_ID", accountId);
        in.put("P_CONTRACT_NUM", contractNum);
        in.put("P_CONTRACT_LINE_NUM", contractLineNum);
        in.put("P_LBU_CONTENTSUB_ID", lbuContentSubId);
        in.put("P_GBS_CONTENTSUB_ID", gbsContentSubId);
        in.put("P_PACKAGE_ID", packageId);
        in.put("P_CONTENT_SUB_NAME", contentSubName);
        in.put("P_SALES_REP_ID", salesRepId);
        in.put("P_AUDIT_USER_ID", auditUserId);
        in.put("P_BEGIN_DATE", beginDate);
        in.put("P_END_DATE", endDate);
        in.put("P_SOURCE_PACKAGE_ID", sourcePackageId);
        in.put("P_ASSET_AGREEMENT_ID", assetAgreementId);

        try {
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
    }
	
	public LbuContentSubs transformContentSubCursorToObject(Map<String, Object> map) throws Exception
	{
		LbuContentSubs lbuContentSubs = new LbuContentSubs();
		lbuContentSubs.setList(new ArrayList<LbuContentSub>());
		
		if(!map.containsKey("P_CONTENTSUB_CURSOR")) {
			return lbuContentSubs;
		}
		
		List  contentSubList = (ArrayList)map.get("P_CONTENTSUB_CURSOR");
		
		if(contentSubList.size() == 0) {
			return lbuContentSubs;
		}
		
		for(Object contentSubElement : contentSubList) {
			Map contentSub = (LinkedCaseInsensitiveMap)contentSubElement;
		
			LbuContentSub lbuContentSub = new LbuContentSub();
			
			String contentSubString = contentSub.toString();
			
			if(contentSub.containsKey("LBU_SET_ID"))
			{
				if(contentSub.get("LBU_SET_ID") != null)
				{
					lbuContentSub.setLbuSetId(contentSub.get("LBU_SET_ID").toString());
				}
			}
			
			if(contentSub.containsKey("LBU_CONTENT_SUB_ID"))
			{
				if(contentSub.get("LBU_CONTENT_SUB_ID") != null)
				{
					lbuContentSub.setLbuContentSubId(contentSub.get("LBU_CONTENT_SUB_ID").toString());
				}
			}
			
			if(contentSub.containsKey("CONTRACT_NUM"))
			{
				if(contentSub.get("CONTRACT_NUM") != null)
				{
					lbuContentSub.setContractNum(contentSub.get("CONTRACT_NUM").toString());
				}
			}
			
			if(contentSub.containsKey("CONTRACT_LINE_NUM"))
			{
				if(contentSub.get("CONTRACT_LINE_NUM") != null)
				{
					lbuContentSub.setContractLineNum(Integer.parseInt(contentSub.get("CONTRACT_LINE_NUM").toString()));
				}
			}
			
			if(contentSub.containsKey("CONTRACT_LINE_NUM_ORIGIN"))
			{
				if(contentSub.get("CONTRACT_LINE_NUM_ORIGIN") != null)
				{
					lbuContentSub.setContractLineNumOrigin(Integer.parseInt(contentSub.get("CONTRACT_LINE_NUM_ORIGIN").toString()));
				}
			}
			
			if(contentSub.containsKey("CONTENT_SUB_NAME"))
			{
				if(contentSub.get("CONTENT_SUB_NAME") != null)
				{
					lbuContentSub.setContentSubName(contentSub.get("CONTENT_SUB_NAME").toString());
				}
			}
			
			if(contentSub.containsKey("LBU_ACCOUNT_ID"))
			{
				if(contentSub.get("LBU_ACCOUNT_ID") != null)
				{
					lbuContentSub.setLbuAccountId(contentSub.get("LBU_ACCOUNT_ID").toString());
				}
			}
			
			if(contentSub.containsKey("LBU_LOCATION_ID"))
			{
				if(contentSub.get("LBU_LOCATION_ID") != null)
				{
					lbuContentSub.setLbuLocationId(contentSub.get("LBU_LOCATION_ID").toString());
				}
			}
			
		
			
			/**
			 * extract the begin date
			 */
			Pattern beginDateTimePattern = Pattern.compile(LbuContentSubSearchStoredProcedure.regexBeginDateTime);
			Matcher beginDateTimeMatcher = beginDateTimePattern.matcher(contentSubString);
			
			if(beginDateTimeMatcher.find())
			{ 
				Pattern beginDateTimeTrimPattern = Pattern.compile(LbuContentSubSearchStoredProcedure.regexDateTimeTrim);
				Matcher beginDateTimeTrimMatcher = beginDateTimeTrimPattern.matcher(beginDateTimeMatcher.group(0).toString());
				
				if(beginDateTimeTrimMatcher.find()) 
				{
					String beginDateTime = beginDateTimeTrimMatcher.group(0).toString();
					lbuContentSub.setBeginDate(beginDateTime);
				}
			}
			
			/**
			 * extract the end date
			 */
			Pattern endDateTimePattern = Pattern.compile(LbuContentSubSearchStoredProcedure.regexEndDateTime);
			Matcher endDateTimeMatcher = endDateTimePattern.matcher(contentSubString);
			
			if(endDateTimeMatcher.find())
			{ 
				Pattern endDateTimeTrimPattern = Pattern.compile(LbuContentSubSearchStoredProcedure.regexDateTimeTrim);
				Matcher endDateTimeTrimMatcher = endDateTimeTrimPattern.matcher(endDateTimeMatcher.group(0).toString());
				
				if(endDateTimeTrimMatcher.find()) 
				{
					String endDateTime = endDateTimeTrimMatcher.group(0).toString();
					lbuContentSub.setEndDate(endDateTime);
				}
			}
			
			if(contentSub.containsKey("AUDIT_USER_ID"))
			{
				if(contentSub.get("AUDIT_USER_ID") != null)
				{
					lbuContentSub.setAuditUserId(contentSub.get("AUDIT_USER_ID").toString());
				}
			}
			
			
			if(contentSub.containsKey("MAX_USERS"))
			{
				if(contentSub.get("MAX_USERS") != null)
				{
					lbuContentSub.setMaxUsers(Integer.parseInt(contentSub.get("MAX_USERS").toString()));
				}
			}
			
			
			if(contentSub.containsKey("MONTHLY_COMMITMENT"))
			{
				if(contentSub.get("MONTHLY_COMMITMENT") != null)
				{
					lbuContentSub.setMonthlyCommitment(Float.parseFloat(contentSub.get("MONTHLY_COMMITMENT").toString()));
				}
			}
			
			if(contentSub.containsKey("MONTHLY_CAP"))
			{
				if(contentSub.get("MONTHLY_CAP") != null)
				{
					lbuContentSub.setMonthlyCap(Float.parseFloat(contentSub.get("MONTHLY_CAP").toString()));
				}
			}
			
			if(contentSub.containsKey("LBU_SOURCE_PACKAGE_ID"))
			{
				if(contentSub.get("LBU_SOURCE_PACKAGE_ID") != null)
				{
					lbuContentSub.setLbuSourcePackageId(contentSub.get("LBU_SOURCE_PACKAGE_ID").toString());
				}
			}
			
			
			/**
			 * extract the created date
			 */
			Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
			Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(contentSubString);
			
			if(createdDateTimeMatcher.find())
			{ 
				Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
				
				if(createdDateTimeTrimMatcher.find()) 
				{
					String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
					lbuContentSub.setCreatedDatetime(createdDateTime);
				}
			}
			
			/**
			 * extract the updated date
			 */
			Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
			Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(contentSubString);
			
			if(updatedDateTimeMatcher.find())
			{ 
				Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
				
				if(updateDateTimeTrimMatcher.find()) 
				{
					String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
					lbuContentSub.setUpdatedDatetime(updateDateTime);
				}
			}
			
			if(contentSub.containsKey("ADDRESS_SEQ"))
			{
				if(contentSub.get("ADDRESS_SEQ") != null)
				{
					lbuContentSub.setAddressSeq(Integer.parseInt(contentSub.get("ADDRESS_SEQ").toString()));
				}
			}
			
			if(contentSub.containsKey("NO_RENEW"))
			{
				if(contentSub.get("NO_RENEW") != null)
				{
					lbuContentSub.setNoReNew(contentSub.get("NO_RENEW").toString());
				}
			}
			
			if(contentSub.containsKey("ASSET_AGREEMENT_ID"))
			{
				if(contentSub.get("ASSET_AGREEMENT_ID") != null)
				{
					lbuContentSub.setAssetAgreementId(contentSub.get("ASSET_AGREEMENT_ID").toString());
				}
			}
			
			if(contentSub.containsKey("AGREEMENT_PGUID"))
			{
				if(contentSub.get("AGREEMENT_PGUID") != null)
				{
					lbuContentSub.setAgreementPguid(contentSub.get("AGREEMENT_PGUID").toString());
				}
			}
			
			lbuContentSubs.getList().add(lbuContentSub);
		}
		

		return lbuContentSubs;
	}
	
	

}
