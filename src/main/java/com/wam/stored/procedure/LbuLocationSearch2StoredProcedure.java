package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.out;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuLocationAdmin;
import com.wam.model.db.gbsd.helper.LbuLocationAdmins;
import com.wam.model.db.gbsd.helper.LbuLocations;

import oracle.jdbc.OracleTypes;

public class LbuLocationSearch2StoredProcedure extends StoredProcedure{

	private static final String LOCATION_SEARCH = "PKG_LBU_LOCATION.P_LOCATION_SEARCH2";
	
	public LbuLocationSearch2StoredProcedure(DataSource ds){
        super(ds,LOCATION_SEARCH);
        
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_LBULOCATION_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_NAME1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CITY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ZIP_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COUNTRY_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE2",Types.VARCHAR));
        declareParameter(new SqlParameter("P_FROM_NUMBER",Types.NUMERIC));
        declareParameter(new SqlParameter("P_NUMBER_PER_PAGE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL_FILTERED",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_LOCATION_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}
	
	public Map execute( 
			String lbuAccountId,
			String lbuLocationId,
			String accountName1,
			String cityName,
			String zipCode,
			String countryCode,
			String addressLine1,
			String addressLine2,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception{
		
		Map in = new HashMap();
		try {
			in.put("P_ACCOUNT_ID", lbuAccountId);
			in.put("P_LBULOCATION_ID", lbuLocationId);
			in.put("P_ACCOUNT_NAME1", accountName1);
			in.put("P_CITY_NAME", cityName);
			in.put("P_ZIP_CODE", zipCode);
			in.put("P_COUNTRY_CODE", countryCode);
			in.put("P_ADDRESS_LINE1", addressLine1);
			in.put("P_ADDRESS_LINE2", addressLine2);
			in.put("P_FROM_NUMBER", fromNumber);
			in.put("P_NUMBER_PER_PAGE", numberPerPage);
			
			
			Map out = execute(in);
	    	
	    	return out;
	    } catch(Exception ex)
	    {
	    	System.out.println(ex.toString());
	    	System.out.println(ex.getMessage());
	    	System.out.println(in);
	    	
	    	return new HashMap();
	    }
	}
	
	public AdminDataTables transformLocationCursorToObject(Map<String, Object> map) throws Exception
	{
		AdminDataTables adminLbuLocations = new AdminDataTables(); 
		LbuLocationAdmins lbuLocationAdmins = new LbuLocationAdmins();
		lbuLocationAdmins.setList(new ArrayList<LbuLocationAdmin>());
		

		List  locationList = (ArrayList)map.get("P_LOCATION_CURSOR");
		
		Integer total = Integer.parseInt(map.get("P_TOTAL").toString());
		Integer filteredTotal = Integer.parseInt(map.get("P_TOTAL_FILTERED").toString());
		
		
		for(Object location : locationList) {
			Map mapLocation = (LinkedCaseInsensitiveMap)location;
			
			LbuLocationAdmin lbuLocation = new LbuLocationAdmin();
			
			String locationString = lbuLocation.toString();
			if(mapLocation.containsKey("LBU_SET_ID")) 
			{
				if(mapLocation.get("LBU_SET_ID") != null)
				{
					lbuLocation.setLbuSetId(mapLocation.get("LBU_SET_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("LBU_ACCOUNT_ID")) 
			{
				if(mapLocation.get("LBU_ACCOUNT_ID") != null)
				{
					lbuLocation.setLbuAccountId(mapLocation.get("LBU_ACCOUNT_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("LBU_LOCATION_ID")) 
			{
				if(mapLocation.get("LBU_LOCATION_ID") != null)
				{
					lbuLocation.setLbuLocationId(mapLocation.get("LBU_LOCATION_ID").toString());
				}
			}
			
			
			if(mapLocation.containsKey("LBU_ADDRESS_ID")) 
			{
				if(mapLocation.get("LBU_ADDRESS_ID") != null)
				{
					lbuLocation.setLbuAddressId(mapLocation.get("LBU_ADDRESS_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("ADDRESS_SEQ")) 
			{
				if(mapLocation.get("ADDRESS_SEQ") != null)
				{
					lbuLocation.setAddressSeq(Integer.parseInt(mapLocation.get("ADDRESS_SEQ").toString()));
				}
			}
			
			if(mapLocation.containsKey("AUDIT_USER_ID")) 
			{
				if(mapLocation.get("AUDIT_USER_ID") != null)
				{
					lbuLocation.setAuditUserId(mapLocation.get("AUDIT_USER_ID").toString());
				}
			}
			
			
			if(mapLocation.containsKey("ADDRESS_LINE1")) 
			{
				if(mapLocation.get("ADDRESS_LINE1") != null)
				{
					lbuLocation.setAddressLine1(mapLocation.get("ADDRESS_LINE1").toString());
				}
			}
			
			if(mapLocation.containsKey("ADDRESS_LINE2")) 
			{
				if(mapLocation.get("ADDRESS_LINE2") != null)
				{
					lbuLocation.setAddressLine2(mapLocation.get("ADDRESS_LINE2").toString());
				}
			}
			
			if(mapLocation.containsKey("CITY_NAME")) 
			{
				if(mapLocation.get("CITY_NAME") != null)
				{
					lbuLocation.setCityName(mapLocation.get("CITY_NAME").toString());
				}
			}
			
			if(mapLocation.containsKey("COUNTY_NAME")) 
			{
				if(mapLocation.get("COUNTY_NAME") != null)
				{
					lbuLocation.setCountryName(mapLocation.get("COUNTY_NAME").toString());
				}
			}
			
			if(mapLocation.containsKey("ZIP_CODE")) 
			{
				if(mapLocation.get("ZIP_CODE") != null)
				{
					lbuLocation.setZipCode(mapLocation.get("ZIP_CODE").toString());
				}
			}
			
			if(mapLocation.containsKey("STATE_OR_PROVINCE_CODE")) 
			{
				if(mapLocation.get("STATE_OR_PROVINCE_CODE") != null)
				{
					lbuLocation.setStateOrProvinceCode(mapLocation.get("STATE_OR_PROVINCE_CODE").toString());
				}
			}
			
			if(mapLocation.containsKey("COUNTRY_CODE")) 
			{
				if(mapLocation.get("COUNTRY_CODE") != null)
				{
					lbuLocation.setCountryCode(mapLocation.get("COUNTRY_CODE").toString());
				}
			}
			
			
			if(mapLocation.containsKey("ADDRESSPGUID")) 
			{
				if(mapLocation.get("ADDRESSPGUID") != null)
				{
					lbuLocation.setAddressPGUID(mapLocation.get("ADDRESSPGUID").toString());
				}
			}
			
			if(mapLocation.containsKey("ACCOUNT_NAME1")) 
			{
				if(mapLocation.get("ACCOUNT_NAME1") != null)
				{
					lbuLocation.setAccountName1(mapLocation.get("ACCOUNT_NAME1").toString());
				}
			}
			
			if(mapLocation.containsKey("POBPGUID")) 
			{
				if(mapLocation.get("POBPGUID") != null)
				{
					lbuLocation.setPobPGUID(mapLocation.get("POBPGUID").toString());
				}
			}
			
			
			Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
			Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(locationString);
			
			if(createdDateTimeMatcher.find())
			{ 
				Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
				
				if(createdDateTimeTrimMatcher.find()) 
				{
					String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
					lbuLocation.setCreatedDatetime(createdDateTime);
				}
			}
			

			Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
			Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(locationString);
			
			if(updatedDateTimeMatcher.find())
			{ 
				Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
				
				if(updateDateTimeTrimMatcher.find()) 
				{
					String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
					lbuLocation.setUpdatedDatetime(updateDateTime);
				}
			}
			
			lbuLocationAdmins.getList().add(lbuLocation);
		}
		
		adminLbuLocations.setData(lbuLocationAdmins.getList());
		adminLbuLocations.setRecordsTotal(total);
		adminLbuLocations.setRecordsFiltered(filteredTotal);
		
		return adminLbuLocations;
		
	}

}
