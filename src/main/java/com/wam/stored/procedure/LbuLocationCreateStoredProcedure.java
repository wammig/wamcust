package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wam.service.BaseService;

public class LbuLocationCreateStoredProcedure extends StoredProcedure{
	
	private static final String LOCATION_CREATE = "PKG_LBU_LOCATION.P_LOCATION_CREATE";

	public LbuLocationCreateStoredProcedure(DataSource ds){
        super(ds,LOCATION_CREATE);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_SEQ",Types.NUMERIC));
        declareParameter(new SqlParameter("P_SALES_REP_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_EXT_ORDER_NO",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE1",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESS_LINE2",Types.VARCHAR));
        declareParameter(new SqlParameter("P_CITY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COUNTY_NAME",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ZIP_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_STATE_OR_PROVINCE_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COUNTRY_CODE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADMIN_USER",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ADDRESSPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_POBPGUID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        
        declareParameter(new SqlOutParameter("O_HISTORY_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_LOCATION_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map execute(
						String lbuAccountId, 
						String lbuLocationId, 
						String gbsLocationId,
						String glpLocationId,
						String lbuAddressId,
						String gbsAddressId,
						Integer addressSeq,
						String addressLine1,
						String addressLine2,
						String cityName,
						String auditUserId,
						String salesRepId,
						String extOrderNo,
						String countryName, 
						String zipCode, 
						String stateOrProvinceCode,
						String countryCode, 
						String updatedDatetime, 
						String createdDatetime, 
						String dataMigration, 
						String lbuSetId, 
						String addressPguid,
						String pobpguid
						) throws Exception{
		
		Map in = new HashMap();
		
		/**
		 * the commented parameters were used in P_LOCATION_INSERT
		 * they are not used anymore
		 */
		
		in.put("P_SET_ID",lbuSetId);
		in.put("P_ACCOUNT_ID",lbuAccountId);
		in.put("P_ADDRESS_ID",lbuAddressId);
		in.put("P_ADDRESS_SEQ",addressSeq);
		in.put("P_SALES_REP_ID",salesRepId);
		in.put("P_EXT_ORDER_NO",extOrderNo);
		in.put("P_ADDRESS_LINE1",addressLine1);
		in.put("P_ADDRESS_LINE2",addressLine2);
		in.put("P_CITY_NAME",cityName);
		in.put("P_COUNTY_NAME",countryName);
		in.put("P_ZIP_CODE",zipCode);
		in.put("P_STATE_OR_PROVINCE_CODE",stateOrProvinceCode);
		in.put("P_COUNTRY_CODE",countryCode);
		in.put("P_ADMIN_USER",auditUserId);
		in.put("P_ADDRESSPGUID",addressPguid);
		in.put("P_POBPGUID",pobpguid);
		in.put("P_TRANS_ID",BaseService.generateTransIdSysHistory());
		
        try {
        	Map out = execute(in);
        	
        	return out;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
    }
	
}