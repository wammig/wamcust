package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class LbuContentSubUpdateAgreementIdStoredProcedure extends StoredProcedure{
	
	private static final String CONTENTSUB_UPDATE = "PKG_LBU_CONTENTSUB.P_CONTENTSUB_UPDATE";
	
	public LbuContentSubUpdateAgreementIdStoredProcedure(DataSource ds){
        super(ds,CONTENTSUB_UPDATE);
        
        declareParameter(new SqlParameter("P_LBU_CONTENT_SUB_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_AGREEMENT_PGUID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_MESSAGE",Types.VARCHAR));
        
        //setFunction(false);//you must set this as it distinguishes it from a sproc
        compile();
    }
	
	public Map execute(String contentSubId, String agreementPguId) throws Exception{
		Map in = new HashMap();
       
        in.put("P_LBU_CONTENT_SUB_ID", contentSubId);
        in.put("P_AGREEMENT_PGUID", agreementPguId);         
        
        try {
        	return execute(in);

        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	System.out.println(ex.getMessage());
        	System.out.println(in);
        	
        	return new HashMap();
        }
    }

}
