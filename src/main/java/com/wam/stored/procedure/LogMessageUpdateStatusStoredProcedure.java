package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.db.gbsd.helper.LogMessages;

import oracle.jdbc.OracleTypes;

import static java.lang.System.out;

public class LogMessageUpdateStatusStoredProcedure extends StoredProcedure{

	private static final String P_MESSAGE_UPDATE_STATUS = "PKG_LOG_MESSAGE.P_MESSAGE_UPDATE_STATUS";
	
	public LogMessageUpdateStatusStoredProcedure(DataSource ds){
        super(ds,P_MESSAGE_UPDATE_STATUS);
        
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_ACTIONID",Types.NUMERIC));
        declareParameter(new SqlParameter("P_STATUS",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(Integer messageId, String status, Integer actionId) throws Exception{

			Map in = new HashMap();
		
			in.put("P_MESSAGE_ID", messageId);
			in.put("P_STATUS", status);
			in.put("P_ACTIONID", actionId);
			
		 	try {
	        	Map outMap = execute(in);
	        	return outMap;
	        } catch(Exception ex)
	        {
	        	System.out.println(ex.toString());
	        	
	        	return new HashMap();
	        }
	}
	
}
