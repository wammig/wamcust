package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.db.gbsd.helper.LogMessages;

import oracle.jdbc.OracleTypes;

import static java.lang.System.out;

public class LogMessageSearchStoredProcedure extends StoredProcedure{

	private static final String P_MESSAGE_SEARCH = "PKG_LOG_MESSAGE.P_MESSAGE_SEARCH";
	
	public LogMessageSearchStoredProcedure(DataSource ds){
        super(ds,P_MESSAGE_SEARCH);
        
        declareParameter(new SqlParameter("P_ENTITY_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PROCESS_TYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_PROCESS_SUBTYPE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACTIONID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_STATUS",Types.VARCHAR));
        declareParameter(new SqlParameter("P_BATCH_FROM",Types.NUMERIC));
        declareParameter(new SqlParameter("P_BATCH_MAX",Types.NUMERIC));
        declareParameter(new SqlOutParameter("P_MESSAGE_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(String entityId, String processType, String processSubType, String actionId, String status, Integer batchFrom, Integer batchMax) throws Exception{

			Map in = new HashMap();
		
			in.put("P_ENTITY_ID", entityId);
			in.put("P_PROCESS_TYPE", processType);
			in.put("P_PROCESS_SUBTYPE", processSubType);
			in.put("P_ACTIONID", actionId);
			in.put("P_STATUS", status);
			in.put("P_BATCH_FROM", batchFrom);
			in.put("P_BATCH_MAX", batchMax);
			
		 	try {
	        	Map outMap = execute(in);
	        	
	        	return outMap;
	        } catch(Exception ex)
	        {
	        	System.out.println(ex.toString());
	        	
	        	return new HashMap();
	        }
	}
	
	
	public LogMessages transformLogMessageCursorToObject(Map<String, Object> map) throws Exception
	{
		LogMessages logMessages= new LogMessages();
		
		if(!map.containsKey("P_MESSAGE_CURSOR")) {
			return null;
		}
		
		List  messageList = (ArrayList)map.get("P_MESSAGE_CURSOR");
		
		for(Object message : messageList)
		{
			Map mapMessage = (LinkedCaseInsensitiveMap)message;
			
			LogMessage logMessage = new LogMessage();
			
			if(mapMessage.containsKey("MESSAGEID"))
			{
				if(mapMessage.get("MESSAGEID") != null)
				{
					logMessage.setMessageId(Integer.parseInt(mapMessage.get("MESSAGEID").toString()));
				}
			}
			
			if(mapMessage.containsKey("STATUS"))
			{
				if(mapMessage.get("STATUS") != null)
				{
					logMessage.setStatus(mapMessage.get("STATUS").toString());
				}
			}
			
			if(mapMessage.containsKey("TRANSID"))
			{
				if(mapMessage.get("TRANSID") != null)
				{
					logMessage.setTransId(mapMessage.get("TRANSID").toString());
				}
			}
			
			if(mapMessage.containsKey("GROUPID"))
			{
				if(mapMessage.get("GROUPID") != null)
				{
					logMessage.setGroupId(mapMessage.get("GROUPID").toString());
				}
			}
			
			if(mapMessage.containsKey("MESSAGE"))
			{
				if(mapMessage.get("MESSAGE") != null)
				{
					logMessage.setMessage(mapMessage.get("MESSAGE").toString());
				}
			}
			
			if(mapMessage.containsKey("APPLICATION"))
			{
				if(mapMessage.get("APPLICATION") != null)
				{
					logMessage.setApplication(mapMessage.get("APPLICATION").toString());
				}
			}
			
			if(mapMessage.containsKey("ACTIONID"))
			{
				if(mapMessage.get("ACTIONID") != null)
				{
					logMessage.setActionId(Integer.parseInt(mapMessage.get("ACTIONID").toString()));
				}
			}
			
			if(mapMessage.containsKey("PROCESS_TYPE"))
			{
				if(mapMessage.get("PROCESS_TYPE") != null)
				{
					logMessage.setProcessType(mapMessage.get("PROCESS_TYPE").toString());
				}
			}
			
			if(mapMessage.containsKey("PROCESS_SUBTYPE"))
			{
				if(mapMessage.get("PROCESS_SUBTYPE") != null)
				{
					logMessage.setProcessSubType(mapMessage.get("PROCESS_SUBTYPE").toString());
				}
			}
			
			if(mapMessage.containsKey("CREATED_BY"))
			{
				if(mapMessage.get("CREATED_BY") != null)
				{
					logMessage.setCreatedBy(mapMessage.get("CREATED_BY").toString());
				}
			}
			
			if(mapMessage.containsKey("UPDATED_BY"))
			{
				if(mapMessage.get("UPDATED_BY") != null)
				{
					logMessage.setUpdatedBy(mapMessage.get("UPDATED_BY").toString());
				}
			}
			
			if(mapMessage.containsKey("CREATED_DATETIME"))
			{
				if(mapMessage.get("CREATED_DATETIME") != null)
				{
					logMessage.setCreatedDatetime(mapMessage.get("CREATED_DATETIME").toString());
				}
			}
			
			if(mapMessage.containsKey("UPDATED_DATETIME"))
			{
				if(mapMessage.get("UPDATED_DATETIME") != null)
				{
					logMessage.setUpdatedDatetime(mapMessage.get("UPDATED_DATETIME").toString());
				}
			}
			
			logMessages.getList().add(logMessage);
		}
		
		return logMessages;
	}
	
}
