package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuLocations;

import oracle.jdbc.OracleTypes;

import static java.lang.System.out;

public class LbuLocationSelectByIdStoredProcedure extends StoredProcedure{

	private static final String LOCATION_SELECT = "PKG_LBU_LOCATION.P_LOCATION_SELECT";
	
	public LbuLocationSelectByIdStoredProcedure(DataSource ds){
        super(ds,LOCATION_SELECT);
        
        declareParameter(new SqlParameter("P_SET_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ACCOUNT_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_LOCATION_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_LOCATION_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}
	
	public Map execute(
			String lbuSetId, 
			String lbuAccountId,
			String lbuLocationId
			) throws Exception{
		
		Map in = new HashMap();
		try {
			in.put("P_SET_ID", lbuSetId);
			in.put("P_ACCOUNT_ID", lbuAccountId);
			in.put("P_LOCATION_ID", lbuLocationId);
			Map outMap = execute(in);
	    	
	    	return outMap;
	    } catch(Exception ex)
	    {
	    	System.out.println(ex.toString());
	    	System.out.println(in);
	    	return new HashMap();
	    }	
	}
	
	public LbuLocation transformLocationCursorToObject(Map<String, Object> map) throws Exception
	{
		LbuLocation lbuLocation = new LbuLocation();
		
		if(!map.containsKey("P_LOCATION_CURSOR")) {
			return null;
		}
		
		List  locationList = (ArrayList)map.get("P_LOCATION_CURSOR");
		
		if(locationList.size() == 0) {
			return null;
		}

			Map mapLocation = (LinkedCaseInsensitiveMap)locationList.get(0);
			
			String locationString = lbuLocation.toString();
			if(mapLocation.containsKey("LBU_SET_ID")) 
			{
				if(mapLocation.get("LBU_SET_ID") != null)
				{
					lbuLocation.setLbuSetId(mapLocation.get("LBU_SET_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("LBU_ACCOUNT_ID")) 
			{
				if(mapLocation.get("LBU_ACCOUNT_ID") != null)
				{
					lbuLocation.setLbuAccountId(mapLocation.get("LBU_ACCOUNT_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("LBU_LOCATION_ID")) 
			{
				if(mapLocation.get("LBU_LOCATION_ID") != null)
				{
					lbuLocation.setLbuLocationId(mapLocation.get("LBU_LOCATION_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("GBS_LOCATION_ID")) 
			{
				if(mapLocation.get("GBS_LOCATION_ID") != null)
				{
					lbuLocation.setGbsLocationId(mapLocation.get("GBS_LOCATION_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("GLP_LOCATION_ID")) 
			{
				if(mapLocation.get("GLP_LOCATION_ID") != null)
				{
					lbuLocation.setGlpLocationId(mapLocation.get("GLP_LOCATION_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("LBU_ADDRESS_ID")) 
			{
				if(mapLocation.get("LBU_ADDRESS_ID") != null)
				{
					lbuLocation.setLbuAddressId(mapLocation.get("LBU_ADDRESS_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("GBS_ADDRESS_ID")) 
			{
				if(mapLocation.get("GBS_ADDRESS_ID") != null)
				{
					lbuLocation.setGbsAddressId(mapLocation.get("GBS_ADDRESS_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("ADDRESS_SEQ")) 
			{
				if(mapLocation.get("ADDRESS_SEQ") != null)
				{
					lbuLocation.setAddressSeq(Integer.parseInt(mapLocation.get("ADDRESS_SEQ").toString()));
				}
			}
			
			if(mapLocation.containsKey("AUDIT_USER_ID")) 
			{
				if(mapLocation.get("AUDIT_USER_ID") != null)
				{
					lbuLocation.setAuditUserId(mapLocation.get("AUDIT_USER_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("SALES_REP_ID")) 
			{
				if(mapLocation.get("SALES_REP_ID") != null)
				{
					lbuLocation.setSalesRepId(mapLocation.get("SALES_REP_ID").toString());
				}
			}
			
			if(mapLocation.containsKey("EXT_ORDER_NO")) 
			{
				if(mapLocation.get("EXT_ORDER_NO") != null)
				{
					lbuLocation.setExtOrderNo(mapLocation.get("EXT_ORDER_NO").toString());
				}
			}
			
			if(mapLocation.containsKey("ADDRESS_LINE1")) 
			{
				if(mapLocation.get("ADDRESS_LINE1") != null)
				{
					lbuLocation.setAddressLine1(mapLocation.get("ADDRESS_LINE1").toString());
				}
			}
			
			if(mapLocation.containsKey("ADDRESS_LINE2")) 
			{
				if(mapLocation.get("ADDRESS_LINE2") != null)
				{
					lbuLocation.setAddressLine2(mapLocation.get("ADDRESS_LINE2").toString());
				}
			}
			
			if(mapLocation.containsKey("CITY_NAME")) 
			{
				if(mapLocation.get("CITY_NAME") != null)
				{
					lbuLocation.setCityName(mapLocation.get("CITY_NAME").toString());
				}
			}
			
			if(mapLocation.containsKey("COUNTY_NAME")) 
			{
				if(mapLocation.get("COUNTY_NAME") != null)
				{
					lbuLocation.setCountryName(mapLocation.get("COUNTY_NAME").toString());
				}
			}
			
			if(mapLocation.containsKey("ZIP_CODE")) 
			{
				if(mapLocation.get("ZIP_CODE") != null)
				{
					lbuLocation.setZipCode(mapLocation.get("ZIP_CODE").toString());
				}
			}
			
			if(mapLocation.containsKey("STATE_OR_PROVINCE_CODE")) 
			{
				if(mapLocation.get("STATE_OR_PROVINCE_CODE") != null)
				{
					lbuLocation.setStateOrProvinceCode(mapLocation.get("STATE_OR_PROVINCE_CODE").toString());
				}
			}
			
			if(mapLocation.containsKey("COUNTRY_CODE")) 
			{
				if(mapLocation.get("COUNTRY_CODE") != null)
				{
					lbuLocation.setCountryCode(mapLocation.get("COUNTRY_CODE").toString());
				}
			}
			
			if(mapLocation.containsKey("ADDRESSPGUID")) 
			{
				if(mapLocation.get("ADDRESSPGUID") != null)
				{
					lbuLocation.setAddressPGUID(mapLocation.get("ADDRESSPGUID").toString());
				}
			}
			
			if(mapLocation.containsKey("POBPGUID")) 
			{
				if(mapLocation.get("POBPGUID") != null)
				{
					lbuLocation.setPobPGUID(mapLocation.get("POBPGUID").toString());
				}
			}
			
			Pattern createdDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexCreatedDateTime);
			Matcher createdDateTimeMatcher = createdDateTimePattern.matcher(locationString);
			
			if(createdDateTimeMatcher.find())
			{ 
				Pattern createdDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher createdDateTimeTrimMatcher = createdDateTimeTrimPattern.matcher(createdDateTimeMatcher.group(0).toString());
				
				if(createdDateTimeTrimMatcher.find()) 
				{
					String createdDateTime = createdDateTimeTrimMatcher.group(0).toString();
					lbuLocation.setCreatedDatetime(createdDateTime);
				}
			}
			

			Pattern updatedDateTimePattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexUpdatedDateTime);
			Matcher updatedDateTimeMatcher = updatedDateTimePattern.matcher(locationString);
			
			if(updatedDateTimeMatcher.find())
			{ 
				Pattern updatedDateTimeTrimPattern = Pattern.compile(LbuAccountSelectByIdStoredProcedure.regexDateTimeTrim);
				Matcher updateDateTimeTrimMatcher = updatedDateTimeTrimPattern.matcher(updatedDateTimeMatcher.group(0).toString());
				
				if(updateDateTimeTrimMatcher.find()) 
				{
					String updateDateTime = updateDateTimeTrimMatcher.group(0).toString();
					lbuLocation.setUpdatedDatetime(updateDateTime);
				}
			}
			
			return lbuLocation;
	}
	
}
