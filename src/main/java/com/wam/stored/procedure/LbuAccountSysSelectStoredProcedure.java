package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.service.AccountWamService;
import com.wam.service.ContentSubWamService;
import com.wam.test.db.BaseTest;
import com.wam.test.db.TestLbuAccountSelectByIdStoredProcedure;

import oracle.jdbc.OracleTypes;

public class LbuAccountSysSelectStoredProcedure extends StoredProcedure{
	
	private static final String P_ACCOUNT_SYS_SELECT = "PKG_LBU_ACCOUNT.P_ACCOUNT_SYS_SELECT";
	
	public LbuAccountSysSelectStoredProcedure(DataSource ds){
        super(ds,P_ACCOUNT_SYS_SELECT);
        
        declareParameter(new SqlParameter("FROM_DATE",Types.VARCHAR));
        declareParameter(new SqlParameter("TO_DATE",Types.VARCHAR));
        declareParameter(new SqlParameter("P_COHORTE",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_ACCOUNT_SYS_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute(String fromDate, String toDate, String cohorte) throws Exception{
		Map in = new HashMap();
		try {
			in.put("P_COHORTE", cohorte);
			in.put("FROM_DATE", fromDate);
			in.put("TO_DATE", toDate);
			
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
		
	}
}
