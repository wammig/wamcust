package com.wam.stored.procedure;

import static java.lang.System.out;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;

import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;

import oracle.jdbc.internal.OracleTypes;


@Service("LbuAgreementUpdateCustomerPGUIDStoredProcedure")
public class LbuAgreementUpdateCustomerPGUIDStoredProcedure extends StoredProcedure{
	
	private static final String P_AGREEMENT_UPDATE_CID = "PKG_LBU_AGREEMENT.P_AGREEMENT_UPDATE_CID";
	
	public LbuAgreementUpdateCustomerPGUIDStoredProcedure(DataSource ds){
        super(ds,P_AGREEMENT_UPDATE_CID);
        
        declareParameter(new SqlParameter("P_ASSET_AGREEMENT_ID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("P_CUSTOMERPGUID",Types.VARCHAR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        
        compile();
	}

	
	public Map execute(
				String assetAgreementId
			) throws Exception{
		Map in = new HashMap();
		try {
			in.put("P_ASSET_AGREEMENT_ID", assetAgreementId);
			
        	Map outMap = execute(in);
        	
        	return outMap;
        } catch(Exception ex)
        {
        	System.out.println(ex.toString());
        	
        	return new HashMap();
        }
		
	}
	
}
