package com.wam.stored.procedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.model.db.gbsd.helper.SysHistories;
import com.wam.model.mvc.response.helper.ErrorObject;
import com.wam.utility.definition.Definition;

import oracle.jdbc.OracleTypes;

public class SysHistorySearch2StoredProcedure extends StoredProcedure{

	private static final String P_HISTORY_SEARCH_2 = "PKG_SYS_HISTORY.P_HISTORY_SEARCH_2";
	
	public SysHistorySearch2StoredProcedure(DataSource ds){
        super(ds,P_HISTORY_SEARCH_2);
        
        declareParameter(new SqlParameter("P_HISTORY_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_ENTITY_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_TRANS_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_MESSAGE_ID",Types.VARCHAR));
        declareParameter(new SqlParameter("P_FROM_NUMBER",Types.NUMERIC));
        declareParameter(new SqlParameter("P_NUMBER_PER_PAGE",Types.NUMERIC));
        
        declareParameter(new SqlOutParameter("P_TOTAL",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_TOTAL_FILTERED",OracleTypes.NUMERIC));
        declareParameter(new SqlOutParameter("P_HISTORY_CURSOR",OracleTypes.CURSOR));
        declareParameter(new SqlOutParameter("ERROR_CODE",Types.NUMERIC));
        declareParameter(new SqlOutParameter("ERROR_DESC",Types.VARCHAR));
        compile();
	}
	
	public Map execute(String historyId,  String entityId, 
			 String transId, String messageId, Integer fromNumber,Integer numberPerPage) throws Exception
	{
			Map in = new HashMap();
		
			in.put("P_HISTORY_ID", historyId);
			in.put("P_ENTITY_ID", entityId);
			in.put("P_TRANS_ID", transId);
			in.put("P_MESSAGE_ID", messageId);
			in.put("P_FROM_NUMBER", fromNumber);
			in.put("P_NUMBER_PER_PAGE", numberPerPage);

		 	try {
	        	Map outMap = execute(in);
	        	
	        	return outMap;
	        } 
		 	catch(Exception ex)
	        {
	        	System.out.println(ex.toString());
	        	
	        	return new HashMap();
	        }
	}
	
	
	public AdminDataTables transformHistoryCursorToObject(Map<String, Object> map) throws Exception
	{
		AdminDataTables adminSysHistories = new AdminDataTables(); 
		SysHistories sysHistories = new SysHistories();
		sysHistories.setList(new ArrayList<SysHistory>());
		
		if(!map.containsKey("P_HISTORY_CURSOR")) {
			return null;
		}
		
		List  sysHistoryList = (ArrayList)map.get("P_HISTORY_CURSOR");
		
		Integer total = Integer.parseInt(map.get("P_TOTAL").toString());
		Integer filteredTotal = Integer.parseInt(map.get("P_TOTAL_FILTERED").toString());
		
		for(Object sysHistoryObject : sysHistoryList) 
		{
			Map mapSysHistory = (LinkedCaseInsensitiveMap)sysHistoryObject;
			
			SysHistory sysHistory = new SysHistory();
			if(mapSysHistory.containsKey("HISTORYID"))
			{
				if(mapSysHistory.get("HISTORYID") != null)
				{
					sysHistory.setHistoryId(Integer.parseInt(mapSysHistory.get("HISTORYID").toString()));
				}
			}
			
			if(mapSysHistory.containsKey("PROCESS_TYPE"))
			{
				if(mapSysHistory.get("PROCESS_TYPE") != null)
				{
					sysHistory.setProcessType(mapSysHistory.get("PROCESS_TYPE").toString());
				}
			}
			
			if(mapSysHistory.containsKey("PROCESS_SUBTYPE"))
			{
				if(mapSysHistory.get("PROCESS_SUBTYPE") != null)
				{
					sysHistory.setProcessSubType(mapSysHistory.get("PROCESS_SUBTYPE").toString());
				}
			}
			
			if(mapSysHistory.containsKey("ENTITYID"))
			{
				if(mapSysHistory.get("ENTITYID") != null)
				{
					sysHistory.setEntityId(mapSysHistory.get("ENTITYID").toString());
				}
			}
			
			if(mapSysHistory.containsKey("ENTITY_SEQ0"))
			{
				if(mapSysHistory.get("ENTITY_SEQ0") != null)
				{
					sysHistory.setEntitySeq0(mapSysHistory.get("ENTITY_SEQ0").toString());
				}
			}
			
			if(mapSysHistory.containsKey("ENTITY_SEQ1"))
			{
				if(mapSysHistory.get("ENTITY_SEQ1") != null)
				{
					sysHistory.setEntitySeq1(mapSysHistory.get("ENTITY_SEQ1").toString());
				}
			}
			
			if(mapSysHistory.containsKey("ENTITY_SEQ2"))
			{
				if(mapSysHistory.get("ENTITY_SEQ2") != null)
				{
					sysHistory.setEntitySeq2(mapSysHistory.get("ENTITY_SEQ2").toString());
				}
			}
			
			if(mapSysHistory.containsKey("SEND_STATUS"))
			{
				if(mapSysHistory.get("SEND_STATUS") != null)
				{
					sysHistory.setSendStatus(mapSysHistory.get("SEND_STATUS").toString());
				}
			}
			
			if(mapSysHistory.containsKey("REPLY_STATUS"))
			{
				if(mapSysHistory.get("REPLY_STATUS") != null)
				{
					sysHistory.setReplyStatus(mapSysHistory.get("REPLY_STATUS").toString());
				}
			}
			
			if(mapSysHistory.containsKey("ERROR_CODE"))
			{
				if(mapSysHistory.get("ERROR_CODE") != null)
				{
					sysHistory.setErrorCode(mapSysHistory.get("ERROR_CODE").toString());
				}
			}
			
			if(mapSysHistory.containsKey("ERROR_MESSAGE"))
			{
				if(mapSysHistory.get("ERROR_MESSAGE") != null)
				{
					sysHistory.setErrorMessage(mapSysHistory.get("ERROR_MESSAGE").toString());
				}
			}
			
			if(mapSysHistory.containsKey("TRANSID"))
			{
				if(mapSysHistory.get("TRANSID") != null)
				{
					sysHistory.setTransId(mapSysHistory.get("TRANSID").toString());
				}
			}
			
			if(mapSysHistory.containsKey("CREATED_BY"))
			{
				if(mapSysHistory.get("CREATED_BY") != null)
				{
					sysHistory.setCreatedBy(mapSysHistory.get("CREATED_BY").toString());
				}
			}
			
			if(mapSysHistory.containsKey("CREATED_DATETIME"))
			{
				if(mapSysHistory.get("CREATED_DATETIME") != null)
				{
					sysHistory.setCreatedDateTime(mapSysHistory.get("CREATED_DATETIME").toString());
				}
			}
			
			if(mapSysHistory.containsKey("UPDATED_BY"))
			{
				if(mapSysHistory.get("UPDATED_BY") != null)
				{
					sysHistory.setUpdatedBy(mapSysHistory.get("UPDATED_BY").toString());
				}
			}
			
			if(mapSysHistory.containsKey("UPDATED_DATETIME"))
			{
				if(mapSysHistory.get("UPDATED_DATETIME") != null)
				{
					sysHistory.setUpdatedDatetime(mapSysHistory.get("UPDATED_DATETIME").toString());
				}
			}
			
			if(mapSysHistory.containsKey("MESSAGEID"))
			{
				if(mapSysHistory.get("MESSAGEID") != null)
				{
					sysHistory.setMessageId(Integer.parseInt(mapSysHistory.get("MESSAGEID").toString()));
				}
			}
			
			sysHistories.getList().add(sysHistory);
		}
		
	
		adminSysHistories.setData(sysHistories.getList());
		adminSysHistories.setRecordsTotal(total);
		adminSysHistories.setRecordsFiltered(filteredTotal);
		
		return adminSysHistories;
	}
	
}
