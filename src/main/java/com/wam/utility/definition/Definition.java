package com.wam.utility.definition;

public class Definition {
	
	public static final String SPACE = " ";
	public static final String EMPTY = "";
	public static final String XML = "XML";
	public static final String JSON = "JSON";
	public static final String OBJECT = "OBJECT";
	public static final String MESSAGE = "MESSAGE";
	public static final String STATUS = "STATUS";
	public static final String DATA_SOURCE = "DATA_SOURCE";
	public static final String DATA_SOURCE2 = "DATA_SOURCE2";
	public static final String PARAMS = "PARAMS";
	public static final String RESPONSE = "RESPONSE";
	public static final String ACTION = "ACTION";
	public static final String RESPONSE_CODE = "RESPONSE_CODE";
	public static final String ERROR_CODE = "ERROR_CODE";
	public static final String REPLY_STATUS = "REPLY_STATUS";
	public static final String TRANS_ID_SYSHISTORY = "TRANS_ID_SYSHISTORY";
	public static final String ACCOUNT = "ACCOUNT";
	public static final String LBU_ACCOUNT = "LBU_ACCOUNT";
	public static final String LOCATION = "LOCATION";
	public static final String LBU_LOCATION = "LBU_LOCATION";
	public static final String CONTENTSUB = "CONTENTSUB";
	public static final String AGREEMENT = "AGREEMENT";
	public static final String AGREEMENTS = "AGREEMENTS";
	public static final String FEATURE = "FEATURE";
	public static final String PRODUCT = "PRODUCT";
	public static final String CREATE = "CREATE";
	public static final String UPDATE = "UPDATE";
	public static final String LANG_FR = "FR";
	public static final String SUCCESS = "SUCCESS";
	public static final String FAILURE = "FAILURE";
	public static final String PLSQL_ACCOUNT = "PLSQL_ACCOUNT";
	public static final String PLSQL_LOCATION = "PLSQL_LOCATION";
	public static final String PLSQL_CONTENTSUB = "PLSQL_CONTENTSUB";
	public static final String PLSQL_AGREEMENT = "PLSQL_AGREEMENT";
	public static final String ECM = "ECM";
	public static final String SYNC_ORDER_REPLY = "SYNC_ORDER_REPLY";
	public static final String IS_REAL_SEND_AGREEMENT = "IS_REAL_SEND_AGREEMENT";
	public static final String RETURNED_MAP_FROM_ECM = "RETURNED_MAP_FROM_ECM";
	public static final String ORACLE_SP_CODE_10010 = "10010";
	public static final String WORKFLOW_MANAGER_SQL_CODE = "1001";
	public static final String WORKFLOW_MANAGER_ECM_CODE = "1002";
	public static final String WORKFLOW_MANAGER_FINISHED_CODE = "9999";
	public static final String WORKFLOW_MANAGER = "WORKFLOW_MANAGER";
	public static final String WORKFLOW_MANAGER_STATUS_BEGINNING = "B";
	public static final String WORKFLOW_MANAGER_STATUS_COMPLETE = "C";
	public static final String WORKFLOW_MANAGER_STATUS_DOING = "R";
	public static final String WORKFLOW_MANAGER_STATUS_ERROR = "E";
	public static final String SUBJECT = "SUBJECT";
	public static final String CONTENT = "CONTENT";
	public static final String ASSET_AGREEMENT_ID = "ASSET_AGREEMENT_ID";
	public static final String GENERATED_TRANS_ID = "GENERATED_TRANS_ID";
	public static final String GENERATED_TRANS_ID_PRODUCT = "GENERATED_TRANS_ID_PRODUCT";
	public static final String GENERATED_TRANS_ID_FEATURE = "GENERATED_TRANS_ID_FEATURE";
	public static final String OUT_AGREEMENT_ID = "OUT_AGREEMENT_ID";
	public static final String ENDDATE_STRING = "12/31/2099 23:59:59";
	public static final Integer LICENSE_COUNT = 999;
	public static final String DATEFORMAT_YYYYMMDD = "yyyyMMdd";
	public static final String DATEFORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String DATEFORMAT_MMDDYYYYHHMMSS =  "MM/dd/yyyy HH:mm:ss";
	public static final String DATEFORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String DATEFORMAT_DDMMYYYY = "dd/MM/yyyy";
	public static final String DATEFORMAT_MM_DD_YYYY = "MM-dd-yyyy";
	public static final String ET_COMMERCIAL_REPLACEMENT = "---et-commercial-and-commercial---";
	public static final String ET_COMMERCIAL = "&";
	public static final String E2E = "e2e";
	public static final String CERT = "cert";
	public static final String PS = "PS";
	public static final String WEB = "WEB";
	public static final String LDAP_USER = "LDAP_USER";
	public static final String ERROR = "ERROR";
	public static final String ROLE_ALL = "ROLE_ALL";
	public static final String ROLE_SEND = "ROLE_SEND";
	public static final String ROLE_READ_ONLY = "ROLE_READ_ONLY";
}
