package com.wam.utility.InputStream;

import static java.lang.System.out;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Scanner;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class InputStreamTransformer {

	public static Integer DEFAULT_INDENT = 2;
	
	/**
	 * N�1
	 * this is the best and most rapid method
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public static String byteArrayOutputStreamTransform(InputStream inputStream) throws IOException
	{
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) != -1) {
		    result.write(buffer, 0, length);
		}

		return result.toString("UTF-8");
	}
	
	public static String scannerTransform(InputStream inputStream) throws IOException
	{
		Scanner s = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
		String result= ""; 
		while(s.hasNext())
		{
			result += s.next();
		}

		return result;
	}
	
	/**
	 * 
	 * @param input
	 * @param indent
	 * @return
	 */
	public static String prettyFormat(String input, int indent) {
	    try {
	        Source xmlInput = new StreamSource(new StringReader(input));
	        StringWriter stringWriter = new StringWriter();
	        StreamResult xmlOutput = new StreamResult(stringWriter);
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        transformerFactory.setAttribute("indent-number", indent);
	        Transformer transformer = transformerFactory.newTransformer(); 
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.transform(xmlInput, xmlOutput);
	        return xmlOutput.getWriter().toString();
	    } catch (Exception e) {
	        throw new RuntimeException(e); 
	    }
	}
	
	
}
