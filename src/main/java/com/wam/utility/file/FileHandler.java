package com.wam.utility.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileHandler {

	
	public static void appendFile(String data, String file) {

        try {
        	if(Files.exists(Paths.get(file)) == true) 
    		{
        	    Files.write(Paths.get(file), data.getBytes(), StandardOpenOption.APPEND);
    		}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public static void appendLog(String data, String file)
	{
			String stringToWrite = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + System.lineSeparator() 
			+ data + System.lineSeparator();
			appendFile(stringToWrite, file);
	}
	
}
