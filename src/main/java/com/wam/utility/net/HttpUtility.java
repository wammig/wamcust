package com.wam.utility.net;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.wam.model.statistic.RequestStatistic;
import com.wam.utility.definition.Definition;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.lang.System.out;


/**
 * HttpUtility is a class based on HttpURLConnection.
 * Call web service with method get or post.
 * 
 * 
 * @author QINC1
 *
 */
public class HttpUtility {

 public static final String METHOD_GET = "GET"; // METHOD GET
 public static final String METHOD_POST = "POST"; // METHOD POST
 public static final String METHOD_PUT = "PUT"; // METHOD PUT
 public static final String METHOD_DELETE = "DELETE"; // METHOD DELETE
 
 public static final String FORMAT_XML = "application/xml";
 public static final String FORMAT_TEXT = "text/plain";
 public static final String FORMAT_JSON = "application/json";
 public static final String FORMAT_HTML = "text/html";
 public static final String CHARSET = "UTF-8";
 
 public static final String SUCCESS_STATUS_OK_CREATED = "OK_CREATED";
 public static final String SUCCESS_STATUS_OK = "OK";
 public static Integer[] SUCCESS_STATUS_ARRAY_OK_CREATED = new Integer[] {200,201};
 public static Integer[] SUCCESS_STATUS_ARRAY_OK = new Integer[] {200};

 
 
 
 
 
 
 
 
 /**
  * static method newRequest
  * type of params is object. 
  * But be careful!!! params will be transformed to a map or a string before calling ws
  * 
  * web_url ==> url to be called
  * method  ==> get or post ...
  * params  ==> map or string
  * paramsFormat ==> content-type
  * successStatus ==> Accept only 200 ? or 200, 201 and more... 
  *	
  * @param web_url
  * @param method
  * @param params
  * @param paramsFormat
  * @param successStatus
 * @throws SocketException 
  * 
  * 
  */
 public static Map newRequest(String web_url, String method, Object params, String paramsFormat, String successStatus) throws SocketException {
	 String url = web_url;
     Map returnedMap = new HashMap();
     // write GET params,append with url
     
     /**
      * check what success status we want
      */
     Integer[] successStatusArray = SUCCESS_STATUS_ARRAY_OK_CREATED;
	 if(successStatus == SUCCESS_STATUS_OK) {
		 successStatusArray = SUCCESS_STATUS_ARRAY_OK;
	 }
	 
	 try {
		 
		 HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
	     urlConnection.setDoOutput(true); // write POST params
	     urlConnection.setUseCaches(false);
	     urlConnection.setRequestProperty("Content-Type", paramsFormat); // handle url encoded form data
	     urlConnection.setRequestProperty("charset", CHARSET);
	     if (method == METHOD_GET) {
	    	 urlConnection.setRequestMethod("GET");
	     } 
	     if (method == METHOD_POST) {
	    	 urlConnection.setRequestMethod("POST");
	     }
	     if (method == METHOD_PUT) {
	    	 urlConnection.setRequestMethod("PUT");
	     }
	     if (method == METHOD_DELETE) {
	    	 urlConnection.setRequestMethod("DELETE");
	     }
	     
	     
	     
	     // Method Get
		 if (method == METHOD_GET && params != null) {
			 /**
			  * prepare parameters for map
			  */
			 if(params instanceof HashMap< ?, ? >)
		     {
			      for (Map.Entry < String, String > item: ((HashMap<String, String>) params).entrySet()) {
		    	       String key = URLEncoder.encode(item.getKey(), CHARSET);
		    	       String value = URLEncoder.encode(item.getValue(), CHARSET);
		    	       if (!url.contains("?")) {
		    	    	   url += "?" + key + "=" + value;
		    	       } else {
		    	    	   url += "&" + key + "=" + value;
		    	       }
			      }
		     }
			 /**
			  * prepare parameters for string (when send raw data as parameters to the ws)
			  */
			 if(params instanceof String)
		     {
				 String value = URLEncoder.encode(params.toString(), CHARSET);
	  	         if (!url.contains("?")) {
	  	    	     url += "?" + value;
	  	         } else {
	  	    	     url += "&" + value;
	  	         }
		     }
	     }
	     
	     

	     

	     //Method POST 
	     if ((method == METHOD_POST || method == METHOD_PUT) && params != null) {
	      StringBuilder postData = new StringBuilder();
	      
	      /**
	       * prepare parameters for map
	       */
	      if(params instanceof HashMap< ?, ? >)
	      {
	    	  for (Map.Entry < String, String > item: ((HashMap<String, String>) params).entrySet()) {
	    	       if (postData.length() != 0) {
	    	    	   postData.append('&');
	    	       }
	    	    	   
	    	       postData.append(URLEncoder.encode(item.getKey(), CHARSET));
	    	       postData.append('=');
	    	       postData.append(URLEncoder.encode(String.valueOf(item.getValue()), CHARSET));
		      }
	      }
	      /**
	       * prepare parameters for string (when send raw data as parameters to the ws)
	       */
	      if(params instanceof String)
	      {
	    	  postData = postData.append(params.toString());
	      }
	      /**
	       * send to url
	       */
	      byte[] postDataBytes = postData.toString().getBytes(CHARSET);
	      urlConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	      urlConnection.getOutputStream().write(postDataBytes);
          //out.println("send data");
	     }
	     // server response code
	     int responseCode = urlConnection.getResponseCode();

	     //out.println("response data");
	     //out.println(responseCode);
	     
	     if (Arrays.asList(successStatusArray).contains(responseCode)) {
		      BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		      StringBuilder response = new StringBuilder();
		      String line;
		      while ((line = reader.readLine()) != null) {
		       response.append(line);
		      }
		      reader.close(); // close BufferReader
		      
		      returnedMap.put(Definition.RESPONSE, response.toString());
		      returnedMap.put(Definition.RESPONSE_CODE, responseCode);
		  
	     } else {
	    	 returnedMap.put(Definition.RESPONSE, urlConnection.getResponseMessage());
		     returnedMap.put(Definition.RESPONSE_CODE, responseCode);
	     }

	     urlConnection.disconnect(); // disconnect connection
	     
	     RequestStatistic.add();
	     RequestStatistic.display();
	    } 
	    
		catch(SocketException e) {
			 throw new SocketException("SocketException: Connection reset");
		}
	 	catch (IOException e) {
	 		 throw new SocketException("IOException");
	    } 
	 
	    
	 return returnedMap;
	   
	 
 }
 
 
 
}