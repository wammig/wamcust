package com.wam.utility.net;

import java.util.Arrays;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.wam.utility.definition.Definition;

public class HttpUtilityMultiThread extends HttpUtility{

	// Callback interface
	 public interface Callback {
	  // abstract methods
	  public void OnSuccess(String response) throws JAXBException;
	  public void OnError(int status_code, String message);
	 }
	 
	 /**
	  * static method newRequestMultiThread
	  * type of params is object. 
	  * But be careful!!! params will be transformed to a map or a string before calling ws
	  * 
	  * web_url ==> url to be called
	  * method  ==> get or post ...
	  * params  ==> map or string
	  * paramsFormat ==> content-type
	  * successStatus ==> Accept only 200 ? or 200, 201 and more... 
	  * callback ==> a function to be called when remote call finished
	  * 
	  * @param web_url
	  * @param method
	  * @param params
	  * @param paramsFormat
	  * @param successStatus
	  * @param callback
	  * 
	  * 
	  * * Example:
	  * 
	  * HttpUtility.newRequestMultiThread(url,HttpUtility.METHOD_POST,params, HttpUtility.XML_FORMAT, HttpUtility.SUCCESS_STATUS_OK_CREATED, new HttpUtility.Callback() {
	        @Override
	        public void OnSuccess(String response) {
	        // on success
	           System.out.println("Server OnSuccess response: "+response);
	        }
	        @Override
	        public void OnError(int status_code, String message) {
	        // on error
	              System.out.println("Server OnError status_code: "+status_code+" message="+message);
	        }
	    });
	  * 
	  * 
	  */
	 public static void newRequestMultiThread(String web_url, String method, Object params, String paramsFormat, String successStatus, Callback callback) {
		 
	  // thread for handling async task
	  new Thread(new Runnable() {
	   @Override
	   public void run() {
	    try {
	    	/**
	         * check what success status we want
	         */
	        Integer[] successStatusArray = SUCCESS_STATUS_ARRAY_OK_CREATED;
		   	 if(successStatus == SUCCESS_STATUS_OK) {
		   		 successStatusArray = SUCCESS_STATUS_ARRAY_OK;
		   	 }
		   	 
		   	 
		   	/**
		   	 * call  newRequest to do the single thread job
		   	 */
	    	Map returnedNewRequest = newRequest(web_url, method, params, paramsFormat, successStatus);
	    	
		     if (Arrays.asList(successStatusArray).contains(returnedNewRequest.get(Definition.RESPONSE_CODE)) && callback != null) {
		
			      // callback success
			      callback.OnSuccess(returnedNewRequest.get(Definition.RESPONSE).toString());
		
		     } else if (callback != null) {
			      // callback error
			      callback.OnError(Integer.parseInt(returnedNewRequest.get(Definition.RESPONSE_CODE).toString()), returnedNewRequest.get(Definition.RESPONSE).toString());
		     }


		}  catch (Exception e) {
	    	 e.printStackTrace();
		     if (callback != null) {
			      // callback error
			      callback.OnError(500, e.getLocalizedMessage());
		     }
		}
	   }
	  }).start(); // start thread
	 }
	
}
