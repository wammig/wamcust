package com.wam.utility.helper;

import com.wam.utility.definition.Definition;

public class StringHelper {

	/**
	 * get the last part of string 
	 * @param inputString
	 * @param subStringLength
	 * @return
	 */
	public static String getLastnCharacters(String inputString,  int subStringLength){
		int length = inputString.length();
		if(length <= subStringLength) {
			return inputString;
		}
		int startIndex = length-subStringLength;
		
		return inputString.substring(startIndex);
	}
	
	/**
	 * remove & from string
	 */
	public static String removeEtCommercial(String str)
	{
		return str.replaceAll(Definition.ET_COMMERCIAL_REPLACEMENT, Definition.ET_COMMERCIAL);
	}
	
	
}
