/*
 * TimeStamp.java
 *
 * Created on 20 f�vrier 2008, 16:22
 */

package com.wam.utility.helper;

import java.text.*;
import java.util.*;

/**
 * 
 * @author baudouhm
 */
public class TimeStamp {

	/** Creates a new instance of TimeStamp */
	public TimeStamp() {
	}

	/*
	 * Cette fonction permet de convertire des formats de date en respectant les
	 * Zones
	 */
	/*
	 * private static String convert(String inTime, String inFormat, String
	 * inZone, String outFormat, String outZone) { SimpleDateFormat sdf = null;
	 * 
	 * // Input format time sdf = new SimpleDateFormat(inFormat);
	 * sdf.setTimeZone((inZone == null) ? TimeZone.getDefault() :
	 * TimeZone.getTimeZone(inZone)); Date inDate = sdf.parse(inTime, new
	 * ParsePosition(0));
	 * 
	 * sdf = new SimpleDateFormat(outFormat); sdf.setTimeZone((outZone == null)
	 * ? TimeZone.getDefault() : TimeZone.getTimeZone(outZone)); String outTime
	 * = sdf.format(inDate);
	 * 
	 * return outTime; }
	 */

	private static String convert(String inTime, String inFormat,
			String inZone, String outFormat, String outZone) {

		return convert(inTime, inFormat, Locale.ENGLISH, inZone, outFormat,
				outZone, Locale.ENGLISH);
	}

	private static String convert(String inTime, String inFormat,
			Locale inLocale, String inZone, String outFormat, String outZone,
			Locale outLocale) {
		SimpleDateFormat sdf = null;

		// Input format time
		sdf = new SimpleDateFormat(inFormat, inLocale);
		sdf.setTimeZone((inZone == null) ? TimeZone.getDefault() : TimeZone
				.getTimeZone(inZone));
		Date inDate = sdf.parse(inTime, new ParsePosition(0));

		sdf = new SimpleDateFormat(outFormat, outLocale);
		sdf.setTimeZone((outZone == null) ? TimeZone.getDefault() : TimeZone
				.getTimeZone(outZone));
		String outTime = sdf.format(inDate);

		return outTime;
	}

	private static String convert(Date inDate, String outFormat,
			String outZone, Locale outLocale) {
		SimpleDateFormat sdf = null;

		sdf = new SimpleDateFormat(outFormat, outLocale);
		sdf.setTimeZone((outZone == null) ? TimeZone.getDefault() : TimeZone
				.getTimeZone(outZone));
		String outTime = sdf.format(inDate);

		return outTime;
	}

	private static String convert(Date inDate, String outFormat, String outZone) {
		return convert(inDate, outFormat, outZone, Locale.ENGLISH);
	}

	private static String convert(Calendar inTime, String outFormat,
			String outZone, Locale outLocale) {
		return convert(inTime.getTime(), outFormat, outZone, outLocale);
	}

	private static String convert(Calendar inTime, String outFormat) {
		return convert(inTime, outFormat, TimeZone.getDefault().getID(),
				Locale.ENGLISH);
	}

	public static String changeFormat(String inTime, String inFormat,
			String outFormat) {
		return convert(inTime, inFormat, null, outFormat, null);
	}

	public static String changeZone(String inTime, String inFormat,
			String inZone, String outZone) {
		return convert(inTime, inFormat, inZone, inFormat, outZone);
	}

	public static String changeZone(String inTime, String inFormat,
			String outZone) {
		return convert(inTime, inFormat, null, inFormat, outZone);
	}

	public static String changeZone(Calendar inTime, String outFormat,
			String outZone, Locale outLocale) {
		return convert(inTime, outFormat, outZone, outLocale);
	}

	public static String changeZone(Calendar inTime, String outFormat,
			String outZone) {
		return convert(inTime, outFormat, outZone, Locale.ENGLISH);
	}

	public static String changeZone(Date inTime, String outFormat,
			String outZone) {
		return convert(inTime, outFormat, outZone);
	}

	public static String currentTime(String outFormat) {
		return convert(Calendar.getInstance(), outFormat);
	}

	public static void main(String[] args) {
		
		//String inTime = "Tue Apr 24 12:54:26 CEST 2018";
		//String inFormat = ConstantsI.DEFAULT_DATE_FORMAT;
		//String vBeginDate = TimeStamp.changeZone(Calendar.getInstance(),"dd-MMM-yyyy HH:mm:ss", ConstantsI.TIME_ZONE);
		//System.out.println("vBeginDate " + vBeginDate);
/*
		Format formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String s = formatter.format(new Date());
		System.out.println("s:"+s);
		
    	String mtime = "Tue Apr 24 12:54:26 CEST 2018";
    	String inFormat = com.lng.util.Token.tokenizer(new String[] { "EEE MMM d HH:mm:ss z yyyy" });
    	String remoteTime = com.lng.util.TimeStamp.changeFormat(mtime, inFormat,com.lng.util.ConstantsI.DEFAULT_DATE_FORMAT);
    	System.out.println("remoteTime:"+remoteTime);	
*/
		// String inFormat = "MM-dd-yy hh:mma";

		// String inZone = TimeZone.getDefault().getID();
		// System.out.println("inZone "+inZone);
		// String inZone = TimeZone.getTimeZone("GMT+2").getID();
		// String outFormat= "dd-MMM-yyyy HH:mm:ss";
		// String outFormat= ConstantsI.COMPARE_DATE_FORMAT;
		// String outFormat= "MM/yyyy";

		// System.out.println(TimeZone.getTimeZone("GMT+0").getID());
		// String outZone = TimeZone.getTimeZone("GMT-5").getID();
		// System.out.println("outZone "+outZone);

		// String outTime =
		// TimeStamp.changeZone(Calendar.getInstance(),outFormat,null);
		// String outTime = changeFormat(inTime,inFormat,outFormat);

		// String vCreatedTime =
		// TimeStamp.changeZone(Calendar.getInstance(),ConstantsI.COMPARE_DATE_FORMAT,
		// null);
		// String vCreatedTime = TimeStamp.changeZone(Calendar.getInstance(),
		// "MM-dd-yyyy HH:mm:ss", "GMT-5");
		// System.out.println("vCreatedTime "+vCreatedTime);

		// String outTime =
		// TimeStamp.changeZone(inTime,inFormat,inZone,outZone);

		// System.out.println("outZone "+outZone);
		// System.out.println("inZone "+inZone);
		// System.out.println("outTime "+outTime);

		// String[] availableTimezones = TimeZone.getAvailableIDs();
		/*
		 * for (int i=0;i<availableTimezones.length;i++) { String TimezoneID =
		 * availableTimezones[i]; TimeZone timezone =
		 * TimeZone.getTimeZone(TimezoneID); System.out.println("TimezoneID   "
		 * + TimezoneID + " GMT" + timezone.getRawOffset()/3600000); }
		 */
		/*
		 * String[] availableTimezones = TimeZone.getAvailableIDs(-5*3600000);
		 * for (int i=0;i<availableTimezones.length;i++) { String TimezoneID =
		 * availableTimezones[i]; System.out.println("TimezoneID   " +
		 * TimezoneID ); }
		 */

		/*
		 * String inTime = "01-18-2010 15:20:00"; String inFormat =
		 * ConstantsI.DEFAULT_DATE_FORMAT; SimpleDateFormat sdf = new
		 * SimpleDateFormat(inFormat);
		 * sdf.setTimeZone((inZone==null)?TimeZone.getDefault
		 * ():TimeZone.getTimeZone(inZone)); Date inDate = sdf.parse(inTime, new
		 * ParsePosition(0)); TimeZone timezone =
		 * TimeZone.getTimeZone("US/Eastern"); int offset =
		 * timezone.getOffset(inDate.getTime()); System.out.println("offset   "
		 * + offset/3600000 );
		 */

		/*
		 * String strTimeOrYear = "10-01 06:40:00"; TimeZone timezone =
		 * TimeZone.getTimeZone("US/Eastern"); String outTime =
		 * TimeStamp.changeZone
		 * (strTimeOrYear,"dd-MM HH:mm:ss","US/Eastern",TimeZone
		 * .getDefault().getID()); System.out.println("outTime   " + outTime );
		 * 
		 * String outFormat =
		 * TimeStamp.changeFormat(outTime,"dd-MM HH:mm:ss","MMddHHmmss"); String
		 * curYear = TimeStamp.currentTime("yyyy");
		 * 
		 * outTime = com.lng.util.Token.tokenizer(new
		 * String[]{curYear,outFormat},""); System.out.println("outTime   " +
		 * outTime );
		 * 
		 * String curTime = TimeStamp.currentTime("yyyyMMddHHmmss");
		 * System.out.println("curTime   " + curTime );
		 * 
		 * long loutTime = Long.parseLong(outTime); long lcurTime =
		 * Long.parseLong(curTime); long lcurYear = Long.parseLong(curYear);
		 * 
		 * if (loutTime>lcurTime) { lcurYear = lcurYear-1;
		 * curYear=String.valueOf(lcurYear); }
		 * 
		 * outTime = com.lng.util.Token.tokenizer(new
		 * String[]{curYear,outFormat},""); String localTime =
		 * com.lng.util.TimeStamp
		 * .changeFormat(outTime,com.lng.util.ConstantsI.COMPARE_DATE_FORMAT
		 * ,com.lng.util.ConstantsI.DEFAULT_DATE_FORMAT);
		 * System.out.println("localTime   " + localTime );
		 * 
		 * String remoteTime =
		 * TimeStamp.changeZone(localTime,com.lng.util.ConstantsI
		 * .DEFAULT_DATE_FORMAT,TimeZone.getDefault().getID(),"US/Eastern");
		 * System.out.println("remoteTime   " + remoteTime );
		 * 
		 * //String curYear = com.lng.util.TimeStamp.changeZone(new
		 * java.util.Date(),"yyyy",TimeZone.getDefault().getID());
		 * 
		 * //String strYear =
		 * (strTimeOrYear.indexOf(":")==-1)?strTimeOrYear:com.
		 * lng.util.TimeStamp.changeZone(new java.util.Date(),"yyyy",null);
		 * //System.out.println("strYear   " + strYear );
		 */
		/*
		 * String strTimeOrYear ="20-01 05:27:00"; String strRemoteTimeZone
		 * ="Europe/Paris"; String strLocalTimeZone ="Europe/Paris";
		 * 
		 * String outTime =
		 * TimeStamp.changeZone(strTimeOrYear,"dd-MM HH:mm:ss",strRemoteTimeZone
		 * ,strLocalTimeZone); System.out.println("outTime   " + outTime );
		 */
		/*
		 * Calendar calendar =
		 * Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		 * System.out.println("calendar   " + calendar.getTime() );
		 * 
		 * System.out.println("timezone   " + TimeZone.getDefault().getID());
		 */

		/*
		 * String[] arrIDs = TimeZone.getAvailableIDs();
		 * 
		 * for (int i=0;i<arrIDs.length;i++) { String TimezoneID = arrIDs[i];
		 * TimeZone timezone = TimeZone.getTimeZone(TimezoneID); String
		 * TimezoneName = timezone.getDisplayName(); int offset =
		 * (timezone.getRawOffset()/3600000); String sign = (offset>0)?"+":"";
		 * System.out.println("Timezone  " + TimezoneID + " (" + TimezoneName +
		 * ") GMT"+sign + offset); }
		 * 
		 * 
		 * int limit = Integer.valueOf(unlimited).intValue();
		 */

		/*
		 String strTimeOrYear = "29-02 8:30:00"; 
		 String strRemoteTimeZone = "US/Eastern"; 
		 String strLocalTimeZone = "Europe/Paris";
		  String outTime = TimeStamp.changeZone(strTimeOrYear,"dd-MM HH:mm:ss",strRemoteTimeZone, strLocalTimeZone);
		  System.out.println("outTime   " + outTime);
		  */
		
		/*
		String unlimited = TimeStamp.changeFormat(
				com.lng.util.ConstantsI.UNLIMITED_DATE,
				com.lng.util.ConstantsI.PSOFT_DATE_FORMAT,
				com.lng.util.ConstantsI.PKG_DATE_FORMAT);
		System.out.println("unlimited   " + unlimited);
		*/
	}

}
