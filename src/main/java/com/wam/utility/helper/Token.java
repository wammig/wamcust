/*
 * Tools.java
 *
 * Created on 30 juillet 2009, 11:34
 */

package com.wam.utility.helper;

import java.util.*;

import com.wam.utility.definition.Definition;
import static java.lang.System.out;

public class Token {

	private static final String DEFAULT_SEPARATOR = Definition.SPACE;

	private String separator;
	private List tokens = null;

	public Token() {
		separator = DEFAULT_SEPARATOR;
	}

	public Token(String sep) {
		this.separator = separator;
	}

	public Token(List tokens, String sep) {
		this.tokens = tokens;
		this.separator = sep;
	}

	public Token(String[] tokens, String sep) {
		this.tokens = null;
		if ((tokens != null) && (tokens.length != 0)) {
			this.tokens = new ArrayList();
			for (int i = 0; i < tokens.length; i++) {
				String element = tokens[i];
				this.tokens.add(element);
			}
		}
		this.separator = sep;
	}

	public String tokenizer() {
		return tokenizer(tokens, separator);
	}

	public static String tokenizer(String[] arrString) {
		return tokenizer(arrString, DEFAULT_SEPARATOR);
	}

	public static String tokenizer(String[] arrString, String separator) {
		String regular = null;
		if ((arrString != null) && (arrString.length > 0)) {
			regular = "";
			for (int i = 0; i < arrString.length; i++) {
				if (i == 0)
					regular = arrString[i];
				else
					regular = regular + separator + arrString[i];
			}
			regular = regular.trim();
		}
		return regular;
	}

	public static String tokenizer(List listString, String separator) {
		separator = (separator == null) ? "" : separator;

		String regular = null;
		int listsize = listString.size();
		if ((listString != null) && (listsize > 0)) {
			regular = "";
			for (int i = 0; i < listsize; i++) {
				String element = (String) listString.get(i);
				element = (element == null) ? "" : element;
				if (i == 0)
					regular = element;
				else
					regular = regular + separator + element;
			}
			regular = regular.trim();
		}
		return regular;
	}

	public static String[] untokenizer(String token, String separator) {
		return token.split(separator);
	}

	public static String[] untokenizer(String token) {
		return untokenizer(token, DEFAULT_SEPARATOR);
	}

	public static String untokenizer(String token, String separator, int i) {
		String i_token = null;
		if (token != null) {
			String[] s_Token = token.split(separator);
			if (i < s_Token.length)
				i_token = s_Token[i];
		}
		return i_token;
	}

	public static String untokenizer(String token, int i) {
		return untokenizer(token, DEFAULT_SEPARATOR, i);
	}

	public static String untokenizer(String token, String separator,
			boolean emptyToNull, int i) {
		String i_token = null;
		if (token != null) {
			String[] s_Token = token.split(separator);
			if (i < s_Token.length) {
				if ((emptyToNull) && (s_Token[i].trim().length() == 0))
					s_Token[i] = null;
				i_token = s_Token[i];
			}
		}
		return i_token;
	}

	/**
	 * Getter for property tokens.
	 * 
	 * @return Value of property tokens.
	 */
	public java.util.List getTokens() {
		return tokens;
	}

	/**
	 * Setter for property tokens.
	 * 
	 * @param tokens
	 *            New value of property tokens.
	 */
	public void setTokens(java.util.List tokens) {
		this.tokens = tokens;
	}

	/**
	 * Setter for property tokens.
	 * 
	 * @param tokens
	 *            New value of property tokens.
	 */
	public boolean add(String value) {
		return this.tokens.add(value);
	}

	/**
	 * Setter for property tokens.
	 * 
	 * @param tokens
	 *            New value of property tokens.
	 */
	public boolean add(int value) {
		return this.tokens.add(String.valueOf(value));
	}

	/**
	 * Setter for property tokens.
	 * 
	 * @param tokens
	 *            New value of property tokens.
	 */
	public void add(int index, String value) {
		this.tokens.add(index, value);
	}

	/**
	 * Returns the element at the specified position in this list.
	 */
	public String get(int index) {
		return (String) this.tokens.get(index);
	}

	public static void main(String[] args) {
		String token = "1 2 3";

		out.println("Token-main val0:" + Token.untokenizer(token, 0));
		out.println("Token-main val1:" + Token.untokenizer(token, 1));
		out.println("Token-main val2:" + Token.untokenizer(token, 2));
		out.println("Token-main val3:" + Token.untokenizer(token, 3));

		String[] arrToken = Token
				.untokenizer(
						"actn_type_01052012.txt,cntnt_grp_01052012.txt,cntnt_type_01052012.txt,doc_type_01052012.txt,staging_01052012.txt,sub_actn_type_01052012.txt,actn_type_01042012.txt,cntnt_grp_01042012.txt,cntnt_type_01042012.txt,doc_type_01042012.txt ",
						",");
		String newToken = Token.tokenizer(arrToken, "\n");
		out.println("Token-main newToken:" + newToken);
	}
}
