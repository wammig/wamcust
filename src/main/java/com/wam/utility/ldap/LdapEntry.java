package com.wam.utility.ldap;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.naming.CommunicationException;
import javax.naming.NamingException;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.utility.helper.Token;

import static java.lang.System.out;

public class LdapEntry {

	public static final String returnedAtts[] = { "userPrincipalName", "mail",
			"sn", "givenName", "sAMAccountName", "displayName", "employeeID",
			"description", "title", "telephonenumber", "manager", "mobile",
			"physicalDeliveryOfficeName", "department", "streetAddress", "co",
			"postalCode", "l", "company", "name", "accountexpires",
			"whenchanged", "whencreated" };

	private String mail;
	private String givenName;
	private String userPrincipalName;
	private String sn;
	private String sAMAccountName;
	private String displayName;
	private String employeeID;
	private String description;
	private String title;
	private String telephonenumber;
	private String physicalDeliveryOfficeName;
	private String manager;
	private String mobile;
	private String department;
	private String streetAddress;
	private String co;
	private String postalCode;
	private String l;
	private String company;
	private String name;
	private String accountexpires;
	private String whenchanged;
	private String whencreated;

	/**
	 * @return the physicalDeliveryOfficeName
	 */
	public String getPhysicalDeliveryOfficeName() {
		return physicalDeliveryOfficeName;
	}

	/**
	 * @param physicalDeliveryOfficeName
	 *            the physicalDeliveryOfficeName to set
	 */
	public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName) {
		this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the manager
	 */
	public String getManager() {
		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getTelephonenumber() {
		return telephonenumber;
	}

	public void setTelephonenumber(String telephonenumber) {
		this.telephonenumber = telephonenumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMail() {
		return mail;
	}

	public String getsAMAccountName() {
		return sAMAccountName;
	}

	public void setsAMAccountName(String sAMAccountName) {
		this.sAMAccountName = sAMAccountName;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getUserPrincipalName() {
		return userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

	public LdapEntry() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setLdapValue(String entry, String value) {
		if (entry.equalsIgnoreCase("mail")) {
			this.setMail(value);
		} else if (entry.equalsIgnoreCase("userPrincipalName")) {
			this.setUserPrincipalName(value);
		} else if (entry.equalsIgnoreCase("givenName")) {
			this.setGivenName(value);
		} else if (entry.equalsIgnoreCase("sn")) {
			this.setSn(value);
		} else if (entry.equalsIgnoreCase("sAMAccountName")) {
			this.setsAMAccountName(value);
		} else if (entry.equalsIgnoreCase("displayName")) {
			this.setDisplayName(value);
		} else if (entry.equalsIgnoreCase("employeeID")) {
			this.setEmployeeID(value);
		} else if (entry.equalsIgnoreCase("description")) {
			this.setDescription(value);
		} else if (entry.equalsIgnoreCase("title")) {
			this.setTitle(value);
		} else if (entry.equalsIgnoreCase("telephonenumber")) {
			this.setTelephonenumber(value);
		} else if (entry.equalsIgnoreCase("manager")) {
			if (value != null) {
				String value1 = Token.untokenizer(value, ",OU", 0)
						.trim();
				value1 = Token.untokenizer(value1, "=", 1).trim();
				value = value1.replaceAll("\\\\", "");
			}

			this.setManager(value);
		} else if (entry.equalsIgnoreCase("mobile")) {
			this.setMobile(value);
		} else if (entry.equalsIgnoreCase("physicalDeliveryOfficeName")) {
			this.setPhysicalDeliveryOfficeName(value);
		} else if (entry.equalsIgnoreCase("department")) {
			this.setDepartment(value);
		} else if (entry.equalsIgnoreCase("streetAddress")) {
			this.setStreetAddress(value);
		} else if (entry.equalsIgnoreCase("co")) {
			this.setCo(value);
		} else if (entry.equalsIgnoreCase("postalCode")) {
			this.setPostalCode(value);
		} else if (entry.equalsIgnoreCase("l")) {
			this.setL(value);
		} else if (entry.equalsIgnoreCase("company")) {
			this.setCompany(value);
		} else if (entry.equalsIgnoreCase("name")) {
			this.setName(value);
		} else if (entry.equalsIgnoreCase("accountexpires")) {
			this.setAccountexpires(value);
		} else if (entry.equalsIgnoreCase("whenchanged")) {
			this.setWhenchanged(value);
		} else if (entry.equalsIgnoreCase("whencreated")) {
			this.setWhencreated(value);
		}
	}

	public String getLdapValue(String entry) {
		String value = null;
		if (entry.equalsIgnoreCase("mail")) {
			value = this.getMail();
		} else if (entry.equalsIgnoreCase("userPrincipalName")) {
			value = this.getUserPrincipalName();
		} else if (entry.equalsIgnoreCase("givenName")) {
			value = this.getGivenName();
		} else if (entry.equalsIgnoreCase("sn")) {
			value = this.getSn();
		} else if (entry.equalsIgnoreCase("sAMAccountName")) {
			value = this.getsAMAccountName();
		} else if (entry.equalsIgnoreCase("displayName")) {
			value = this.getDisplayName();
		} else if (entry.equalsIgnoreCase("employeeID")) {
			value = this.getEmployeeID();
		} else if (entry.equalsIgnoreCase("description")) {
			value = this.getDescription();
		} else if (entry.equalsIgnoreCase("title")) {
			value = this.getTitle();
		} else if (entry.equalsIgnoreCase("telephonenumber")) {
			value = this.getTelephonenumber();
		} else if (entry.equalsIgnoreCase("manager")) {
			value = this.getManager();
		} else if (entry.equalsIgnoreCase("mobile")) {
			value = this.getMobile();
		} else if (entry.equalsIgnoreCase("physicalDeliveryOfficeName")) {
			value = this.getPhysicalDeliveryOfficeName();
		} else if (entry.equalsIgnoreCase("department")) {
			value = this.getDepartment();
		} else if (entry.equalsIgnoreCase("streetAddress")) {
			value = this.getStreetAddress();
		} else if (entry.equalsIgnoreCase("co")) {
			value = this.getCo();
		} else if (entry.equalsIgnoreCase("postalCode")) {
			value = this.getPostalCode();
		} else if (entry.equalsIgnoreCase("l")) {
			value = this.getL();
		} else if (entry.equalsIgnoreCase("company")) {
			value = this.getCompany();
		} else if (entry.equalsIgnoreCase("name")) {
			value = this.getName();
		} else if (entry.equalsIgnoreCase("accountexpires")) {
			value = this.getAccountexpires();
		} else if (entry.equalsIgnoreCase("whenchanged")) {
			value = this.getWhenchanged();
		} else if (entry.equalsIgnoreCase("whencreated")) {
			value = this.getWhencreated();
		}

		return value;
	}

	public List<LdapEntry> search() {

		ArrayList<LdapEntry> ldapEntrys = null;

		try {

			java.util.HashMap<String, String> hsReturnedAtts = new java.util.HashMap<String, String>();
			hsReturnedAtts.put("sAMAccountName", sAMAccountName);
			hsReturnedAtts.put("displayName", displayName);
			hsReturnedAtts.put("manager", manager);
			hsReturnedAtts.put("description", description);

			java.util.List<LdapEntry> ldapEntrySearch = LdapBrowser
					.getLdapEntityAll();

			int j = 0;
			for (int i = 0; i < ldapEntrySearch.size(); i++) {
				LdapEntry ldapEntry = (LdapEntry) ldapEntrySearch.get(i);
				String sAMAccountName = ldapEntry.getsAMAccountName();
				// com.lng.util.ConstantsI.println("com.lng.util.ldap.LdapEntry sAMAccountName: "
				// + sAMAccountName,3);
				if (sAMAccountName == null)
					continue;

				boolean blnLdapEntry = true;

				Iterator<String> itrSetReturnedAtts = (hsReturnedAtts.keySet())
						.iterator();
				while (itrSetReturnedAtts.hasNext()) {
					String searchAtts = (String) itrSetReturnedAtts.next();
					String searchVal = (String) hsReturnedAtts.get(searchAtts);

					// com.lng.util.ConstantsI.println("com.lng.util.ldap.LdapEntry searchAttsVal: "
					// + searchAtts + "-" + searchVal,3);
					if (searchVal != null) {
						String sLdapEntry = ldapEntry.getLdapValue(searchAtts);
						// com.lng.util.ConstantsI.println("com.lng.util.ldap.LdapEntry sLdapEntry: "
						// + sLdapEntry,3);

						if (sLdapEntry == null) {
							blnLdapEntry = false;
							continue;
						}

						boolean blnStartsWith = false;
						boolean blnEndsWith = false;
						String unlc = searchVal;

						if ((unlc.startsWith("%")) && (unlc.endsWith("%"))) {
							blnStartsWith = true;
							blnEndsWith = true;
							unlc = searchVal.substring(1,
									searchVal.length() - 1).toLowerCase();
						} else if ((unlc.startsWith("%"))
								&& (!unlc.endsWith("%"))) {
							blnStartsWith = true;
							blnEndsWith = false;
							unlc = searchVal.substring(1).toLowerCase();
						} else if ((!unlc.startsWith("%"))
								&& (unlc.endsWith("%"))) {
							blnStartsWith = false;
							blnEndsWith = true;
							unlc = searchVal.substring(0,
									searchVal.length() - 1).toLowerCase();
						} else {
							unlc = searchVal.toLowerCase();
						}

						if (sLdapEntry != null) {
							String snlc = sLdapEntry.toLowerCase();

							if ((blnStartsWith) && (blnEndsWith)) {
								if (!snlc.contains(unlc)) {
									blnLdapEntry = false;
								}
							} else if ((blnStartsWith) && (!blnEndsWith)) {
								if (!snlc.endsWith(unlc)) {
									blnLdapEntry = false;
								}
							} else if ((!blnStartsWith) && (blnEndsWith)) {
								if (!snlc.startsWith(unlc)) {
									blnLdapEntry = false;
								}
							} else {
								if (!snlc.equals(unlc)) {
									blnLdapEntry = false;
								}
							}
						}
					}
					// com.lng.util.ConstantsI.println("com.lng.util.ldap.LdapEntry blnLdapEntry: "
					// + blnLdapEntry,3);
				}

				if (blnLdapEntry) {
					if (ldapEntrys == null) {
						ldapEntrys = new java.util.ArrayList<LdapEntry>();
						out.println(
								"com.lng.util.ldap.LdapEntry 1. ldapEntry.getsAMAccountName(): "
										+ ldapEntry.getsAMAccountName());
						ldapEntrys.add(ldapEntry);
					} else {
						out.println(
								"com.lng.util.ldap.LdapEntry ldapEntrys.size(): "
										+ ldapEntrys.size());
						out.println(
								"com.lng.util.ldap.LdapEntry ldapEntry.getsAMAccountName(): "
										+ ldapEntry.getsAMAccountName());
						boolean blnCompareTo = false;
						for (int k = 0; k < ldapEntrys.size(); k++) {

							// com.lng.util.ConstantsI.println(2,"com.lng.util.ldap.LdapEntry k: "
							// + k);
							// LdapEntry inLdapEntry =
							// (LdapEntry)ldapEntrys.get(k);
							out.println(									
									"com.lng.util.ldap.LdapEntry ((LdapEntry)ldapEntrys.get("
											+ k
											+ ")).getsAMAccountName(): "
											+ ((LdapEntry) ldapEntrys.get(k))
													.getsAMAccountName());

							if (ldapEntry.getsAMAccountName().compareTo(
									((LdapEntry) ldapEntrys.get(k))
											.getsAMAccountName()) < 0) {
								out.println("com.lng.util.ldap.LdapEntry k: " + k);
								ldapEntrys.add(k, ldapEntry);
								blnCompareTo = true;
								break;
							}
						}
						if (!blnCompareTo)
							ldapEntrys.add(ldapEntry);
					}

				}

			}

		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ldapEntrys;
	}

	public LdapEntry getLdapEntryManager() {
		LdapEntry ldapEntryManager = null;

		LdapEntry ldapEntrySearch = new LdapEntry();
		ldapEntrySearch.setDisplayName(manager);
		List<LdapEntry> webLdapEntryResult = ldapEntrySearch
				.search();
		if ((webLdapEntryResult != null) && (webLdapEntryResult.size() != 0))
			ldapEntryManager = (LdapEntry) webLdapEntryResult
					.get(0);

		return ldapEntryManager;
	}

	public static void println(LdapEntry ldapentry) {
		Class<LdapEntry> myClass = (Class<LdapEntry>) ldapentry.getClass();
		java.lang.reflect.Field[] fields = myClass.getDeclaredFields();
		if (fields.length != 0) {
			for (int i = 0; i < fields.length; i++) {

				java.lang.reflect.Field field = fields[i];
				String fieldName = field.getName();
				Object obj;
				try {
					obj = field.get(ldapentry);
					// com.lng.util.ConstantsI.println("com.lng.util.ldap.LdapEntry "
					// + fieldName + ": " + obj,3);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// //com.lng.util.ConstantsI.println("com.lng.admintool.bean.gbs.ContactBean webAuditUserId:"
		// + bean.getWebAuditUserId(),3);
		// //com.lng.util.ConstantsI.println("com.lng.admintool.bean.gbs.ContactBean webCreatedTime:"+
		// bean.getWebCreatedTime(),3);
		// //com.lng.util.ConstantsI.println("com.lng.admintool.bean.gbs.ContactBean webUpdatedTime:"
		// + bean.getWebUpdatedTime(),3);
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department
	 *            the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress
	 *            the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the co
	 */
	public String getCo() {
		return co;
	}

	/**
	 * @param co
	 *            the co to set
	 */
	public void setCo(String co) {
		this.co = co;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the l
	 */
	public String getL() {
		return l;
	}

	/**
	 * @param l
	 *            the l to set
	 */
	public void setL(String l) {
		this.l = l;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the accountexpires
	 */
	public String getAccountexpires() {
		return accountexpires;
	}

	/**
	 * @param accountexpires
	 *            the accountexpires to set
	 */
	public void setAccountexpires(String accountexpires) {
		this.accountexpires = accountexpires;
	}

	/**
	 * @return the whenchanged
	 */
	public String getWhenchanged() {
		return whenchanged;
	}

	/**
	 * @param whenchanged
	 *            the whenchanged to set
	 */
	public void setWhenchanged(String whenchanged) {
		this.whenchanged = whenchanged;
	}

	/**
	 * @return the whencreated
	 */
	public String getWhencreated() {
		return whencreated;
	}

	/**
	 * @param whencreated
	 *            the whencreated to set
	 */
	public void setWhencreated(String whencreated) {
		this.whencreated = whencreated;
	}
	
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}

