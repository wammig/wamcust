package com.wam.utility.ldap;

import java.util.List;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.CommunicationException;
import java.net.UnknownHostException;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import static java.lang.System.out;

public class LdapBrowser {

	private static InitialDirContext context;// = InitialLdapContext();
	private static final String LDAP_BASE_DN = ("OU�PAR,OU�EMEA,OU�users,OU�user accounts,DC�legal,DC�regn,DC�net")
			.replaceAll("�", "=");
	
	private static final String PROVIDER_URL = "ldap://lngpardcxp021.legal.regn.net:389"; 

	/** Creates a new instance of LdapBrowser */
	public LdapBrowser() {
	}

	private static InitialDirContext InitialLdapContext()
			throws CommunicationException, NamingException {
		InitialDirContext context = null;
		// set properties for our connection and provider
		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		properties.put(Context.PROVIDER_URL, "ldap://lngpardcxp021.legal.regn.net:389");
		// properties.put( Context.REFERRAL, "ignore" );
		// set properties for authentication
		properties.put(Context.SECURITY_PRINCIPAL,
				"svc-leglngparadu002@LEGAL.REGN.NET");
		properties.put(Context.SECURITY_CREDENTIALS,
				"Adulexis123");
		// properties.put("java.naming.ldap.version", "2");

		context = new InitialDirContext(properties);
		/*
		 * try { context = new InitialDirContext(properties); } catch
		 * (javax.naming.CommunicationException ex) { ex.printStackTrace();
		 * System.out.println(
		 * "com.lng.util.ldap.InitialLdapContext javax.naming.CommunicationException-->"
		 * +ex.getMessage()); } catch (javax.naming.NamingException ex) {
		 * ex.printStackTrace(); System.out.println(
		 * "com.lng.util.ldap.InitialLdapContext javax.naming.NamingException-->"
		 * +ex.getMessage()); }
		 */
		return context;
	}
	
	public static InitialDirContext InitialLdapContextWithPassword(String windowsId, String password)
			throws CommunicationException, NamingException {
		InitialDirContext context = null;
		// set properties for our connection and provider
		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		properties.put(Context.PROVIDER_URL, PROVIDER_URL);

		// set properties for authentication
		properties.put(Context.SECURITY_PRINCIPAL,
				windowsId);
		properties.put(Context.SECURITY_CREDENTIALS,
				password);

		context = new InitialDirContext(properties);

		return context;
	}

	public static List<LdapEntry> getLdapEntity(String searchFilter)
			throws NamingException, CommunicationException {
		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		
		/**
		 * with setCountLimit
		 * we return more rapide
		 */
		searchCtls.setCountLimit(1);
		// specify the LDAP search filter, just users
		// Specify the attributes to return
		String returnedAtts[] = LdapEntry.returnedAtts;
		searchCtls.setReturningAttributes(returnedAtts);

		NamingEnumeration answer = null;
		if (context == null)
			context = InitialLdapContext();
		// answer = context.search(LDAP_BASE_DN, searchFilter,searchCtls);

		try {
			answer = context.search(LDAP_BASE_DN, searchFilter, searchCtls);
		} catch (javax.naming.CommunicationException comex) {			
			/*
			if (ConstantsI.LNF_PRINTSTACKTRACE) {
				comex.printStackTrace();
			}
			*/
			out.println("LdapBrowser.getLdapEntity-->CommunicationException:" + comex.getMessage()) ;
			context = InitialLdapContext();
			answer = context.search(LDAP_BASE_DN, searchFilter, searchCtls);
		}

		java.util.List<LdapEntry> lstLdapEntry = null;

		try {
		while (answer.hasMore()) {
			if (lstLdapEntry == null) {
				lstLdapEntry = new java.util.ArrayList();
			}

			LdapEntry ldapentry = new LdapEntry();
			String vals = answer.next().toString();
			// System.out.println("vals:"+vals);

			String param = vals.substring(vals.indexOf('{') + 1,
					vals.indexOf('}'));
			// System.out.println("param:"+param);
			// String[] values = param.split(",");

			for (int i = 0; i < returnedAtts.length; i++) {
				String ikeyname = returnedAtts[i];
				// System.out.println("ikeyname:"+ikeyname);

				String ikeyInput = ikeyname.toLowerCase() + "="
						+ ikeyname.toLowerCase() + ":";
				int ikeyIdx = param.toLowerCase().indexOf(ikeyInput);
				if (ikeyIdx == -1)
					continue;

				// System.out.println("ikeyIdx :"+ikeyIdx);
				int supidx = param.length();
				for (int j = 0; j < returnedAtts.length; j++) {
					if (j != i) {
						String jkeyname = returnedAtts[j];
						String jkeyInput = jkeyname.toLowerCase() + "="
								+ jkeyname.toLowerCase() + ":";
						int jkeyIdx = param.toLowerCase().indexOf(jkeyInput);
						if ((jkeyIdx > ikeyIdx) && (jkeyIdx < supidx))
							supidx = jkeyIdx;
					}
				}
				// System.out.println("supidx :"+supidx);
				String value = param.substring(ikeyIdx + ikeyInput.length(),
						supidx).trim();
				String ikeyvalue = (value.endsWith(",")) ? value.substring(0,
						value.length() - 1) : value;
				ldapentry.setLdapValue(ikeyname, ikeyvalue);
				// System.out.println(ikeyname+ ":"+ikeyvalue);
			}
			lstLdapEntry.add(ldapentry);
		}
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return lstLdapEntry;
	}

	public static void getLdapEntity() throws NamingException {
		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		// specify the LDAP search filter, just users
		// Specify the attributes to return
		// String
		// returnedAtts[]={"userPrincipalName","mail","sn","givenName","sAMAccountName"};
		// searchCtls.setReturningAttributes(returnedAtts);
		String searchFilter = "(sAMAccountName=LAMYT)";
		// String searchFilter = "(objectclass=person)";
		if (context == null)
			context = InitialLdapContext();
		NamingEnumeration answer = context.search(LDAP_BASE_DN, searchFilter,
				searchCtls);
		// System.out.println(answer);
		while (answer.hasMore()) {
			String vals = answer.next().toString();
			System.out.println(vals);
		}
	}

	public static java.util.List<LdapEntry> getLdapEntityAll()
			throws NamingException, CommunicationException,
			UnknownHostException {
		String searchFilter = "(objectclass=person)";
		return getLdapEntity(searchFilter);
	}

	public static LdapEntry getLdapEntityLogin(String userID)
			throws NamingException, CommunicationException,
			UnknownHostException {
		String searchFilter = "(sAMAccountName=" + userID + ")";
		return (LdapEntry) getLdapEntity(searchFilter).get(0);
	}

	public static LdapEntry getLdapEntityName1(String userName)
			throws NamingException, CommunicationException,
			UnknownHostException {
		String searchFilter = "(sn=" + userName + ")";
		return (LdapEntry) getLdapEntity(searchFilter).get(0);
	}

	/*
	 * public static java.util.List<LdapEntry> getLdapEntitySearchSn(String
	 * searchVal) throws
	 * NamingException,CommunicationException,UnknownHostException { String
	 * searchAtts = "sn"; return getLdapEntitySearch(searchAtts, searchVal); }
	 * 
	 * public static java.util.List<LdapEntry> getLdapEntitySearchLogin(String
	 * searchVal) throws
	 * NamingException,CommunicationException,UnknownHostException { String
	 * searchAtts = "sAMAccountName"; return getLdapEntitySearch(searchAtts,
	 * searchVal); }
	 * 
	 * public static java.util.List<LdapEntry> getLdapEntitySearchMail(String
	 * searchVal) throws
	 * NamingException,CommunicationException,UnknownHostException { String
	 * searchAtts = "mail"; return getLdapEntitySearch(searchAtts, searchVal); }
	 * 
	 * public static java.util.List<LdapEntry>
	 * getLdapEntitySearchDescription(String searchVal) throws
	 * NamingException,CommunicationException,UnknownHostException { String
	 * searchAtts = "description"; return getLdapEntitySearch(searchAtts,
	 * searchVal); }
	 * 
	 * public static java.util.List<LdapEntry> getLdapEntitySearchManager(String
	 * searchVal) throws
	 * NamingException,CommunicationException,UnknownHostException { String
	 * searchAtts = "manager"; return getLdapEntitySearch(searchAtts,
	 * searchVal); }
	 */

	/*
	 * public static java.util.List<LdapEntry> getLdapEntitySearch(String
	 * searchAtts, String searchVal) throws
	 * NamingException,CommunicationException,UnknownHostException {
	 * 
	 * java.util.List<LdapEntry> ldapEntityAllSn = null;
	 * 
	 * boolean blnStartsWith = false; boolean blnEndsWith = false; String unlc =
	 * searchVal;
	 * 
	 * if ((unlc.startsWith("%"))&&(unlc.endsWith("%"))) { blnStartsWith = true;
	 * blnEndsWith = true; unlc =
	 * searchVal.substring(1,searchVal.length()-1).toLowerCase(); } else if
	 * ((unlc.startsWith("%"))&&(!unlc.endsWith("%"))) { blnStartsWith = true;
	 * blnEndsWith = false; unlc = searchVal.substring(1).toLowerCase(); } else
	 * if ((!unlc.startsWith("%"))&&(unlc.endsWith("%"))) { blnStartsWith =
	 * false; blnEndsWith = true; unlc =
	 * searchVal.substring(0,searchVal.length()-1).toLowerCase(); }
	 * 
	 * //System.out.println("unlc:" + unlc); java.util.List<LdapEntry>
	 * ldapEntityAll = getLdapEntityAll(); for (int
	 * i=0;i<ldapEntityAll.size();i++) { LdapEntry ldapEntry = (LdapEntry)
	 * ldapEntityAll.get(i);
	 * 
	 * String sldapEntry = null;
	 * 
	 * if (searchAtts.equals("sn")) sldapEntry = ldapEntry.getSn(); else if
	 * (searchAtts.equals("sAMAccountName")) sldapEntry =
	 * ldapEntry.getsAMAccountName(); else if (searchAtts.equals("description"))
	 * sldapEntry = ldapEntry.getsAMAccountName(); else if
	 * (searchAtts.equals("manager")) sldapEntry =
	 * ldapEntry.getsAMAccountName(); else if (searchAtts.equals("mail"))
	 * sldapEntry = ldapEntry.getsAMAccountName();
	 * 
	 * if (sldapEntry!=null) { boolean blnLdapEntry = false; String snlc =
	 * sldapEntry.toLowerCase();
	 * 
	 * if ((blnStartsWith)&&(blnEndsWith)) { if (snlc.contains(unlc)) {
	 * blnLdapEntry = true; LdapEntry.println(ldapEntry); } } else if
	 * ((blnStartsWith)&&(!blnEndsWith)) { if (snlc.endsWith(unlc)) {
	 * blnLdapEntry = true; LdapEntry.println(ldapEntry); } } else if
	 * ((!blnStartsWith)&&(blnEndsWith)) { if (snlc.startsWith(unlc)) {
	 * blnLdapEntry = true; LdapEntry.println(ldapEntry); } }
	 * 
	 * if (blnLdapEntry) { if (ldapEntityAllSn==null) ldapEntityAllSn = new
	 * java.util.ArrayList<LdapEntry>(); ldapEntityAllSn.add(ldapEntry); } } }
	 * 
	 * return ldapEntityAllSn; }
	 */

	public static void main(String[] args) throws NamingException,
			CommunicationException, UnknownHostException {

		String login = System.getProperty("user.name");
		System.out.println(login);

		LdapEntry ldapentry = LdapBrowser.getLdapEntityLogin(login);
		out.println(ldapentry);
		/*
		 * LdapEntry ldapentry2 = ldapentry.getLdapEntryManager();
		 * out.println(ldapentry2);
		 */

		// LdapBrowser.getLdapEntity();

		// java.util.List<LdapEntry> ldapEntrys =
		// LdapBrowser.getLdapEntitySearchSn("BA%");
		/*
		 * java.util.List<LdapEntry> ldapEntrys =
		 * LdapBrowser.getLdapEntitySearchManager("BESSI%"); for (int
		 * i=0;i<ldapEntrys.size();i++) { LdapEntry ldapentry =
		 * (LdapEntry)ldapEntrys.get(i); out.println(ldapentry); }
		 */

		/*
		 * LdapEntry ldapentry = null;
		 * 
		 * String name = "WAGER"; //String login2 = "BAUDOUHM"; try { ldapentry
		 * = LdapBrowser.getLdapEntitySimpleName(name); //ldapentry =
		 * LdapBrowser.getLdapEntityLogin(login2); out.println(ldapentry);
		 * } catch (javax.naming.CommunicationException comex) {
		 * System.out.println
		 * ("com.lng.util.ldap.main javax.naming.CommunicationException-->"
		 * +comex.getMessage()); }
		 */
		// java.util.List lsldapentry = LdapBrowser.getLdapEntityAll();

		 //String searchFilter = "(objectclass=person)";
		 //String searchFilter = "(sAMAccountName=QINC1)";
		 //List<LdapEntry> listLdapEntry = LdapBrowser.getLdapEntity(searchFilter);
		 //out.println(listLdapEntry);
		 //out.println("finish");
		
		try {
			Properties props = new Properties();
	        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	        props.put(Context.PROVIDER_URL, PROVIDER_URL);
	        props.put(Context.SECURITY_PRINCIPAL, "QINC1@legal.regn.net");
	        props.put(Context.SECURITY_CREDENTIALS, "scorpions@06");

	        context = new InitialDirContext(props);
	        
	        out.println("ok"); 
	    } catch (Exception e) {
	    	out.println("ko");
	    	out.println(e.getMessage());
	    }

	}

}
