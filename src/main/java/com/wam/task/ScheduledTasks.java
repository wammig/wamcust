package com.wam.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wam.service.WorkFlowManagerService;

import lexis.wamcust.WamcustApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Scheduled(fixedDelay = 600000)
	public void scheduleTaskWithFixedDelay() {

	    try {
			WorkFlowManagerService.main(WamcustApplication.ARGS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	}


}