package com.wam.service;

import static java.lang.System.out;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.wam.model.config.order.AgreementUnitMap;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.stored.packages.LbuAccountPackage;
import com.wam.stored.packages.LbuContentSubPackage;
import com.wam.stored.packages.LbuSourcePackagePackage;


@Service("ContentSubWamService")
@PropertySources({
    @PropertySource(value="classpath:ecmws.properties", ignoreResourceNotFound = true),
    @PropertySource(value="classpath:errors.properties", ignoreResourceNotFound = true)
})
public class ContentSubWamService {

	public final static Logger logger = LoggerFactory.getLogger(ContentSubWamService.class);
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	private Environment env;
	
	@Autowired
	public LbuContentSubPackage lbuContentSubPackage;
	
	@Autowired
	@Qualifier("LbuAccountPackage")
	LbuAccountPackage lbuAccountPackage;
	
	@Autowired
	@Qualifier("LbuSourcePackagePackage")
	public LbuSourcePackagePackage lbuSourcePackagePackage;
	
	@Autowired
	@Qualifier("agreementUnitMap")
	private AgreementUnitMap agreementUnitMap;
	
	/**
	 * LbuContentSubs key in the map
	 */
	public static String lbuContentSubs = "lbuContentSubs";
	
	
	/**
	 * valid one single contentSub line
	 * something like:
	 * 
	 * [cnum=0000137248;clnm=1;clnmo=1;cnam=ABTINTLEXIS360PCKOPT JD;prid=AIK4CKIT1;bdat=20180110;edat=20181231;muid=99;amnt=244.6;spid=(AIK4CPO|AIK4CJD);loid=60898404545;norenew=Y]
	 * 
	 */
	public static String regexContentSub = "([\\[]{1}(clnm=){1}[0-9]{1,5}[;]{1}(clnmo=){1}[0-9]{1,5}[;]{1}(prid=){1}[A-Z0-9]{3,25}[;]{1}(bdat=){1}[19|20]{2}[0-9]{6}[;]{1}(edat=){1}[19|20]{2}[0-9]{6}[;]{1}(muid=)[0-9]{0,10}[;]{1}(norenew=){1}[Y|N][\\]]([\\s]+)?[\\|]?([\\s]+)?)";
	
	
	/**
	 * valid multi contetnSub lines
	 * something like:
	 * 
	 * [clnm=1;clnmo=1;cnam=ABTINTLEXIS360PCKOPT JD;prid=AIK4CKIT1;bdat=20180110;edat=20181231;muid=99;amnt=244.6;spid=(AIK4CPO|AIK4CJD);loid=60898404545;norenew=Y]| 
	 * [clnm=2;clnmo=1;cnam=ABTINTLEXIS360SPOPTIONAF;prid=AIK4CSPAS;bdat=20180110;edat=20181231;muid=99;amnt=105;spid=(AIK4CSPAS);loid=60898404545;norenew=N]
	 * 
	 */
	public static String regexContentSubs = "(" + regexContentSub + "+?)";
	
	public static String regexSpid = "(spid=){1}[(]{1}([A-Z0-9]{3,}[\\|]?)+[)]{1}";
	
	public static String regexSpidTrim = "([^0-9A-Z\\|]+)";
	
	public static String regexCnum = "(cnum=){1}[0-9A-Z]{10,}";
	
	public static String regexCnumTrim = "([^0-9A-Z]+)";
	
	public static String regexClnm = "(clnm=){1}[0-9]{1,5}[;]{1}";
	
	public static String regexClnmTrim = "([^0-9]{1})";
	
	public static String regexClnmo = "(clnmo=){1}[0-9]{1,5}[;]{1}";
	
	public static String regexClnmoTrim = "([^0-9]{1})";
	
	public static String regexCnam = "(cnam=){1}[\\w\\s�-�-.]{0,50}[;]{1}";
	
	public static String regexCnamTrim = "([\\w\\s�-�-.]{0,50})(?=\\;)";
	
	public static String regexCsid = "(csid=){1}[0-9A-Za-z]{3,25}[;]{1}";
	
	public static String regexCsidTrim = "[0-9A-Za-z]{3,25}(?=\\;)";
	
	public static String regexPrid = "(prid=){1}[A-Z0-9]{3,25}[;]{1}";
	
	public static String regexPridTrim = "([A-Z0-9]{3,25})(?=\\;)";
	
	public static String regexBdate = "(bdat=){1}[19|20]{2}[0-9]{6}[;]{1}";
	
	public static String regexBdateTrim = "([^0-9]+)";
	
	public static String regexEdate = "(edat=){1}[19|20]{2}[0-9]{6}[;]{1}";
	
	public static String regexEdateTrim = "([^0-9]+)";
	
	public static String regexMuid = "(muid=){1}[0-9]{0,10}[;]{1}";
	
	public static String regexMuidTrim = "([^0-9]+)";
	
	public static String regexAmnt = "(amnt=){1}([0-9]{1,})([\\.\\,]{1})?([0-9]{1,})?[;]{1}";
	
	public static String regexAmntTrim = "([^0-9\\.\\,]+)";
	
	/**
	 * valid the loid part in one single address
	 * something like : 
	 * 
	 * loid=6089840S01;
	 * 
	 */
	public static String regexLoid = "(loid=){1}[0-9S]{3,25}[;]{1}";
	
	/**
	 * regex helps to remove "loid=" and ";" to extract the number part something like (6089840S01)
	 */ 
	public static String regexLoidTrim = "([^0-9S]+)";
	
	public static String regexLosq = "(losq=){1}[0-9]{1,}[;]{1}";
	
	public static String regexLosqTrim = "([^0-9]{1,})";
	
	public static String regexNoRenew = "(norenew=)[Y|N]";
	
	public static String regexNoRenewTrim = "(norenew=)";
	
	public  Map ContentSubsTransform(Map map) throws Exception
	{
		return this.validTransformContentSubs(map);
	}
	
	/**
	 * valid multi contentSubs from parameters
	 * transform to contentSub objects
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public Map validTransformContentSubs(Map map) throws Exception
	{
		Object csubs = map.get("csub");

		/**
		 * fetch lbuAccount for AuditUserId and lbuSetId
		 */
		
		if(!map.containsKey("uid"))
		{
			throw new Exception("uid not found in map");
		}
		String auditUserId = map.get("uid").toString();
		
		if(!map.containsKey("lbu"))
		{
			throw new Exception("lbu not found in map");
		}
		String lbuSetId = map.get("lbu").toString();
		
		/**
		 * end fetch lbuAccount
		 */

		if(!map.containsKey("cnum"))
		{
			throw new Exception("cnum not found in map");
		}
		
		if(null == map.get("cnum"))
		{
			throw new Exception("cnum is null");
		}

		String contractNum = map.get("cnum").toString();

		/**
		 * something shared for every contentSub line 
		 * for example: locationId, sequence, contract number
		 */

		String locationId = LocationWamService.extractLocationId(map);

		if(locationId == null)
		{
			throw new Exception("loid not found in map");
		}
		
		String addressSeq = LocationWamService.extractAddressSq(map);
		
		if(addressSeq == null)
		{
			throw new Exception("losq not found in map");
		}

		Map<String, LbuContentSubs> returnedMap = new HashMap<String, LbuContentSubs>();
		
		
		if(csubs == null)
		{
			throw new Exception(" csub is not nullable ");
		}
		
		LbuContentSubs lbuContentSubs = new LbuContentSubs();
		
		Pattern csubsPattern = Pattern.compile(ContentSubWamService.regexContentSubs);
		Matcher csubsMatcher = csubsPattern.matcher(csubs.toString());
		
		if(csubsMatcher.matches()) {
			
			/**
			 * define the regex for each single line
			 */
			Pattern csubPattern = Pattern.compile(ContentSubWamService.regexContentSub);
			Matcher csubMatcher = csubPattern.matcher(csubs.toString());
			
			while(csubMatcher.find()) 
			{
				/**
				 * this collections stock each contentSub line duplicated by spid 
				 * example: spid=(AIK4CPO|AIK4CJD|AIK4CSPAS) it generates 3 lines of contentSubs
				 */
				LbuContentSubs lbuContentSubsLine = new LbuContentSubs();
				
				String csubLine  = csubMatcher.group(1);
				
				/**
				 * extract spid (LBU_SOURCE_PACKAGE_ID)
				 */
				/*
				Pattern spidPattern = Pattern.compile(ContentSubWamService.regexSpid);
				Matcher spidMatcher = spidPattern.matcher(csubLine);

				if(spidMatcher.find()) 
				{
					String spids = spidMatcher.group(0).toString().replaceAll(ContentSubWamService.regexSpidTrim, "");
					String[] spidsArray = spids.split("\\|");
					

					for(String spid : spidsArray) 
					{
						LbuContentSub lbuContentSub = new LbuContentSub();
						lbuContentSub.setLbuSourcePackageId(spid);
						lbuContentSubsLine.getList().add(lbuContentSub);
					}
				}
			    */
				/**
				 * end extract spid
				 */
				lbuContentSubsLine.getList().add(new LbuContentSub());
				
				/**
				 * extract AccountId.
				 * set account id for each contentSub.
				 * because each contentSub is linked to an account id.
				 */
				for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
				{
					lbuContentSub.setLbuAccountId(map.get("aid").toString());
					lbuContentSub.setAuditUserId(auditUserId);
					lbuContentSub.setLbuSetId(lbuSetId);
				}	
				/**
				 * end extract AccountId 
				 */
				
				
				/**
				 * extract cnum (CONTRACT_NUM)
				 */
				/*
				Pattern cnumPattern = Pattern.compile(ContentSubWamService.regexCnum);
				Matcher cnumMatcher = cnumPattern.matcher(csubLine);
				
				if(cnumMatcher.find())
				{
					String cnum = cnumMatcher.group(0).toString().replaceAll(ContentSubWamService.regexCnumTrim, "");
					
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setContractNum(cnum);
					}					
				}
				*/
				/**
				 * end extract cnum
				 */
				for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
				{
					lbuContentSub.setContractNum(contractNum);
				}	
				
				
				/**
				 * extract clnm
				 */
				Pattern clnmPattern = Pattern.compile(ContentSubWamService.regexClnm);
				Matcher clnmMatcher = clnmPattern.matcher(csubLine);
				
				if(clnmMatcher.find())
				{
					String clnm = clnmMatcher.group(0).toString().replaceAll(ContentSubWamService.regexClnmTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setContractLineNum(Integer.parseInt(clnm));
					}
				}
				/**
				 * end extract clnm
				 */
				
				/**
				 * extract clnmo
				 */
				Pattern clnmoPattern = Pattern.compile(ContentSubWamService.regexClnmo);
				Matcher clnmoMatcher = clnmoPattern.matcher(csubLine);
				
				if(clnmoMatcher.find())
				{
					String clnmo = clnmoMatcher.group(0).toString().replaceAll(ContentSubWamService.regexClnmoTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setContractLineNumOrigin(Integer.parseInt(clnmo));
					}
				}
				/**
				 * end extract clnmo
				 */
				
				
				/**
				 * extract cnam
				 */
/*
				Pattern cnamPattern = Pattern.compile(ContentSubWamService.regexCnam);
				Matcher cnamMatcher = cnamPattern.matcher(csubLine);
				
				if(cnamMatcher.find())
				{
					Pattern cnamTrimPattern = Pattern.compile(ContentSubWamService.regexCnamTrim);
					Matcher cnamTrimMatcher = cnamTrimPattern.matcher(cnamMatcher.group(0).toString());
					
					if(cnamTrimMatcher.find())
					{
						String cnam = cnamTrimMatcher.group(0).toString();
						for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
						{
							lbuContentSub.setContentSubName(cnam);
						}
					}
				}
*/
				/**
				 * end extract cnam
				 */
				
				
				/**
				 * extract csid
				 */
				/*
				Pattern csidPattern = Pattern.compile(ContentSubWamService.regexCsid);
				Matcher csidMatcher = csidPattern.matcher(csubLine);
				
				if(csidMatcher.find())
				{
					Pattern csidTrimPattern = Pattern.compile(ContentSubWamService.regexCsidTrim);
					Matcher csidTrimMatcher = csidTrimPattern.matcher(csidMatcher.group(0).toString());
					
					if(csidTrimMatcher.find())
					{
						String csid = csidTrimMatcher.group(0).toString();
						for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
						{
							// to do
							// we set the LbuContentSubId like this for the moment
							// because LbuContentSubId is unique. we can't insert the same LbuContentSubId the second time
							lbuContentSub.setLbuContentSubId(csid+lbuContentSub.getLbuSourcePackageId());
						}
					}
				}
				*/
				/**
				 * end extract csid
				 */

				
				/**
				 * extract prid
				 */
				Pattern pridPattern = Pattern.compile(ContentSubWamService.regexPrid);
				Matcher pridMatcher = pridPattern.matcher(csubLine);
				
				if(pridMatcher.find())
				{
					Pattern pridTrimPattern = Pattern.compile(ContentSubWamService.regexPridTrim);
					Matcher pridTrimMatcher = pridTrimPattern.matcher(pridMatcher.group(0).toString());
					
					if(pridTrimMatcher.find())
					{
						String prid = pridTrimMatcher.group(0).toString();
						for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
						{
							lbuContentSub.setLbuSourcePackageId(prid);
						}
					}
				}
				/**
				 * end extract prid
				 */
				
				
				/**
				 * extract bdat 
				 */
				Pattern bdatPattern = Pattern.compile(ContentSubWamService.regexBdate);
				Matcher bdatMatcher = bdatPattern.matcher(csubLine);
				
				if(bdatMatcher.find())
				{
					String bdat = bdatMatcher.group(0).toString().replaceAll(ContentSubWamService.regexBdateTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setBeginDate(bdat);
					}
				}
				/**
				 * extract bdat
				 */
				
				
				/**
				 * extract edat 
				 */
				Pattern edatPattern = Pattern.compile(ContentSubWamService.regexEdate);
				Matcher edatMatcher = edatPattern.matcher(csubLine);
				
				if(edatMatcher.find())
				{
					String edat = edatMatcher.group(0).toString().replaceAll(ContentSubWamService.regexEdateTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setEndDate(edat);
					}
				}
				/**
				 * extract edat
				 */
				
				
				
				/**
				 * extract muid 
				 */
				Pattern muidPattern = Pattern.compile(ContentSubWamService.regexMuid);
				Matcher muidMatcher = muidPattern.matcher(csubLine);
				
				if(muidMatcher.find())
				{
					String muid = muidMatcher.group(0).toString().replaceAll(ContentSubWamService.regexMuidTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setMaxUsers(Integer.parseInt(muid));
					}
				}
				/**
				 * extract muid
				 */
				
				
				/**
				 * extract amnt 
				 */
				/*
				Pattern amntPattern = Pattern.compile(ContentSubWamService.regexAmnt);
				Matcher amntMatcher = amntPattern.matcher(csubLine);
				
				if(amntMatcher.find())
				{
					String amnt = amntMatcher.group(0).toString().replaceAll(ContentSubWamService.regexAmntTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setMonthlyCommitment(Float.parseFloat(amnt));
						lbuContentSub.setMonthlyCap(Float.parseFloat(amnt));
					}
				}
				*/
				/**
				 * end extract amnt
				 */
				for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
				{
					lbuContentSub.setMonthlyCommitment(Float.parseFloat("0"));
					lbuContentSub.setMonthlyCap(Float.parseFloat("0"));
				}
				
				/**
				 * extract loid
				 */
				/*
				Pattern loidPattern = Pattern.compile(ContentSubWamService.regexLoid);
				Matcher loidMatcher = loidPattern.matcher(csubLine);
				
				if(loidMatcher.find())
				{
					String loid = loidMatcher.group(0).toString().replaceAll(ContentSubWamService.regexLoidTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setLbuLocationId(loid);
					}
				}
				*/
				/**
				 * end extract loid
				 */
				
				
				/**
				 * extract losq
				 */
				/*
				Pattern losqPattern = Pattern.compile(ContentSubWamService.regexLosq);
				Matcher losqMatcher = losqPattern.matcher(csubLine);
				
				if(losqMatcher.find())
				{
					String losq = losqMatcher.group(0).toString().replaceAll(ContentSubWamService.regexLosqTrim, "");
					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setAddressSeq(Integer.parseInt(losq));
					}
				}
				*/
				/**
				 * end extract losq
				 */
				for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
				{
					lbuContentSub.setLbuLocationId(locationId);
					lbuContentSub.setAddressSeq(Integer.parseInt(addressSeq));
				}
				
				
				/**
				 * extract norenew
				 */
				Pattern norenewPattern = Pattern.compile(ContentSubWamService.regexNoRenew);
				Matcher norenewMatcher = norenewPattern.matcher(csubLine);
				
				if(norenewMatcher.find())
				{
					String norenew = norenewMatcher.group(0).toString().replaceAll(ContentSubWamService.regexNoRenewTrim, "");

					for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
					{
						lbuContentSub.setNoReNew(norenew.toUpperCase());
					}
				}
				/**
				 * end extract norenew
				 */
				
				
				/**
				 * copy contractNum for agreementPguid
				 */
				for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
				{
					if(lbuContentSub.getAgreementPguid() == null)
					{
						//out.println(agreementUnitMap.getMap());
						if(agreementUnitMap.getMap().containsKey(lbuContentSub.getLbuSetId()))
						{
							lbuContentSub.setAgreementPguid(agreementUnitMap.getMap().get(lbuContentSub.getLbuSetId()).getPrefix() + lbuContentSub.getContractNum());
						} else {
							throw new Exception(lbuContentSub.getLbuSetId() + " not found in order beans context xml 598 ");
						}
					}
				}
				
				
				
				for(LbuContentSub lbuContentSub : lbuContentSubsLine.getList())
				{
					/*
					for(LbuContentSub lbuContentSubInList : lbuContentSubs.getList())
					{
						lbuContentSub = this.compareLbuContentSub(lbuContentSubInList, lbuContentSub);
					}
					*/
					lbuContentSubs.getList().add(lbuContentSub);
				}
				
			}
		} else {
			/**
			 * if the input multi contentSubs invalid
			 * we check which part broken and tell the problem.  
			 */
			
			/**
			 *  we extract the invalid contentSub part
			 */
			String invalidCsubs = csubs.toString().replaceAll(ContentSubWamService.regexContentSub, "");
			
			out.println(" invalid Csubs: ");
			out.println(invalidCsubs);
			
			
			/**
			 * check if it begins with "[" or not
			 */
			String checkRegexBegin = "^[\\[]{1}";
			Pattern checkRegexBeginPattern = Pattern.compile(checkRegexBegin);
			Matcher checkRegexBeginMatcher = checkRegexBeginPattern.matcher(invalidCsubs);

			if(!checkRegexBeginMatcher.find())
			{ 

				String failedCheckRegexBegin = "^(.){1}";
				Pattern failedCheckRegexBeginPattern = Pattern.compile(failedCheckRegexBegin);
				Matcher failedCheckRegexBeginMatcher = failedCheckRegexBeginPattern.matcher(invalidCsubs);
				String message = " an contentSub should begin with \" [ \" " ;
				if(failedCheckRegexBeginMatcher.find()) 
				{
					String invalidPart = failedCheckRegexBeginMatcher.group(0);
					message += ", but we found an contentSub begin with something like \""+ invalidPart +"\" ";
				}
				
				throw new Exception(message);
			}
			
			
			/**
			 * check if it contains "cnum=" or not
			 */
			/*
			String checkRegexCnum = checkRegexBegin + "(cnum=){1}";
			Pattern checkRegexCnumPattern = Pattern.compile(checkRegexCnum);
			Matcher checkRegexCnumMatcher = checkRegexCnumPattern.matcher(invalidCsubs);
			
			if(!checkRegexCnumMatcher.find())
			{ 

				String failedCheckRegexCnum = checkRegexBegin + "[\\W\\w]{5}";
				Pattern failedCheckRegexCnumPattern = Pattern.compile(failedCheckRegexCnum);
				Matcher failedCheckRegexCnumMatcher = failedCheckRegexCnumPattern.matcher(invalidCsubs);
				String message = " an contentSub should contain \" cnum= \" after the beginning \"[\" " ;
				if(failedCheckRegexCnumMatcher.find()) 
				{
					String invalidPart = failedCheckRegexCnumMatcher.group(0);
					message += ", but we found something like \""+ invalidPart +"\" after the beginning \"[\" ";
				}
				
				throw new Exception(message);
			}
			*/
			
			/**
			 * check if it contains "cnum=x;" or not
			 */
			/*
			String checkRegexCnumData = checkRegexCnum + "[0-9A-Z]{5,25}[;]{1}";
			Pattern checkRegexCnumDataPattern = Pattern.compile(checkRegexCnumData);
			Matcher checkRegexCnumDataMatcher = checkRegexCnumDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexCnumDataMatcher.find())
			{ 

				String failedCheckRegexCnumData = checkRegexCnum + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexCnumDataPattern = Pattern.compile(failedCheckRegexCnumData);
				Matcher failedCheckRegexCnumDataMatcher = failedCheckRegexCnumDataPattern.matcher(invalidCsubs);
				String message = " a contetnSub should contain \" cnum=x; (x is a Uppercase String between 5 letters and 25 letters, Example: cnum=99000010AILSPOM879; ) \" after the beginning \"[\" " ;

				if(failedCheckRegexCnumDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexCnumDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart + " \" after the beginning \"[\" ";
				}
				
				throw new Exception(message);
			}
			*/
			
			/**
			 * check if it contains "clnm=" or not
			 */
			String checkRegexClnm = checkRegexBegin + "(clnm=){1}";
			Pattern checkRegexClnmPattern = Pattern.compile(checkRegexClnm);
			Matcher checkRegexClnmMatcher = checkRegexClnmPattern.matcher(invalidCsubs);
			
			if(!checkRegexClnmMatcher.find())
			{ 

				String failedCheckRegexClnm = checkRegexBegin + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexClnmPattern = Pattern.compile(failedCheckRegexClnm);
				Matcher failedCheckRegexClnmMatcher = failedCheckRegexClnmPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" clnm= \" after the parameter cnum " ;
				if(failedCheckRegexClnmMatcher.find()) 
				{
					String invalidPart = failedCheckRegexClnmMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexBegin, "") +"\" after the parameter cnum ";
				}
				
				throw new Exception(message);
			}
			
			
			
			
			/**
			 * check if it contains "clnm=x;" or not
			 */
			String checkRegexClnmData = checkRegexClnm + "[0-9]{1,5}[;]{1}";
			Pattern checkRegexClnmDataPattern = Pattern.compile(checkRegexClnmData);
			Matcher checkRegexClnmDataMatcher = checkRegexClnmDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexClnmDataMatcher.find())
			{ 

				String failedCheckRegexClnmData = checkRegexClnm + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexClnmDataPattern = Pattern.compile(failedCheckRegexClnmData);
				Matcher failedCheckRegexClnmDataMatcher = failedCheckRegexClnmDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" clnm=x; (x is an Integer number at most 5 figures) \" after the parameter cnum";
				
				if(failedCheckRegexClnmDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexClnmDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexBegin, "") +"\" (clnm should be an Integer number at most 5 figures and end by \";\") ";
				}
				
				throw new Exception(message);
			}
			
			
			/**
			 * check if it contains "clnmo=" or not
			 */
			String checkRegexClnmo = checkRegexClnmData + "(clnmo=){1}";
			Pattern checkRegexClnmoPattern = Pattern.compile(checkRegexClnmo);
			Matcher checkRegexClnmoMatcher = checkRegexClnmoPattern.matcher(invalidCsubs);
			
			if(!checkRegexClnmoMatcher.find())
			{ 

				String failedCheckRegexClnmo = checkRegexClnmData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexClnmoPattern = Pattern.compile(failedCheckRegexClnmo);
				Matcher failedCheckRegexClnmoMatcher = failedCheckRegexClnmoPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" clnmo= \" after the parameter clnm " ;
				if(failedCheckRegexClnmoMatcher.find()) 
				{
					String invalidPart = failedCheckRegexClnmoMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexClnmData, "") +"\" after the parameter clnm ";
				}
				
				throw new Exception(message);
			}
			
			
			/**
			 * check if it contains "clnmo=x;" or not
			 */
			String checkRegexClnmoData = checkRegexClnmo + "[0-9]{1,5}[;]{1}";
			Pattern checkRegexClnmoDataPattern = Pattern.compile(checkRegexClnmoData);
			Matcher checkRegexClnmoDataMatcher = checkRegexClnmoDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexClnmoDataMatcher.find())
			{ 

				String failedCheckRegexClnmoData = checkRegexClnmo + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexClnmoDataPattern = Pattern.compile(failedCheckRegexClnmoData);
				Matcher failedCheckRegexClnmoDataMatcher = failedCheckRegexClnmoDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" clnmo=x; (x is an Integer number at most 5 figures) \" after the parameter clnm";
				
				if(failedCheckRegexClnmoDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexClnmoDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexClnmoData, "") +"\" (clnmo should be an Integer number at most 5 figures and end by \";\") ";
				}
				
				throw new Exception(message);
			}
			
			
			
			/**
			 * check if it contains "cnam=" or not
			 */
			/*
			String checkRegexCnam = checkRegexClnmoData + "(cnam=){1}";
			Pattern checkRegexCnamPattern = Pattern.compile(checkRegexCnam);
			Matcher checkRegexCnamMatcher = checkRegexCnamPattern.matcher(invalidCsubs);

			if(!checkRegexCnamMatcher.find())
			{ 
				String failedCheckRegexCnam = checkRegexClnmoData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexCnamPattern = Pattern.compile(failedCheckRegexCnam);
				Matcher failedCheckRegexCnamMatcher = failedCheckRegexCnamPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" cnam= \" after the parameter clnmo " ;
				if(failedCheckRegexCnamMatcher.find()) 
				{
					String invalidPart = failedCheckRegexCnamMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexClnmoData, "") +"\" after the parameter clnmo ";
				}
				
				throw new Exception(message);
			}
			*/
			
			/**
			 * check if it contains "cnam=x;" or not
			 */
			/*
			String checkRegexCnamData = checkRegexCnam + "[\\w\\s�-�-.]{0,50}[;]{1}";
			Pattern checkRegexCnamDataPattern = Pattern.compile(checkRegexCnamData);
			Matcher checkRegexCnamDataMatcher = checkRegexCnamDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexCnamDataMatcher.find())
			{ 
				String message = " a contentSub should contain \" cnam=x; (x is a string less than 50 letters) \" after the parameter clnm";
				
				throw new Exception(message);
			}
			
			*/
			
			
			/**
			 * check if it contains "csid=" or not
			 */
			/*
			String checkRegexCsid = checkRegexClnmoData + "(csid=){1}";
			Pattern checkRegexCsidPattern = Pattern.compile(checkRegexCsid);
			Matcher checkRegexCsidMatcher = checkRegexCsidPattern.matcher(invalidCsubs);
			
			if(!checkRegexCsidMatcher.find())
			{ 

				String failedCheckRegexCsid = checkRegexClnmoData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexCsidPattern = Pattern.compile(failedCheckRegexCsid);
				Matcher failedCheckRegexCsidMatcher = failedCheckRegexCsidPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" csid= \" after the parameter cnam " ;
				if(failedCheckRegexCsidMatcher.find()) 
				{
					String invalidPart = failedCheckRegexCsidMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexClnmoData, "") +"\" after the parameter cnam ";
				}
				
				throw new Exception(message);
			}
			*/
			
			
			/**
			 * check if it contains "csid=x;" or not
			 */
			/*
			String checkRegexCsidData = checkRegexCsid + "[0-9A-Za-z]{3,25}[;]{1}";
			Pattern checkRegexCsidDataPattern = Pattern.compile(checkRegexCsidData);
			Matcher checkRegexCsidDataMatcher = checkRegexCsidDataPattern.matcher(invalidCsubs);
			
			out.println(invalidCsubs);
			if(!checkRegexCsidDataMatcher.find())
			{ 
				String message = " a contentSub should contain \" csid=x; (x is a string less than 25 letters) \" after the parameter cnam";
				
				throw new Exception(message);
			}
			*/
			
			
			
			/**
			 * check if it contains "prid=" or not
			 */
			String checkRegexPrid = checkRegexClnmoData + "(prid=){1}";
			Pattern checkRegexPridPattern = Pattern.compile(checkRegexPrid);
			Matcher checkRegexPridMatcher = checkRegexPridPattern.matcher(invalidCsubs);
			
			if(!checkRegexPridMatcher.find())
			{ 

				String failedCheckRegexPrid = checkRegexClnmoData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexPridPattern = Pattern.compile(failedCheckRegexPrid);
				Matcher failedCheckRegexPridMatcher = failedCheckRegexPridPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" prid= \" after the parameter clnmo " ;
				
				if(failedCheckRegexPridMatcher.find()) 
				{
					String invalidPart = failedCheckRegexPridMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexClnmoData, "") +"\" after the parameter clnmo ";
				}
				
				throw new Exception(message);
			}
			
			
			
			/**
			 * check if it contains "prid=x" or not
			 */
			String checkRegexPridData = checkRegexPrid + "[A-Z0-9]{3,25}[;]{1}";
			Pattern checkRegexPridDataPattern = Pattern.compile(checkRegexPridData);
			Matcher checkRegexPridDataMatcher = checkRegexPridDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexPridDataMatcher.find())
			{ 

				String failedCheckRegexPridData = checkRegexPrid + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexPridDataPattern = Pattern.compile(failedCheckRegexPridData);
				Matcher failedCheckRegexPridDataMatcher = failedCheckRegexPridDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" prid=x; \" (x contains only uppercase letters and Integer number) after the parameter csid " ;
				
				if(failedCheckRegexPridDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexPridDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexClnmoData, "") +"\" after the parameter csid ";
				}
				
				throw new Exception(message);
			}
			
			
			
			/**
			 * check if it contains "bdat=" or not
			 */
			String checkRegexBdat = checkRegexPridData + "(bdat=){1}";
			Pattern checkRegexBdatPattern = Pattern.compile(checkRegexBdat);
			Matcher checkRegexBdatMatcher = checkRegexBdatPattern.matcher(invalidCsubs);
			
			if(!checkRegexBdatMatcher.find())
			{ 

				String failedCheckRegexBdat = checkRegexPridData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexBdatPattern = Pattern.compile(failedCheckRegexBdat);
				Matcher failedCheckRegexBdatMatcher = failedCheckRegexBdatPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" bdat= \" after the parameter prid " ;
				
				if(failedCheckRegexBdatMatcher.find()) 
				{
					String invalidPart = failedCheckRegexBdatMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexPridData, "") +"\" after the parameter prid ";
				}
				
				throw new Exception(message);
			}
			
			
			/**
			 * check if it contains "bdat=yyyymmdd" or not
			 */
			String checkRegexBdatData = checkRegexBdat + "[19|20]{2}[0-9]{6}[;]{1}";
			Pattern checkRegexBdatDataPattern = Pattern.compile(checkRegexBdatData);
			Matcher checkRegexBdatDataMatcher = checkRegexBdatDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexBdatDataMatcher.find())
			{ 

				String failedCheckRegexBdatData = checkRegexBdat + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexBdatDataPattern = Pattern.compile(failedCheckRegexBdatData);
				Matcher failedCheckRegexBdatDataMatcher = failedCheckRegexBdatDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" bdat=yyyymmdd; \" after the parameter prid " ;
				
				if(failedCheckRegexBdatDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexBdatDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexPridData, "") +"\" after the parameter prid ";
				}
				
				throw new Exception(message);
			}
			
			
			/**
			 * check if it contains "edat=" or not
			 */
			String checkRegexEdat = checkRegexBdatData + "(edat=){1}";
			Pattern checkRegexEdatPattern = Pattern.compile(checkRegexEdat);
			Matcher checkRegexEdatMatcher = checkRegexEdatPattern.matcher(invalidCsubs);
			
			if(!checkRegexEdatMatcher.find())
			{ 

				String failedCheckRegexEdat = checkRegexBdatData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexEdatPattern = Pattern.compile(failedCheckRegexEdat);
				Matcher failedCheckRegexEdatMatcher = failedCheckRegexEdatPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" edat= \" after the parameter bdat " ;
				
				if(failedCheckRegexEdatMatcher.find()) 
				{
					String invalidPart = failedCheckRegexEdatMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexBdatData, "") +"\" after the parameter bdat ";
				}
				
				throw new Exception(message);
			}
			
			
			
			
			/**
			 * check if it contains "edat=yyyymmdd" or not
			 */
			String checkRegexEdatData = checkRegexEdat + "[19|20]{2}[0-9]{6}[;]{1}";
			Pattern checkRegexEdatDataPattern = Pattern.compile(checkRegexEdatData);
			Matcher checkRegexEdatDataMatcher = checkRegexEdatDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexEdatDataMatcher.find())
			{ 

				String failedCheckRegexEdatData = checkRegexEdat + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexEdatDataPattern = Pattern.compile(failedCheckRegexEdatData);
				Matcher failedCheckRegexEdatDataMatcher = failedCheckRegexEdatDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" edat=yyyymmdd; \" after the parameter bdat " ;
				
				if(failedCheckRegexEdatDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexEdatDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexBdatData, "") +"\" after the parameter bdat ";
				}
				
				throw new Exception(message);
			}
			
			
			
			/**
			 * check if it contains "muid=x" or not
			 */
			String checkRegexMuidData = checkRegexEdatData + "(muid=){1}[0-9]{0,10}[;]{1}";
			Pattern checkRegexMuidDataPattern = Pattern.compile(checkRegexMuidData);
			Matcher checkRegexMuidDataMatcher = checkRegexMuidDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexMuidDataMatcher.find())
			{ 

				String failedCheckRegexMuidData = checkRegexEdatData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexMuidDataPattern = Pattern.compile(failedCheckRegexMuidData);
				Matcher failedCheckRegexMuidDataMatcher = failedCheckRegexMuidDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" muid=x; (x is an Integer number) \" after the parameter edat " ;
				
				if(failedCheckRegexMuidDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexMuidDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexEdatData, "") +"\" after the parameter edat ";
				}
				
				throw new Exception(message);
			}
			
			
			
			/**
			 * check if it contains "amnt=x" or not
			 */
			/*
			String checkRegexAmntData = checkRegexMuidData + "(amnt=){1}[0-9\\.]{1,}[;]{1}";
			Pattern checkRegexAmntDataPattern = Pattern.compile(checkRegexAmntData);
			Matcher checkRegexAmntDataMatcher = checkRegexAmntDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexAmntDataMatcher.find())
			{ 

				String failedCheckRegexAmntData = checkRegexMuidData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexAmntDataPattern = Pattern.compile(failedCheckRegexAmntData);
				Matcher failedCheckRegexAmntDataMatcher = failedCheckRegexAmntDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" amnt=x; (x is a float number something like 96.35) \" after the parameter muid " ;
				
				if(failedCheckRegexAmntDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexAmntDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexMuidData, "") +"\" after the parameter muid ";
				}
				
				throw new Exception(message);
			}
			*/
			
			
			/**
			 * check if it contains "spid=(x|y|z)" or not
			 */
			/*
			String checkRegexSpidData = checkRegexMuidData + "(spid=){1}[(]{1}([A-Z0-9]{3,}[\\|]?)+[)]{1}[;]{1}";
			Pattern checkRegexSpidDataPattern = Pattern.compile(checkRegexSpidData);
			Matcher checkRegexSpidDataMatcher = checkRegexSpidDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexSpidDataMatcher.find())
			{ 

				String failedCheckRegexSpidData = checkRegexMuidData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexSpidDataPattern = Pattern.compile(failedCheckRegexSpidData);
				Matcher failedCheckRegexSpidDataMatcher = failedCheckRegexSpidDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" spid=(x|y|z); (x is a uppercase string something like ) \" after the parameter muid " ;
				
				if(failedCheckRegexSpidDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexSpidDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexMuidData, "") +"\" after the parameter muid ";
				}
				
				throw new Exception(message);
			}
			*/
			
			/**
			 * check if it contains "loid=x" or not
			 */
			/*
			String checkRegexLoidData = checkRegexMuidData + "(loid=){1}[0-9S]{3,25}[;]{1}";
			Pattern checkRegexLoidDataPattern = Pattern.compile(checkRegexLoidData);
			Matcher checkRegexLoidDataMatcher = checkRegexLoidDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexLoidDataMatcher.find())
			{ 

				String failedCheckRegexLoidData = checkRegexMuidData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexLoidDataPattern = Pattern.compile(failedCheckRegexLoidData);
				Matcher failedCheckRegexLoidDataMatcher = failedCheckRegexLoidDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" loid=x; (x is a string something like 1100308) \" after the parameter muid " ;
				
				if(failedCheckRegexLoidDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLoidDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexMuidData, "") +"\" after the parameter muid ";
				}
				
				throw new Exception(message);
			}
			*/
			/**
			 * check if it contains "losq=x" or not
			 */
			/*
			String checkRegexLosqData = checkRegexLoidData + "(losq=){1}[0-9]{1,}[;]{1}";
			Pattern checkRegexLosqDataPattern = Pattern.compile(checkRegexLosqData);
			Matcher checkRegexLosqDataMatcher = checkRegexLosqDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexLosqDataMatcher.find())
			{ 

				String failedCheckRegexLosqData = checkRegexLoidData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexLosqDataPattern = Pattern.compile(failedCheckRegexLosqData);
				Matcher failedCheckRegexLosqDataMatcher = failedCheckRegexLosqDataPattern.matcher(invalidCsubs);
				String message = " a contentSub should contain \" losq=x; (x is an integer number something like 1) \" after the parameter loid " ;
				
				if(failedCheckRegexLosqDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLosqDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexLoidData, "") +"\" after the parameter loid ";
				}
				
				throw new Exception(message);
			}
			*/
			/**
			 * check if it contains "renew=Y|N" or not
			 */
			String checkRegexRenewData = checkRegexMuidData + "(renew=){1}[Y|N]";
			Pattern checkRegexRenewDataPattern = Pattern.compile(checkRegexRenewData);
			Matcher checkRegexRenewDataMatcher = checkRegexRenewDataPattern.matcher(invalidCsubs);
			
			if(!checkRegexRenewDataMatcher.find())
			{ 
				String failedCheckRegexRenewData = checkRegexMuidData + "([^;]*[;])?([^\\]]*[\\]])?";
				Pattern failedCheckRegexRenewDataPattern = Pattern.compile(failedCheckRegexRenewData);
				Matcher failedCheckRegexRenewDataMatcher = failedCheckRegexRenewDataPattern.matcher(invalidCsubs);
				String message = env.getProperty("contentSub.renew.error.message1") ;
				
				if(failedCheckRegexRenewDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexRenewDataMatcher.group(0);
					message += MessageFormat.format(env.getProperty("contentSub.renew.error.message2"), invalidPart.replaceAll(checkRegexMuidData, ""));
				}
				
				throw new Exception(message);
			}
			
			
			
			
			/**
			 * check the end
			 */
			String endCheckRegex = this.regexContentSub;
			invalidCsubs = invalidCsubs.replaceAll(endCheckRegex, "");
			
			Pattern endCheckRegexPattern = Pattern.compile(endCheckRegex);
			Matcher endCheckRegexMatcher = endCheckRegexPattern.matcher(invalidCsubs);
						
			if(!endCheckRegexMatcher.find())
			{
				
				String failedCheckRegexEnd = checkRegexRenewData;
				Pattern failedCheckRegexEndPattern = Pattern.compile(failedCheckRegexEnd);
				Matcher failedCheckRegexEndMatcher = failedCheckRegexEndPattern.matcher(invalidCsubs);
				String message = " a contentSub should end with \" ]\" after the parameter renew " ;
				
				if(failedCheckRegexEndMatcher.lookingAt()) 
				{
					message += ", but we found something like \""+ invalidCsubs.replaceAll(checkRegexRenewData, "")  +"\" after the parameter losq ";
				}
				throw new Exception(message);
			}
			
			
			
			String message = " csub is invalid. "
					+ "Example: \r\n" 
					+ env.getProperty("contentSubs.example")
					;
			throw new Exception(message);
		}
		
		returnedMap.put(ContentSubWamService.lbuContentSubs, lbuContentSubs);
		
		return returnedMap;
	}
	
	/**
	 * insert contentSub into table LBU_CONTENTSUB
	 * 
	 * @param lbuContentSub
	 * @return
	 * @throws Exception
	 */
	public LbuContentSub insertLbuContentSub(LbuContentSub lbuContentSub) throws Exception
	{
		return lbuContentSubPackage.insertLbuContentSub(lbuContentSub);
	}
	
	/**
	 * 
	 * @param lbuContentSub
	 * @param sessionId
	 * @return
	 * @throws Exception
	 */
	public LbuContentSub updateLbuContentSub(LbuContentSub lbuContentSub, String sessionId) throws Exception
	{
		return lbuContentSubPackage.updateLbuContentSub(lbuContentSub, sessionId);
	}
	
	/**
	 * call stored procedure to update agreementId
	 * @param contentSubId
	 * @param agreementId
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> updateLbuContentSubAgreementId(String contentSubId, String agreementId) throws Exception
	{		
		return lbuContentSubPackage.updateLbuContentSubAgreementId(contentSubId, agreementId);
	}
	
	/**
	 * 
	 * @param setId
	 * @param accountId
	 * @param contractNum
	 * @param contractLineNum
	 * @param lbuContentSubId
	 * @param gbsContentSubId
	 * @param packageId
	 * @param contentSubName
	 * @param salesRepId
	 * @param auditUserId
	 * @param beginDate
	 * @param endDate
	 * @param sourcePackageId
	 * @return
	 * @throws Exception
	 */
	public LbuContentSubs lbuContentSubSearch(
			 String setId,
			 String accountId,
			 String contractNum,
			 String contractLineNum,
			 String lbuContentSubId,
			 String gbsContentSubId,
			 String packageId,
			 String contentSubName,
			 String salesRepId,
			 String auditUserId,
			 String beginDate,
			 String endDate,
			 String sourcePackageId,
			 String assetAgreementId
			) throws Exception
	{
		return lbuContentSubPackage.lbuContentSubSearch(setId, accountId, contractNum, contractLineNum, lbuContentSubId, gbsContentSubId, packageId, contentSubName, salesRepId, auditUserId, beginDate, endDate, sourcePackageId, assetAgreementId);
	}
	
	/**
	 * 
	 * @param setId
	 * @param contentSubId
	 * @return
	 * @throws Exception
	 */
	public LbuContentSub LbuContentSubSelectById(
			 String accountId,
			 String contractNum,
			 Integer contractLineNum,
			 String sourcePackageId
			 ) throws Exception
	{
		return lbuContentSubPackage.LbuContentSubSelectById(accountId, contractNum, contractLineNum, sourcePackageId);
	}
	
	/**
	 * insert lbuContentSubs if not found in db.
	 * update if found in db.
	 * 
	 * Why pass lbuAccount for lbuContentSubs
	 * because we need to check if all lbuContentSubs belong to the lbuAccount
	 * reject If lbuContentSubs occupied by other lbuAccount
	 * 
	 * 
	 * @param lbuContentSubs
	 * @param lbuAccount
	 * @throws Exception
	 */
	public LbuContentSubs handleLbuContentSubs(LbuContentSubs lbuContentSubs, LbuAccount lbuAccount) throws Exception
	{
		try {
		LbuContentSub returnedLbuContentSub;
		LbuContentSubs returnedLbuContentSubs = new LbuContentSubs();
		returnedLbuContentSubs.setList(new ArrayList<LbuContentSub>());
		/**
		 * loop lbuContentSubs
		 */
		for(LbuContentSub lbuContentSub : lbuContentSubs.getList())
		{
			String assemblyId = lbuSourcePackagePackage.lbuSourcePackageSelectAssemblyId(lbuContentSub.getLbuSetId(), lbuContentSub.getLbuSourcePackageId());
			/**
			 * pass it and handle the next contentSub if assemblyId not found by LbuSourcePackageId
			 */
			if(assemblyId == null)
			{
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(lbuContentSub.getLbuSourcePackageId() + " not found in LBU_SOURCEPACKAGE", ResponseMessage.FAILED));
				
				continue;
				//throw new Exception(lbuContentSub.getLbuSourcePackageId() + " not found in LBU_SOURCEPACKAGE");
			}
			
			/**
			 * fetch LbuContentSub from db
			 */
			LbuContentSub lbuContentSubFromDb = this.lbuContentSubPackage.LbuContentSubSelectById(lbuContentSub.getLbuAccountId(), lbuContentSub.getContractNum(), lbuContentSub.getContractLineNum(), lbuContentSub.getLbuSourcePackageId());
			
			/**
			 * create if the lbuContentSubFromDb not found 
			 * 
			 */
			if(lbuContentSubFromDb == null) {
				/**
				 * insert new contentSub in db
				 */
				returnedLbuContentSub = this.lbuContentSubPackage.insertLbuContentSub(lbuContentSub);
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" The contentSub : " + lbuContentSub.getCombinationIdentifier(true) + " inserted successfully.", ResponseMessage.SUCCESS));
			} else {
				
				if(!lbuContentSubFromDb.getLbuAccountId().equals(lbuAccount.getLbuAccountId()))
				{
					throw new Exception(" lbuContentSub " + lbuContentSubFromDb.getLbuContentSubId() + new ResponseMessage(" is already occupied by account " + lbuContentSubFromDb.getLbuAccountId() + " conflict with account " + lbuAccount.getLbuAccountId(), ResponseMessage.FAILED));
				}
				
				/**
				 *  update
				 */
				returnedLbuContentSub = this.lbuContentSubPackage.updateLbuContentSub(lbuContentSub, null);

				if(returnedLbuContentSub.getIsChanged()) {
					ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" The contentSub : " + lbuContentSub.getCombinationIdentifier(true) + " updated successfully.", ResponseMessage.SUCCESS));
				} else {
					ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" The contentSub : " + lbuContentSub.getCombinationIdentifier(true) + " is already updated. Nothing changed.", ResponseMessage.SUCCESS));
				}
			}
			
			returnedLbuContentSubs.getList().add(returnedLbuContentSub);
		}
		
		return returnedLbuContentSubs;
		
		}
		catch(Exception e)
		{
			out.println(" Exception 1418 : ");
			
			out.println(e.getMessage());
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(e.getMessage(), ResponseMessage.FAILED));
		}
		return null;
	}

}
