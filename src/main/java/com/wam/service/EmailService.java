package com.wam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.utility.definition.Definition;
import com.wam.utility.ldap.LdapBrowser;
import com.wam.utility.ldap.LdapEntry;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.CommunicationException;
import javax.naming.NamingException;

import static java.lang.System.out;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Service("EmailService")
@PropertySource("classpath:workflow-manager.properties")
public class EmailService {

	@Autowired
	private JavaMailSender  mailSender;
	
	@Autowired
    private SimpleMailMessage templateMessage;
	
	@Autowired
    public Environment env;
 
    public Properties workflowProperty;

    public String mailTo;
    
    public String mailFrom;
    
    public ArrayList<?> attachements;
    
    public void setWorkflowProperty(Properties workflowProperty)
    {
        this.workflowProperty = workflowProperty;
    }


    public void setMailSender(JavaMailSender  mailSender) {
        this.mailSender = mailSender;
    }
    
    public JavaMailSender getMailSender() {
        return this.mailSender;
    }

    public void setTemplateMessage(SimpleMailMessage templateMessage) {
        this.templateMessage = templateMessage;
    }
    
    public SimpleMailMessage getTemplateMessage()
    {
    	return this.templateMessage;
    }
    
    public EmailService()
    {
    }
    
    public void setMailTo(String mailTo)
    {
    	this.mailTo = mailTo;
    }
    
    public void setMailFrom(String mailFrom)
    {
    	this.mailFrom = mailFrom;
    }
    
    /**
     * 
     * @return
     * @throws Exception
     */
    public String getMailTo() throws Exception
    {
       if(this.mailTo != null)
       {
    	   return this.mailTo;
       }
    	
       return this.getMailToLdap();
    }
    
    /**
     * search for the mail with ldap by passing the windows login
     * @return
     * @throws Exception 
     */
    public String getMailToLdap() throws Exception
    {
    	/*
    	if(true)
    	{
    		throw new Exception("shit is null ");
    	}
    	*/
    	String login = System.getProperty("user.name");

    	if(login == null)
    	{
    		throw new Exception(" login in windows is null ");
    	}
    	
		LdapEntry ldapentry = LdapBrowser.getLdapEntityLogin(login);
		
		if(ldapentry == null)
		{
			throw new Exception(" ldapentry is null while login in windows is : " + login);
		}
		
		if(ldapentry.getMail() == null)
		{
			throw new Exception(" ldapentry mail is null while login in windows is : " + login);
		}
		
		return ldapentry.getMail();
    }
    
    /**
     * isRealSend
     * check the configuration properties if real send
     * @return
     */
    public Boolean isRealSend()
    {
       if(this.env != null)
       {
           return Boolean.valueOf(this.env.getProperty("workflow.manager.service.handle.real.send"));
       } else {
           return Boolean.valueOf(this.workflowProperty.getProperty("workflow.manager.service.handle.real.send"));
       }
    }
    
    
    
    /**
     * 
     * @return
     */
    public String getMockSubject()
    {
    	return Definition.SUBJECT;
    }
    
    /**
     * 
     * @return
     */
    public String getMockTextContent()
    {
    	return Definition.CONTENT;
    }
    
    /**
     * 
     * @return
     */
    public ArrayList<?> getAttachements()
    {
    	return this.attachements;
    }
    
    /**
     * 
     * @param attachements
     */
    public void setAttachements(ArrayList<?> attachements)
    {
    	this.attachements = attachements;
    }
    
    /**
     *
     * @param from
     * @param to
     * @param subject
     * @param textContent
     * @param attachements
     * @throws MessagingException
     */
    public void sendMail(String from, String to, String subject, String textContent, List<?> attachements) throws MessagingException
    {
       SimpleMailMessage message = new SimpleMailMessage(this.templateMessage);
       message.setSubject(subject);
       message.setTo(to);
       message.setFrom(from);
       message.setText(textContent);

        try{
           this.mailSender.send(message);
           out.println("sendMail messages finished");
        }
        catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
