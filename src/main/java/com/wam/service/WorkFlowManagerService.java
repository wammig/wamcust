package com.wam.service;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.wam.aspect.EcmCustomerHistoryLoggingAspect;
import com.wam.aspect.EcmLocationHistoryLoggingAspect;
import com.wam.model.config.ecm.BusinessUnitMap;
import com.wam.model.config.order.AgreementUnitMap;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.db.gbsd.helper.LogMessages;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.WsMessageHelper;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.packages.LbuAccountPackage;
import com.wam.stored.packages.LbuAgreementPackage;
import com.wam.stored.packages.LbuContentSubPackage;
import com.wam.stored.packages.LbuLocationPackage;
import com.wam.stored.packages.LbuSourcePackagePackage;
import com.wam.stored.packages.LogMessagePackage;
import com.wam.test.db.BaseTest;
import com.wam.utility.definition.Definition;
import com.wam.utility.file.FileHandler;

import static java.lang.System.out;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author QINC1
 * 
 * we need to comment @Service("WorkFlowManagerService")
 * otherwise it will block tomcat when servlet starts
 */
//@Service("WorkFlowManagerService")
public class WorkFlowManagerService extends BaseService {

	public final static Logger logger = LoggerFactory.getLogger(WorkFlowManagerService.class);
	
	@Value("${ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.ValidAddressFlag}")
	public String validAddressFlag;
	
	public Boolean isRealSendWs = true;
	public Boolean isRealSendMail = true;
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	@Qualifier("LogMessagePackage")
	LogMessagePackage logMessagePackage;
	
	@Autowired
	@Qualifier("AccountWamService")
	public AccountWamService accountWamService;
	
	@Autowired
	@Qualifier("LocationWamService")
	public LocationWamService locationWamService;
	
	@Autowired
	@Qualifier("LbuAccountPackage")
	public LbuAccountPackage lbuAccountPackage;
	
	@Autowired
	@Qualifier("LbuContentSubPackage")
	public LbuContentSubPackage lbuContentSubPackage;
	
	@Autowired
	@Qualifier("LbuLocationPackage")
	public LbuLocationPackage lbuLocationPackage;
	
	@Autowired
	@Qualifier("AgreementService")
	public AgreementService agreementService;
	
	@Autowired
	@Qualifier("SysHistoryService")
	SysHistoryService sysHistoryService;
	
	@Autowired
	@Qualifier("LbuAgreementPackage")
	public LbuAgreementPackage lbuAgreementPackage;
	
	@Autowired
	@Qualifier("LbuSourcePackagePackage")
	public LbuSourcePackagePackage lbuSourcePackagePackage;
	
	EcmLocationHistoryLoggingAspect ecmLocationHistoryLoggingAspect;
	EcmCustomerHistoryLoggingAspect ecmCustomerHistoryLoggingAspect;
	
	@Autowired
	@Qualifier("EmailService")
	public EmailService emailService;
	
	public String[] processSubTypes = {Definition.CREATE, Definition.UPDATE};
	public String[] processTypes = {Definition.ACCOUNT, Definition.LOCATION, Definition.CONTENTSUB, Definition.AGREEMENT};
	
	public static String regexAccountMessage = "[A-Z]{2}(\\s)[0-9A-Z-]{4,}";
	public static String regexLocationMessage = "[A-Z]{2}(\\s)[0-9A-Z-]{4,}(\\s)[0-9A-Z-]{4,}";
	public static String regexContentSubMessage = "[A-Z]{2}(\\s)[0-9A-Z-]{4,}(\\s)[0-9A-Z-]{4,}";
	public static String regexAgreementMessage = "[A-Z]{2}(\\s)[0-9A-Z-]{4,}(\\s)[0-9A-Z-]{4,}";

	public Properties ecmws;
	public Properties urlws;
	public Properties workflowProperty;
	public BusinessUnitMap businessUnitMap;
	public AgreementUnitMap agreementUnitMap;
	
	public HistoryPackage historyPackage;
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public LogMessagePackage getLogMessagePackage()
	{
		LogMessagePackage logMessagePackage = new LogMessagePackage();
		logMessagePackage.setDataSource(dataSource);
		
		return logMessagePackage;
	}
	
	public LbuAgreementPackage getLbuAgreementPackage()
	{
		LbuAgreementPackage lbuAgreementPackage = new LbuAgreementPackage();
		lbuAgreementPackage.setDataSource(dataSource);
		
		return lbuAgreementPackage;
	}
	
	public EcmLocationHistoryLoggingAspect getEcmLocationHistoryLoggingAspect()
	{
		EcmLocationHistoryLoggingAspect ecmLocationHistoryLoggingAspect = new EcmLocationHistoryLoggingAspect();
		ecmLocationHistoryLoggingAspect.setDataSource(this.dataSource);
		ecmLocationHistoryLoggingAspect.setHistoryPackage(this.historyPackage);
		
		return ecmLocationHistoryLoggingAspect;
	}
	
	public EcmCustomerHistoryLoggingAspect getEcmCustomerHistoryLoggingAspect()
	{
		EcmCustomerHistoryLoggingAspect ecmCustomerHistoryLoggingAspect = new EcmCustomerHistoryLoggingAspect();
		ecmCustomerHistoryLoggingAspect.setDataSource(this.dataSource);
		ecmCustomerHistoryLoggingAspect.setHistoryPackage(this.historyPackage);
		
		return ecmCustomerHistoryLoggingAspect;
	}
	
	public AccountWamService getAccountWamService()
	{
		AccountWamService accountWamService = new AccountWamService();
		accountWamService.setDataSource(dataSource);
		
		return accountWamService;
	}
	
	public AgreementService getAgreementService()
	{
		AgreementService agreementService = new AgreementService();
		agreementService.setDataSource(dataSource);

		return agreementService;
	}
	
	public SysHistoryService getSysHistoryService()
	{
		SysHistoryService sysHistoryService = new SysHistoryService();
		sysHistoryService.setDataSource(dataSource);
		
		return sysHistoryService;
	}
	
	public EmailService getEmailService()
	{
		EmailService emailService = new EmailService();
		
		return emailService;
	}
	
	public LbuAccountPackage getLbuAccountPackage()
	{
		LbuAccountPackage lbuAccountPackage = new LbuAccountPackage();
		lbuAccountPackage.setDataSource(dataSource);
		
		return lbuAccountPackage;
	}
	
	public LocationWamService getLocationWamService()
	{
		LocationWamService locationWamService = new LocationWamService();
		locationWamService.setDataSource(dataSource);
		
		return locationWamService;
	}
	
	
	public LbuLocationPackage getLbuLocationPackage()
	{
		LbuLocationPackage lbuLocationPackage = new LbuLocationPackage();
		lbuLocationPackage.setDataSource(dataSource);
		
		return lbuLocationPackage;
	}
	
	public LbuContentSubPackage getLbuContentSubPackage()
	{
		LbuContentSubPackage lbuContentSubPackage = new LbuContentSubPackage();
		lbuContentSubPackage.setDataSource(dataSource);
		
		return lbuContentSubPackage;
	}
	
	public LbuSourcePackagePackage getLbuSourcePackagePackage()
	{
		LbuSourcePackagePackage lbuSourcePackagePackage = new LbuSourcePackagePackage();
		lbuSourcePackagePackage.setDataSource(dataSource);
		
		return lbuSourcePackagePackage;
	}

	public WorkFlowManagerService() throws IOException
	{
		/**
		 * change the context to batch context
		 * for env
		 */
		BaseTest.setContextBatch();
		
		if(Definition.E2E.equals(EnvFlag))
		{
			out.println(EnvFlag);
			BaseTest.setContextBatchE2e();
		}
		
		if(Definition.CERT.equals(EnvFlag))
		{
			out.println(EnvFlag);
			BaseTest.setContextBatchCert();
		}
			
		if(this.dataSource == null)
		{
			this.dataSource = super.getDataSrouce();
		}
		
        if( this.logMessagePackage == null )
        {
        	this.logMessagePackage = this.getLogMessagePackage();
        }
        
        if(this.lbuAccountPackage == null)
        {
        	this.lbuAccountPackage = this.getLbuAccountPackage();
        	this.lbuAccountPackage.setDataSource(dataSource);
        }
        
        if(this.accountWamService == null)
        {
        	this.accountWamService = this.getAccountWamService();
        	this.accountWamService.lbuAccountPackage = this.lbuAccountPackage;
        }
        
        if(this.businessUnitMap == null)
        {
        	this.businessUnitMap = (BusinessUnitMap)context.getBean("businessUnitMap");
        	this.accountWamService.setBusinessUnitMap(this.businessUnitMap);
        }
        
        if(this.agreementUnitMap == null)
        {
        	this.agreementUnitMap = (AgreementUnitMap)context.getBean("agreementUnitMap");
        }
        
        if(this.lbuAgreementPackage == null)
        {
        	this.lbuAgreementPackage = this.getLbuAgreementPackage();
        	this.lbuAgreementPackage.setDataSource(dataSource);
        }
        
        if(this.agreementService == null)
        {
        	this.agreementService = this.getAgreementService();
        	this.agreementService.setLbuAgreementPackage(this.lbuAgreementPackage);
        	this.agreementService.setAgreementUnitMap(agreementUnitMap);
        	this.agreementService.setLbuAccountPackage(this.lbuAccountPackage);
        }
        
        if(this.lbuLocationPackage == null)
        {
        	this.lbuLocationPackage = this.getLbuLocationPackage();
        }
        
        if(this.locationWamService == null)
        {
        	this.locationWamService = this.getLocationWamService();
        	this.locationWamService.setLbuLocationPackage(this.lbuLocationPackage);
        }
        
        if(this.lbuContentSubPackage == null)
        {
        	this.lbuContentSubPackage = this.getLbuContentSubPackage();
        	this.agreementService.setLbuContentSubPackage(this.lbuContentSubPackage);
        }
        
        if(this.lbuSourcePackagePackage == null)
        {
        	this.lbuSourcePackagePackage = this.getLbuSourcePackagePackage();
        	this.agreementService.setLbuSourcePackagePackage(this.lbuSourcePackagePackage);
        }
        
        if(this.ecmws == null)
        {
        	this.ecmws = (Properties) context.getBean("ecmws");
        	this.accountWamService.setEcmws(this.ecmws);
        	this.agreementService.setEcmws(this.ecmws);
        	this.locationWamService.setEcmws(this.ecmws);
        }
        
        if(this.urlws == null)
        {        	
        	this.urlws = (Properties) context.getBean("urlws");
        	this.accountWamService.setUrlws(this.urlws);
        	this.agreementService.setUrlws(this.urlws);
        	this.locationWamService.setUrlws(this.urlws);
        	
        	logPath = this.urlws.getProperty("log.path");
        }
        
        if(this.workflowProperty == null)
        {
        	this.workflowProperty = (Properties) context.getBean("workflowmanager");
        }
        
        this.emailService = this.getEmailService();
        this.emailService.setWorkflowProperty(this.workflowProperty);
        this.emailService.setTemplateMessage((SimpleMailMessage)context.getBean("templateMessage"));
        this.emailService.setMailSender((JavaMailSender)context.getBean("mailSender"));
        
        this.historyPackage = (HistoryPackage) context.getBean("historyPackage");
        this.historyPackage.setDataSrouce(this.dataSource);
        this.accountWamService.setHistoryPackage(this.historyPackage);
        this.agreementService.setHistoryPackage(this.historyPackage);
        
        this.sysHistoryService = this.getSysHistoryService();
        this.sysHistoryService.setHistoryPackage(historyPackage);
        
        this.agreementService.setSysHistoryService(sysHistoryService);
        
        this.ecmLocationHistoryLoggingAspect = this.getEcmLocationHistoryLoggingAspect();
        this.locationWamService.setEcmLocationHistoryLoggingAspect(this.ecmLocationHistoryLoggingAspect);
        
        this.ecmCustomerHistoryLoggingAspect = this.getEcmCustomerHistoryLoggingAspect();
        this.accountWamService.setEcmCustomerHistoryLoggingAspect(ecmCustomerHistoryLoggingAspect);
        
	}
	
	/**
	 * @throws Exception
	 */
	public void fetchLogMessages() throws Exception
	{
		try {
			Boolean messageNotFinished = true;
			Integer batchFrom = 0;
			Integer batchMax = Integer.parseInt(this.workflowProperty.getProperty("workflow.manager.service.handle.message.number"));
	
			/**
			 * why here is a outer while englobe the other inside while ?
			 * this is for Locations and Agreements.
			 * Imagine: When the batch has finished the part of accounts. When the batch is running to handle the agreement one by one.
			 * It considers that all the accounts already finished and sent.
			 * But all the accounts, locations, contentSubs, logMessages are imported by the tools BI developped by 
			 * our data scientist MAX Baudou. It means the data are imported automatically 24/24 7/7 by the tool itself.
			 * MeanWhile There are some new accounts arrived in the oracle db which accompanied by their agreements.
			 * If these new arrvial accounts not yet sent. It will be failure when we send Their agreements belonged to the new arrived accounts.  
			 * So we need to jump to the outer side to check again if there some new arrived accounts or not.
			 * If yes. It's priority to handle the new arrived accounts before all agreements. 
			 */
			processOuter : while(messageNotFinished)
			{
				/**
				 * handles accounts before other type messages (location and agreement).
			     * Because we need the CUSTOMERPGUID and the POBPGUID before ws ecm for location and order
				 */
				while(messageNotFinished)
				{
					LogMessages logMessagesAccount = this.getLogMessagesAccount(null, null, null, Definition.WORKFLOW_MANAGER_STATUS_BEGINNING, batchFrom, batchMax);
					if(logMessagesAccount.getList().size() == 0)
					{
						messageNotFinished = false;
					} else {
						this.handleMessages(logMessagesAccount);
						Thread.sleep(Integer.parseInt(this.workflowProperty.getProperty("workflow.manager.service.handle.message.batch.sleep.time")));
					}
				}
				
				/**
				 * handles locations after account. 
				 * Because we need the ADDRESSPGUID before ws ecm order
				 */
				messageNotFinished = true;
				while(messageNotFinished)
				{
					LogMessages logMessagesLocation = this.getLogMessagesLocation(null, null, null, Definition.WORKFLOW_MANAGER_STATUS_BEGINNING, batchFrom, batchMax);
					if(logMessagesLocation.getList().size() == 0)
					{
						messageNotFinished = false;
					} else {
						this.handleMessages(logMessagesLocation);
						Thread.sleep(Integer.parseInt(this.workflowProperty.getProperty("workflow.manager.service.handle.message.batch.sleep.time")));
						continue processOuter;
					}
				}
				
				
				/**
				 * after account and location
				 * finally we handles agreement
				 */
				messageNotFinished = true;
				while(messageNotFinished)
				{
					LogMessages logMessagesAgreement = this.getLogMessagesAgreement(null, null, Definition.WORKFLOW_MANAGER_SQL_CODE, Definition.WORKFLOW_MANAGER_STATUS_BEGINNING, batchFrom, batchMax);
					
					if(logMessagesAgreement.getList().size() == 0)
					{
						messageNotFinished = false;
					} else {
						this.handleMessages(logMessagesAgreement);
						Thread.sleep(Integer.parseInt(this.workflowProperty.getProperty("workflow.manager.service.handle.message.batch.sleep.time")));
						continue processOuter;
					}
				}
			
			}
			out.println("WorkFlowManagerService is finished!");
		} 
		catch(Exception e)
		{
			logger.error("Exception in fetchLogMessages ");
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param logMessages
	 * @throws Exception
	 */
	public void handleMessages(LogMessages logMessages) throws Exception
	{
		try {
			isRealSendWs = Boolean.valueOf(this.workflowProperty.getProperty("workflow.manager.service.handle.real.send.ws"));
			
			for(LogMessage logMessage : logMessages.getList())
			{
				// clear messages
				ResponseHelper.cleanMessageList();
				WsMessageHelper.cleanMessageList();
				
				String trace = "";
				
				Map<String, String> mailSubjectContent = null;
				
				/**
				 * create or update
				 */
				if( !Arrays.asList(processSubTypes).contains(logMessage.getProcessSubType()) )
				{
					logger.error(logMessage.getMessageId() + " " + logMessage.getProcessSubType() + "ProcessSubType problem");
					continue;
				}
				
				/**
				 * which table?
				 */
				if( !Arrays.asList(processTypes).contains(logMessage.getProcessType()) )
				{
					logger.error(logMessage.getMessageId() + " " + logMessage.getProcessType() + "processType problem");
					continue;
				}
				
				try {
					if(logMessage.getProcessType().equals(Definition.ACCOUNT) && logMessage.getActionId().toString().equals(Definition.WORKFLOW_MANAGER_ECM_CODE))
					{
						LbuAccount lbuAccount = AccountWsNotify(logMessage, isRealSendWs);
						mailSubjectContent = getMailSubjectContent(logMessage);
						
						trace = "Account " + logMessage.getProcessSubType() + Definition.SPACE + logMessage.getMessage() + Definition.SPACE + lbuAccount.getCustomerPguid() ;
	
						if(null != lbuAccount.getCustomerPguid())
						{
							LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_COMPLETE, Integer.parseInt(Definition.WORKFLOW_MANAGER_FINISHED_CODE));
							trace += " status: " + Definition.WORKFLOW_MANAGER_STATUS_COMPLETE;
						} else {
							LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_ERROR);
							trace += " status: " + Definition.WORKFLOW_MANAGER_STATUS_ERROR;
						}
					}
					
					if(logMessage.getProcessType().equals(Definition.LOCATION) && logMessage.getActionId().toString().equals(Definition.WORKFLOW_MANAGER_ECM_CODE))
					{
						LbuLocation location = LocationWsNotify(logMessage, isRealSendWs);
						mailSubjectContent = getMailSubjectContent(logMessage);
						
						trace = "Location " + logMessage.getProcessSubType() + Definition.SPACE + logMessage.getMessage() + Definition.SPACE + location.getAddressPGUID() ;
						
						if(null != location.getAddressPGUID())
						{
							LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_COMPLETE, Integer.parseInt(Definition.WORKFLOW_MANAGER_FINISHED_CODE));
							trace += " status: " + Definition.WORKFLOW_MANAGER_STATUS_COMPLETE;
						} else {
							LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_ERROR);
							trace += " status: " + Definition.WORKFLOW_MANAGER_STATUS_ERROR;
						}
					}
		
					if(logMessage.getProcessType().equals(Definition.AGREEMENT) && logMessage.getActionId().toString().equals(Definition.WORKFLOW_MANAGER_SQL_CODE))
					{
						Map<String,Object> map = agreementWsNotify(logMessage, isRealSendWs);
						mailSubjectContent = getMailSubjectContent(logMessage);
						
						trace = "Agreement " + logMessage.getProcessSubType() + Definition.SPACE + logMessage.getMessage() ;
						
						if(null != map)
						{	
							LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_COMPLETE, Integer.parseInt(Definition.WORKFLOW_MANAGER_FINISHED_CODE));
							trace += " status: " + Definition.WORKFLOW_MANAGER_STATUS_COMPLETE;
						} else {
							LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_ERROR);
							trace += " status: " + Definition.WORKFLOW_MANAGER_STATUS_ERROR;
						}
					}
					out.println(trace);
					
					FileHandler.appendLog(trace , logPath);
					logger.info(trace);
					
					//this.prepareMailParametersBatch(mailSubjectContent.get(Definition.SUBJECT), mailSubjectContent.get(Definition.CONTENT));
					Thread.sleep(Integer.parseInt(this.workflowProperty.getProperty("workflow.manager.service.handle.message.single.sleep.time")));
				}
				catch(Exception e)
				{
					LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_ERROR);
					
					trace +=  " status: " + Definition.WORKFLOW_MANAGER_STATUS_ERROR;
					out.println(trace);
	
					FileHandler.appendLog(trace , logPath);
					FileHandler.appendLog(" ErrorMessage: " + e.getMessage() , logPath);
					FileHandler.appendLog(" ErrorStackTrace: " + e.getStackTrace() , logPath);
					
					logger.error(trace);
					logger.error(" ErrorMessage logMessage loop: " + e.getMessage());
					logger.error(" ErrorStackTrace: " + e.getStackTrace().toString());
				}
			}
		}catch(Exception e)
		{
			logger.error(" ErrorMessage handleMessages: " + e.getMessage());
			logger.error(" ErrorStackTrace handleMessages: " + e.getStackTrace().toString());
		}
		
	}
	
	/**
	 * 
	 * @param logMessage
	 * @throws Exception
	 */
	public LbuAccount AccountWsNotify(LogMessage logMessage, Boolean isRealSend) throws Exception
	{
		try {
			LbuAccount lbuAccount = (LbuAccount)getEntityObject(logMessage);
			
			if(lbuAccount == null)
			{
				logger.error(" lbuAccount " + logMessage.getMessage() + " from logMessage not found");
				return null;
			}
			
			LbuLocations lbuLocations = locationWamService.lbuLocationSearch(lbuAccount.getLbuSetId(), lbuAccount.getLbuAccountId(), null, null, null, null, null, null, null);
	
			if(lbuLocations.getList().size() == 0)
			{
				logger.error(" no lbuLocation associated to lbuAccount " + lbuAccount.getLbuAccountId());
				return null;
			}
			
			/**
			 * fetch default location for the account before sending
			 */
			LbuLocation lbuLocation = lbuLocations.getList().get(0);
			lbuAccount.setAddressLine1(lbuLocation.getAddressLine1());
			lbuAccount.setAddressLine2(lbuLocation.getAddressLine2());
			lbuAccount.setCityName(lbuLocation.getCityName());
			lbuAccount.setCountryCode(lbuLocation.getCountryCode());
			lbuAccount.setCountryName(lbuLocation.getCountryName());
			lbuAccount.setZipCode(lbuLocation.getZipCode());
			
			/**
			 * update status to doing before send
			 */
			this.LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_DOING, logMessage.getActionId());
			
			if(isRealSend)
			{
				Map<?,?> map =  accountWamService.createOrUpdateCustomerEcmWs(lbuAccount, lbuLocations, logMessage.getProcessSubType());
				
				lbuAccount = (LbuAccount) map.get(Definition.LBU_ACCOUNT);
			} 
			
			return lbuAccount;
		
		}
		catch(Exception e)
		{
			logger.error(" AccountWsNotify error: ");
			logger.error(e.getMessage() + e.getCause() + e.getStackTrace());
		}
		return null;
	}
	
	/**
	 * 
	 * @param logMessage
	 * @throws Exception
	 */
	public LbuLocation LocationWsNotify(LogMessage logMessage, Boolean isRealSend) throws Exception
	{
		try {
			LbuLocation lbuLocation = (LbuLocation)getEntityObject(logMessage);
			LbuAccount lbuAccount = accountWamService.selectLbuAccountById(lbuLocation.getLbuAccountId(), lbuLocation.getLbuSetId());
			
			if(lbuLocation == null)
			{
				logger.error(" lbuLocation " +lbuLocation.getLbuLocationId()+ " from logMessage not found");
				return null;	
			}
			
			if(lbuAccount == null)
			{
				logger.error(" No lbuAccount associated to lbuLocation " + lbuLocation.getLbuLocationId());
				return null;	
			}
			
			/**
			 * update status to doing before send
			 */
			this.LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_DOING, logMessage.getActionId());
			
			if(isRealSend)
			{
				locationWamService.createPlaceOfBusinessEcmWs(lbuLocation, lbuAccount.getCustomerPguid(), logMessage.getProcessSubType());
			} 
			
			return lbuLocation;
			
		}
		catch(Exception e)
		{
			logger.error(" LocationWsNotify error: ");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param logMessage
	 * @throws Exception
	 */
	public Map<String,Object> agreementWsNotify(LogMessage logMessage, Boolean isRealSend) throws Exception
	{
		try {
			Map<String, Object> map = (Map<String, Object>)getEntityObject(logMessage);
			LbuAgreements lbuAgreements = (LbuAgreements) map.get(Definition.AGREEMENTS);
			String transIdSysHistory = (String)map.get(Definition.TRANS_ID_SYSHISTORY);
			
			if(transIdSysHistory == null)
			{
				transIdSysHistory = BaseService.generateTransIdSysHistory();
			}
			
			if(lbuAgreements.getList().size() == 0)
			{
				logger.error(" lbuAgreements "+ logMessage.getMessage() +" from logMessage is empty ");
				return null;
			}
			/**
			 * in the loop
			 * we check if the customerPguid is empty in agreement
			 */
			for(LbuAgreement lbuAgreement : lbuAgreements.getList())
			{
				if(lbuAgreement.getCustomerPguid() == null)
				{
					String customerPguid = lbuAgreementPackage.LbuAgreementUpdateCustomerPGUID(lbuAgreement.getAssetAgreementId());
					lbuAgreement.setCustomerPguid(customerPguid);
				}
			}
	
			String customerPguid = lbuAgreements.getList().get(0).getCustomerPguid();
	
			LbuAccount lbuAccount = accountWamService.lbuAccountPackage.selectLbuAccountByCustomerPGUID(customerPguid);
			
			/**
			 * if bug customerPguid in agreement. 
			 */
			if(lbuAccount == null)
			{
				String assetAgreementId = lbuAgreements.getList().get(0).getAssetAgreementId();
				customerPguid = lbuAccountPackage.selectCustomerPguidByAsset(assetAgreementId);
				lbuAgreementPackage.LbuAgreementUpdateCustomerPGUID(assetAgreementId);
				
				lbuAccount = accountWamService.lbuAccountPackage.selectLbuAccountByCustomerPGUID(customerPguid);
			}
			
			/**
			 * update status to doing before send
			 */
			this.LogMessageUpdate(logMessage.getMessageId(), Definition.WORKFLOW_MANAGER_STATUS_DOING, logMessage.getActionId());
			
			
			if(isRealSend)
			{
			    return agreementService.createSyncOrderCemeaWs(lbuAgreements, lbuAccount, Definition.AGREEMENT, transIdSysHistory);
			} else {
				return new HashMap<String,Object>();
			}
		
		}
		catch(Exception e)
		{
//			FileHandler.appendLog(e.getMessage() , logPath);
			
			logger.error(" agreementWsNotify error: ");
			logger.error(e.getMessage());
		}
		
		return null;
	}
	/**
	 * 
	 * @param logMessage
	 * @return
	 * @throws Exception 
	 */
	public Object getEntityObject(LogMessage logMessage) throws Exception
	{
		/**
		 * fetch account
		 */
		if(logMessage.getProcessType().equals(Definition.ACCOUNT))
		{
			return (Object)this.getAccountEntityObject(logMessage);
		}
		
		/**
		 * fetch location
		 */
		if(logMessage.getProcessType().equals(Definition.LOCATION))
		{
			return (Object)this.getLocationEntityObject(logMessage);
		}
		
		/**
		 * fetch agreement
		 */
		if(logMessage.getProcessType().equals(Definition.AGREEMENT))
		{
			return (Object)this.getAgreementsEntityObject(logMessage);
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param logMessage
	 * @return
	 * @throws Exception
	 */
	public LbuAccount getAccountEntityObject(LogMessage logMessage) throws Exception
	{
		try {
		LbuAccount lbuAccount = new LbuAccount();
		Pattern accountMessagePattern = Pattern.compile(WorkFlowManagerService.regexAccountMessage);
		Matcher accountMessageMatcher = accountMessagePattern.matcher(logMessage.getMessage());
		if(accountMessageMatcher.matches())
		{
			String[] messageParts = logMessage.getMessage().toString().split(Definition.SPACE);
			lbuAccount.setLbuSetId(messageParts[0]);
			lbuAccount.setLbuAccountId(messageParts[1]);
			
			lbuAccount = accountWamService.selectLbuAccountById(lbuAccount.getLbuAccountId(), lbuAccount.getLbuSetId());
			
		} else {
			logger.error(" Message " + logMessage.getMessageId() + " AccountMessage format: " + logMessage.getMessage().toString());
		}
		
		return lbuAccount;
		
		}
		catch(Exception e)
		{
			logger.error(" getAccountEntityObject error: ");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param logMessage
	 * @return
	 * @throws Exception
	 */
	public LbuLocation getLocationEntityObject(LogMessage logMessage) throws Exception
	{
		try {
			LbuLocation lbuLocation = new LbuLocation(); 
			Pattern locationMessagePattern = Pattern.compile(WorkFlowManagerService.regexLocationMessage);
			Matcher locationMessageMatcher = locationMessagePattern.matcher(logMessage.getMessage());
			if(locationMessageMatcher.matches())
			{
				String[] messageParts = logMessage.getMessage().toString().split(Definition.SPACE);
				lbuLocation.setLbuSetId(messageParts[0]);
				lbuLocation.setLbuAccountId(messageParts[1]);
				lbuLocation.setLbuLocationId(messageParts[2]);
				
				lbuLocation = locationWamService.selectLbuLocation(lbuLocation.getLbuSetId(), lbuLocation.getLbuAccountId(), lbuLocation.getLbuLocationId());
			} else {
				logger.error(" Message " + logMessage.getMessageId() + " LocationMessage format: " + logMessage.getMessage().toString());
			}
			
			return lbuLocation;
			
		}
		catch(Exception e)
		{
			logger.error(" getLocationEntityObject error: ");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param logMessage
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> getAgreementsEntityObject(LogMessage logMessage) throws Exception
	{
		try {
		LbuAgreements lbuAgreements = new LbuAgreements();
		String transIdSysHistory = null;
		
		Pattern agreementMessagePattern = Pattern.compile(WorkFlowManagerService.regexAgreementMessage);
		Matcher agreementMessageMatcher = agreementMessagePattern.matcher(logMessage.getMessage());
		if(agreementMessageMatcher.matches())
		{
			String[] messageParts = logMessage.getMessage().toString().split(Definition.SPACE);
			
			/**
			 * fetch the 3ed part of the string
			 */
			String agreementId = messageParts[2];
				
			//lbuAgreements = agreementService.lbuAgreementPackage.LbuAgreementSelectById(agreementId);

			LbuContentSubs lbuContentSubs = this.lbuContentSubPackage.lbuContentSubSearch(null, null, agreementId, null, null, null, null, null, null, null, null, null, null, null);

			if(lbuContentSubs.getList().size() > 0)
			{
				/**
				 * set additional messageId for every contentSub
				 */

				for(int index = 0; index < lbuContentSubs.getList().size(); index++)
				{
					(lbuContentSubs.getList().get(index)).setMessageId(logMessage.getMessageId());
				}

				transIdSysHistory = BaseService.generateTransIdSysHistory();
                lbuAgreements = agreementService.transformLbuContentSubsToLbuAgreementsFromDb(lbuContentSubs, transIdSysHistory);
			}else {
				logger.error(" No contentSub found for contact num: " + agreementId);
			}
			

			
		} else {
			logger.error(" Message " + logMessage.getMessageId() + " agreementMessage format: " + logMessage.getMessage().toString());
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(Definition.AGREEMENTS, lbuAgreements);
		map.put(Definition.TRANS_ID_SYSHISTORY, transIdSysHistory);
		
		return map; 
		
		}
		catch(Exception e)
		{
			logger.error(" getAgreementsEntityObject error: ");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param table
	 * @param action
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public LogMessages getLogMessagesAccount(String entityId, String processSubType, String actionId, String status, Integer batchFrom, Integer batchMax) throws Exception
	{
		return logMessagePackage.LogMessageSearch(entityId, Definition.ACCOUNT, processSubType, actionId, status, batchFrom, batchMax);
	}
	
	/**
	 * 
	 * @param entityId
	 * @param processSubType
	 * @param actionId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public LogMessages getLogMessagesLocation(String entityId, String processSubType, String actionId, String status, Integer batchFrom, Integer batchMax) throws Exception
	{
		return logMessagePackage.LogMessageSearch(entityId, Definition.LOCATION, processSubType, actionId, status, batchFrom, batchMax);
	}
	
	/**
	 * 
	 * @param entityId
	 * @param processSubType
	 * @param actionId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public LogMessages getLogMessagesAgreement(String entityId, String processSubType, String actionId, String status, Integer batchFrom, Integer batchMax) throws Exception
	{
		return logMessagePackage.LogMessageSearch(entityId, Definition.AGREEMENT, processSubType, actionId, status, batchFrom, batchMax);
	}
	
	/**
	 * 
	 * @param entityId
	 * @param table
	 * @param processSubType
	 * @param actionId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public LogMessages getLogMessages(String entityId, String table, String processSubType, String actionId, String status, Integer batchFrom, Integer batchMax) throws Exception
	{
		if(table == Definition.ACCOUNT)
		{
			return this.getLogMessagesAccount(entityId, processSubType, actionId, status, batchFrom, batchMax);
		}
		if(table == Definition.LOCATION)
		{
			return this.getLogMessagesLocation(entityId, processSubType, actionId, status, batchFrom, batchMax);
		}
		if(table == Definition.AGREEMENT)
		{
			return this.getLogMessagesAgreement(entityId, processSubType, actionId, status, batchFrom, batchMax);
		}
		return null;
	}
	
	/**
	 * 
	 * @param messageId
	 * @param status
	 * @param actionId
	 * @return
	 * @throws Exception
	 */
	public Boolean LogMessageUpdate(Integer messageId, String status, Integer actionId) throws Exception
	{
		return logMessagePackage.LogMessageUpdate(messageId, status, actionId);
	}
	
	/**
	 * 
	 * @param messageId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public Boolean LogMessageUpdate(Integer messageId, String status) throws Exception
	{
		return logMessagePackage.LogMessageUpdate(messageId, status);
	}
	
	/**
	 * Fetch template for each kind mail
	 * @return
	 */
	public Map<String, String> getMailSubjectContent(LogMessage logMessage)
	{
		Map<String, String> map = new HashMap<String, String>();
		
		String subject = null;
		String content = null;
		
		if(ResponseHelper.getMessageList().size() > 0)
		{
			subject = ResponseHelper.getMessageList().get(0).getMessage();
			content = ResponseHelper.getMessageList().get(0).getMessage();
		}
		
		map.put(Definition.SUBJECT, subject);
		map.put(Definition.CONTENT, content);
		
		return map;
	}
	
	/**
	 * 
	 * @param subject
	 * @param textContent
	 * @throws Exception
	 */
    public void prepareMailParametersBatch(String subject,String textContent) throws Exception
     {

    	try {
	       if(subject == null || textContent == null)
	       {
	    	   if(ResponseHelper.getMessageList().size() == 0)
	    	   {
	    		   logger.error("message list is empty");
	    	   }
	    	   
	    	   if(subject == null)
	           {
	               subject = ResponseHelper.getMessageList().get(0).getMessage();
	           }
	           
	           if(textContent == null)
	           {
	        	   textContent = ResponseHelper.getMessageList().get(0).getMessage();
	           }
	       }
	       
	       /**
	        * mock data if not real send mail
	        */
	       if(!emailService.isRealSend())
	       {
	           subject = emailService.getMockSubject();
	           textContent = emailService.getMockTextContent();
	       }
	
	       /**
	        * check subject
	        */
	       if(subject == null)
	       {
	    	   logger.error("no subject");
	       }
	       
	       /**
	        * check textContent
	        */
	       if(textContent == null)
	       {
	    	   logger.error("no Email textContent");
	       }
	
	
	       String to = "Cheng.QIN@lexisnexis.fr"; //emailService.getMailTo(); //
	       String from = emailService.getTemplateMessage().getFrom();
	
	       /**
	        * prepare attachements
	        */
	       ArrayList<?> attachements = emailService.getAttachements();
	
	       isRealSendMail = Boolean.valueOf(this.workflowProperty.getProperty("workflow.manager.service.handle.test.mail.real.send"));
	       if(isRealSendMail)
	       {
	    	   emailService.sendMail(from, to, subject, textContent, attachements);
	       }
       
    	}
    	catch(Exception e)
    	{
    		logger.error(" prepareMailParametersBatch error: ");
    		logger.error(e.getMessage());
    	}
       
    }
    
    public static void main(String[] args) throws Exception
	{	
		try {
			//getConstantFilePath();
			
			if(args.length > 0)
			{
				WorkFlowManagerService.EnvFlag = args[0];
			}
			
			WorkFlowManagerService workFlowManagerService = new WorkFlowManagerService();
			workFlowManagerService.fetchLogMessages();
			
		} 
		catch(Exception e)
		{
			logger.error("Exception in main WorkFlowManagerService");
			logger.error(e.getMessage());
		}
	}
	
	
}
