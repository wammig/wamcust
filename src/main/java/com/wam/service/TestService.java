package com.wam.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.stored.packages.LbuAccountPackage;
import com.wam.stored.procedure.LbuAgreementSelectByAssetStoredProcedure;

import static java.lang.System.out;

@Service("TestService")
public class TestService {

	@Autowired
	@Qualifier("WamService")
	public WamService wamService;
	
	@Autowired
	@Qualifier("AgreementService")
	public AgreementService agreementService;
	
	@Autowired
	@Qualifier("ContentSubWamService")
	public ContentSubWamService contentSubWamService;
	
	
	@Autowired
	@Qualifier("LbuAccountPackage")
	public LbuAccountPackage lbuAccountPackage;
	
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	private Environment env;
	
	
	
	public List<Map<String, String>> getTestCaseAgreement()
	{
		Connection conn = null;
	    String sql = " select * from LBU_AGREEMENT WHERE ROWNUM <= 3 ORDER BY AGREEMENT_ID DESC ";

	    Map<String, String> map = new HashMap<String, String>();
	    List<Map<String, String>> list = new ArrayList<Map<String, String>>();
	    
	    try {
	    	conn = this.dataSource.getConnection();
	    	PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet resultSet = ps.executeQuery();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (resultSet.next()) {
				map = new HashMap<String, String>();
				
			    for (int i = 1; i <= columnsNumber; i++) {
			        String columnValue = resultSet.getString(i);
			        map.put(rsmd.getColumnName(i).toString(), columnValue);
			    }
			    
			    list.add(map);
			}
			ps.close();
	    	
	    } catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	    
	    return list;
	}
	
	/*
	public void agreementSendWs() throws Exception
	{
		List<Map<String,String>> list = getTestCaseAgreement();
		LbuAgreements lbuAgreements = new LbuAgreements();
		lbuAgreements.setList(new ArrayList<LbuAgreement>());
		out.println(list);
		for(Map<String, String> map: list) {
			String assetAgreementId = map.get("ASSET_AGREEMENT_ID").toString();
			LbuAgreement lbuAgreement = agreementService.selectLbuAgreement(assetAgreementId);

			String customerPguid = lbuAccountPackage.selectCustomerPguidByAsset(assetAgreementId);
			
			if(customerPguid == null)
			{
				throw new Exception("no contentSub and account found for assetAgreementId " + assetAgreementId);
			}
			
			lbuAgreement.setCustomerPguid(customerPguid);
			
			lbuAgreements.getList().add(lbuAgreement);			
		}

		agreementService.createSyncOrderCemeaWs(lbuAgreements);
	}
	*/
}
