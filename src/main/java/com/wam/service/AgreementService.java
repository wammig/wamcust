package com.wam.service;

import static java.lang.System.out;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.wam.model.config.order.AgreementUnitMap;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.LbuFeature;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LbuAgreementMap;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.db.gbsd.helper.LbuFeatures;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.model.mvc.response.helper.WsMessage;
import com.wam.model.mvc.response.helper.WsMessageHelper;
import com.wam.model.ws.out.sync.order.cemea.CommonTrackingMessageWrapper;
import com.wam.model.ws.out.sync.order.cemea.DataAreaWrapper;
import com.wam.model.ws.out.sync.order.cemea.DataGroupWrapper;
import com.wam.model.ws.out.sync.order.cemea.HeaderWrapper;
import com.wam.model.ws.out.sync.order.cemea.ListOfOrderLineItemsWrapper;
import com.wam.model.ws.out.sync.order.cemea.MigrationActionsWrapper;
import com.wam.model.ws.out.sync.order.cemea.OrderBOWrapper;
import com.wam.model.ws.out.sync.order.cemea.OrderLineItemWrapper;
import com.wam.model.ws.out.sync.order.cemea.OrderRootWrapper;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.packages.LbuAccountPackage;
import com.wam.stored.packages.LbuAgreementPackage;
import com.wam.stored.packages.LbuContentSubPackage;
import com.wam.stored.packages.LbuSourcePackagePackage;
import com.wam.utility.InputStream.InputStreamTransformer;
import com.wam.utility.definition.Definition;
import com.wam.utility.helper.StringHelper;
import com.wam.utility.net.HttpUtility;

@Service("AgreementService")
@PropertySources({
    @PropertySource(value="classpath:ecmws.properties", ignoreResourceNotFound = true)
})
public class AgreementService extends BaseService{
	
	public final static Logger logger = LoggerFactory.getLogger(AgreementService.class);
	
	@Autowired
	private Environment env;
	
	@Autowired
    DataSource dataSource;
	
	@Value( "${order.ws.url.creation.Order}" )
	private String orderWsUrlCreationOrder;
	
	@Autowired
	@Qualifier("agreementUnitMap")
	private AgreementUnitMap agreementUnitMap;
	
	@Autowired
	@Qualifier("LbuAgreementPackage")
	public LbuAgreementPackage lbuAgreementPackage;
	
	@Autowired
	@Qualifier("LbuContentSubPackage")
	public LbuContentSubPackage lbuContentSubPackage;
	
	@Autowired
	@Qualifier("LbuAccountPackage")
	public LbuAccountPackage lbuAccountPackage;
	
	@Autowired
	@Qualifier("LbuSourcePackagePackage")
	public LbuSourcePackagePackage lbuSourcePackagePackage;
	
	@Autowired
	@Qualifier("HistoryPackage")
	HistoryPackage historyPackage;
	
	@Autowired
	@Qualifier("SysHistoryService")
	SysHistoryService sysHistoryService;
	
	
	public static String endDateString = Definition.ENDDATE_STRING;
	
	public static Integer licenseCount = Definition.LICENSE_COUNT;
	
	public static String dateFormatYyyyMMdd = Definition.DATEFORMAT_YYYYMMDD;
	
	public static String dateTimeMMddyyyyHHmmss = Definition.DATEFORMAT_MMDDYYYYHHMMSS;
	
	@Value( "${strRemoteTimeZone}" )
	public String strRemoteTimeZone;
	
	@Value( "${strLocalTimeZone}" )
	public String strLocalTimeZone;
	
	public Properties ecmws;
	public Properties urlws;
	
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public void setEcmws(Properties ecmws)
	{
		this.ecmws = ecmws;
	}
	
	public void setUrlws(Properties urlws)
	{
		this.urlws = urlws;
	}
	
	public String getStrRemoteTimeZone()
	{
		return TimeZone.getTimeZone("UTC").getID();
		/*
		if(env != null)
		{
			return this.env.getProperty("strRemoteTimeZone");
		} else {
			return this.ecmws.getProperty("strRemoteTimeZone");
		}
		*/
	}
	
	public String getStrLocalTimeZone()
	{
		if(env != null)
		{
			return this.env.getProperty("strLocalTimeZone");
		} else {
			return this.ecmws.getProperty("strLocalTimeZone");
		}
	}
	
	public String getOrderWsUrlCreationOrder()
	{
		if(this.orderWsUrlCreationOrder == null)
		{
			this.orderWsUrlCreationOrder = this.urlws.getProperty("order.ws.url.creation.Order");
		} 
		
		return this.orderWsUrlCreationOrder;
	}
	
	public void setLbuAccountPackage(LbuAccountPackage lbuAccountPackage)
	{
		this.lbuAccountPackage = lbuAccountPackage;
	}
	
	public void setLbuAgreementPackage(LbuAgreementPackage lbuAgreementPackage)
	{
		this.lbuAgreementPackage = lbuAgreementPackage;
	}
	
	public void setLbuSourcePackagePackage(LbuSourcePackagePackage lbuSourcePackagePackage)
	{
		this.lbuSourcePackagePackage = lbuSourcePackagePackage;
	}
	
	public void setLbuContentSubPackage(LbuContentSubPackage lbuContentSubPackage)
	{
		this.lbuContentSubPackage = lbuContentSubPackage;
	}
	
	public void setHistoryPackage(HistoryPackage historyPackage)
	{
		this.historyPackage = historyPackage;
	}
	
	public void setSysHistoryService(SysHistoryService sysHistoryService)
	{
		this.sysHistoryService = sysHistoryService;
	}
	
	public void setAgreementUnitMap(AgreementUnitMap agreementUnitMap)
	{
		this.agreementUnitMap = agreementUnitMap;
	}
	
	public String getProductPGUIDPrefix()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.DataArea.OrderBO.ListOfOrderLineItems.OrderLineItem.ProductPGUID.prefix");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.DataArea.OrderBO.ListOfOrderLineItems.OrderLineItem.ProductPGUID.prefix");
		}
	}
	
	public String getSellingType()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.DataArea.OrderBO.ListOfOrderLineItems.OrderLineItem1.SellingType");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.DataArea.OrderBO.ListOfOrderLineItems.OrderLineItem1.SellingType");
		}
	}
	
	public String getAgreementPGUIDFRPrefix()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.DataArea.OrderBO.ListOfOrderLineItems.OrderLineItem.AgreementPGUID.FR.prefix");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.DataArea.OrderBO.ListOfOrderLineItems.OrderLineItem.AgreementPGUID.FR.prefix");
		}
	}
	
	public String getSendVCEmail()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.Header.MigrationActions.SendVCEmail");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.Header.MigrationActions.SendVCEmail");
		}
	}
	
	public String getSendWelcomeKit()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.Header.MigrationActions.SendWelcomeKit");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.Header.MigrationActions.SendWelcomeKit");
		}
	}
	
	public String getSourceApp()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.Header.commonTrackingMessage.dataGroup.sourceApp");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.Header.commonTrackingMessage.dataGroup.sourceApp");
		}
	}
	
	public String getCommonTrackingMessageXmlns()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.Header.commonTrackingMessage.xmlns");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.Header.commonTrackingMessage.xmlns");
		}
	}
	
	public String getHeaderXmlns()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.Header.xmlns");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.Header.xmlns");
		}
	}
	
	public String getMigrationFlag()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.Header.MigrationFlag");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.Header.MigrationFlag");
		}
	}
	
	public String getHeaderCreatedDate()
	{
		DateFormat dateFormat = new SimpleDateFormat(Definition.DATEFORMAT_MMDDYYYYHHMMSS);
		Date date = new Date();
		
		return getTranslatedDateTimeZone(dateFormat.format(date), getStrLocalTimeZone(), getStrRemoteTimeZone());
	}

	public String getHeaderLastUpdatedDate()
	{
		return this.getHeaderCreatedDate();
	}
	
	public String getOrderXmlns()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.xmlns");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.xmlns");
		}
	}
	
	public String getSoapEnv()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.soapEnv");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.soapEnv");
		}
	}
	
	public String getOrderXsi()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.xsi");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.xsi");
		}
	}
	
	public String getOrderXsd()
	{
		if(env != null)
		{
			return env.getProperty("order.ws.creation.Order.xsd");
		} else {
			return this.ecmws.getProperty("order.ws.creation.Order.xsd");
		}
	}

	/**
	 * 
	 * @param lbuContentSubs
	 * @return
	 * @throws Exception 
	 * 
	 * transformLbuContentSubsToLbuAgreements is not in use now.
	 */
	
	public LbuAgreements transformLbuContentSubsToLbuAgreementsFromUrl(LbuContentSubs lbuContentSubs, String customerPguid) throws Exception
	{
		try {
		
		
			/**
			 * list to be returned back
			 */
			LbuAgreements lbuAgreements = new LbuAgreements();
			lbuAgreements.setList(new ArrayList<LbuAgreement>());
			
			/**
			 * map used to compare  the agreements
			 */
			LbuAgreementMap lbuAgreementMap = new LbuAgreementMap();
			lbuAgreementMap.setMap(new HashMap<String, LbuAgreement>());
			
			/**
			 * format when receive date
			 */
			SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_YYYYMMDD);
			/**
			 * format transformed into ws.
			 */
			SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_MMDDYYYYHHMMSS);
			
			for(LbuContentSub lbuContentSub : lbuContentSubs.getList())
			{
				/**
				 * put it if AssetAgreementId not found in the map.
				 * 
				 */
				if(!lbuAgreementMap.getMap().containsKey(lbuContentSub.getAssetAgreementId()))
				{
					
					Date lbuContentSubBeginDate = formatter.parse(lbuContentSub.getBeginDate());
					Date lbuContentSubEndDate = formatter.parse(lbuContentSub.getEndDate());
					
					LbuAgreement lbuAgreement = new LbuAgreement();
					lbuAgreement.setAgreementId(lbuContentSub.getContractNum());
					lbuAgreement.setAssetAgreementId(lbuContentSub.getAssetAgreementId());
					//lbuAgreement.setProductId(lbuContentSub.getProductId());
					lbuAgreement.setBeginDate(formatter2.format(lbuContentSubBeginDate));
					lbuAgreement.setEndDate(formatter2.format(lbuContentSubEndDate));
					lbuAgreement.setLicenseCount(lbuContentSub.getMaxUsers());
					
					/**
					 * transform peoplesoft code to assemblyId
					 */
					String assemblyId = lbuSourcePackagePackage.lbuSourcePackageSelectAssemblyId(lbuContentSub.getLbuSetId(), lbuContentSub.getLbuSourcePackageId());
					lbuAgreement.setProductId(assemblyId);
					
					lbuAgreement.setCustomerPguid(customerPguid);
					
					lbuAgreementMap.getMap().put(lbuAgreement.getAssetAgreementId(), lbuAgreement);
				} else {
					/**
					 * else it exists in the map
					 * we need to campare date time to make the combination of agreements
					 */
					LbuAgreement lbuAgreement = (LbuAgreement)lbuAgreementMap.getMap().get(lbuContentSub.getAssetAgreementId());
					
					Date lbuContentSubEndDate = formatter.parse(lbuContentSub.getEndDate());
					Date lbuAgreementEndDate = formatter2.parse(lbuAgreement.getEndDate());
	
					/**
					 * replace end date time if the ContentSubEndDate is bigger than AgreementEndDate
					 */
					if(lbuContentSubEndDate.compareTo(lbuAgreementEndDate) > 0)
					{
						lbuAgreement.setEndDate(formatter2.format(lbuContentSubEndDate));
					}
					
					Date lbuContentSubBeginDate = formatter.parse(lbuContentSub.getBeginDate());
					Date lbuAgreementBeginDate = formatter2.parse(lbuAgreement.getBeginDate());
					
					/**
					 * replace begin date if the lbuContentSubBeginDate is smaller than lbuAgreementBeginDate
					 */
					if(lbuContentSubBeginDate.compareTo(lbuAgreementBeginDate) < 0)
					{
						lbuAgreement.setBeginDate(formatter2.format(lbuContentSubBeginDate));
					}
					
					/**
					 * update the agreement in the map
					 */
					lbuAgreementMap.getMap().put(lbuAgreement.getAssetAgreementId(), lbuAgreement);
				}
			}
			
			/**
			 * dump the map into the list before return
			 */
			for(Map.Entry<String, LbuAgreement> entry : lbuAgreementMap.getMap().entrySet())
			{
				lbuAgreements.getList().add((LbuAgreement)entry.getValue());
			}
			
			return lbuAgreements;
		
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param lbuContentSubs
	 * @return
	 * @throws Exception
	 */
	public LbuAgreements transformLbuContentSubsToLbuAgreementsFromDb(LbuContentSubs lbuContentSubs, String transIdSysHistory) throws Exception
	{
		try {
			LbuAgreements lbuAgreements= new LbuAgreements();
			lbuAgreements.setList(new ArrayList<LbuAgreement>());
			
			LbuAgreementMap lbuAgreementMap = new LbuAgreementMap();
			lbuAgreementMap.setMap(new HashMap<String, LbuAgreement>());
			
			Map<String,Object> mapLbuAgreementPopulate = new HashMap<String,Object>();
			/**
			 * here we translate all the contentSubs to agreements with the same contract number 
			 * (AgreementID (contract number in peoplesoft) something like 0000124968, AgreementPGUID sometyhing like urn:FR:Ag:0000124968)
			 * It means all the contentSubs here belongs to 1 contract
			 */

			for(LbuContentSub lbuContentSub : lbuContentSubs.getList())
			{
				
				
				String assemblyId = lbuSourcePackagePackage.lbuSourcePackageSelectAssemblyId(lbuContentSub.getLbuSetId(), lbuContentSub.getLbuSourcePackageId());
				/**
				 * pass it and handle the next contentSub if assemblyId not found by LbuSourcePackageId
				 */
				if(assemblyId == null)
				{
					/**
					 * output error message for workflow manager in batch console.
					 * Don't remove the out.println
					 */
					String messageSourcePackageNotFound = lbuContentSub.getLbuSourcePackageId() + " not found in LBU_SOURCEPACKAGE";
					
					logger.error(lbuContentSub.getLbuSourcePackageId() + " " + lbuContentSub.getLbuSetId() + " SourcePackageId not found for ContractNum " + lbuContentSub.getContractNum());
	
					ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(messageSourcePackageNotFound, ResponseMessage.FAILED));
	
					continue;
				}
				
				/**
				 * test if the account exists or not?
				 */
				try {
					this.lbuAccountPackage.selectLbuAccountById(lbuContentSub.getLbuAccountId(), lbuContentSub.getLbuSetId());
				} catch(Exception e) {
					logger.error(" No Account found for contentSub line 522 : " + lbuContentSub.getLbuContentSubId());
					continue;
				}
				
				
				/**
				 * here update asset
				 */
				this.lbuContentSubPackage.updateLbuContentSubAssetAgreementId(lbuContentSub.getLbuContentSubId(), lbuContentSub.getAssetAgreementId());
				lbuContentSub = this.lbuContentSubPackage.LbuContentSubSelectById(lbuContentSub.getLbuAccountId(), lbuContentSub.getContractNum(), lbuContentSub.getContractLineNum(), lbuContentSub.getLbuSourcePackageId());
			
	
				/**
				 * here update pguid
				 */
				if(agreementUnitMap.getMap().containsKey(lbuContentSub.getLbuSetId()))
				{
					String agreementPguid = agreementUnitMap.getMap().get(lbuContentSub.getLbuSetId()).getPrefix() + lbuContentSub.getContractNum();
					this.lbuContentSubPackage.updateLbuContentSubAgreementPguid(lbuContentSub.getLbuContentSubId(), agreementPguid);
				} else {
					logger.error(lbuContentSub.getContractNum() + " " + lbuContentSub.getLbuSetId() + " not found in order beans context xml");
					continue;
					//throw new Exception(lbuContentSub.getLbuSetId() + " not found in order beans context xml");
				}
				
				
				
				/**
				 * fetch the same AssetAgreementId only 1 time.
				 */
				/*
				if(lbuAgreementMap.getMap().containsKey(lbuContentSub.getAssetAgreementId()))
				{
					continue;
				}
				*/
	
				mapLbuAgreementPopulate = lbuAgreementPackage.LbuAgreementPopulate(lbuContentSub, transIdSysHistory, assemblyId);
				
				LbuAgreement lbuAgreement = lbuAgreementPackage.LbuAgreementSelectByAsset(lbuContentSub.getAssetAgreementId());
				
				if(lbuAgreement == null)
				{
					lbuAgreement = lbuAgreementPackage.LbuAgreementSelectByAsset(lbuContentSub.getAssetAgreementIdFromDb());
				}
				
				if(lbuAgreement == null)
				{
					String message = "lbuAgreement not found for lbuContentSub AssetAgreementId: " + lbuContentSub.getAssetAgreementId() + " or " + lbuContentSub.getAssetAgreementIdFromDb();
	
					logger.error(message);
					
					continue;
				}
				
				if( ((BigDecimal)mapLbuAgreementPopulate.get("AGREEMENT_IS_CREATED")).intValue() > 0 || ((BigDecimal)mapLbuAgreementPopulate.get("AGREEMENT_HAS_UPDATED")).intValue() > 0 )
				{
					lbuAgreements.setIsChanged(true);
				} 
				
				lbuAgreementMap.getMap().put(lbuAgreement.getAssetAgreementId(), lbuAgreement);
			}
			
	
			for(Map.Entry<String, LbuAgreement> entry : lbuAgreementMap.getMap().entrySet())
			{
				lbuAgreements.getList().add((LbuAgreement)entry.getValue());
			}
			
			return lbuAgreements;
		
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return null;
	}
	
	
	/**
	 * if the customer is new.
	 * we need to add some fixed agreements for him.
	 * @param lbuAgreements
	 * @param lbuAccount
	 * @return
	 * @throws Exception
	 */
	public LbuAgreements addFixedAgreementsWhenCreation(LbuAccount lbuAccount) throws Exception
	{
		try {
		LbuAgreements lbuAgreements = new LbuAgreements();
		lbuAgreements.setList(new ArrayList<LbuAgreement>());
		
		/**
		 * fetch the fixed products for a new customer
		 */
		LbuFeatures lbuFeatures = lbuAgreementPackage.LbuFeatureSelectAll();
		Integer index = 1;
		
		SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_MMDDYYYYHHMMSS);
		Date beginDate = new Date();
		
		Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.HOUR_OF_DAY, 0);
        
        String beginDateFormatted = formatter.format(now.getTime());
        
		/**
		 * add to the agreement list
		 */
		for(LbuFeature lbuFeature : lbuFeatures.getList())
		{
			LbuAgreement lbuAgreement = new LbuAgreement();
			lbuAgreement.setAgreementId(lbuAccount.getLbuAccountId());
			
			/**
			 * setAssetAgreementId is the same principal as the getAssetAgreementId in LbuContentSub
			 */
			lbuAgreement.setAssetAgreementId(lbuAgreement.getAgreementId() + "L" + StringHelper.getLastnCharacters("00" + index++, 3));
			lbuAgreement.setProductId(lbuFeature.getAssemblyId());
			lbuAgreement.setLicenseCount(licenseCount);
			lbuAgreement.setBeginDate(beginDateFormatted);
			lbuAgreement.setEndDate(endDateString);
			lbuAgreement.setCustomerPguid(lbuAccount.getCustomerPguid());
			
			lbuAgreements.getList().add(lbuAgreement);
		}
		
		return lbuAgreements;
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param assetAgreementId
	 * @return
	 * @throws Exception
	 */
	public LbuAgreement selectLbuAgreement(String assetAgreementId) throws Exception
	{
		LbuAgreement lbuAgreement = lbuAgreementPackage.LbuAgreementSelectByAsset(assetAgreementId);
		
		return lbuAgreement;
	}
	
	/**
	 * 
	 * @param lbuAgreement
	 * @return
	 * @throws Exception
	 */
	public LbuAgreement insertLbuAgreement(LbuAgreement lbuAgreement, String transId) throws Exception
	{
		lbuAgreementPackage.LbuAgreementCreate(lbuAgreement, transId);
		
		return lbuAgreement;
	}
	
	/**
	 * 
	 * @param lbuAgreement
	 * @return
	 * @throws Exception
	 */
	public LbuAgreement updateLbuAgreement(LbuAgreement lbuAgreement) throws Exception
	{
		lbuAgreementPackage.LbuAgreementUpdate(lbuAgreement);
		
		return lbuAgreement;
	}
	
	/**
	 * 
	 * @param customerPguid
	 * @return
	 * @throws Exception
	 */
	public LbuAgreements getLbuAgreementSelectByCustomer(String customerPguid) throws Exception
	{
		return lbuAgreementPackage.LbuAgreementSelectByCustomer(customerPguid);
	}
	
	/**
	 * 
	 * @param lbuAccount
	 * @return
	 * @throws Exception
	 */
	public Map<String, LbuAgreements> getLbuAgreementSelectByCustomerByType(LbuAccount lbuAccount) throws Exception
	{
		try {
			LbuAgreements lbuAgreementsProduct = new LbuAgreements();
			lbuAgreementsProduct.setList(new ArrayList<LbuAgreement>());
			LbuAgreements lbuAgreementsFeature = new LbuAgreements();
			lbuAgreementsFeature.setList(new ArrayList<LbuAgreement>());
			
			LbuAgreements lbuAgreements = getLbuAgreementSelectByCustomer(lbuAccount.getCustomerPguid());
			
			if(null != lbuAgreements)
			{
				for(LbuAgreement lbuAgreement:lbuAgreements.getList())
				{
					if(lbuAgreement.getAgreementId().equals(lbuAccount.getLbuAccountId()))
					{
						lbuAgreementsFeature.getList().add(lbuAgreement);
					} else {
						lbuAgreementsProduct.getList().add(lbuAgreement);
					}
				}
			}
			
			HashMap<String, LbuAgreements> map = new HashMap<String, LbuAgreements>();
			map.put(Definition.PRODUCT.toLowerCase(), lbuAgreementsProduct);
			map.put(Definition.FEATURE.toLowerCase(), lbuAgreementsFeature);

			return map;
			
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param lbuAgreements
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> createSyncOrderCemeaWs(LbuAgreements lbuAgreements, LbuAccount lbuAccount, String agreementType, String transIdSysHistory) throws Exception
	{
		try {
			String url = this.getOrderWsUrlCreationOrder();
			logger.info("createSyncOrderCemeaWs: " + url);
			
			
			Map<String,Object> returnMap =  new HashMap<String,Object>();
			
			SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_MMDDYYYYHHMMSS);
			String beginDateString = null;
			
			ListOfOrderLineItemsWrapper listOfOrderLineItemsWrapper = new ListOfOrderLineItemsWrapper();
			listOfOrderLineItemsWrapper.setList(new ArrayList<OrderLineItemWrapper>());
	
			OrderLineItemWrapper orderLineItemWrapper = new OrderLineItemWrapper();
			for(LbuAgreement lbuAgreement : lbuAgreements.getList()) {
				orderLineItemWrapper = new OrderLineItemWrapper();
				
				String remoteBeginDateStr = this.getTranslatedDateTimeZone(lbuAgreement.getBeginDate().toString(), getStrLocalTimeZone(), getStrRemoteTimeZone());
				String remoteEndDateStr = this.getTranslatedDateTimeZone(lbuAgreement.getEndDate(), getStrLocalTimeZone(), getStrRemoteTimeZone());
	
				orderLineItemWrapper.setAssetIntegrationID(lbuAgreement.getAssetAgreementId() + "P" + lbuAgreement.getProductId());
				orderLineItemWrapper.setProductPGUID(this.getProductPGUIDPrefix() + lbuAgreement.getProductId());
				orderLineItemWrapper.setSellingType(this.getSellingType());
				orderLineItemWrapper.setLicenseCount(lbuAgreement.getLicenseCount().toString());
				orderLineItemWrapper.setStartDate(remoteBeginDateStr);
				orderLineItemWrapper.setEndDate(remoteEndDateStr);
				listOfOrderLineItemsWrapper.getList().add(orderLineItemWrapper);
				
				if(beginDateString == null)
				{
					beginDateString = remoteBeginDateStr;
				} else {
					Date beginDate = formatter.parse(beginDateString);
				    Date date2 = formatter.parse(remoteBeginDateStr);
				    
				    if (beginDate.compareTo(date2) > 0) {
				    	beginDate = date2;
				    	beginDateString = formatter.format(beginDate).toString();
				    }
				}
			}
	
			/**
			 * fetch the customerPguid from agreement
			 */
			LbuAgreement firstLbuAgreement = lbuAgreements.getList().get(0);
			String customerPguid = firstLbuAgreement.getCustomerPguid();
	
			//String beginDateFormatted = this.getTranslatedDateTimeZone(beginDateString, getStrLocalTimeZone(), getStrRemoteTimeZone());
			
			/**
			 * AgreementId ==> contract num in peopleSoft
			 */
			OrderBOWrapper orderBOWrapper = new OrderBOWrapper();
			orderBOWrapper.setOrgCustomerID(customerPguid);
			orderBOWrapper.setAgreementStartDate(beginDateString);
			
			String remoteEndDateString = this.getTranslatedDateTimeZone(endDateString, getStrLocalTimeZone(), getStrRemoteTimeZone());
			orderBOWrapper.setAgreementEndDate(remoteEndDateString);
	
			if(!agreementUnitMap.getMap().containsKey(lbuAccount.getLbuSetId()))
			{
				logger.error(" LbuSetId in account is " + lbuAccount.getLbuSetId() + " not found in order-beans-context.xml ");
				throw new Exception(" LbuSetId in account is " + lbuAccount.getLbuSetId() + " not found in order-beans-context.xml ");
			}
			
			String agreementPrefix = agreementUnitMap.getMap().get(lbuAccount.getLbuSetId()).getPrefix();
	
			if(agreementType == Definition.AGREEMENT)
			{	
				orderBOWrapper.setAgreementPGUID( agreementPrefix + firstLbuAgreement.getAgreementId());		
				orderBOWrapper.setAgreementID(firstLbuAgreement.getAgreementId());
				orderBOWrapper.setMessageID(transIdSysHistory);
			} 
			
			if(agreementType == Definition.FEATURE)
			{	
				orderBOWrapper.setAgreementPGUID( agreementPrefix + lbuAccount.getLbuAccountId());		
				orderBOWrapper.setAgreementID(lbuAccount.getLbuAccountId());
				orderBOWrapper.setMessageID(transIdSysHistory);
			}
			orderBOWrapper.setListOfOrderLineItems(listOfOrderLineItemsWrapper);
	
			DataAreaWrapper dataAreaWrapper = new DataAreaWrapper();
			dataAreaWrapper.setOrderBO(orderBOWrapper);
			
			MigrationActionsWrapper migrationActionsWrapper = new MigrationActionsWrapper();
			migrationActionsWrapper.setSendVCEmail(this.getSendVCEmail());
			migrationActionsWrapper.setSendWelcomeKit(this.getSendWelcomeKit());
			
			DataGroupWrapper dataGroupWrapper = new DataGroupWrapper();
			dataGroupWrapper.setSourceApp(this.getSourceApp());
			
			CommonTrackingMessageWrapper commonTrackingMessageWrapper = new CommonTrackingMessageWrapper();
			commonTrackingMessageWrapper.setXmlns(this.getCommonTrackingMessageXmlns());
			commonTrackingMessageWrapper.setDataGroup(dataGroupWrapper);
			
			HeaderWrapper headerWrapper = new HeaderWrapper();
			headerWrapper.setXmlns(this.getHeaderXmlns());
			headerWrapper.setCommonTrackingMessage(commonTrackingMessageWrapper);
			headerWrapper.setMigrationFlag(this.getMigrationFlag());
			headerWrapper.setMigrationActions(migrationActionsWrapper);
			headerWrapper.setCreatedDate(this.getHeaderCreatedDate());
			headerWrapper.setLastUpdatedDate(this.getHeaderLastUpdatedDate());
			
			OrderRootWrapper orderRootWrapper = new OrderRootWrapper();
			orderRootWrapper.setXmlns(this.getOrderXmlns());
			orderRootWrapper.setSoapEnv(this.getSoapEnv());
			orderRootWrapper.setXsi(this.getOrderXsi());
			orderRootWrapper.setXsd(this.getOrderXsd());
			orderRootWrapper.setHeader(headerWrapper);
			orderRootWrapper.setDataArea(dataAreaWrapper);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(OrderRootWrapper.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    
		    StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(orderRootWrapper, sw);
	
			String params = sw.toString();

			/**
			 * check the transId exist or not.
			 * Create one if not found.
			 * Because send agreement in bowam generate nothing for sysHistory.
			 * So we need to add history for the this kind agreement sending
			 */
			sysHistoryService.findOneHistoryByTransIdOrCreate(null, Definition.AGREEMENT, Definition.CREATE, 
					lbuAccount.getLbuSetId() + Definition.SPACE + lbuAccount.getLbuAccountId() + Definition.SPACE + firstLbuAgreement.getAgreementId(),
					null, null, null, null, transIdSysHistory, null, null, Definition.PLSQL_AGREEMENT);
			
			/**
			 * update field message in SYS_HISTORY
			 */
			historyPackage.sysHistoryUpdateMessage(transIdSysHistory, params);
			
			/**
			 * update SEND_STATUS in SYS_HISTORY
			 */
			historyPackage.sysHistoryUpdateSendStatus(transIdSysHistory, Definition.SUCCESS);
			
			/**
			 * call remote ws
			 */
			Map<?, ?> returnedMapFromSyncOrderCemea = HttpUtility.newRequest(url,HttpUtility.METHOD_POST,params, HttpUtility.FORMAT_XML, HttpUtility.SUCCESS_STATUS_OK_CREATED);

			if(returnedMapFromSyncOrderCemea.containsKey(Definition.RESPONSE_CODE))
			{
				if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(
							Integer.parseInt(returnedMapFromSyncOrderCemea.get(Definition.RESPONSE_CODE).toString())
						))
				{
					logger.error("Exception returnedMapFromSyncOrderCemea: ");
					logger.error(returnedMapFromSyncOrderCemea.get(Definition.RESPONSE_CODE).toString() + " " + returnedMapFromSyncOrderCemea.get(Definition.RESPONSE_CODE).toString());
					throw new Exception(returnedMapFromSyncOrderCemea.get(Definition.RESPONSE_CODE).toString() + " " + returnedMapFromSyncOrderCemea.get(Definition.RESPONSE_CODE).toString());
				}
			} else {
				logger.error("returnedMapFromSyncOrderCemea is empty");
			}
	
			String responsePrettyFormat = InputStreamTransformer.prettyFormat(returnedMapFromSyncOrderCemea.get(Definition.RESPONSE).toString(), InputStreamTransformer.DEFAULT_INDENT);
			WsMessageHelper.getMessageList().add(new WsMessage(url, HttpUtility.METHOD_PUT, params, responsePrettyFormat, returnedMapFromSyncOrderCemea.get(Definition.RESPONSE_CODE).toString(), firstLbuAgreement.getAssetAgreementId() ));
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The lbuAgreement " + firstLbuAgreement.getAssetAgreementId() + "P" + firstLbuAgreement.getProductId() + " is sent ", ResponseMessage.SUCCESS));
	
			return returnMap;	
		}
		catch(Exception e)
		{			
			logger.error(e.getMessage());
			logger.error(e.getCause().toString());
			logger.error(e.getStackTrace().toString());
			
			//throw new Exception(e);
		}
		return null;
	}
	
	/**
	 * 
	 * @param lbuContentSubs
	 * @param lbuAccount
	 * @param accountStatus
	 * @throws Exception
	 */
	public Map<?,?> handleLbuAgreements(LbuContentSubs lbuContentSubs,LbuAccount lbuAccount, String accountStatus) throws Exception
	{
		try {
		Boolean isRealSendAgreement = false;
		if(lbuContentSubs.getList().size() == 0)
		{
			logger.error(" lbuContentSubs list is empty. No agreement to send.");
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" lbuContentSubs list is empty. No agreement to send.", ResponseMessage.FAILED));
			return null;
		}

		/**
		 * transIdSysHistory for product agreements
		 */
		String transIdSysHistoryProduct = BaseService.generateTransIdSysHistory();
		
		LbuAgreements lbuAgreements = this.transformLbuContentSubsToLbuAgreementsFromDb(lbuContentSubs, transIdSysHistoryProduct);
		
		if(lbuAgreements.getIsChanged() == true)
		{
			if(this.isRealSendWs())
			{
				this.createSyncOrderCemeaWs(lbuAgreements, lbuAccount, Definition.AGREEMENT, transIdSysHistoryProduct);
				isRealSendAgreement = true;
			}
		}
		
		Map<String, Object> mapLbuAgreementCreate = new HashMap<String, Object>();

		if(accountStatus == Definition.CREATE)
		{
			/**
			 * transIdSysHistory for feature agreements when creation of account
			 */
			String transIdSysHistoryFeature = BaseService.generateTransIdSysHistory();
			
			lbuAgreements = this.addFixedAgreementsWhenCreation(lbuAccount);
			/**
			 * insert feature agreement into db
			 */
			if(lbuAgreements.getList().size() > 0)
			{
				for(LbuAgreement lbuAgreement: lbuAgreements.getList())
				{
					mapLbuAgreementCreate = lbuAgreementPackage.LbuAgreementCreate(lbuAgreement, transIdSysHistoryFeature);
				}

				if(this.isRealSendWs())
				{
					this.createSyncOrderCemeaWs(lbuAgreements, lbuAccount, Definition.FEATURE, transIdSysHistoryFeature);
					isRealSendAgreement = true;
				}
			}
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Definition.GENERATED_TRANS_ID_PRODUCT, transIdSysHistoryProduct);
		map.put(Definition.IS_REAL_SEND_AGREEMENT, isRealSendAgreement);
		
		return map;
		
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
		return ecmws;
	}
	
	/**
	 * 
	 * @param listAgreementsId
	 * @throws Exception
	 */
	public void sendAgreementsToWs(List<String> listAssetAgreementId) throws Exception
	{
		try {
			ResponseHelper.cleanMessageList();
			
			Map<String, LbuAgreements> LbuAgreementsMap = new HashMap<String, LbuAgreements>();
	
	
			for(String assetAgreementId : listAssetAgreementId)
			{
				LbuAgreement lbuAgreement =  this.selectLbuAgreement(assetAgreementId);
				if(lbuAgreement != null)
				{
					/**
					 * if the agreemnt list for this agreementId already exist
					 */
					if(LbuAgreementsMap.containsKey(lbuAgreement.getAgreementId()))
					{
						LbuAgreements lbuAgreements = LbuAgreementsMap.get(lbuAgreement.getAgreementId());
						lbuAgreements.getList().add(lbuAgreement);
						
						LbuAgreementsMap.put(lbuAgreement.getAgreementId(), lbuAgreements);
					} else {
						/**
						 * else create a new agreement list
						 */
						LbuAgreements lbuAgreements = new LbuAgreements();
						lbuAgreements.getList().add(lbuAgreement);
						
						LbuAgreementsMap.put(lbuAgreement.getAgreementId(), lbuAgreements);
					}
				}
			}
			
			
			/**
			 * regroup agreements in different list by agreementId
			 */
			for(Map.Entry<String, LbuAgreements> entry : LbuAgreementsMap.entrySet())
			{
				LbuAgreements lbuAgreements = entry.getValue();
				if(lbuAgreements.getList().size() != 0)
				{
					String transIdSysHistory = BaseService.generateTransIdSysHistory();
					
					LbuAccount lbuAccount = lbuAccountPackage.selectLbuAccountByCustomerPGUID(lbuAgreements.getList().get(0).getCustomerPguid());
					this.createSyncOrderCemeaWs(lbuAgreements, lbuAccount, Definition.AGREEMENT, transIdSysHistory);
				}
			}
		
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
		}
	}
	
	
	/**
	 * after update contentSub
	 * You need to trigger this method to recalculate the agreement
	 * @throws Exception 
	 */
	public boolean reCalculateAgreement(String agreementId) throws Exception
	{
		LbuAgreements lbuAgreements = new LbuAgreements();
		String transIdSysHistory = null;
		
		LbuContentSubs lbuContentSubs = this.lbuContentSubPackage.lbuContentSubSearch(null, null, agreementId, null, null, null, null, null, null, null, null, null, null, null);
	
		if(lbuContentSubs.getList().size() > 0)
		{
			transIdSysHistory = BaseService.generateTransIdSysHistory();
			
            lbuAgreements = this.transformLbuContentSubsToLbuAgreementsFromDb(lbuContentSubs, transIdSysHistory);
            
            if(lbuAgreements.getIsChanged()) {
            	return true;
            } else { 
            	return false;
            }
		}else {
			logger.error(" No contentSub found for contact num: " + agreementId);
		}
		
		return false;
	}
}
