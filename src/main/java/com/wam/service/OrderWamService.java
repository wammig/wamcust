package com.wam.service;

import static java.lang.System.out;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.db.gbsd.helper.Orders;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.ws.in.sync.order.reply.SyncOrderReply;
import com.wam.model.ws.validation.BaseValidationEventHandler;
import com.wam.utility.InputStream.InputStreamTransformer;
import com.wam.utility.definition.Definition;

/**
 * 
 * @author QINC1
 *
 */
@Service("OrderWamService")
@PropertySource("classpath:application.properties")
public class OrderWamService extends BaseService{
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("AccountWamService")
	private AccountWamService accountWamService;
	
	@Autowired
	@Qualifier("LocationWamService")
	private LocationWamService locationWamService;
	
	
	@Autowired
	@Qualifier("ContentSubWamService")
	private ContentSubWamService contentSubWamService;
	
	@Value( "${jaxb.validation.SyncOrderReply1}" )
	private String pathValidationSyncOrderReply;
	
	// receiving format
	public SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_YYYYMMDDHHMMSS);
	// insert format
	public SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_DDMMYYYY);
	
	public Pattern datePattern = Pattern.compile(AccountWamService.regexDate);
	public Matcher dateMatcher;
	
	public LocationWamService getLocationWamService()
	{
		return this.locationWamService;
	}
	
	public ContentSubWamService getContentSubWamService()
	{
		return this.contentSubWamService;
	}
	
	
	/**
	 * Transform url parameters to 
	 * 1 account
	 * n locations
	 * n contentSubs
	 * 
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public  Orders OrderTransform(Map map) throws Exception
	{

		LbuAccount lbuAccount = new LbuAccount();

		/**
		 *  aid ==> LBU_ACCOUNT_ID. Not nullable
		 */
		lbuAccount = accountWamService.validationAid(map, lbuAccount);
		
		/**
		 *  match acn to an1(ACCOUNT_NAME1) and an2(ACCOUNT_NAME2)
		 *  an1 is not nullable
		 */
		/**
		 *  begin extract acn
		 */
		lbuAccount = accountWamService.validationAcn(map, lbuAccount);
		/**
		 * end extract acn
		 */
		
		/**
		 * uid ==> AUDIT_USER_ID. Not nullable
		 */
		lbuAccount = accountWamService.validationUid(map, lbuAccount);
		
		/**
		 * bst ==> BILLABLE_STATUS. Not nullable
		 */
		lbuAccount = accountWamService.validationBst(map, lbuAccount);
		

		/**
		 * act ==> ACCOUNT_TYPE. Not nullable
		 */
		lbuAccount = accountWamService.validationAct(map, lbuAccount);

		/**
		 * dte ==> CREATED_DATETIME. Not nullable
		 */
		lbuAccount = accountWamService.validationDte(map, lbuAccount);
		
		/**
		 * udte ==> UPDATED_DATETIME. nullable
		 */
		lbuAccount = accountWamService.validationUdte(map, lbuAccount);
		
		/**
		 * lbu ==> LBU_SET_ID. Not nullable
		 */
		lbuAccount = accountWamService.validationLbu(map, lbuAccount);
		
		/**
		 * set helper text in account name
		 */
		lbuAccount.setAccountName1(lbuAccount.getAccountNameWithHelper());
		
		
		Map locationsTransformReturnedMap = locationWamService.LocationsTransform(map);

		/**
		 * if primaryLbuLocation exist.
		 * update the account with the primary Location
		 */
		LbuLocations lbuLocations = (LbuLocations)locationsTransformReturnedMap.get(LocationWamService.lbuLocations);
		
		if(lbuLocations.getList().size() == 1)
		{
			LbuLocation primaryLbuLocation = lbuLocations.getList().get(0);
			
			lbuAccount.setAddressLine1(primaryLbuLocation.getAddressLine1());
			lbuAccount.setAddressLine2(primaryLbuLocation.getAddressLine2());
			lbuAccount.setCityName(primaryLbuLocation.getCityName());
			lbuAccount.setCountryName(primaryLbuLocation.getCountryName());
			lbuAccount.setZipCode(primaryLbuLocation.getZipCode());
			lbuAccount.setStateOrProvinceCode(primaryLbuLocation.getStateOrProvinceCode());
			lbuAccount.setCountryCode(primaryLbuLocation.getCountryCode());
			
		} else if(locationsTransformReturnedMap.containsKey(LocationWamService.primaryLbuLocation)) {
			
			LbuLocation primaryLbuLocation = (LbuLocation)locationsTransformReturnedMap.get(LocationWamService.primaryLbuLocation);
			
			lbuAccount.setAddressLine1(primaryLbuLocation.getAddressLine1());
			lbuAccount.setAddressLine2(primaryLbuLocation.getAddressLine2());
			lbuAccount.setCityName(primaryLbuLocation.getCityName());
			lbuAccount.setCountryName(primaryLbuLocation.getCountryName());
			lbuAccount.setZipCode(primaryLbuLocation.getZipCode());
			lbuAccount.setStateOrProvinceCode(primaryLbuLocation.getStateOrProvinceCode());
			lbuAccount.setCountryCode(primaryLbuLocation.getCountryCode());
		}

		Map contentSubsTransformReturnedMap = contentSubWamService.ContentSubsTransform(map);
		LbuContentSubs lbuContentSubs = (LbuContentSubs)contentSubsTransformReturnedMap.get(ContentSubWamService.lbuContentSubs);

		Orders orders = new Orders();
		orders.setLbuAccount(lbuAccount);
		orders.setLbuLocations(lbuLocations);
		orders.setLbuContentSubs(lbuContentSubs);
		
		
		return orders;
	}
	
	/**
	 * validate input xml
	 * transform input xml to java object
	 * @param inputStream
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> SyncOrderReplyTransformInputStream(InputStream inputStream) throws Exception
	{
		try {
			String input = InputStreamTransformer.byteArrayOutputStreamTransform(inputStream);
			
			SyncOrderReply syncOrderReply = (SyncOrderReply)SyncOrderReplyTransformString(input);

			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(Definition.SYNC_ORDER_REPLY, syncOrderReply);
			map.put(Definition.XML, input);
			
			return map;
		}
		catch(Exception ex)
		{	
			throw new Exception(ex.getMessage());
		}
		
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public SyncOrderReply SyncOrderReplyTransformString(String input) throws Exception
	{
		try {
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(new File(this.pathValidationSyncOrderReply));
			// projectDir + this.pathValidationSyncOrderReply
			
			JAXBContext jaxbContextInComing = JAXBContext.newInstance(SyncOrderReply.class);
			Unmarshaller jaxbUnmarshaller = jaxbContextInComing.createUnmarshaller();
			jaxbUnmarshaller.setSchema(schema);
			BaseValidationEventHandler baseValidationEventHandler = new BaseValidationEventHandler();
			jaxbUnmarshaller.setEventHandler(baseValidationEventHandler);

			StringReader inputReader = new StringReader(input);
			SyncOrderReply syncOrderReply = (SyncOrderReply)jaxbUnmarshaller.unmarshal(inputReader);

			if(baseValidationEventHandler.hasEvents())
	        {
				for(ValidationEvent event:baseValidationEventHandler.getEvents())
	            {					
					throw new Exception("code: " + HttpStatus.BAD_REQUEST + " message: " + event.getMessage().toString());
	            }
	        }
			
			return syncOrderReply;
		}
		catch(Exception ex)
		{
			String message = "";
			if(ex.getMessage() != null) {
				message = ex.getMessage();
			} 
			else if(ex.getCause() != null) {
				message = ex.getCause().toString();
			}
			
			throw new Exception(message);
		}
	}
	
}
