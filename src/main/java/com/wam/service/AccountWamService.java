package com.wam.service;

import static java.lang.System.out;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wam.aspect.EcmCustomerHistoryLoggingAspect;
import com.wam.model.admin.AdminDataTables;
import com.wam.model.config.ecm.BusinessUnitMap;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.model.mvc.response.helper.WsMessage;
import com.wam.model.mvc.response.helper.WsMessageHelper;
import com.wam.model.ws.out.creation.customer.CommonTrackingMessageWrapper;
import com.wam.model.ws.out.creation.customer.CommunicationWrapper;
import com.wam.model.ws.out.creation.customer.DataAreaWrapper;
import com.wam.model.ws.out.creation.customer.DataGroupWrapper;
import com.wam.model.ws.out.creation.customer.HeaderWrapper;
import com.wam.model.ws.out.creation.customer.ListOfCommunicationWrapper;
import com.wam.model.ws.out.creation.customer.ListOfPlaceOfBusinessWrapper;
import com.wam.model.ws.out.creation.customer.ListOfSegmentWrapper;
import com.wam.model.ws.out.creation.customer.OrgCustomerBOWrapper;
import com.wam.model.ws.out.creation.customer.OrgCustomerWrapper;
import com.wam.model.ws.out.creation.customer.PlaceOfBusinessWrapper;
import com.wam.model.ws.out.creation.customer.SegmentWrapper;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.packages.LbuAccountPackage;
import com.wam.stored.packages.LbuLocationPackage;
import com.wam.stored.procedure.LbuAccountSelectByIdStoredProcedure;
import com.wam.utility.InputStream.InputStreamTransformer;
import com.wam.utility.definition.Definition;
import com.wam.utility.helper.StringHelper;
import com.wam.utility.net.HttpUtility;

/**
 * 
 * @author QINC1
 *
 */
@Service("AccountWamService")
@PropertySources({
    @PropertySource(value="classpath:ecmws.properties", ignoreResourceNotFound = true),
    @PropertySource(value="classpath:application.properties", ignoreResourceNotFound = true)
})
public class AccountWamService extends BaseService{
	
	public final static Logger logger = LoggerFactory.getLogger(AccountWamService.class);
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("businessUnitMap")
	private BusinessUnitMap businessUnitMap;
	
	@Autowired
	@Qualifier("LbuAccountPackage")
	LbuAccountPackage lbuAccountPackage;
	
	@Autowired
	@Qualifier("LbuLocationPackage")
	LbuLocationPackage lbuLocationPackage;
	
	@Autowired
	HistoryPackage historyPackage;
	
	@Autowired
	@Qualifier("EcmCustomerHistoryLoggingAspect")
	EcmCustomerHistoryLoggingAspect ecmCustomerHistoryLoggingAspect;
	
	public void setEcmCustomerHistoryLoggingAspect(EcmCustomerHistoryLoggingAspect ecmCustomerHistoryLoggingAspect)
	{
		 this.ecmCustomerHistoryLoggingAspect = ecmCustomerHistoryLoggingAspect;
	}
	
	@Value( "${ecm.ws.url.creation.customer}" )
	private String ecmWsUrlCreationCustomer;
	
	/**
	 * regex to check if the acn string conform or not
	 */
	public static String regexAcn = "[\\w\\s�-�-.,&ÜÄÖüäöß]+[|]{1}([\\w\\s�-�-.,&ÜÄÖüäöß]+)?";
	public static String regexAdr = regexAcn + "[|]{1}" + regexAcn + "[|]{1}([\\d\\w\\W\\s]{1,15})?[|]{1}([\\w\\s�-�-.,&ÜÄÖüäöß]+)?[|]{1}[A-Z]{3}";
	public static String regexDate = "[19|20]{2}[0-9]{12}";
	
	public static Boolean isChanged = false;
	
	public static Map<String, String> bstAllowedTypes = createBstAllowedTypes();
	
	public Properties ecmws;
	public Properties urlws;
	
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public void setEcmws(Properties ecmws)
	{
		this.ecmws = ecmws;
	}
	
	public void setUrlws(Properties urlws)
	{
		this.urlws = urlws;
	}
	
	public void setBusinessUnitMap(BusinessUnitMap businessUnitMap)
	{
		this.businessUnitMap = businessUnitMap;
	}
	
	public String getEcmWsUrlCreationCustomer()
	{
		if(this.ecmWsUrlCreationCustomer == null)
		{
			this.ecmWsUrlCreationCustomer = this.urlws.getProperty("ecm.ws.url.creation.customer");
		}
		
		return this.ecmWsUrlCreationCustomer;
	}
	
	public void setHistoryPackage(HistoryPackage historyPackage)
	{
		this.historyPackage = historyPackage;
	}
	
	public HistoryPackage getHistoryPackage()
	{
		return this.historyPackage;
	}
	
	/**
	 * check the billable status
	 * @return
	 */
	private static Map<String, String> createBstAllowedTypes(){
		Map<String, String> aMap = new HashMap();
		aMap.put("Billable", "Billable");
		aMap.put("Internal", "Internal");
		aMap.put("Privileged", "Privileged");
		return Collections.unmodifiableMap(aMap);
	}
	
	
	
	// receiving format example: 20180301124809
	public SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_YYYYMMDDHHMMSS);
	// insert format
	public SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_DDMMYYYY);
	
	public Pattern datePattern = Pattern.compile(AccountWamService.regexDate);
	public Matcher dateMatcher;
	
	
	public String getValidAddressFlag()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.ValidAddressFlag");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.ValidAddressFlag");
		}
	}
	
	public String getGeographicLocationFlag()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.GeographicLocationFlag");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.GeographicLocationFlag");
		}
	}
	
	public String getPrimaryAddressIndicator()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.PrimaryAddressIndicator");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ListOfPlaceOfBusiness.PlaceOfBusiness1.PrimaryAddressIndicator");
		}
	}
	
	public String getTrustLifeCyclePGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.TrustLifeCyclePGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.TrustLifeCyclePGUID");
		}
	}
	
	public String getTrustLifeCycleDesc()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.TrustLifeCycleDesc");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.TrustLifeCycleDesc");
		}
	}
	
	public String getAgencyType()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.AgencyType");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.AgencyType");
		}
	}
	
	public String getCustomerClassTypePGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.CustomerClassTypePGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.CustomerClassTypePGUID");
		}
	}
	
	public String getCustomerClassTypeDesc()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.CustomerClassTypeDesc");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.CustomerClassTypeDesc");
		}
	}
	
	public String getCustomerSubClassTypePGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.CustomerSubClassTypePGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.CustomerSubClassTypePGUID");
		}
	}
	
	public String getCustomerSubClassTypeDesc()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.CustomerSubClassTypeDesc");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.CustomerSubClassTypeDesc");
		}
	}
	
	public String getSecondarySubClassTypePGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.SecondarySubClassTypePGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.SecondarySubClassTypePGUID");
		}
	}
	
	public String getIndustryClassTypePGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.IndustryClassTypePGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.IndustryClassTypePGUID");
		}
	}
	
	public String getIndustrySubClassTypePGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.IndustrySubClassTypePGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.IndustrySubClassTypePGUID");
		}
	}	
	
	public String getIPLockoutPGUID()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.IPLockoutPGUID");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.IPLockoutPGUID");
		}
	}
	
	public String getCustomerEmulationIndicator()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.CustomerEmulationIndicator");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.CustomerEmulationIndicator");
		}
	}
	
	public String getServiceConsumerAssetId()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ServiceConsumerAssetId");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ServiceConsumerAssetId");
		}
	}
	
	public String getModifiedBy()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ModifiedBy");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ModifiedBy");
		}
	}
	
	public String getSourceAppName()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.SourceAppName");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.SourceAppName");
		}
	}
	
	public String getNs6()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ns6");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ns6");
		}
	}
	
	public String getNs11()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ns11");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ns11");
		}
	}
	
	public String getNs12()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ns12");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ns12");
		}
	}
	
	public String getNs13()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.customer.ns13");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.customer.ns13");
		}
	}
	
	/**
	 *  aid ==> LBU_ACCOUNT_ID. Not nullable
	 * @param map
	 * @param lbuAccount
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount validationAid(Map map,LbuAccount lbuAccount) throws Exception
	{
		if(map.get("aid") == null)
		{
			logger.error(" aid is not nullable");
			throw new Exception(" aid is not nullable");
		}
		lbuAccount.setLbuAccountId(map.get("aid").toString());
		lbuAccount.setGbsAccountId(map.get("aid").toString());
		lbuAccount.setGlpAccountId(map.get("aid").toString());
		
		return lbuAccount;
	}
	
	/**
	 *  match acn to an1(ACCOUNT_NAME1) and an2(ACCOUNT_NAME2)
	 *  an1 is not nullable
	 *  
	 * @param map
	 * @param lbuAccount
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount validationAcn(Map map,LbuAccount lbuAccount) throws Exception
	{
		if(!map.containsKey("acn")) {
			logger.error(" acn is invalid: an1|an2. an1 is not nullable");
			throw new Exception(" acn is invalid: an1|an2. an1 is not nullable");
		}
		
		String acnString = map.get("acn").toString();
		
		acnString = StringHelper.removeEtCommercial(acnString);
		
		Pattern acnPattern = Pattern.compile(AccountWamService.regexAcn);
		Matcher acnMatcher = acnPattern.matcher(acnString);

		if(acnMatcher.lookingAt() != true) {
			logger.error(" acn is invalid: an1|an2. an1 is not nullable");
			throw new Exception(" acn is invalid: an1|an2. an1 is not nullable");
		}
		
		String[] acnParts = acnString.split("\\|");
		if(acnParts.length == 0)
		{
			logger.error(" an1 is not nullable");
			throw new Exception(" an1 is not nullable");
		}
		if(acnParts.length >= 1) {
			lbuAccount.setAccountName1(acnParts[0]);
		}
		if(acnParts.length == 2) {
			lbuAccount.setAccountName2(acnParts[1]);
		} 
		
		return lbuAccount;
	}
	
	/**
	 * uid ==> AUDIT_USER_ID. Not nullable
	 * @param map
	 * @param lbuAccount
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount validationUid(Map map,LbuAccount lbuAccount) throws Exception
	{
		if(map.get("uid") == null)
		{
			logger.error(" uid is not nullable");
			throw new Exception(" uid is not nullable");
		}
		lbuAccount.setAuditUserId(map.get("uid").toString());
		
		return lbuAccount;
	}
	
	/**
	 * bst ==> BILLABLE_STATUS. Not nullable
	 * @param map
	 * @param lbuAccount
	 * @return
	 * @throws Exception
	 */
	public LbuAccount validationBst(Map map,LbuAccount lbuAccount) throws Exception
	{
		/*
		if(map.get("bst") == null)
		{
			throw new Exception(" bst is not nullable");
		}
		if(bstAllowedTypes.containsKey(map.get("bst").toString()) != true)
		{
			String message = generateErrorMessageBillableStatusType();
			throw new Exception(message);
		}
		*/
		lbuAccount.setBillableStatus("Billable");
		
		return lbuAccount;
	}
	
	/**
	 * act ==> ACCOUNT_TYPE. Not nullable
	 * @param map
	 * @param lbuAccount
	 * @return
	 * @throws Exception
	 */
	public LbuAccount validationAct(Map map,LbuAccount lbuAccount ) throws Exception
	{
		/*
		if(map.get("act") == null)
		{
			throw new Exception(" act is not nullable");
		}
		*/
		lbuAccount.setAccountType("Corporate");
		
		return lbuAccount;
	}
	
	/**
	 * match adr to ad1(ADDRESS_LINE1), ad2(ADDRESS_LINE2), cty(CITY_NAME), ctn(COUNTY_NAME), 
	 * zip(ZIP_CODE), spc(STATE_OR_PROVINCE_CODE), ctr(COUNTRY_CODE)
	 * @param map
	 * @param lbuAccount
	 * @return
	 * @throws Exception
	 */
	public LbuAccount validationAdr(Map map,LbuAccount lbuAccount ) throws Exception
	{
		if(map.get("adr") == null) {
			logger.error("adr is invalid: adr1|adr2|cty|ctn|zip|spc|ctr. adr1,cty,zip,ctr are not nullable");
			throw new Exception("adr is invalid: adr1|adr2|cty|ctn|zip|spc|ctr. adr1,cty,zip,ctr are not nullable");
		}

		String[] adrParts = map.get("adr").toString().split("\\|");
		
		Pattern adrPattern = Pattern.compile(AccountWamService.regexAdr);
		Matcher adrMatcher = adrPattern.matcher(map.get("adr").toString());

		if(adrMatcher.lookingAt() != true)
		{
			logger.error("adr is invalid: adr1|adr2|cty|ctn|zip|spc|ctr. adr1,cty,zip,ctr are not nullable");
			throw new Exception("adr is invalid: adr1|adr2|cty|ctn|zip|spc|ctr. adr1,cty,zip,ctr are not nullable");
		}
		if(adrParts.length >= 1) {
			lbuAccount.setAddressLine1(adrParts[0]);
		}
		if(adrParts.length >= 2) {
			lbuAccount.setAddressLine2(adrParts[1]);
		}
		if(adrParts.length >= 3) {
			lbuAccount.setCityName(adrParts[2]);
		}
		if(adrParts.length >= 4) {
			lbuAccount.setCountryName(adrParts[3]);
		}
		if(adrParts.length >= 5) {
			lbuAccount.setZipCode(adrParts[4]);
		}
		if(adrParts.length >= 6) {
			lbuAccount.setStateOrProvinceCode(adrParts[5]);
		}
		if(adrParts.length >= 7) {
			lbuAccount.setCountryCode(adrParts[6]);
		}
		
		return lbuAccount;
	}
	
	
	/**
	 * dte ==> CREATED_DATETIME. Not nullable
	 * @param map
	 * @param lbuAccount
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount validationDte(Map map,LbuAccount lbuAccount) throws Exception
	{
		try {
			String dte = map.get("dte").toString();
			dateMatcher = datePattern.matcher(dte);
			
			if(dateMatcher.lookingAt() != true)
			{
				logger.error(" dte is not nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
				throw new Exception(" dte is not nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
			}
			
			Date dateTime = formatter.parse(map.get("dte").toString());
			
			lbuAccount.setCreatedDatetime(formatter2.format(dateTime));
			
			return lbuAccount;
		} catch(Exception ex) {
			
			logger.error(" dte is not nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
			throw new Exception(" dte is not nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
		}
	}
	
	/**
	 * udte ==> UPDATED_DATETIME. nullable
	 * @param map
	 * @param lbuAccount
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount validationUdte(Map map,LbuAccount lbuAccount) throws Exception
	{
		try {
			// id updated datetime is empty. We set it with created datetime
			String udte = map.get("dte").toString();
			if(map.get("udte") != null) {
				udte = map.get("udte").toString();
			}
			
			dateMatcher = datePattern.matcher(udte);
			
			if(dateMatcher.lookingAt() != true)
			{
				logger.error(" udte is not nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
				throw new Exception(" udte is not nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
			}

			Date updateddateTime = formatter.parse(udte);
			
			lbuAccount.setUpdatedDatetime(formatter2.format(updateddateTime));
			
			return lbuAccount;
		} catch(Exception ex) {
			logger.error(" udte is nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
			throw new Exception(" udte is nullable. Format: yyyyMMddHHmmss. Example: 20180301124809");
		}
	}
	
	
	/**
	 * lbu ==> LBU_SET_ID. Not nullable
	 * @param map
	 * @param lbuAccount
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount validationLbu(Map map,LbuAccount lbuAccount) throws Exception
	{
		if(map.get("lbu") == null)
		{
			logger.error(" lbu is not nullable");
			throw new Exception(" lbu is not nullable");
		}
		lbuAccount.setLbuSetId(map.get("lbu").toString());
		
		return lbuAccount;
	}
	
	
	
	/**
	 * LbuAccountTransform
	 * receive all the parameters and extract them to insert into table LBU_ACCOUNT
	 * 
	 * List of all parameters from the map 
	 * 	prc=acc
	 *	prs=cre
	 *	aid
	 *	acn=an1|an2
	 *	uid
	 *	act
	 *	adr=ad1|ad2|cty|ctn|zip|spc|ctr
	 *	dte
	 *	lbu
	 *
	 *	acn ==> is a composition of an1 and an2
	 *	adr ==> is a composition of ad1, ad2, cty, ctn, zip, spc, ctr
	 *
	 * all parameters match the colomn names in table LBU_ACCOUNT
	 *  prc	==> PROCESS_TYPE
	 *  prs	==> PROCESS_SUBTYPE
	 *  aid	==> LBU_ACCOUNT_ID
	 *  an1	==> ACCOUNT_NAME1
	 *  an2	==> ACCOUNT_NAME2
	 *  uid	==> AUDIT_USER_ID
	 *          SALES_REP_ID
	 *          EXT_ORDER_NO
	 *          MAX_USERS
	 *          BILLABLE_STATUS
	 *  act	==> ACCOUNT_TYPE
	 *  ad1	==> ADDRESS_LINE1
	 *	ad2	==> ADDRESS_LINE2
	 *  cty	==> CITY_NAME
	 *	ctn	==> COUNTY_NAME
     *  zip	==> ZIP_CODE
	 *	spc	==> STATE_OR_PROVINCE_CODE
	 *  ctr	==> COUNTRY_CODE
	 *          UPDATED_DATETIME
     *  dte	==> CREATED_DATETIME
	 *          DATA_MIGRATION
     *  lbu	==> LBU_SET_ID
	 * 
	 * @param map
	 * @return LbuAccount
	 * @throws Exception
	 */
	public LbuAccount LbuAccountTransform(Map map) throws Exception
	{
		
		LbuAccount lbuAccount = new LbuAccount();
		//lbuAccountTransformer.setProcessType(map.get("prc").toString());
		//lbuAccountTransformer.setProcessSubtype(map.get("prs").toString());
		
		/**
		 *  aid ==> LBU_ACCOUNT_ID. Not nullable
		 */
		lbuAccount = this.validationAid(map, lbuAccount);
		
		/**
		 *  match acn to an1(ACCOUNT_NAME1) and an2(ACCOUNT_NAME2)
		 *  an1 is not nullable
		 */
		lbuAccount = this.validationAcn(map, lbuAccount);

		/**
		 * uid ==> AUDIT_USER_ID. Not nullable
		 */
		lbuAccount = this.validationUid(map, lbuAccount);
		
		/**
		 * bst ==> BILLABLE_STATUS. Not nullable
		 */
		lbuAccount = this.validationBst(map, lbuAccount);
		
		/**
		 * act ==> ACCOUNT_TYPE. Not nullable
		 */
		lbuAccount = this.validationAct(map, lbuAccount);
		
		/**
		 *  match adr to ad1(ADDRESS_LINE1), ad2(ADDRESS_LINE2), cty(CITY_NAME), ctn(COUNTY_NAME), 
		 *  zip(ZIP_CODE), spc(STATE_OR_PROVINCE_CODE), ctr(COUNTRY_CODE)
		 */
		lbuAccount = this.validationAdr(map, lbuAccount);

		/**
		 * dte ==> CREATED_DATETIME. Not nullable
		 */
		lbuAccount = this.validationDte(map, lbuAccount);
		
		/**
		 * udte ==> UPDATED_DATETIME. nullable
		 */
		lbuAccount = this.validationUdte(map, lbuAccount);
		
		/**
		 * lbu ==> LBU_SET_ID. Not nullable
		 */
		lbuAccount = this.validationLbu(map, lbuAccount);
		
		/**
		 * set helper text in account name
		 */
		lbuAccount.setAccountName1(lbuAccount.getAccountNameWithHelper());
		
		
		return lbuAccount;
	}
	
	/**
	 * call insertLbuAccount in the package
	 * @param lbuAccount
	 * @throws Exception 
	 */
	public LbuAccount insertLbuAccount(LbuAccount lbuAccount) throws Exception
	{
		Map<String,Object> map = (HashMap<String, Object>)lbuAccountPackage.insertLbuAccount(lbuAccount);
		
		return (LbuAccount)map.get("lbuAccount");
	}
	
	/**
	 * call updateLbuAccount in the package
	 * @param lbuAccount
	 * @throws Exception 
	 */
	public LbuAccount updateLbuAccount(LbuAccount lbuAccount) throws Exception
	{
		Map<String,Object> map = (HashMap<String, Object>)lbuAccountPackage.updateLbuAccount(lbuAccount);
		
		return (LbuAccount)map.get("lbuAccount");
	}
	
	
	/**
	 * call the stocked procedure defined in oracle to select the account by id
	 * @param lbuAccount
	 * @throws Exception 
	 */
	public LbuAccount selectLbuAccountById(String lbuAccountId, String lbuSetId) throws Exception
	{
		try {
			LbuAccountSelectByIdStoredProcedure sp = new LbuAccountSelectByIdStoredProcedure(dataSource);
			
			Map outMap = sp.execute(
					lbuSetId,
					lbuAccountId
					);
			
			LbuAccount lbuAccountDb = sp.transformAccountCursorToObject(outMap);
			
			return lbuAccountDb;
		} catch(Exception e) {
			
		}
		return null;
	}
	
	/**
	 * search LbuAccount with criteras
	 * @return
	 * @throws Exception
	 */
	public LbuAccounts searchLbuAccount(
			String lbuSetId, 
			String lbuAccountId, 
			String gbsAccountId,
			String glpAccountId, 
			String accountName1,
			String accountName2,
			String salesRepId,
			String accountType,
			String zipCode,
			String countryCode,
			String billableStatus,
			String addressLine1
			) throws Exception
	{
		LbuAccounts lbuAccounts =(LbuAccounts)lbuAccountPackage.searchLbuAccount(
				lbuSetId,
				lbuAccountId,
				gbsAccountId,
				glpAccountId,
				accountName1,
				accountName2,
				salesRepId,
				accountType,
				zipCode,
				countryCode,
				billableStatus,
				addressLine1);
		
		return lbuAccounts;
	}
	
	
	public AdminDataTables searchLbuAccount2(
			String lbuSetId, 
			String lbuAccountId, 
			String gbsAccountId,
			String accountName1,
			String accountName2,
			String city,
			String zipCode,
			String countryCode,
			String billableStatus,
			String addressLine1,
			String customerPguid,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception
	{
		AdminDataTables adminLbuAccounts =(AdminDataTables)lbuAccountPackage.searchLbuAccount2(
												lbuSetId,
												lbuAccountId,
												gbsAccountId,
												accountName1,
												accountName2,
												city,
												zipCode,
												countryCode,
												billableStatus,
												addressLine1,
												customerPguid,
												fromNumber,
											    numberPerPage
											);
		
		return adminLbuAccounts;
	}
	
	/**
	 * 
	 * @param lbuAccountId
	 * @param setId
	 * @param action
	 * @return
	 * @throws Exception 
	 */
	public Map<String,Object> sendCustomerToEcmWs(String lbuAccountId, String setId, String action) throws Exception
	{
		ResponseHelper.cleanMessageList();
		
		LbuAccount lbuAccount = this.selectLbuAccountById(lbuAccountId, setId);
		
		if(lbuAccount == null)
		{
			logger.error("lbuAccount " + lbuAccountId + " not found ");
			throw new Exception("lbuAccount " + lbuAccountId + " not found ");
		}
		
		LbuLocations lbuLocations = lbuLocationPackage.searchLbuLocation(lbuAccount.getLbuSetId(), lbuAccount.getLbuAccountId(), null, null, null, null, null, null, null);		
		Map<String,Object> mapCreateOrUpdateCustomerEcmWs = createOrUpdateCustomerEcmWs(lbuAccount, lbuLocations, action);
		
		return mapCreateOrUpdateCustomerEcmWs;
	}
	
	
	/**
	 * call ecm ws to create or update customer in ecm
	 * @param lbuAccountId
	 * @return
	 * @throws Exception 
	 */
	public Map<String,Object> createOrUpdateCustomerEcmWs(LbuAccount lbuAccount, LbuLocations lbuLocations, String action) throws Exception
	{	
		String url = this.getEcmWsUrlCreationCustomer();
		
		String replyStatus = "";
		Map<String,Object> returnMap =  new HashMap<String,Object>();

		LbuAccount lbuAccountDb = this.selectLbuAccountById(lbuAccount.getLbuAccountId(), lbuAccount.getLbuSetId());
		
		/**
		 * single SegmentWrapper
		 */
		SegmentWrapper segmentWrapper = new SegmentWrapper();
		segmentWrapper.setSegmentTypePGUID(null);
		segmentWrapper.setSegmentType(null);
		segmentWrapper.setSegmentPGUID(null);
		segmentWrapper.setSegmentDescription(null);
		
		/**
		 * add single SegmentWrapper into ListOfSegmentWrapper
		 */
		ListOfSegmentWrapper listOfSegmentWrapper = new ListOfSegmentWrapper();
		listOfSegmentWrapper.setList(new ArrayList<SegmentWrapper>());
		listOfSegmentWrapper.getList().add(segmentWrapper);
		
		
		/**
		 * single CommunicationWrapper
		 */
		CommunicationWrapper communicationWrapper = new CommunicationWrapper();
		communicationWrapper.setCommTypeDesc(null);
		communicationWrapper.setCommSubTypeDesc(null);
		communicationWrapper.setCommValue(null);
		
		/**
		 * add single CommunicationWrapper into ListOfCommunicationWrapper
		 */
		ListOfCommunicationWrapper listOfCommunicationWrapper = new ListOfCommunicationWrapper();
		listOfCommunicationWrapper.setList(new ArrayList<CommunicationWrapper>());
		listOfCommunicationWrapper.getList().add(communicationWrapper);
		
		
		/**
		 * Single CommunicationWrapper in place
		 */
		CommunicationWrapper communicationInPlaceWrapper = new CommunicationWrapper();
		communicationInPlaceWrapper.setCommTypeDesc(null);
		communicationInPlaceWrapper.setCommSubTypeDesc(null);
		communicationInPlaceWrapper.setCommValue(null);
		
		
		/**
		 * add single CommunicationWrapper place into ListOfCommunicationWrapper place
		 */
		ListOfCommunicationWrapper listOfCommunicationInPlaceWrapper = new ListOfCommunicationWrapper();
		listOfCommunicationInPlaceWrapper.setList(new ArrayList<CommunicationWrapper>());
		listOfCommunicationInPlaceWrapper.getList().add(communicationInPlaceWrapper);
		
		ListOfPlaceOfBusinessWrapper listOfPlaceOfBusinessWrapper = new ListOfPlaceOfBusinessWrapper();
		listOfPlaceOfBusinessWrapper.setList(new ArrayList<PlaceOfBusinessWrapper>());
		
		/**
		 * if the lbuLocations list is prepared.
		 * use the lbuLocations list.
		 * Otherwise use the default location in lbuAccount.
		 */
		if(lbuLocations != null && lbuLocations.getList().size() > 0 )
		{
			for(LbuLocation lbuLocation : lbuLocations.getList())
			{
				
				PlaceOfBusinessWrapper placeOfBusinessWrapper = new PlaceOfBusinessWrapper();
				
				/**
				 * addressPGUID is different for create and update
				 */
				if(Definition.CREATE.equals(action))
				{
					placeOfBusinessWrapper.setAddressPGUID(null);
				}
				
				if(Definition.UPDATE.equals(action))
				{
					placeOfBusinessWrapper.setAddressPGUID(lbuLocation.getAddressPGUID());
				}
				placeOfBusinessWrapper.setAddress1(lbuLocation.getAddressLine1());
				placeOfBusinessWrapper.setAddress2(lbuLocation.getAddressLine2());
				placeOfBusinessWrapper.setAddress3(null);
				placeOfBusinessWrapper.setCity(lbuLocation.getCityName());
				placeOfBusinessWrapper.setCountrySubdivisionDescription(null);
				placeOfBusinessWrapper.setCountry(lbuLocation.getCountryName());
				placeOfBusinessWrapper.setCountryPGUID(lbuLocation.getCountryCode());
				placeOfBusinessWrapper.setCountrySubdivisionPGUID(null);
				placeOfBusinessWrapper.setCountrySubdivisionCode(null);
				placeOfBusinessWrapper.setCounty(null);
				placeOfBusinessWrapper.setTaxGeocode(null);
				placeOfBusinessWrapper.setPostalCode(lbuLocation.getZipCode());
				
				placeOfBusinessWrapper.setValidAddressFlag(this.getValidAddressFlag());
				placeOfBusinessWrapper.setGeographicLocationFlag(this.getGeographicLocationFlag());

				/**
				 * pobpguid is different for create and update
				 */
				if(Definition.CREATE.equals(action))
				{
					placeOfBusinessWrapper.setPobPGUID(null);
				} 
				
				if(Definition.UPDATE.equals(action))
				{
					placeOfBusinessWrapper.setPobPGUID(lbuLocation.getPobPGUID());
				}
				
				placeOfBusinessWrapper.setPrimaryAddressIndicator(this.getPrimaryAddressIndicator());
				placeOfBusinessWrapper.setListOfCommunication(listOfCommunicationInPlaceWrapper);
				
				listOfPlaceOfBusinessWrapper.getList().add(placeOfBusinessWrapper);
			}
		} else {
			
			PlaceOfBusinessWrapper placeOfBusinessWrapper = new PlaceOfBusinessWrapper();
			
			/**
			 * addressPGUID is different for create and update
			 */
			if(Definition.CREATE.equals(action))
			{
				placeOfBusinessWrapper.setAddressPGUID(null);
			}
			
			if(Definition.UPDATE.equals(action))
			{
				placeOfBusinessWrapper.setAddressPGUID(lbuAccountDb.getAddressPGUID());
			}
			placeOfBusinessWrapper.setAddress1(lbuAccountDb.getAddressLine1());
			placeOfBusinessWrapper.setAddress2(lbuAccountDb.getAddressLine2());
			placeOfBusinessWrapper.setAddress3(null);
			placeOfBusinessWrapper.setCity(lbuAccountDb.getCityName());
			placeOfBusinessWrapper.setCountrySubdivisionDescription(null);
			placeOfBusinessWrapper.setCountry(lbuAccountDb.getCountryName());
			placeOfBusinessWrapper.setCountryPGUID(lbuAccountDb.getCountryCode());
			placeOfBusinessWrapper.setCountrySubdivisionPGUID(null);
			placeOfBusinessWrapper.setCountrySubdivisionCode(null);
			placeOfBusinessWrapper.setCounty(null);
			placeOfBusinessWrapper.setTaxGeocode(null);
			placeOfBusinessWrapper.setPostalCode(lbuAccountDb.getZipCode());
			
			placeOfBusinessWrapper.setValidAddressFlag(this.getValidAddressFlag());
			placeOfBusinessWrapper.setGeographicLocationFlag(this.getGeographicLocationFlag());

			/**
			 * pobpguid is different for create and update
			 */
			if(Definition.CREATE.equals(action))
			{
				placeOfBusinessWrapper.setPobPGUID(null);
			} 
			
			if(Definition.UPDATE.equals(action))
			{
				placeOfBusinessWrapper.setPobPGUID(lbuAccountDb.getPobpguid());
			}
			
			placeOfBusinessWrapper.setPrimaryAddressIndicator(this.getPrimaryAddressIndicator());
			placeOfBusinessWrapper.setListOfCommunication(listOfCommunicationInPlaceWrapper);
			
			listOfPlaceOfBusinessWrapper.getList().add(placeOfBusinessWrapper);
			
		}
		
		
		/**
		 * OrgCustomerBO
		 */
		OrgCustomerBOWrapper orgCustomerBOWrapper = new OrgCustomerBOWrapper();
		
		if(Definition.CREATE.equals(action))
		{
			orgCustomerBOWrapper.setOrgCustomerPGUID(null);
		}
		if(Definition.UPDATE.equals(action))
		{
			orgCustomerBOWrapper.setOrgCustomerPGUID(lbuAccountDb.getCustomerPguid());
		}
		
		String businessUnitPGUID = "";
		String businessUnitDescription = "";
		
		if(lbuAccountDb.getLbuSetId() == null)
		{
			logger.error(" LbuSetId in account is null ");
			throw new Exception(" LbuSetId in account is null ");
		}
		
		if(!businessUnitMap.getMap().containsKey(lbuAccountDb.getLbuSetId()))
		{
			logger.error(" LbuSetId in account is " + lbuAccountDb.getLbuSetId() + " not found in ecm-beans-context.xml ");
			throw new Exception(" LbuSetId in account is " + lbuAccountDb.getLbuSetId() + " not found in ecm-beans-context.xml ");
		}

		businessUnitPGUID = businessUnitMap.getMap().get(lbuAccountDb.getLbuSetId()).getPguid();
		businessUnitDescription = businessUnitMap.getMap().get(lbuAccountDb.getLbuSetId()).getDesc();
		
		orgCustomerBOWrapper.setTrustLifeCyclePGUID(this.getTrustLifeCyclePGUID());
		orgCustomerBOWrapper.setTrustLifeCycleDesc(this.getTrustLifeCycleDesc());
		orgCustomerBOWrapper.setBusinessUnitPGUID(businessUnitPGUID);
		orgCustomerBOWrapper.setBusinessUnitDescription(businessUnitDescription);
		orgCustomerBOWrapper.setAgencyType(this.getAgencyType());
		orgCustomerBOWrapper.setOrgCustomerName(lbuAccountDb.getAccountName1());
		orgCustomerBOWrapper.setListOfSegment(listOfSegmentWrapper);
		orgCustomerBOWrapper.setListOfCommunication(listOfCommunicationWrapper);
		orgCustomerBOWrapper.setCustomerClassTypePGUID(this.getCustomerClassTypePGUID());
		orgCustomerBOWrapper.setCustomerClassTypeDesc(this.getCustomerClassTypeDesc());
		orgCustomerBOWrapper.setCustomerSubClassTypePGUID(this.getCustomerSubClassTypePGUID());
		orgCustomerBOWrapper.setCustomerSubClassTypeDesc(this.getCustomerSubClassTypeDesc());
		orgCustomerBOWrapper.setSecondarySubClassTypePGUID(this.getSecondarySubClassTypePGUID());
		orgCustomerBOWrapper.setSecondarySubClassTypeDesc(null);
		orgCustomerBOWrapper.setIndustryClassTypePGUID(this.getIndustryClassTypePGUID());
		orgCustomerBOWrapper.setIndustryClassTypeDesc(null);
		orgCustomerBOWrapper.setIndustrySubClassTypePGUID(this.getIndustrySubClassTypePGUID());
		orgCustomerBOWrapper.setIndustrySubClassTypeDesc(null);
		orgCustomerBOWrapper.setNoOfAttorneys(null);
		orgCustomerBOWrapper.setListOfPlaceOfBusiness(listOfPlaceOfBusinessWrapper);
		orgCustomerBOWrapper.setiPLockoutPGUID(this.getIPLockoutPGUID());
		orgCustomerBOWrapper.setCustomerEmulationIndicator(this.getCustomerEmulationIndicator());
		
		DataAreaWrapper dataAreaWrapper = new DataAreaWrapper();
		dataAreaWrapper.setOrgCustomerBO(orgCustomerBOWrapper);
		
		DataGroupWrapper dataGroupWrapper = new DataGroupWrapper();
		dataGroupWrapper.setTransId(BaseService.generateTransIdEcm());
		
		CommonTrackingMessageWrapper commonTrackingMessageWrapper = new CommonTrackingMessageWrapper();
		commonTrackingMessageWrapper.setDataGroup(dataGroupWrapper);
		
		HeaderWrapper headerWrapper = new HeaderWrapper();
		headerWrapper.setCommonTrackingMessage(commonTrackingMessageWrapper);
		headerWrapper.setServiceConsumerAssetId(this.getServiceConsumerAssetId());
		headerWrapper.setModifiedBy(this.getModifiedBy());
		headerWrapper.setSourceAppName(this.getSourceAppName());
		
		OrgCustomerWrapper orgCustomer = new OrgCustomerWrapper();
		orgCustomer.setNs6(this.getNs6());
		orgCustomer.setNs11(this.getNs11());
		orgCustomer.setNs12(this.getNs12());
		orgCustomer.setNs13(this.getNs13());
		orgCustomer.setHeader(headerWrapper);
		orgCustomer.setDataArea(dataAreaWrapper);
		
		/**
		 * JAXB transform java objact to xml
		 */
		JAXBContext jaxbContext = JAXBContext.newInstance(OrgCustomerWrapper.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    //jaxbMarshaller.marshal(orgCustomer, System.out);
	    
	    // use StringWriter to convert jaxbMarshaller to String before calling remote ws
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(orgCustomer, sw);
		
		String params = sw.toString();
		
		/**
		 * initialize the orgCustomerWrapper for outside try catch
		 */
		com.wam.model.ws.in.creation.customer.OrgCustomerWrapper orgCustomerWrapper;
		
		Map returnedMapFromEcm = null;
		
		/**
		 * call remote ws
		 */
		try {
			returnedMapFromEcm = HttpUtility.newRequest(url,HttpUtility.METHOD_PUT,params, HttpUtility.FORMAT_XML, HttpUtility.SUCCESS_STATUS_OK_CREATED);
			
			if(returnedMapFromEcm.isEmpty())
			{
				logger.error(" returnedMapFromEcm Empty ");
				throw new Exception(" returnedMapFromEcm Empty ");
			}
			
			String response = returnedMapFromEcm.get(Definition.RESPONSE).toString();
				
			String responsePrettyFormat = InputStreamTransformer.prettyFormat(response, InputStreamTransformer.DEFAULT_INDENT);
			WsMessageHelper.getMessageList().add(new WsMessage(url, HttpUtility.METHOD_PUT, params, responsePrettyFormat, returnedMapFromEcm.get(Definition.RESPONSE_CODE).toString(), lbuAccountDb.getLbuAccountId()));
			
			 StringReader responseReader = new StringReader(response);
		        
		        /**
		         * when errors.
		         */
		        if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) 
		        {
		        	replyStatus = Definition.FAILURE;
		        }

				/**
				 *  when success
				 */
				if(Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) 
				{
					replyStatus = Definition.SUCCESS;
				}
				
				
				
				if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) 
				{
					logger.error(" Ecm returned error. " + returnedMapFromEcm.get(Definition.RESPONSE).toString());
		        	throw new Exception(" Ecm returned error. " + returnedMapFromEcm.get(Definition.RESPONSE).toString());
		        }
				
				/**
		         * JAXB transform xml to java object
		         */
		        JAXBContext jaxbContextInComing = JAXBContext.newInstance(com.wam.model.ws.in.creation.customer.OrgCustomerWrapper.class);
				Unmarshaller jaxbUnmarshaller = jaxbContextInComing.createUnmarshaller();
				orgCustomerWrapper = (com.wam.model.ws.in.creation.customer.OrgCustomerWrapper) jaxbUnmarshaller.unmarshal(responseReader);
					
				returnMap.put(Definition.LBU_ACCOUNT, lbuAccount);
				returnMap.put(Definition.PARAMS, params);
				returnMap.put(Definition.ACTION, action);
				returnMap.put(Definition.RETURNED_MAP_FROM_ECM, returnedMapFromEcm);
				returnMap.put(Definition.REPLY_STATUS, replyStatus);
				returnMap.put(Definition.RESPONSE, response);
		} 
		catch(SocketException e) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("SocketException: " + e.getMessage(), ResponseMessage.FAILED));
			ecmCustomerHistoryLoggingAspect.historyLoggingCustomerEcmWsAround(returnMap); 
			
			logger.error(" SocketException AccountWamService: " + e.getMessage() + "    " + e.getCause() + "    " + e.getStackTrace());
			
			return returnMap;
		}
		catch (IOException e) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("IOException: " + e.getMessage(), ResponseMessage.FAILED));
			ecmCustomerHistoryLoggingAspect.historyLoggingCustomerEcmWsAround(returnMap); 
			
			logger.error(" IOException AccountWamService: " + e.getMessage() + "    " + e.getCause() + "    " + e.getStackTrace());

			return returnMap;
	    }
		catch (Exception e) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("Exception: " + e.getMessage(), ResponseMessage.FAILED));
			ecmCustomerHistoryLoggingAspect.historyLoggingCustomerEcmWsAround(returnMap); 
			
			logger.error(" Exception AccountWamService: " + returnedMapFromEcm.toString());
			
			return returnMap;
	    }
       

		/**
		 * extract OrgCustomerPGUID, POBPGUID
		 * 
		 */
		try {
			List list = orgCustomerWrapper.getDataArea().getOrgCustomerBO().getListOfPlaceOfBusiness().getList();
		
			if(list.size() == 1)
			{
				com.wam.model.ws.in.creation.customer.PlaceOfBusinessWrapper placeOfBusinessInResponse = (com.wam.model.ws.in.creation.customer.PlaceOfBusinessWrapper)list.get(0);
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" Ecm " + action  +  " customer " +lbuAccount.getLbuAccountId() + " successfully.", ResponseMessage.SUCCESS));
				
				if(lbuAccountDb.getCustomerPguid() == null)
				{
					updateAccountCustomerPGUIDId(lbuAccount.getLbuAccountId(), placeOfBusinessInResponse.getOrgCustomerPGUID(), placeOfBusinessInResponse.getpOBPGUID(), placeOfBusinessInResponse.getAddressPGUID());
				}
				
				lbuAccount.setCustomerPguid(placeOfBusinessInResponse.getOrgCustomerPGUID());
				lbuAccount.setPobpguid(placeOfBusinessInResponse.getpOBPGUID());
				
				/**
				 * AddressPGUID of primary location
				 */
				lbuAccount.setAddressPGUID(placeOfBusinessInResponse.getAddressPGUID());
			
				/**
				 * lbuAccount is changed. so we need to put it again in the map
				 */
				returnMap.put(Definition.LBU_ACCOUNT, lbuAccount);

				ecmCustomerHistoryLoggingAspect.historyLoggingCustomerEcmWsAround(returnMap); 
				
				return returnMap; 
			} else {
				logger.error(" Ecm returned error. The size of ListOfPlaceOfBusiness is not 1 ");
				throw new Exception(" Ecm returned error. The size of ListOfPlaceOfBusiness is not 1  ");
			}
			
			

		} catch (Exception e) {
			logger.error(" Exception AccountWamService: " + returnedMapFromEcm.toString());
			
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(returnMap.get(Definition.RESPONSE).toString(), ResponseMessage.FAILED));
			ecmCustomerHistoryLoggingAspect.historyLoggingCustomerEcmWsAround(returnMap); 
			
			return returnMap; 
		}
		
		
		//return returnMap;
     }
	
	
	/**
	 * After calling the remote ws orgCustomer (creation customer)
	 * We need to update the account with the returned OrgCustomerPGUID and the returned POBPGUID
	 * 
	 * @param accountId
	 * @param customerPGUID
	 * @param pOBPGUID
	 * @throws Exception
	 */
	public List<ResponseMessage> updateAccountCustomerPGUIDId(String accountId, String customerPGUID, String pOBPGUID, String addressPGUID) throws Exception
	{
		return lbuAccountPackage.updateAccountCustomerPGUIDId(accountId, customerPGUID, pOBPGUID, addressPGUID);
	}
	
	/**
	 * generate error message if the table type not found
	 * return all Supported prc values
	 * @return
	 */
	public static String generateErrorMessageBillableStatusType()
	{
		String message= "parameter bst is not correct. Supported bst values: ";
		for(Map.Entry<String, String> entry : bstAllowedTypes.entrySet())
		{
			message += entry.getKey() + " ";
		}
		
		return message;
	}
	
	/**
	 * 
	 * @param lbuAccount
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> handleAccount(LbuAccount lbuAccount) throws Exception
	{
		LbuAccount lbuAccountFromDb = this.selectLbuAccountById(lbuAccount.getLbuAccountId(), lbuAccount.getLbuSetId());
		Boolean hasCreate = false;
		
		String accountStatus = Definition.CREATE;
		if(lbuAccountFromDb == null) {
			lbuAccountFromDb = this.insertLbuAccount(lbuAccount);
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("account " + lbuAccountFromDb.getLbuAccountId() + " inserted successfully.", ResponseMessage.SUCCESS));
			
			if(this.isRealSendWs())
			{
				this.createOrUpdateCustomerEcmWs(lbuAccountFromDb, new LbuLocations(), accountStatus);
				
				if(!hasCreate)
				{
					hasCreate = true;
				}
			}
		} else {
			lbuAccountFromDb = this.updateLbuAccount(lbuAccount);
			accountStatus = Definition.UPDATE;
			
			if(lbuAccountFromDb.getIsChanged()) {
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The account " + lbuAccountFromDb.getLbuAccountId() + " CustomerPguid " + lbuAccountFromDb.getCustomerPguid() + " updated successfully.", ResponseMessage.SUCCESS));
				if(this.isRealSendWs())
				{
					this.createOrUpdateCustomerEcmWs(lbuAccountFromDb, new LbuLocations(), accountStatus);
				}
			} else {
				
				if(lbuAccountFromDb.getCustomerPguid() == null)
				{
					if(this.isRealSendWs())
					{
						this.createOrUpdateCustomerEcmWs(lbuAccountFromDb, new LbuLocations(), accountStatus);
						
						if(!hasCreate)
						{
							hasCreate = true;
						}
					}
				}
				
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The account " + lbuAccountFromDb.getLbuAccountId() + " CustomerPguid " + lbuAccountFromDb.getCustomerPguid() + " is already updated. Nothing changed.", ResponseMessage.SUCCESS));
			}
			
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("accountStatus", accountStatus);
		map.put("lbuAccountFromDb", lbuAccountFromDb);
		
		if(hasCreate)
		{
			/**
			 * sleep when new location created
			 */
			//out.println("primary location sleep");
			Thread.sleep(Integer.parseInt(this.env.getProperty("before.order.sleep.time")));
			//out.println("primary location wakeup");
		}
		
		return map;
	}
	
	/**
	 * find account by customerPguid
	 * @param customerPguid
	 * @return
	 * @throws Exception
	 */
	public LbuAccount getLbuAccountByCustomerPguid(String customerPguid) throws Exception
	{
		return lbuAccountPackage.selectLbuAccountByCustomerPGUID(customerPguid);
	}
	
	
	public List<?> findAccountSys(String fromDate, String toDate, String cohorte) throws Exception
	{
		return historyPackage.findAccountSys(fromDate, toDate, cohorte);
	}
	
}
