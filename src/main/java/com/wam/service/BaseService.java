package com.wam.service;

import javax.naming.CommunicationException;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

import com.wam.test.db.BaseTest;
import com.wam.utility.definition.Definition;
import com.wam.utility.helper.IdGenerator;
import com.wam.utility.helper.TimeStamp;
import com.wam.utility.ldap.LdapBrowser;
import com.wam.utility.ldap.LdapEntry;

import static java.lang.System.out;

import java.net.UnknownHostException;

@PropertySources({
    @PropertySource(value="${urlws.environment.properties}", ignoreResourceNotFound = true)
})
public class BaseService extends BaseTest{

	@Autowired
	public Environment env;
	
	public static String EnvFlag = null;
	
	public String projectDir = getProjectDir();
	
	public static String logPath;
	
	
	/**
	 * return the project directory 
	 * @return
	 */
	private String getProjectDir()
	{
		return  this.getClass().getClassLoader().getResource("").getPath();//System.getProperty("user.dir");
	}

	
	@Autowired
	@Qualifier("dataSource")
    public DataSource dataSource;
	
//	@Autowired
//	@Qualifier("dataSourceMig")
//	public DataSource dataSourceMig;
	
	public void setDataSrouce(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSrouce()
	{
		if(dataSource == null)
		{
			return super.getDataSource();
		}
		
		return dataSource;
	}
	
//	public void setDataSrouceMig(DataSource dataSourceMig)
//	{
//		this.dataSourceMig = dataSourceMig;
//	}
	
	/**
	 * generate a transaction id for ecm ws
	 * @return
	 */
	public static String generateTransIdEcm()
	{
		return IdGenerator.createSessionId(24);
	}
	
	public static String generateTransIdSysHistory()
	{
		return IdGenerator.createSessionId(24);
	}
	
	/**
	 * 
	 * @return
	 * @throws CommunicationException
	 * @throws UnknownHostException
	 * @throws NamingException
	 */
	public static LdapEntry getLdapEntry() throws CommunicationException, UnknownHostException, NamingException
	{
		String login = System.getProperty("user.name");

    	if(login == null)
    	{
    		return null;
    	}
    	
		LdapEntry ldapentry = LdapBrowser.getLdapEntityLogin(login);
		
		return ldapentry;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public static boolean isITParis() throws Exception
	{
		LdapEntry ldapentry = getLdapEntry();
		
		if(ldapentry == null)
		{
			return false;
		}
		
		if(!isFranceOrParis(ldapentry))
		{
			return false;
		}
		
		if(!isIT(ldapentry))
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public static boolean isFranceOrParis(LdapEntry ldapentry) throws Exception
	{
		if(ldapentry == null)
		{
			ldapentry = getLdapEntry();
		}
		
		if(ldapentry == null)
		{
			return false;
		}
		
		if(!ldapentry.getL().equals("FRA") && !ldapentry.getL().equals("PARIS"))
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public static boolean isIT(LdapEntry ldapentry) throws Exception
	{
		if(ldapentry == null)
		{
			ldapentry = getLdapEntry();
		}
		
		if(ldapentry == null)
		{
			return false;
		}
		
		if(!ldapentry.getDepartment().contains("INFORMATIQUE"))
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param dateTime
	 * @param strLocalTimeZone
	 * @param strRemoteTimeZone
	 * @return
	 */
	public String getTranslatedDateTimeZone(String dateTime, String strLocalTimeZone, String strRemoteTimeZone)
	{
		return TimeStamp.changeZone(dateTime, Definition.DATEFORMAT_MMDDYYYYHHMMSS, strLocalTimeZone, strRemoteTimeZone); 
	}
	
	/**
	 * isRealSend decides send to WS or not after saved in db
	 * @return
	 */
	public Boolean isRealSendWs()
	{
		Boolean isRealSend = true;
		try {
			String isRealSendString = env.getProperty("workflow.manager.service.handle.real.send.ws");
			isRealSend = Boolean.valueOf(isRealSendString);

		} catch(Exception e) {
			return isRealSend;
		}
		
		return isRealSend;
	}
	
	public Boolean isRealSendEmail()
	{
		Boolean isRealSend = true;
		try {
			String isRealSendString = env.getProperty("workflow.manager.service.handle.test.mail.real.send");
			isRealSend = Boolean.valueOf(isRealSendString);

		} catch(Exception e) {			
			return isRealSend;
		}
		
		return isRealSend;
	}
	
	public int getBatchSleepTime()
	{
		int batchSleepTime = 1000;
		try {
			String batchSleepTimeString = env.getProperty("workflow.manager.service.handle.message.batch.sleep.time");
		
			return Integer.parseInt(batchSleepTimeString);
		} catch(Exception e) {
			return batchSleepTime;
		}
		
	}
	
	public int getSingleSleepTime()
	{
		int singleSleepTime = 1000;
		try {
			String singleSleepTimeString = env.getProperty("workflow.manager.service.handle.message.single.sleep.time");
			//out.println(singleSleepTimeString);
			
			return Integer.parseInt(singleSleepTimeString);
		} catch(Exception e) {			
			return singleSleepTime;
		}
		
	}
	
	public int getBatchHandlerNumber()
	{
		int batchHandleNumber = 1000;
		try {
			String batchHandleNumberString = env.getProperty("workflow.manager.service.handle.message.number");
		
			return Integer.parseInt(batchHandleNumberString);
		} catch(Exception e) {			
			return batchHandleNumber;
		}	
	}
	
	public String getBatchEmailAddress()
	{
		try {
			String email = env.getProperty("workflow.manager.service.handle.test.mail.to");
		
			return email;
		} catch(Exception e) {			
			return "Cheng.QIN@lexisnexis.fr";
		}	
	}
}
