package com.wam.service;

import static java.lang.System.out;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.stored.packages.HistoryPackage;
import com.wam.stored.procedure.SysHistoryCreateStoredProcedure;
import com.wam.utility.definition.Definition;

@Service("SysHistoryService")
public class SysHistoryService {

	@Autowired
	@Qualifier("HistoryPackage")
	HistoryPackage historyPackage;
	
	@Autowired
    DataSource dataSource;
	
	public void setHistoryPackage(HistoryPackage historyPackage)
	{
		this.historyPackage = historyPackage;
	}
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public SysHistory findOneHistoryByTransIdOrCreate(String historyId, String processType, 
			String processSubType, String entityId, String sendStatus, String replyStatus, String errorCode, 
			String errorMessage, String transId, String messageId, String message, String createdBy) throws Exception
	{
		SysHistory sysHistory = historyPackage.findSysHistoryByTransId(transId);

		if(null == sysHistory)
		{
			SysHistoryCreateStoredProcedure shsp = new SysHistoryCreateStoredProcedure(dataSource);
			
			sysHistory = new SysHistory();
			sysHistory.setHistoryId(null);
			sysHistory.setProcessType(Definition.AGREEMENT);
			sysHistory.setProcessSubType(Definition.CREATE);
			sysHistory.setEntityId(entityId);
			sysHistory.setSendStatus(null);
			sysHistory.setReplyStatus(null);
			sysHistory.setErrorCode(null);
			sysHistory.setErrorMessage(null);
			sysHistory.setTransId(transId);
			sysHistory.setMessageId(null);
			sysHistory.setMessage(null);
			sysHistory.setCreatedBy(Definition.PLSQL_AGREEMENT);
			
			Map shspOutmap = shsp.execute(sysHistory.getHistoryId(), sysHistory.getProcessType(), sysHistory.getProcessSubType(),
								sysHistory.getEntityId(), sysHistory.getSendStatus(), sysHistory.getReplyStatus(),
								sysHistory.getErrorCode(), sysHistory.getErrorMessage(), sysHistory.getTransId(),
								sysHistory.getMessageId(), sysHistory.getMessage(), sysHistory.getCreatedBy()
							 );
		} 
		
		return sysHistory;
		
	}
	
}
