package com.wam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.wam.aspect.HistoryLoggingSyncOrderReplyAspect;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.db.gbsd.helper.Orders;
import com.wam.model.mvc.response.helper.ErrorObject;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.model.ws.in.sync.order.reply.SyncOrderReply;
import com.wam.stored.packages.LbuSourcePackagePackage;
import com.wam.utility.definition.Definition;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.System.out;

import java.io.InputStream;

/**
 * 
 * @author QINC1
 *
 */
@Service("WamService")
public class WamService extends BaseService{
	
	@Autowired
	@Qualifier("AccountWamService")
	public AccountWamService accountWamService;

	@Autowired
	@Qualifier("LocationWamService")
	public LocationWamService locationWamService;
	
	@Autowired
	@Qualifier("OrderWamService")
	public OrderWamService orderWamService;
	
	@Autowired
	@Qualifier("ContentSubWamService")
	public ContentSubWamService contentSubWamService;
	
	@Autowired
	@Qualifier("AgreementService")
	public AgreementService agreementService;
	
	/*
	@Autowired
	@Qualifier("WorkFlowManagerService")
	public WorkFlowManagerService workFlowManagerService;
	*/
	@Autowired
	@Qualifier("LbuSourcePackagePackage")
	public LbuSourcePackagePackage lbuSourcePackagePackage;
	
	@Autowired
	public HistoryLoggingSyncOrderReplyAspect historyLoggingSyncOrderReplyAspect;
	
	public static final Map<String, String> mapDbTableService = createMapDbTablemapDbTableService();
	
	public static final Map<String, String> mapAction = createMapDbAction();
	
	/**
	 * A map Defines all parameters and their matched db table transformer
	 * @return
	 */
	private static Map<String, String> createMapDbTablemapDbTableService(){
		Map<String, String> aMap = new HashMap();
		aMap.put("acc", "LbuAccountTransform");
		aMap.put("ord", "OrderTransform");
		return Collections.unmodifiableMap(aMap);
	}
	
	/**
	 * map to the action
	 * @return
	 */
	private static Map<String, String> createMapDbAction(){
		Map<String, String> aMap = new HashMap();
		aMap.put("cre", "cre");
		aMap.put("udt", "udt");
		aMap.put("dlt", "dlt");
		return Collections.unmodifiableMap(aMap);
	}
	
	/**
	 * this method which table to map for the current parameters
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public Map<?, ?> wamTransform(Map<String, String> map) throws Exception
	{		
		Map<String, Object> returndMap = new HashMap<String, Object>();
		
		switch(mapDbTableService.get(map.get("prc")))
		{
			case "LbuAccountTransform":
					this.lbuAccountCreateOrUpdate(map);
				break;
			case "OrderTransform":
					Map<?,?> mapOrderCreateOrUpdate = this.orderCreateOrUpdate(map);

					if(mapOrderCreateOrUpdate.containsKey(Definition.GENERATED_TRANS_ID_PRODUCT))
					{
						returndMap.put(Definition.GENERATED_TRANS_ID_PRODUCT, mapOrderCreateOrUpdate.get(Definition.GENERATED_TRANS_ID_PRODUCT));
					}
					
					if(mapOrderCreateOrUpdate.containsKey(Definition.GENERATED_TRANS_ID_FEATURE))
					{
						returndMap.put(Definition.GENERATED_TRANS_ID_FEATURE, mapOrderCreateOrUpdate.get(Definition.GENERATED_TRANS_ID_FEATURE));
					}
					
					if(mapOrderCreateOrUpdate.containsKey(Definition.IS_REAL_SEND_AGREEMENT))
					{
						returndMap.put(Definition.IS_REAL_SEND_AGREEMENT, mapOrderCreateOrUpdate.get(Definition.IS_REAL_SEND_AGREEMENT));
					}
				break;
			default:
				String message = generateErrorMessageDbTableServiceType();
				throw new Exception(message);
		}
		
		return returndMap;
	}
	
	/**
	 * lbuAccountCreateOrUpdate
	 * 
	 * create if not found in db
	 * else update it
	 *  
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<ResponseMessage> lbuAccountCreateOrUpdate(Map<String, String> map) throws Exception
	{
		AccountWamService.isChanged = false;
		
		LbuAccount lbuAccount = this.accountWamService.LbuAccountTransform(map);
		LbuAccount lbuAccountFromDb = this.accountWamService.selectLbuAccountById(lbuAccount.getLbuAccountId(), lbuAccount.getLbuSetId());
		
		if(lbuAccountFromDb == null) {
			lbuAccountCreation(map);
		} else {
			lbuAccountUpdate(map);
		}
		
		return  ResponseHelper.getMessageList();
	}
	
	/**
	 * lbuAccountCreation 
	 * create only
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<ResponseMessage> lbuAccountCreation(Map<String, String> map) throws Exception
	{
		LbuAccount lbuAccount = this.accountWamService.LbuAccountTransform(map);
		Map locationsTransformReturnedMap = locationWamService.LocationsTransform(map);

		LbuLocations lbuLocations = (LbuLocations)locationsTransformReturnedMap.get(LocationWamService.lbuLocations);
		
		/**
		 *  insert account in db
		 */
		LbuAccount returnedLbuAccount = accountWamService.insertLbuAccount(lbuAccount);

		ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("account " + returnedLbuAccount.getLbuAccountId() + " inserted successfully.", ResponseMessage.SUCCESS));
		/**
		 * create account in ecm ws
		 */
		this.accountWamService.createOrUpdateCustomerEcmWs(lbuAccount, lbuLocations, Definition.CREATE);
		
		return  ResponseHelper.getMessageList();
	}
	
	/**
	 * lbuAccountUpdate
	 * update only 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<ResponseMessage> lbuAccountUpdate(Map<String, String> map) throws Exception
	{
		LbuAccount lbuAccount = this.accountWamService.LbuAccountTransform(map);
		Map locationsTransformReturnedMap = locationWamService.LocationsTransform(map);

		LbuLocations lbuLocations = (LbuLocations)locationsTransformReturnedMap.get(LocationWamService.lbuLocations);
		
		/**
		 * update account in db
		 */
		LbuAccount returnedLbuAccount = accountWamService.updateLbuAccount(lbuAccount);

		if(returnedLbuAccount.getIsChanged()) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The account " + returnedLbuAccount.getLbuAccountId() + " updated successfully.", ResponseMessage.SUCCESS));
		} else {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("The account " + returnedLbuAccount.getLbuAccountId() + " is already updated. Nothing changed.", ResponseMessage.SUCCESS));
		}
		
		
		if(returnedLbuAccount.getIsChanged()) {
			/**
			 * update account in ecm ws
			 */
			this.accountWamService.createOrUpdateCustomerEcmWs(lbuAccount, lbuLocations, Definition.UPDATE);
		}
		
		return ResponseHelper.getMessageList();
	}
	
	/**
	 * orderCreateOrUpdate
	 * 
	 * create if account not found
	 * else update.
	 * 
	 * create if location not found
	 * else update
	 * 
	 * create if contentSub not found
	 * else update 
	 * 
	 * update 1 LbuAccount
	 *        n LbuLocations
	 *        n LbuContentSubs
	 *        
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public Map<?,?> orderCreateOrUpdate(Map<?, ?> map) throws Exception
	{
		/**
		 *  build the order object
		 */
		Orders orders = orderWamService.OrderTransform(map);

		
		/**
		 * create if account not found 
		 */
		Map<String, Object> mapAccountWamService = accountWamService.handleAccount(orders.getLbuAccount());
		
		LbuAccount lbuAccountFromDb = (LbuAccount)mapAccountWamService.get("lbuAccountFromDb");
		String accountStatus = mapAccountWamService.get("accountStatus").toString();

		locationWamService.handleLocations(orders.getLbuLocations(), lbuAccountFromDb);

		
		/**
		 * insert or update the LbuContentSubs.
		 * reject if LbuContentSub occupied by others
		 */
		LbuContentSubs returnedLbuContentSubs = contentSubWamService.handleLbuContentSubs(orders.getLbuContentSubs(), lbuAccountFromDb);

		Map<String, Object> mapHandleLbuAgreements = (Map<String, Object>) agreementService.handleLbuAgreements(returnedLbuContentSubs, lbuAccountFromDb, accountStatus);

		return mapHandleLbuAgreements;
	}
	
	/**
	 * SyncOrderReplyTransform
	 * @param inputStream
	 * @return
	 * @throws Exception 
	 */
	public SyncOrderReply SyncOrderReplyTransform(final InputStream inputStream) throws Exception
	{
		try {
			/**
			 * handle input xml
			 */
			Map<String, Object> mapSyncOrderReplyTransform = orderWamService.SyncOrderReplyTransformInputStream(inputStream);
			/**
			 * fetch the transformed object
			 */
			SyncOrderReply syncOrderReply = (SyncOrderReply)mapSyncOrderReplyTransform.get(Definition.SYNC_ORDER_REPLY);
			/**
			 * fetch the messageId
			 * this is the transId in our db SYS_HISTORY
			 */
			String transId = ((SyncOrderReply)mapSyncOrderReplyTransform.get(Definition.SYNC_ORDER_REPLY)).getMessageId();
			
			SysHistory sysHistory = new SysHistory();

			/**
			 * connect the 1st dataSource
			 */
			agreementService.historyPackage.setDataSrouce(super.dataSource);
			sysHistory = agreementService.historyPackage.findSysHistoryByTransId(transId);
			
			/**
			 * if not found. connect the 2ed dataSource
			 * it's useless on prod
			 */
//			if(sysHistory == null)
//			{
//				agreementService.historyPackage.setDataSrouce(super.dataSourceMig);
//				sysHistory = agreementService.historyPackage.findSysHistoryByTransId(transId);
//			}
			
			/**
			 * if always not found
			 */
			if(sysHistory == null)
			{
				ErrorObject errorObject = new ErrorObject();
				errorObject.setResultStatus(Definition.FAILURE);
				errorObject.setHttpStatus(HttpStatus.BAD_REQUEST);
				errorObject.setMessage("history not found with messageId " + transId);

				throw new Exception(errorObject.toString());
			}
			
			sysHistory.setReplyMessage(mapSyncOrderReplyTransform.get(Definition.XML).toString());
			sysHistory.setUpdatedBy(Definition.SYNC_ORDER_REPLY);
			/**
			 * check reply status from the SyncOrderReply object
			 */
			if( Definition.SUCCESS.equals(syncOrderReply.getMessageStatus()) ) {
				sysHistory.setReplyStatus(Definition.SUCCESS);
			} else {
				sysHistory.setReplyStatus(Definition.FAILURE);
			}

			historyLoggingSyncOrderReplyAspect.historyLoggingSyncOrderReplyAround(sysHistory, agreementService.historyPackage.getDataSrouce());
			
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" agreementPguid " + syncOrderReply.getAgreementPguid() + " updated ", ResponseMessage.SUCCESS));
			
			return syncOrderReply;
		} 
		catch(Exception e)
		{
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(e.getMessage(), ResponseMessage.FAILED));
			throw new Exception(e);
		}
	}
	
	/**
	 * 
	 * @param transId
	 * @return
	 * @throws Exception
	 */
	public SyncOrderReply getSyncOrderReplyFromHistory(String transId) throws Exception
	{
		agreementService.historyPackage.setDataSrouce(super.dataSource);
		SysHistory sysHistory = agreementService.historyPackage.findSysHistoryByTransId(transId);
		if(sysHistory == null) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" No history found with " + transId, ResponseMessage.FAILED));
			return null;
		}
		
		String message = sysHistory.getReplyMessage();
		
		if(message == null)
		{
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" Agreement synchronisation is being proceesed ", ResponseMessage.WAIT));
		} else {
			SyncOrderReply syncOrderReply = (SyncOrderReply)orderWamService.SyncOrderReplyTransformString(message);
			if(Definition.SUCCESS.equals(syncOrderReply.getMessageStatus()))
			{
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" Agreement synchronisation is finished successfully ", ResponseMessage.SUCCESS));
			} else {
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" Agreement synchronisation is failed ", ResponseMessage.FAILED));
			}
			
			return syncOrderReply;
		}
		return null;
		
	}
	
	
	
	
	/**
	 * generate error message if the table type not found
	 * return all Supported prc values
	 * @return
	 */
	public String generateErrorMessageDbTableServiceType()
	{
		String message= "parameter prc is not correct. Supported prc values: ";
		for(Map.Entry<String, String> entry : this.mapDbTableService.entrySet())
		{
			message += entry.getKey() + " ";
		}
		
		return message;
	}
	
	/**
	 * generate error message if the action type not found
	 * return all Supported prs values
	 * @return
	 */
	public String generateErrorMessageActionType()
	{
		String message= "parameter prs is not correct. Supported prs values: ";
		for(Map.Entry<String, String> entry : this.mapAction.entrySet())
		{
			message += entry.getKey() + " ";
		}
		
		return message;
	}
	
}
