package com.wam.service.java8;

import java.util.concurrent.Callable;

import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

import com.wam.model.admin.AdminDataTables;
import com.wam.service.AccountWamService;

import reactor.core.publisher.Mono;

@Service("AccountWamServiceJava8")
public class AccountWamServiceJava8 extends AccountWamService{

	public Publisher<AdminDataTables> searchLbuAccount2Java8(
				String lbuSetId, 
				String lbuAccountId, 
				String gbsAccountId,
				String accountName1,
				String accountName2,
				String city,
				String zipCode,
				String countryCode,
				String billableStatus,
				String addressLine1,
				String customerPguid,
				Integer fromNumber,
				Integer numberPerPage
			) throws Exception
	{
		AdminDataTables adminDataTables = (AdminDataTables) super.searchLbuAccount2(
					lbuSetId, 
					lbuAccountId, 
					gbsAccountId, 
					accountName1, 
					accountName2, 
					city, 
					zipCode, 
					countryCode, 
					billableStatus, 
					addressLine1, 
					customerPguid,
					fromNumber, 
					numberPerPage
				)
				;
		
		Callable<AdminDataTables> task = () -> adminDataTables;
		/**
		 * or Mono.just(adminDataTables)
		 */
		
		Mono<AdminDataTables> just = Mono.fromCallable(task);
		
		return just;
	}
	
}
