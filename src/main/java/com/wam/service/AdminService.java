package com.wam.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.db.gbsd.validation.LocationFormValidator;
import com.wam.model.ldap.AdminUser;
import com.wam.model.mvc.response.helper.AccountFormJsonResponse;
import com.wam.model.mvc.response.helper.ContentSubFormJsonResponse;
import com.wam.model.mvc.response.helper.LocationFormJsonResponse;
import com.wam.model.mvc.response.helper.MessageFormJsonResponse;
import com.wam.stored.packages.LogMessagePackage;
import com.wam.stored.packages.WindowsPackage;
import com.wam.utility.definition.Definition;
import com.wam.utility.ldap.LdapBrowser;
import com.wam.utility.ldap.LdapEntry;

import static java.lang.System.out;

@Service("AdminService")
public class AdminService {

	@Autowired
	@Qualifier("WamService")
	public WamService wamService;
	
	@Autowired
	@Qualifier("LogMessagePackage")
	public LogMessagePackage logMessagePackage;
	
	@Autowired
	@Qualifier("WindowsPackage")
	public WindowsPackage windowsPackage;
	
	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("AccountFormValidator")
	private Validator accountFormValidator;
	
	@Autowired
	@Qualifier("ContentSubFormValidator")
	private Validator contentSubFormValidator;
	
	@Autowired
	@Qualifier("LocationFormValidator")
	private Validator locationFormValidator;
	
	@Autowired
	@Qualifier("MessageFormValidator")
	private Validator messageFormValidator;
	
	@Autowired
	@Qualifier("AgreementService")
	public AgreementService agreementService;
	
	@Resource(name = "adminUser")
	public AdminUser adminUser;
	
	public static final Map<String, String> mapDataMigrationList = createMapDataMigrationList();
	
	public static final Map<String, String> mapPromoIndList = createMapPromoIndList();
	
	public static final Map<String, String> mapContentSubTypeList = createMapContentSubTypeList();
	
	public static final Map<String, String> mapContentSubNoReNewList = createMapContentSubNoReNewList();
	
	public static final Map<String, String> mapActionIdList = createMapActionIdList();
	
	public static final Map<String, String> mapMessageStatusList = createMapMessageStatusList();
			
	private static Map<String, String> createMapDataMigrationList(){
		Map<String, String> aMap = new HashMap();
		aMap.put("YES", "YES");
		aMap.put("NO", "NO");
		
		return Collections.unmodifiableMap(aMap);
	}		
	
	private static Map<String, String> createMapPromoIndList()
	{
		Map<String, String> aMap = new HashMap();
		aMap.put("G", "G");
		aMap.put("T", "T");
		
		return Collections.unmodifiableMap(aMap);
	}
	
	private static Map<String, String> createMapContentSubTypeList()
	{
		Map<String, String> aMap = new HashMap();
		aMap.put("S", "S");
		aMap.put("T", "T");
		
		return Collections.unmodifiableMap(aMap);
	}
	
	private static Map<String, String> createMapContentSubNoReNewList()
	{
		Map<String, String> aMap = new HashMap();
		aMap.put("Y", "Y");
		aMap.put("N", "N");
		
		return Collections.unmodifiableMap(aMap);
	}
	
	private static Map<String, String> createMapActionIdList()
	{
		Map<String, String> aMap = new HashMap();
		aMap.put(Definition.WORKFLOW_MANAGER_SQL_CODE, Definition.WORKFLOW_MANAGER_SQL_CODE);
		aMap.put(Definition.WORKFLOW_MANAGER_ECM_CODE, Definition.WORKFLOW_MANAGER_ECM_CODE);
		aMap.put(Definition.WORKFLOW_MANAGER_FINISHED_CODE, Definition.WORKFLOW_MANAGER_FINISHED_CODE);
		
		return Collections.unmodifiableMap(aMap);
	}
	
	private static Map<String, String> createMapMessageStatusList()
	{
		Map<String, String> aMap = new HashMap();
		aMap.put(Definition.WORKFLOW_MANAGER_STATUS_BEGINNING, Definition.WORKFLOW_MANAGER_STATUS_BEGINNING);
		aMap.put(Definition.WORKFLOW_MANAGER_STATUS_COMPLETE, Definition.WORKFLOW_MANAGER_STATUS_COMPLETE);
		aMap.put(Definition.WORKFLOW_MANAGER_STATUS_ERROR, Definition.WORKFLOW_MANAGER_STATUS_ERROR);
		
		return Collections.unmodifiableMap(aMap);
	}
	
	
	
	/**
	 * 
	 * @param account
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public AccountFormJsonResponse updateAccount(@ModelAttribute("accountForm") @Validated LbuAccount account,
	         BindingResult result) throws Exception {
		AccountFormJsonResponse response = new AccountFormJsonResponse();
		
		/**
		 * update location part with the default location in account from db
		 */
		LbuAccount lbuAccountFromDb = (LbuAccount)wamService.accountWamService.selectLbuAccountById(account.getLbuAccountId(), account.getLbuSetId());
		account.setAddressLine1(lbuAccountFromDb.getAddressLine1());
		account.setAddressLine2(lbuAccountFromDb.getAddressLine2());
        account.setCityName(lbuAccountFromDb.getCityName());
        account.setCountryName(lbuAccountFromDb.getCountryName());
        account.setZipCode(lbuAccountFromDb.getZipCode());
        account.setStateOrProvinceCode(lbuAccountFromDb.getStateOrProvinceCode());
        account.setCountryCode(lbuAccountFromDb.getCountryCode());
		/**
		 * end update
		 */
		accountFormValidator.validate(account, result);
		
		if(result.hasErrors()) {
			Map<String, String> errors = new HashMap();
			
			for(FieldError error : result.getFieldErrors()) {
				//out.println(error.getCode());
				//out.println(error.getObjectName());
				//out.println(error.getField());
				errors.put(error.getField(), env.getProperty(error.getCode() + "." + error.getObjectName() + "." + error.getField())); 
			}

			response.setErrorMessages(errors);
			response.setValidated(false);
		} else {
			LbuAccount accountFromDb = (LbuAccount)wamService.accountWamService.updateLbuAccount(account);
			
			response.setAccountForm(accountFromDb);
			response.setValidated(true);
		}
		return response;
	}
	
	public LocationFormJsonResponse updateLocation(@ModelAttribute("locationForm") @Validated LbuLocation location,
	         BindingResult result) throws Exception {
		LocationFormJsonResponse response = new LocationFormJsonResponse();
		
		LbuLocation locationDb = wamService.locationWamService.selectLbuLocation(location.getLbuSetId(), location.getLbuAccountId(), location.getLbuLocationId());
		location.setGbsLocationId(locationDb.getGbsLocationId());
		location.setGlpLocationId(locationDb.getGlpLocationId());
		location.setLbuAddressId(locationDb.getLbuAddressId());
		location.setGbsAddressId(locationDb.getGbsAddressId());
		location.setExtOrderNo(location.getExtOrderNo());
		location.setSalesRepId(location.getSalesRepId());
		location.setDataMigration(location.getDataMigration());
		
		//out.println(location);
		
		this.locationFormValidator.validate(location, result);
		
		if(result.hasErrors()) {
			Map<String, String> errors = new HashMap();
			
			for(FieldError error : result.getFieldErrors()) {
				errors.put(error.getField(), env.getProperty(error.getCode() + "." + error.getObjectName() + "." + error.getField())); 
			}

			response.setErrorMessages(errors);
			response.setValidated(false);
		} else {
			location = wamService.locationWamService.updateLbuLocation(location);
			
			response.setLocationForm(location);
			response.setValidated(true);
		}
		return response;
	}
	
	public MessageFormJsonResponse updateMessage(@ModelAttribute("messageForm") @Validated LogMessage message,
    BindingResult result) throws Exception { 
		MessageFormJsonResponse response = new MessageFormJsonResponse();
		
		AdminDataTables adminDataTables = logMessagePackage.LogMessageSearch2(message.getMessageId().toString(), null, null, null, null, null, null, null);
		
		ArrayList<LogMessage> list = (ArrayList<LogMessage>)adminDataTables.getData();
		
		
		if(list.size() == 1)
		{
			message.setApplication(list.get(0).getApplication());
			message.setCreatedBy(list.get(0).getCreatedBy());
			message.setCreatedDatetime(list.get(0).getCreatedDatetime());
			message.setGroupId(list.get(0).getGroupId());
			message.setMessage(list.get(0).getMessage());
			message.setProcessSubType(list.get(0).getProcessSubType());
			message.setProcessType(list.get(0).getProcessType());
			message.setTransId(list.get(0).getTransId());
			message.setUpdatedBy(list.get(0).getUpdatedBy());
			message.setUpdatedDatetime(list.get(0).getUpdatedDatetime());
			
			this.messageFormValidator.validate(message, result);
			
			if(result.hasErrors()) {
				Map<String, String> errors = new HashMap();
				
				for(FieldError error : result.getFieldErrors()) {
					errors.put(error.getField(), env.getProperty(error.getCode() + "." + error.getObjectName() + "." + error.getField())); 
				}

				response.setErrorMessages(errors);
				response.setValidated(false);
			} else {
				logMessagePackage.LogMessageUpdate(message.getMessageId(), message.getStatus(), message.getActionId());
				
				response.setMessageForm(message);
				response.setValidated(true);
			}
			return response;
		}
		return response;
		
		
	}
	
	public LogMessage findLogMessageById(String messageId) throws Exception
	{
		AdminDataTables adminDataTables = logMessagePackage.LogMessageSearch2(messageId, null, null, null, null, null, null, null);
		
		ArrayList<LogMessage> list = (ArrayList<LogMessage>)adminDataTables.getData();
		
		LogMessage message = new LogMessage();
		
		if(list.size() == 1)
		{
			return list.get(0);
		}
		
		return message;
	}
	
	
	/**
	 * 
	 * @param contentSub
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public ContentSubFormJsonResponse updateContentSub(@ModelAttribute("contentSubForm") @Validated LbuContentSub contentSub,
	         BindingResult result) throws Exception {
		
		ContentSubFormJsonResponse response = new ContentSubFormJsonResponse();
		contentSubFormValidator.validate(contentSub, result);
		
		if(result.hasErrors()) {
			Map<String, String> errors = new HashMap();
			
			for(FieldError error : result.getFieldErrors()) {
				//out.println(error.getCode());
				//out.println(error.getObjectName());
				//out.println(error.getField());
				errors.put(error.getField(), env.getProperty(error.getCode() + "." + error.getObjectName() + "." + error.getField())); 
			}

			response.setErrorMessages(errors);
			response.setValidated(false);
		} else {
			/**
			 * define the receive format from admin back office
			 */
			SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_MM_DD_YYYY);

			Date beginDate = formatter.parse(contentSub.getBeginDate());
			Date endDate = formatter.parse(contentSub.getEndDate());
			
			/**
			 * define the target format 
			 */
			SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_YYYYMMDD);
			
			/**
			 * transform before sp
			 */
			contentSub.setBeginDate(formatter2.format(beginDate).toString());
			contentSub.setEndDate(formatter2.format(endDate).toString());
			
			LbuContentSub contentSubFromDb = (LbuContentSub)wamService.contentSubWamService.updateLbuContentSub(contentSub, null);
			
			boolean isAgreementChanged = agreementService.reCalculateAgreement(contentSub.getContractNum());
			
			response.setAgreementChanged(isAgreementChanged);
			response.setContentSubForm(contentSubFromDb);
			response.setValidated(true);
		}
		return response;
		
	}
	
	public Boolean logMessageCreateForPSOrderUrl(String url) throws Exception
	{
		Integer messageId = null;
		String message = url;
		String processType = Definition.PS;
		String processSubType = Definition.CREATE;
		Integer actionId = Integer.parseInt(Definition.WORKFLOW_MANAGER_FINISHED_CODE);
		String status = Definition.WORKFLOW_MANAGER_STATUS_COMPLETE;
		String transId = BaseService.generateTransIdSysHistory();
		String groupId = null;
		String application = Definition.WEB;
		String createdBy = Definition.PS;
		//String updatedBy = Definition.PS;
		
		return logMessagePackage.LogMessageCreateStoredProcedure(messageId, processType, processSubType, actionId, status, transId, groupId, message, application, createdBy);
	}
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public Map<String, ?> getLdapEntryWithUsernamePassword(String username, String password)
	{
		String userPrincipalName = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			LdapEntry ldapentry = LdapBrowser.getLdapEntityLogin(username);

			if(ldapentry != null)
			{
				userPrincipalName = ldapentry.getUserPrincipalName();
			}
			
			try {	
				LdapBrowser.InitialLdapContextWithPassword(userPrincipalName, password);
			} catch(Exception e) {
				map.put(Definition.LDAP_USER, null);
				map.put(Definition.MESSAGE, "Password incorrect");	
				map.put(Definition.ERROR, "password");
				map.put(Definition.STATUS, false);
				
				return map;
			}
			
			
			adminUser.setWindowsId(username);
			adminUser.setLdapId(userPrincipalName);
			adminUser.setEmail(ldapentry.getMail());
			
			String role = windowsPackage.WindowsAccountRoleSearchStoredProcedure(username);
			out.println(role);
			adminUser.setRole(role);
			
			map.put(Definition.LDAP_USER, ldapentry);
			map.put(Definition.MESSAGE, "Welcome " + username);	
			map.put(Definition.STATUS, true);
			
			return map;

		} catch(Exception e) {
			map.put(Definition.LDAP_USER, null);
			map.put(Definition.MESSAGE, "Windows ID not found : " + username);	
			map.put(Definition.ERROR, "username");
			map.put(Definition.STATUS, false);
			
			return map;
		}
	}
	
}
