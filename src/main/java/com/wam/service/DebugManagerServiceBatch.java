package com.wam.service;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.model.ws.in.creation.customer.OrgCustomerWrapper;
import com.wam.model.ws.in.creation.customer.PlaceOfBusinessWrapper;

import static java.lang.System.out;

public class DebugManagerServiceBatch extends WorkFlowManagerService {
	
	public DebugManagerServiceBatch() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	public void updatePguidByCorte() throws Exception
	{
		try {
			List<?> listAccountSysCursor = this.accountWamService.findAccountSys("2019-01-22", "2019-01-22", "C040");
	
			for(Object accountSysCursor : listAccountSysCursor) {

				Map<?, ?> mapAccountSysCursor = (LinkedCaseInsensitiveMap)accountSysCursor;

				StringReader responseReader = new StringReader(mapAccountSysCursor.get("REPLY_MESSAGE").toString());
				JAXBContext jaxbContextInComing = JAXBContext.newInstance(com.wam.model.ws.in.creation.customer.OrgCustomerWrapper.class);
				Unmarshaller jaxbUnmarshaller = jaxbContextInComing.createUnmarshaller();
				OrgCustomerWrapper orgCustomerWrapper = (OrgCustomerWrapper) jaxbUnmarshaller.unmarshal(responseReader);
				PlaceOfBusinessWrapper placeOfBusinessInResponse = orgCustomerWrapper.getDataArea().getOrgCustomerBO().getListOfPlaceOfBusiness().getList().get(0);	
				
				String lbuAccountId = mapAccountSysCursor.get("ENTITY_SEQ1").toString();
				
                if(orgCustomerWrapper.getDataArea().getOrgCustomerBO().getListOfPlaceOfBusiness().getList().size() != 1)
                {
                	throw new Exception(" orgCustomerWrapper.getDataArea().getOrgCustomerBO().getListOfPlaceOfBusiness().getList().size() not 1 ");
                }
                
                this.accountWamService.updateAccountCustomerPGUIDId(lbuAccountId, placeOfBusinessInResponse.getOrgCustomerPGUID(), placeOfBusinessInResponse.getpOBPGUID(), placeOfBusinessInResponse.getAddressPGUID());
 
			}
			LbuAgreements lbuAgreements = this.lbuAgreementPackage.LbuAgreementIdSelectAll();
			
			for(LbuAgreement lbuAgreement : lbuAgreements.getList())
			{
				String CustomerPGUID = this.lbuAgreementPackage.LbuAgreementUpdateCustomerPGUID(lbuAgreement.getAssetAgreementId());
				out.println(CustomerPGUID);
			}
			
		} catch(Exception e) {
			
		}
		
	}
	
	public static void main(String[] args) throws Exception
	{
		DebugManagerServiceBatch debugManagerBatch = new DebugManagerServiceBatch();
		debugManagerBatch.updatePguidByCorte();
	}
	
}
