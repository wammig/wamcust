package com.wam.service;

import static java.lang.System.out;

import java.io.IOException;
import java.io.StringWriter;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLInputFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.wam.aspect.EcmLocationHistoryLoggingAspect;
import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.model.mvc.response.helper.WsMessage;
import com.wam.model.mvc.response.helper.WsMessageHelper;
import com.wam.model.ws.in.creation.placeofbusiness.PlaceOfBusinessRoot;
import com.wam.model.ws.out.creation.placeofbusiness.CommonTrackingMessageWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.CommunicationWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.DataAreaWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.DataGroupWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.HeaderWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.ListOfCommunicationWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.ListOfPlaceOfBusinessWrapperBO;
import com.wam.model.ws.out.creation.placeofbusiness.PlaceOfBusinessRootWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.PlaceOfBusinessWrapper;
import com.wam.stored.packages.LbuLocationPackage;
import com.wam.utility.InputStream.InputStreamTransformer;
import com.wam.utility.definition.Definition;
import com.wam.utility.net.HttpUtility;


/**
 * 
 * @author QINC1
 *
 */
@Service("LocationWamService")
@PropertySources({
    @PropertySource(value="classpath:ecmws.properties", ignoreResourceNotFound = true),
    @PropertySource(value="classpath:application.properties", ignoreResourceNotFound = true)
})
public class LocationWamService extends BaseService{

	public final static Logger logger = LoggerFactory.getLogger(LocationWamService.class);
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("LbuLocationPackage")
	LbuLocationPackage lbuLocationPackage;
	
	@Value( "${ecm.ws.url.creation.placeOfBusiness}" )
	private String ecmWsUrlCreationPlaceOfBusiness;
	
	@Autowired
	@Qualifier("EcmLocationHistoryLoggingAspect")
	public EcmLocationHistoryLoggingAspect ecmLocationHistoryLoggingAspect;
	
	/**
	 * primary LbuLocation key in the map
	 */
	public static String primaryLbuLocation = "primaryLbuLocation";
	
	/**
	 * LbuLocations key in the map
	 */
	public static String lbuLocations = "lbuLocations";
	
	
	// receiving format
	public SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_YYYYMMDDHHMMSS);
	// insert format
	public SimpleDateFormat formatter2 = new SimpleDateFormat(Definition.DATEFORMAT_YYYY_MM_DD);
	
	/**
	 * valid single address line
	 * something like : 
	 * 
	 * [loid=6089840;losq=1;ladt=p;ladr=4RUELABBE|BATIMENTA|BESANCON||25000||FRA]
	 * 
	 */
	public static String regexAdr = "([\\[]{1}(loid=){1}[0-9S]{1,}[;]{1}(losq=){1}[0-9]{1,}[;]{1}(ladt=){1}[p|s]{1}[;]{1}(ladr=){1}[\\w\\s�-�-./ÜÄÖüäöß]+[|]{1}([\\w\\s�-�-./ÜÄÖüäöß]+)?[|]{1}[\\w\\s�-�-./ÜÄÖüäöß]+[|]{1}([\\w\\s�-�-./ÜÄÖüäöß]+)?[|]{1}([\\d\\w\\W\\s]{1,15})?[|]{1}([\\w\\s�-�-./]+)?[|]{1}[A-Z]{2,4}[\\]])";
	
	/**
	 * valid multi addresses
	 * something like :
	 * 
	 * [loid=6089840;losq=1;ladt=p;ladr=4 RUE LABBE|BATIMENTA|BESANCON||25000||FRA]
	 * [loid=6089841;losq=1;ladt=s;ladr=480 Boulevard Provinces|BATIMENTA|NANTERRE||92000||FRA]
	 * [loid=6089842;losq=1;ladt=s;ladr=82 Boulevard Massena|BATIMENTA|Paris||75013||FRA]
	 * [loid=6089843;losq=1;ladt=s;ladr=11 rue bella|BATIMENTA|Paris||75009||FRA]
	 * 
	 */
	public static String regexAddr = "(" + regexAdr + "+?)";
	
	/**
	 * valid the loid part in one single address
	 * something like : 
	 * 
	 * loid=6089840S01;
	 * 
	 */
	public static String regexLoid = "((loid=){1}[0-9S]{3,25}[;]{1})";
	
	/**
	 * regex helps to remove "loid=" and ";" to extract the number part something like (6089840S01)
	 */
	public static String regexLoidTrim = "([^0-9S]+)";
	
	/**
	 * valid the losq part in one single address
	 * something like : 
	 * 
	 * losq=1;
	 * 
	 */
	public static String regexLosq = "((losq=){1}[0-9]{1,}[;]{1})";
	
	/**
	 * regex helps to remove "losq=" and ";" to extract the number part something like (1)
	 */
	public static String regexLosqTrim = "([^0-9]{1,})";
	
	/**
	 * valid the ladt part in one single address
	 * something like : 
	 * 
	 * ladt=p;
	 * 
	 */
	public static String regexLadt = "((ladt=){1}(p|s)[;]{1})";
	
	/**
	 * regex helps to remove "ladt=" and ";" to extract the letter (p or s) 
	 */
	public static String regexLadtTrim = "([^(p|s)]+)";
	/**
	 * transform parameters to multi locations
	 * @param map
	 * @return
	 * @throws Exception
	 */
	
	public Properties ecmws;
	public Properties urlws;
	
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
	
	public void setEcmws(Properties ecmws)
	{
		this.ecmws = ecmws;
	}
	
	public void setUrlws(Properties urlws)
	{
		this.urlws = urlws;
	}
	
	public String getEcmWsUrlCreationPlaceOfBusiness()
	{
		if(this.ecmWsUrlCreationPlaceOfBusiness == null)
		{
			this.ecmWsUrlCreationPlaceOfBusiness = this.urlws.getProperty("ecm.ws.url.creation.placeOfBusiness");
		}
		
		return this.ecmWsUrlCreationPlaceOfBusiness;
	}
	
	public void setLbuLocationPackage(LbuLocationPackage lbuLocationPackage)
	{
		this.lbuLocationPackage = lbuLocationPackage;
	}
	
	public void  setEcmLocationHistoryLoggingAspect(EcmLocationHistoryLoggingAspect ecmLocationHistoryLoggingAspect)
	{
		this.ecmLocationHistoryLoggingAspect = ecmLocationHistoryLoggingAspect;
	}
	
	public String getValidAddressFlag()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.ValidAddressFlag");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.ValidAddressFlag");
		}
	}
	
	public String getGeographicLocationFlag()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.GeographicLocationFlag");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.GeographicLocationFlag");
		}
	}
	
	public String getPrimaryAddressIndicator()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.PrimaryAddressIndicator");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.PrimaryAddressIndicator");
		}
	}
	
	public String getCredentialingFlag()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.CredentialingFlag");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.CredentialingFlag");
		}
	}
	
	public String getRequestedPRLevel()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.RequestedPRLevel");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ListOfPlaceOfBusiness.PlaceOfBusiness1.RequestedPRLevel");
		}
	}
	
	public String getUserPermId()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.userPermId");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.userPermId");
		}
	}
	
	public String getSourceAppName()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.SourceAppName");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.SourceAppName");
		}
	}
	
	public String getPriority()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.Priority");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.Priority");
		}
	}
	
	public String getNs7()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ns7");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ns7");
		}
	}
	
	public String getNs11()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ns11");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ns11");
		}
	}
	
	public String getNs12()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ns12");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ns12");
		}
	}
	
	public String getNs13()
	{
		if(env != null)
		{
			return env.getProperty("ecm.ws.creation.placeOfBusiness.ns13");
		} else {
			return this.ecmws.getProperty("ecm.ws.creation.placeOfBusiness.ns13");
		}
	}
	
	/**
	 * this method extract addressSeq if found
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public static String extractAddressSq(Map map) throws Exception
	{
		Object addr = map.get("addr");
		
		Pattern addrPattern = Pattern.compile(LocationWamService.regexAddr);
		Matcher addrMatcher = addrPattern.matcher(addr.toString());
		
		if(addrMatcher.matches())
		{
			Pattern adrPattern = Pattern.compile(LocationWamService.regexAdr);
			Matcher adrMatcher = adrPattern.matcher(addr.toString());
			
			while(adrMatcher.find()) 
			{
				String addressLine = adrMatcher.group(1);
				
				// address seq
				Pattern losqPattern = Pattern.compile(LocationWamService.regexLosq);
				Matcher losqMatcher = losqPattern.matcher(addressLine);
				if(losqMatcher.find())
				{
					return losqMatcher.group(1).toString().replaceAll(regexLosqTrim, "");
				}
			}
			
		}
		
		return null;
	}
	
	/**
	 * this method extract locationId if found
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public static String extractLocationId(Map map) throws Exception
	{
		Object addr = map.get("addr");
		
		Pattern addrPattern = Pattern.compile(LocationWamService.regexAddr);
		Matcher addrMatcher = addrPattern.matcher(addr.toString());

		if(addrMatcher.matches())
		{
			Pattern adrPattern = Pattern.compile(LocationWamService.regexAdr);
			Matcher adrMatcher = adrPattern.matcher(addr.toString());

			while(adrMatcher.find()) 
			{
				String addressLine = adrMatcher.group(1);

				Pattern loidPattern = Pattern.compile(LocationWamService.regexLoid);
				Matcher loidMatcher = loidPattern.matcher(addressLine);
				if(loidMatcher.find())
				{
					return loidMatcher.group(1).toString().replaceAll(regexLoidTrim, "");
				}
			}
			
		}
		
		return null;
	}
	
	
	public  Map LocationsTransform(Map map) throws Exception
	{
		return this.validTransformLocations(map);
	}
	
	/**
	 * valid multi addresses from string parameters addr
	 * transform to address objects
	 * 
	 * Example:
	 * 
	 * [loid=6089840;losq=1;ladt=p;ladr=4 RUE LABBE|BATIMENTA|BESANCON||25000||FRA]
	 * [loid=6089841;losq=1;ladt=s;ladr=480 Boulevard Provinces|BATIMENTA|NANTERRE||92000||FRA]
	 * [loid=6089842;losq=1;ladt=s;ladr=82 Boulevard Massena|BATIMENTA|Paris||75013||FRA]
	 * [loid=6089843;losq=1;ladt=s;ladr=11 rue bella|BATIMENTA|Paris||75009||FRA]
	 * 
	 * @return Map
	 * "primaryLbuLocation" => new LbuLocation()
	 * "lbuLocations" => new LbuLocations()
	 */
	public Map validTransformLocations(Map map) throws Exception
	{
		Object addr = map.get("addr");
		
		Map returnedMap = new HashMap();
		LbuLocations lbuLocations = new LbuLocations();
		
		/**
		 * fetch lbuAccount for AuditUserId and lbuSetId
		 */
		
		if(!map.containsKey("uid"))
		{
			throw new Exception("uid not found in map");
		}
		String auditUserId = map.get("uid").toString();
		
		if(!map.containsKey("lbu"))
		{
			throw new Exception("lbu not found in map");
		}
		String lbuSetId = map.get("lbu").toString();
		
		/**
		 *  addr ==> map to multi addresses. Not nullable
		 */
		if(addr == null)
		{
			throw new Exception(" addr is not nullable");
		}
		
		Pattern addrPattern = Pattern.compile(LocationWamService.regexAddr);
		Matcher addrMatcher = addrPattern.matcher(addr.toString());
		
		/**
		 *  the complete addr string is valid.
		 */
		if(addrMatcher.matches())
		{	
			/**
			 * define the regex for each single line
			 */
			Pattern adrPattern = Pattern.compile(LocationWamService.regexAdr);
			Matcher adrMatcher = adrPattern.matcher(addr.toString());

			while(adrMatcher.find()) 
			{
				String ladt = "";
				String losq = "";

				LbuLocation lbuLocation = new LbuLocation();
				
				// set the account id for a location object.
				// it means a location is linked to an account.
				lbuLocation.setLbuAccountId(map.get("aid").toString());
				// new address
				String addressLine = adrMatcher.group(1);
				
				// address seq
				Pattern losqPattern = Pattern.compile(LocationWamService.regexLosq);
				Matcher losqMatcher = losqPattern.matcher(addressLine);
				if(losqMatcher.find())
				{
					losq = losqMatcher.group(1).toString().replaceAll(regexLosqTrim, "");
					lbuLocation.setAddressSeq(Integer.parseInt(losq));
				}
				
				Pattern loidPattern = Pattern.compile(LocationWamService.regexLoid);
				Matcher loidMatcher = loidPattern.matcher(addressLine);
				if(loidMatcher.find())
				{
					String loid = loidMatcher.group(1).toString().replaceAll(regexLoidTrim, "");
					lbuLocation.setLbuLocationId(lbuLocation.generateLocationId(loid, losq));
					lbuLocation.setGbsLocationId(lbuLocation.generateLocationId(loid, losq));
					lbuLocation.setGlpLocationId(lbuLocation.generateLocationId(loid, losq));
					lbuLocation.setLbuAddressId(loid);
					lbuLocation.setGbsAddressId(loid);
				}
				
				
				
				Pattern ladtPattern = Pattern.compile(LocationWamService.regexLadt);
				Matcher ladtMatcher = ladtPattern.matcher(addressLine);
				if(ladtMatcher.find())
				{
					ladt = ladtMatcher.group(1).toString().replaceAll(regexLadtTrim, "");
				}
				
				
				Pattern ladrPattern = Pattern.compile(AccountWamService.regexAdr);
				Matcher ladrMatcher = ladrPattern.matcher(addressLine);

				if(ladrMatcher.find())
				{
					String ladr = ladrMatcher.group(0).toString();

					// separate the address to 7 parts
					String[] adrParts = ladr.split("\\|");
					
					if(adrParts.length >= 1) {
						lbuLocation.setAddressLine1(adrParts[0]);
					}
					if(adrParts.length >= 2) {
						lbuLocation.setAddressLine2(adrParts[1]);
					}
					if(adrParts.length >= 3) {
						lbuLocation.setCityName(adrParts[2]);
					}
					if(adrParts.length >= 4) {
						lbuLocation.setCountryName(adrParts[3]);
					}
					if(adrParts.length >= 5) {
						lbuLocation.setZipCode(adrParts[4]);
					}
					if(adrParts.length >= 6) {
						lbuLocation.setStateOrProvinceCode(adrParts[5]);
					}
					if(adrParts.length >= 7) {
						lbuLocation.setCountryCode(adrParts[6]);
					}
					

					
					lbuLocation.setAuditUserId(auditUserId);
					lbuLocation.setLbuSetId(lbuSetId);
					
					Date dateTime = new Date();
					lbuLocation.setCreatedDatetime(formatter2.format(dateTime));
					lbuLocation.setUpdatedDatetime(formatter2.format(dateTime));

					if(ladt.equals("p"))
					{
						if(returnedMap.containsKey(LocationWamService.primaryLbuLocation)) 
						{
							throw new Exception(" More than one primary address detected ");
						}
						returnedMap.put(LocationWamService.primaryLbuLocation, lbuLocation);
					}
					
					lbuLocations.getList().add(lbuLocation);
				} 
			}
			
		} else {
			/**
			 * if the input multi addresses invalid
			 * we check which part broken and tell the problem.  
			 */
			/**
			 *  we extract the invalid address part
			 */
			String invalidAddr = addr.toString().replaceAll(LocationWamService.regexAdr, "");
			out.println(invalidAddr);
			/**
			 * check if it begins with "[" or not
			 */
			String checkRegexBegin = "^[\\[]{1}";
			Pattern checkRegexBeginPattern = Pattern.compile(checkRegexBegin);
			Matcher checkRegexBeginMatcher = checkRegexBeginPattern.matcher(invalidAddr);

			if(!checkRegexBeginMatcher.find())
			{ 

				String failedCheckRegexBegin = "^(.){1}";
				Pattern failedCheckRegexBeginPattern = Pattern.compile(failedCheckRegexBegin);
				Matcher failedCheckRegexBeginMatcher = failedCheckRegexBeginPattern.matcher(invalidAddr);
				String message = " an address should begin with \" [ \" " ;
				if(failedCheckRegexBeginMatcher.find()) 
				{
					String invalidPart = failedCheckRegexBeginMatcher.group(0);
					message += ", but we found an address begin with something like \""+ invalidPart +"\" ";
				}
				
				throw new Exception(message);
			}
			
			/**
			 * check if it contains "loid=" or not
			 */
			String checkRegexLoid = checkRegexBegin + "(loid=){1}";
			Pattern checkRegexLoidPattern = Pattern.compile(checkRegexLoid);
			Matcher checkRegexLoidMatcher = checkRegexLoidPattern.matcher(invalidAddr);
			
			if(!checkRegexLoidMatcher.find())
			{ 

				String failedCheckRegexLoid = checkRegexBegin + "[\\W\\w]{5}";
				Pattern failedCheckRegexLoidPattern = Pattern.compile(failedCheckRegexLoid);
				Matcher failedCheckRegexLoidMatcher = failedCheckRegexLoidPattern.matcher(invalidAddr);
				String message = " an address should contain \" loid= \" after the beginning \"[\" " ;
				if(failedCheckRegexLoidMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLoidMatcher.group(0);
					message += ", but we found something like \""+ invalidPart +"\" after the beginning \"[\" ";
				}
				
				throw new Exception(message);
			}
			
			/**
			 * check if it contains "loid=x;" or not
			 */
			String checkRegexLoidData = checkRegexLoid + "[\\d]{1,}[;]{1}";
			Pattern checkRegexLoidDataPattern = Pattern.compile(checkRegexLoidData);
			Matcher checkRegexLoidDataMatcher = checkRegexLoidDataPattern.matcher(invalidAddr);
			
			if(!checkRegexLoidDataMatcher.find())
			{ 

				String failedCheckRegexLoidData = checkRegexLoid + "[^;]*[;]";
				Pattern failedCheckRegexLoidDataPattern = Pattern.compile(failedCheckRegexLoidData);
				Matcher failedCheckRegexLoidDataMatcher = failedCheckRegexLoidDataPattern.matcher(invalidAddr);
				String message = " an address should contain \" loid=x; (x is an Integer number) \" after the beginning \"[\" " ;

				if(failedCheckRegexLoidDataMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLoidDataMatcher.group(0);
					message += ", but we found something like \""+ invalidPart +"\" (the parameter loid should be an Integer number and end by \";\") ";
				}
				
				throw new Exception(message);
			}
			
			/**
			 * check if it contains "losq=x;" or not
			 */
			String checkRegexLosq = checkRegexLoidData + "(losq=){1}[0-9]{1,5}[;]{1}";
			Pattern checkRegexLosqPattern = Pattern.compile(checkRegexLosq);
			Matcher checkRegexLosqMatcher = checkRegexLosqPattern.matcher(invalidAddr);
			
			if(!checkRegexLosqMatcher.find())
			{ 

				String failedCheckRegexLosq = checkRegexLoidData + "[^;]*[;]";
				Pattern failedCheckRegexLosqPattern = Pattern.compile(failedCheckRegexLosq);
				Matcher failedCheckRegexLosqMatcher = failedCheckRegexLosqPattern.matcher(invalidAddr);
				String message = " an address should contain \" losq=x; (x is an Integer number) \" after the the parameter loid";
				if(failedCheckRegexLosqMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLosqMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexLoidData, "") +"\" (losq should be an Integer number and end by \";\") ";
				}
				
				throw new Exception(message);
			}
			
			/**
			 * check if it contains "ladt=p;" or "ladt=s;"
			 */
			String checkRegexLadt = checkRegexLosq + "(ladt=){1}[p|s]{1}[;]{1}";
			Pattern checkRegexLadtPattern = Pattern.compile(checkRegexLadt);
			Matcher checkRegexLadtMatcher = checkRegexLadtPattern.matcher(invalidAddr);
			
			if(!checkRegexLadtMatcher.find())
			{ 
				String failedCheckRegexLadt = checkRegexLosq + "[^;]*[;]";
				Pattern failedCheckRegexLadtPattern = Pattern.compile(failedCheckRegexLadt);
				Matcher failedCheckRegexLadtMatcher = failedCheckRegexLadtPattern.matcher(invalidAddr);
				String message = " an address should contain \" ladt=p or ladt=s \" after the the parameter losq";
				if(failedCheckRegexLadtMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLadtMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexLosq, "") +"\" (ladt should be \" p \" or \" s \" and end by \";\") ";
				}
				
				throw new Exception(message);
			}
			
			/**
			 * check if the invalid address end with ]
			 * we check the end before the ladr part. 
			 */
			String endCheckRegex = "[\\]]{1}$";
			Pattern endCheckRegexPattern = Pattern.compile(endCheckRegex);
			Matcher endCheckRegexMatcher = endCheckRegexPattern.matcher(invalidAddr);
						
			
			if(!endCheckRegexMatcher.find())
			{
				throw new Exception(" Each address Should End with \"]\" ");
			}
			
			
			
			
			/**
			 * check the 7 parts of ladr
			 */
			String checkRegexLadr = checkRegexLadt + "(ladr=){1}[\\W\\w]+";
			Pattern checkRegexLadrPattern = Pattern.compile(checkRegexLadr);
			Matcher checkRegexLadrMatcher = checkRegexLadrPattern.matcher(invalidAddr);

			if(checkRegexLadrMatcher.find())
			{
				String messageNotNullable = "ladr is invalid. Format: adr1|adr2|cty|ctn|zip|spc|ctr. 7 parts at total. adr1,cty,zip,ctr are not nullable.";
				String ladr = checkRegexLadrMatcher.group(0).toString();

				// separate the address to 7 parts
				String[] adrParts = ladr.split("\\|");

				
				if(adrParts.length != 7 ) {
					throw new Exception(messageNotNullable + " But we found "+ adrParts.length +" parts in ladr");
				}
				
				if(adrParts[0].isEmpty()) {
					throw new Exception(messageNotNullable + " adr1 is null ");
				}
				
				if(adrParts[2].isEmpty()) {
					throw new Exception(messageNotNullable + " cty is null ");
				}
				
				if(adrParts[4].isEmpty()) {
					throw new Exception(messageNotNullable + " zip is null ");
				}
				
				if(adrParts[6].isEmpty()) {
					throw new Exception(messageNotNullable + " ctr is null ");
				}
				
				String zipCheckRegex = "^([\\d\\w\\W\\s]{1,15})?$";
				Pattern zipCheckRegexPattern = Pattern.compile(zipCheckRegex);
				Matcher zipCheckRegexMatcher = zipCheckRegexPattern.matcher(adrParts[4]);

				if(!zipCheckRegexMatcher.find()) {
					throw new Exception(" zip should be an Integer number (1-15 figures) (Example: 75020) or an String (1-15 figures) (Example: EC1Y 8YZ) for UK  or '_'. We found something like: " + adrParts[4]);
				}
				
				
				String ctrCheckRegex = "^[A-Z]{2,3}";
				Pattern ctrCheckRegexPattern = Pattern.compile(ctrCheckRegex);
				Matcher ctrCheckRegexMatcher = ctrCheckRegexPattern.matcher(adrParts[6]);

				if(!ctrCheckRegexMatcher.find()) {
					throw new Exception(" ctr should be an uppsercase code of 2-3 letters (Example: FRA). We found something like: " + adrParts[6]);
				}
		
			} else {
				/**
				 * if not
				 * maybe there exists something abnormal before the ladt part
				 */
				String failedCheckRegexLadr = checkRegexLadt + "[^(;|,|\\])]*[(;|,|\\])]";
				Pattern failedCheckRegexLadrPattern = Pattern.compile(failedCheckRegexLadr);
				Matcher failedCheckRegexLadrMatcher = failedCheckRegexLadrPattern.matcher(invalidAddr);
				String message = " an address should contain \" ladr=adr1|adr2|cty|ctn|zip|spc|ctr \" after the the parameter ladt";

				if(failedCheckRegexLadrMatcher.find()) 
				{
					String invalidPart = failedCheckRegexLadrMatcher.group(0);
					message += ", but we found something like \""+ invalidPart.replaceAll(checkRegexLadt, "");
				}
				throw new Exception(message);
				
			}
			
			
			
			
			
			String message = " addr is invalid. "
					+ "Example: \r\n" + 
					"	 [loid=6089840;losq=1;ladt=p;ladr=4 RUE LABBE|BATIMENTA|BESANCON||25000||FRA]\r\n" + 
					"	 [loid=6089841;losq=1;ladt=s;ladr=480 Boulevard Provinces|BATIMENTA|NANTERRE||92000||FRA]\r\n" 
					;
			throw new Exception(message);
		}
		
		if(!returnedMap.containsKey(LocationWamService.primaryLbuLocation)) 
		{
			throw new Exception(" No primary address. There should be one primary address ");
		}
		
		returnedMap.put("lbuLocations", lbuLocations);

		return returnedMap;
	}
	
	
	/**
	 * call the stocked procedure defined in oracle
	 * @param lbuLocation
	 * @throws Exception 
	 */
	public LbuLocation insertLbuLocation(LbuLocation lbuLocation) throws Exception
	{
		return lbuLocationPackage.insertLbuLocation(lbuLocation);
	}
	
	
	/**
	 * call ecm ws to create location in ecm
	 * @throws Exception 
	 */
	public Map<String,Object> createPlaceOfBusinessEcmWs(LbuLocation lbuLocation, String orgCustomerPGUID, String action) throws Exception
	{
		String url = this.getEcmWsUrlCreationPlaceOfBusiness();
		String replyStatus = "";
		
		Map<String,Object> returnMap =  new HashMap<String,Object>();
		
		CommunicationWrapper communicationWrapper = new CommunicationWrapper();
		communicationWrapper.setCommTypePGUID(null);
		communicationWrapper.setCommSubTypePGUID(null);
		communicationWrapper.setCommTypeDesc(null);
		communicationWrapper.setCommSubTypeDesc(null);
		communicationWrapper.setCommValue(null);
		
		ListOfCommunicationWrapper listOfCommunicationWrapper = new ListOfCommunicationWrapper();
		listOfCommunicationWrapper.setList(new ArrayList<CommunicationWrapper>());
		listOfCommunicationWrapper.getList().add(communicationWrapper);
		
		PlaceOfBusinessWrapper placeOfBusinessWrapper = new PlaceOfBusinessWrapper();
		placeOfBusinessWrapper.setAddress1(lbuLocation.getAddressLine1());
		placeOfBusinessWrapper.setAddress2(lbuLocation.getAddressLine2());
		placeOfBusinessWrapper.setAddress3(null);
		placeOfBusinessWrapper.setCity(lbuLocation.getCityName());
		placeOfBusinessWrapper.setCountry(lbuLocation.getCountryName());
		placeOfBusinessWrapper.setCountryPGUID(lbuLocation.getCountryCode());
		placeOfBusinessWrapper.setCountrySubdivisionPGUID(null);
		placeOfBusinessWrapper.setTaxGeocode(null);
		placeOfBusinessWrapper.setPostalCode(lbuLocation.getZipCode());
		placeOfBusinessWrapper.setValidAddressFlag(this.getValidAddressFlag());
		placeOfBusinessWrapper.setGeographicLocationFlag(this.getGeographicLocationFlag());
		placeOfBusinessWrapper.setOrgCustomerPGUID(orgCustomerPGUID);
		placeOfBusinessWrapper.setPrimaryAddressIndicator(this.getPrimaryAddressIndicator());
		placeOfBusinessWrapper.setCredentialingFlag(this.getCredentialingFlag());
		placeOfBusinessWrapper.setRequestedPRLevel(this.getRequestedPRLevel());
		placeOfBusinessWrapper.setListOfCommunication(listOfCommunicationWrapper);

		if(action == Definition.UPDATE)
		{
			placeOfBusinessWrapper.setAddressPGUID(lbuLocation.getAddressPGUID());
			placeOfBusinessWrapper.setPobPGUID(lbuLocation.getPobPGUID());
		}
		
		ListOfPlaceOfBusinessWrapperBO listOfPlaceOfBusinessWrapperBO = new ListOfPlaceOfBusinessWrapperBO();
		listOfPlaceOfBusinessWrapperBO.setList(new ArrayList<PlaceOfBusinessWrapper>());
		listOfPlaceOfBusinessWrapperBO.getList().add(placeOfBusinessWrapper);
		
		DataAreaWrapper dataAreaWrapper = new DataAreaWrapper();
		dataAreaWrapper.setListOfPlaceOfBusinessBO(listOfPlaceOfBusinessWrapperBO);
		
		DataGroupWrapper dataGroupWrapper = new DataGroupWrapper();
		dataGroupWrapper.setUserPermId(this.getUserPermId());
		
		CommonTrackingMessageWrapper commonTrackingMessageWrapper = new CommonTrackingMessageWrapper();
		commonTrackingMessageWrapper.setDataGroup(dataGroupWrapper);
		
		HeaderWrapper headerWrapper = new HeaderWrapper();
		headerWrapper.setCommonTrackingMessage(commonTrackingMessageWrapper);
		headerWrapper.setSourceAppName(this.getSourceAppName());
		headerWrapper.setPriority(this.getPriority());
		
		
		PlaceOfBusinessRootWrapper placeOfBusinessRootWrapper = new PlaceOfBusinessRootWrapper();
		placeOfBusinessRootWrapper.setNs7(this.getNs7());
		placeOfBusinessRootWrapper.setNs11(this.getNs11());
		placeOfBusinessRootWrapper.setNs12(this.getNs12());
		placeOfBusinessRootWrapper.setNs13(this.getNs13());
		placeOfBusinessRootWrapper.setHeader(headerWrapper);
		placeOfBusinessRootWrapper.setDataArea(dataAreaWrapper);

		/**
		 * JAXB transform java objact to xml
		 */
		JAXBContext jaxbContext = JAXBContext.newInstance(PlaceOfBusinessRootWrapper.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    //jaxbMarshaller.marshal(placeOfBusinessRootWrapper, System.out);
	    
    	// use StringWriter to convert jaxbMarshaller to String before calling remote ws
 		StringWriter sw = new StringWriter();
 		jaxbMarshaller.marshal(placeOfBusinessRootWrapper, sw);
 		
 		String params = sw.toString();

 		/**
		 * call remote ws
		 */
 		try {
			Map returnedMapFromEcm = HttpUtility.newRequest(url,HttpUtility.METHOD_PUT,params, HttpUtility.FORMAT_XML, HttpUtility.SUCCESS_STATUS_OK_CREATED);

			String response = returnedMapFromEcm.get(Definition.RESPONSE).toString();
			// on success
	        String responsePrettyFormat = InputStreamTransformer.prettyFormat(response, InputStreamTransformer.DEFAULT_INDENT);
	        WsMessageHelper.getMessageList().add(new WsMessage(url, HttpUtility.METHOD_PUT, params, responsePrettyFormat, returnedMapFromEcm.get(Definition.RESPONSE_CODE).toString(), lbuLocation.getLbuAccountId()));

			/**
	         * when errors.
	         */
	        if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) {
	        	replyStatus = Definition.FAILURE;
	        }

			/**
			 *  when success
			 */
			if(Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) 
			{
				replyStatus = Definition.SUCCESS;
			}
			
			
			if(!Arrays.asList(HttpUtility.SUCCESS_STATUS_ARRAY_OK_CREATED).contains(returnedMapFromEcm.get(Definition.RESPONSE_CODE))) {
	        	throw new Exception(" Ecm returned error. " + returnedMapFromEcm.get(Definition.RESPONSE).toString());
	        }

			returnMap.put(Definition.LBU_LOCATION, lbuLocation);
			returnMap.put(Definition.PARAMS, params);
			returnMap.put(Definition.ACTION, action);
			returnMap.put(Definition.RETURNED_MAP_FROM_ECM, returnedMapFromEcm);
			returnMap.put(Definition.REPLY_STATUS, replyStatus);
			returnMap.put(Definition.RESPONSE, response);
			/**
			 * Jackson transform 
			 * receive the returned data
			 */
			XMLInputFactory input = new WstxInputFactory();
			// configure for namespace
			input.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.TRUE);
			ObjectMapper objectMapper = new ObjectMapper(new XmlFactory(input, new WstxOutputFactory()));
			objectMapper.registerModule(new JaxbAnnotationModule());
			// we don't care about the unknown tags
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			
			PlaceOfBusinessRoot listOfPlaceOfBusinessWrapper = objectMapper.readValue(
					response,
					PlaceOfBusinessRoot.class);
			
			if(listOfPlaceOfBusinessWrapper.getDataArea() == null)
			{
				replyStatus = Definition.FAILURE;
				returnMap.put(Definition.REPLY_STATUS, replyStatus);
				throw new Exception(response);
			}
			
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" Ecm update location " + lbuLocation.getLbuLocationId() + " successfully ", ResponseMessage.SUCCESS));

			String addressPGUID = listOfPlaceOfBusinessWrapper.getDataArea().getListOfPlaceOfBusinessBO().getPlaceOfBusiness().getAddressPGUID().toString();
			String pobPguid = listOfPlaceOfBusinessWrapper.getDataArea().getListOfPlaceOfBusinessBO().getPlaceOfBusiness().getpOBPGUID().toString();

			updateLocationAddressPGUID(lbuLocation.getLbuAccountId(), lbuLocation.getLbuLocationId(),addressPGUID, pobPguid);
 		}
 		catch(SocketException e) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("SocketException: " + e.getMessage(), ResponseMessage.FAILED));
			ecmLocationHistoryLoggingAspect.historyLoggingLocationEcmWsAround(returnMap); 
			
			return returnMap;
		}
		catch (IOException e) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("IOException: " + e.getMessage(), ResponseMessage.FAILED));
			ecmLocationHistoryLoggingAspect.historyLoggingLocationEcmWsAround(returnMap); 
			
			return returnMap;
	    }
		catch (Exception e) {
			ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage("Exception: " + e.getMessage(), ResponseMessage.FAILED));
			ecmLocationHistoryLoggingAspect.historyLoggingLocationEcmWsAround(returnMap); 
			
			return returnMap;
	    }
 		
 		ecmLocationHistoryLoggingAspect.historyLoggingLocationEcmWsAround(returnMap); 
 		
		return returnMap;
	}
	
	/**
	 * update addressPguid for location after calling ecm ws
	 * @param accountId
	 * @param addressPguid
	 * @return
	 * @throws Exception
	 */
	public List<ResponseMessage> updateLocationAddressPGUID(String accountId, String locationId, String addressPguid, String pobPGUID) throws Exception
	{
		return lbuLocationPackage.updateLocationAddressPGUID(accountId, locationId, addressPguid, pobPGUID);
	}
	
	/**
	 * 
	 * @param lbuSetId
	 * @param lbuAccountId
	 * @param lbuLocationId
	 * @param gbsLocationId
	 * @param glpLocationId
	 * @param zipCode
	 * @param countryCode
	 * @param addressLine1
	 * @param addressLine2
	 * @return
	 * @throws Exception
	 */
	public LbuLocations lbuLocationSearch(
			String lbuSetId, 
			String lbuAccountId,
			String lbuLocationId,
			String gbsLocationId,
			String glpLocationId,
			String zipCode,
			String countryCode,
			String addressLine1,
			String addressLine2
			) throws Exception
	{
		LbuLocations lbuLocations = lbuLocationPackage.searchLbuLocation(
				 lbuSetId, 
				 lbuAccountId,
				 lbuLocationId,
				 gbsLocationId,
				 glpLocationId,
				 zipCode,
				 countryCode,
				 addressLine1,
				 addressLine2
				); 
	
		return lbuLocations;
	}
	
	
	/**
	 * 
	 * @param lbuAccountId
	 * @param lbuLocationId
	 * @param zipCode
	 * @param countryCode
	 * @param addressLine1
	 * @param addressLine2
	 * @return
	 * @throws Exception
	 */
	public AdminDataTables lbuLocationSearch2(
			String lbuAccountId,
			String lbuLocationId,
			String accountName1,
			String cityName,
			String zipCode,
			String countryCode,
			String addressLine1,
			String addressLine2,
			Integer fromNumber,
			Integer numberPerPage
			) throws Exception
	{
		AdminDataTables lbuLocations = lbuLocationPackage.searchLbuLocation2(
				 lbuAccountId,
				 lbuLocationId,
				 accountName1,
				 cityName,
				 zipCode,
				 countryCode,
				 addressLine1,
				 addressLine2,
				 fromNumber,
				 numberPerPage
				); 
	
		return lbuLocations;
	}
	
	/**
	 * 
	 * @param lbuSetId
	 * @param lbuAccountId
	 * @param lbuLocationId
	 * @return
	 * @throws Exception
	 */
	public LbuLocation selectLbuLocation(
			String lbuSetId, 
			String lbuAccountId,
			String lbuLocationId
			) throws Exception
	{
		LbuLocation lbuLocation = lbuLocationPackage.selectLbuLocation(lbuSetId, lbuAccountId, lbuLocationId);
		
		return lbuLocation;
	}
	
	/**
	 * 
	 * @param lbuLocations
	 * @param lbuAccount
	 * @throws Exception
	 */
	public void handleLocations(LbuLocations lbuLocations, LbuAccount lbuAccount) throws Exception
	{
		String customerPguid = lbuAccount.getCustomerPguid();
		Boolean hasCreate = false;

		for(LbuLocation lbuLocation : lbuLocations.getList())
		{
			LbuLocation lbuLocationFromDb = this.lbuLocationPackage.selectLbuLocation(lbuLocation.getLbuSetId(), lbuLocation.getLbuAccountId(), lbuLocation.getLbuLocationId());
			/**
			 * create if the lbuLocationFromDb not found.
			 * else update it
			 */

			if(lbuLocationFromDb == null) {
				/**
				 * if the lbuLocation is one in account.
				 * we need to update the AddressPGUID provided by lbuAccount model
				 */

				if(lbuLocation.getAddressLine1().equals(lbuAccount.getAddressLine1()))
				{
					if(lbuAccount.getAddressPGUID() != null)
					{
						lbuLocation.setAddressPGUID(lbuAccount.getAddressPGUID());
					}
					
					if(lbuAccount.getPobpguid() != null)
					{
						lbuLocation.setPobPGUID(lbuAccount.getPobpguid());
					}
				}
				/**
				 * insert new location in db
				 */
				lbuLocation = this.insertLbuLocation(lbuLocation);
				/**
				 * primary location already created in customer xml. 
				 * No need to call ecm ws location when create customer if only one location.
				 */
				if(lbuLocations.getList().size() > 1)
				{
					/**
					 * if the location is not the location in lbuAccount
					 */
					if(!lbuLocation.getAddressLine1().equals(lbuAccount.getAddressLine1()))
					{
						/**
						 * if account has more locations.
						 * except primary location. Other locations should be sent to ecm ws.
						 */
						if(this.isRealSendWs())
						{
							this.createPlaceOfBusinessEcmWs(lbuLocation, customerPguid, Definition.CREATE);

							if(!hasCreate)
							{
								hasCreate = true;
							}
						}
						
					}
				}
				
				ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" The location of account: " + lbuAccount.getLbuAccountId() + " location:  " + lbuLocation.getLbuLocationId() + " inserted successfully.", ResponseMessage.SUCCESS));
			
			} else {
				/**
				 *  update
				 */
				lbuLocation = this.lbuLocationPackage.updateLbuLocation(lbuLocation);

				if(lbuLocation.getIsChanged()) {
					ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" The location of account: " + lbuAccount.getLbuAccountId() + " location:  " + lbuLocation.getLbuLocationId() + " updated successfully.", ResponseMessage.SUCCESS));
					
					/**
					 * fetch the updated location with addressPguid and pobPguid
					 */
					lbuLocation = this.lbuLocationPackage.selectLbuLocation(lbuLocation.getLbuSetId(), lbuLocation.getLbuAccountId(), lbuLocation.getLbuLocationId());

					if(this.isRealSendWs())
					{
						this.createPlaceOfBusinessEcmWs(lbuLocation, customerPguid, Definition.UPDATE);
					}

				} else {
					/**
					 * update AddressPGUID if it is null
					 */
					if(lbuLocationFromDb.getAddressPGUID() == null)
					{
						if(this.isRealSendWs())
						{
							this.createPlaceOfBusinessEcmWs(lbuLocation, customerPguid, Definition.CREATE);
							
							if(!hasCreate)
							{
								hasCreate = true;
							}
						}
						
					}
					
					ResponseHelper.getMessageList().add(ResponseHelper.listPosition++, new ResponseMessage(" The location of account: " + lbuAccount.getLbuAccountId() + " location:  " + lbuLocation.getLbuLocationId() + " is already updated. Nothing changed.", ResponseMessage.SUCCESS));
				}
				
			}
			
		}
		
		if(hasCreate)
		{
			/**
			 * sleep when new location created
			 */
			Thread.sleep(Integer.parseInt(this.env.getProperty("before.order.sleep.time")));
		}
	}
	
	public LbuLocation updateLbuLocation(LbuLocation lbuLocation)  throws Exception
	{
		return this.lbuLocationPackage.updateLbuLocation(lbuLocation);
	}
}
