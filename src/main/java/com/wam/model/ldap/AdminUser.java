package com.wam.model.ldap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

public class AdminUser {

	public String windowsId;
	public String ldapId;
	public String email;
	public String role;
	
	


	public String getWindowsId() {
		return windowsId;
	}




	public void setWindowsId(String windowsId) {
		this.windowsId = windowsId;
	}




	public String getLdapId() {
		return ldapId;
	}




	public void setLdapId(String ldapId) {
		this.ldapId = ldapId;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}



	public String getRole() {
		return role;
	}




	public void setRole(String role) {
		this.role = role;
	}




	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
