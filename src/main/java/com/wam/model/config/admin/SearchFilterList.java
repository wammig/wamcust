package com.wam.model.config.admin;

import java.util.List;

import org.springframework.stereotype.Component;

@Component("SearchFilterList")
public class SearchFilterList {

	private List<SearchFilter> list;

	public List<SearchFilter> getList() {
		return list;
	}

	public void setList(List<SearchFilter> list) {
		this.list = list;
	}
}
