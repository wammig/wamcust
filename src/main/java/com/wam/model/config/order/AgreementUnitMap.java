package com.wam.model.config.order;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("AgreementUnitMap")
public class AgreementUnitMap {

	private Map<String, AgreementUnit> map = new HashMap<String, AgreementUnit>();

	public Map<String, AgreementUnit> getMap() {
		return map;
	}

	public void setMap(Map<String, AgreementUnit> map) {
		this.map = map;
	}
	
}
