package com.wam.model.config.ecm;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("BusinessUnitMap")
public class BusinessUnitMap {

	private Map<String, BusinessUnit> map = new HashMap<String, BusinessUnit>();

	public Map<String, BusinessUnit> getMap() {
		return map;
	}

	public void setMap(Map<String, BusinessUnit> map) {
		this.map = map;
	}
	
}
