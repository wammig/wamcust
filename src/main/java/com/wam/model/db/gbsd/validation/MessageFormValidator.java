package com.wam.model.db.gbsd.validation;

import java.util.Arrays;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.utility.definition.Definition;

@Service("MessageFormValidator")
public class MessageFormValidator implements Validator{

	String[] actionValues = {Definition.WORKFLOW_MANAGER_SQL_CODE, Definition.WORKFLOW_MANAGER_ECM_CODE, Definition.WORKFLOW_MANAGER_FINISHED_CODE};
	
	String[] statusValues = {Definition.WORKFLOW_MANAGER_STATUS_BEGINNING, Definition.WORKFLOW_MANAGER_STATUS_COMPLETE, Definition.WORKFLOW_MANAGER_STATUS_ERROR};
	
	@Override
	public boolean supports(Class<?> paramClass) {
		return LogMessage.class.equals(paramClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		LogMessage messageForm = (LogMessage)obj;
		
		if(!Arrays.asList(actionValues).contains(messageForm.getActionId().toString())) 
		{
			errors.rejectValue("actionId", "value");
		}
		
		if(!Arrays.asList(statusValues).contains(messageForm.getStatus())) 
		{
			errors.rejectValue("status", "value");
		}
		
	}
	
}
