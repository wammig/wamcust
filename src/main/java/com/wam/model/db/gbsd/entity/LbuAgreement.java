package com.wam.model.db.gbsd.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name="LBU_ACCOUNT")
public class LbuAgreement implements Serializable{

	private String agreementId;
	private String assetAgreementId;
	private String productId;
	private String beginDate;
	private String endDate;
	private Integer licenseCount;
	private String customerPguid;
	private Boolean IsChanged;
	
	
	
	public String getAgreementId() {
		return agreementId;
	}



	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}



	public String getAssetAgreementId() {
		return assetAgreementId;
	}



	public void setAssetAgreementId(String assetAgreementId) {
		this.assetAgreementId = assetAgreementId;
	}



	public String getProductId() {
		return productId;
	}



	public void setProductId(String productId) {
		this.productId = productId;
	}



	public String getBeginDate() {
		return beginDate;
	}



	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}



	public String getEndDate() {
		return endDate;
	}



	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}



	public Integer getLicenseCount() {
		return licenseCount;
	}



	public void setLicenseCount(Integer licenseCount) {
		this.licenseCount = licenseCount;
	}


	
	
	public String getCustomerPguid() {
		return customerPguid;
	}



	public void setCustomerPguid(String customerPguid) {
		this.customerPguid = customerPguid;
	}

	

	public Boolean getIsChanged() {
		return IsChanged;
	}



	public void setIsChanged(Boolean isChanged) {
		IsChanged = isChanged;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
