package com.wam.model.db.gbsd.entity;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import javax.persistence.*;

@Entity
@Table(name="LBU_LOCATION")
public class LbuLocation implements Serializable, Cloneable {

	private String lbuLocationId;
	private String lbuAccountId;
	private String gbsLocationId;
	private String glpLocationId;
	private String lbuAddressId;
	private String gbsAddressId;
	private Integer addressSeq;
	private String addressLine1;
	private String addressLine2;
	private String cityName;
	private String auditUserId;
	private String salesRepId;
	private String extOrderNo;
	private String countryName;
	private String zipCode;
	private String stateOrProvinceCode;
	private String countryCode;
	private String updatedDatetime;
	private String createdDatetime;
	private String dataMigration;
	private String lbuSetId;
	private String addressPGUID;
	private String pobPGUID;
	
	/**
	 * additional fields
	 * 
	 */
	private Boolean isChanged;

	
	
	public Boolean getIsChanged() {
		return isChanged;
	}



	public void setIsChanged(Boolean isChanged) {
		this.isChanged = isChanged;
	}
	
	public String getLbuLocationId() {
		return lbuLocationId;
	}



	public void setLbuLocationId(String lbuLocationId) {
		this.lbuLocationId = lbuLocationId;
	}



	public String getLbuAccountId() {
		return lbuAccountId;
	}



	public void setLbuAccountId(String lbuAccountId) {
		this.lbuAccountId = lbuAccountId;
	}



	public String getGbsLocationId() {
		return gbsLocationId;
	}



	public void setGbsLocationId(String gbsLocationId) {
		this.gbsLocationId = gbsLocationId;
	}



	public String getGlpLocationId() {
		return glpLocationId;
	}



	public void setGlpLocationId(String glpLocationId) {
		this.glpLocationId = glpLocationId;
	}



	public String getLbuAddressId() {
		return lbuAddressId;
	}



	public void setLbuAddressId(String lbuAddressId) {
		this.lbuAddressId = lbuAddressId;
	}



	public String getGbsAddressId() {
		return gbsAddressId;
	}



	public void setGbsAddressId(String gbsAddressId) {
		this.gbsAddressId = gbsAddressId;
	}



	public Integer getAddressSeq() {
		return addressSeq;
	}



	public void setAddressSeq(Integer addressSeq) {
		this.addressSeq = addressSeq;
	}



	public String getAddressLine1() {
		return addressLine1;
	}



	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}



	public String getAddressLine2() {
		return addressLine2;
	}



	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}



	public String getCityName() {
		return cityName;
	}



	public void setCityName(String cityName) {
		this.cityName = cityName;
	}



	public String getAuditUserId() {
		return auditUserId;
	}



	public void setAuditUserId(String auditUserId) {
		this.auditUserId = auditUserId;
	}



	public String getSalesRepId() {
		return salesRepId;
	}



	public void setSalesRepId(String salesRepId) {
		this.salesRepId = salesRepId;
	}



	public String getExtOrderNo() {
		return extOrderNo;
	}



	public void setExtOrderNo(String extOrderNo) {
		this.extOrderNo = extOrderNo;
	}



	public String getCountryName() {
		return countryName;
	}



	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}



	public String getZipCode() {
		return zipCode;
	}



	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}



	public String getStateOrProvinceCode() {
		return stateOrProvinceCode;
	}



	public void setStateOrProvinceCode(String stateOrProvinceCode) {
		this.stateOrProvinceCode = stateOrProvinceCode;
	}
	
	

	public String getCountryCode() {
		return countryCode;
	}



	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}



	public String getUpdatedDatetime() {
		return updatedDatetime;
	}



	public void setUpdatedDatetime(String updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}



	public String getCreatedDatetime() {
		return createdDatetime;
	}



	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}



	public String getDataMigration() {
		return dataMigration;
	}



	public void setDataMigration(String dataMigration) {
		this.dataMigration = dataMigration;
	}



	public String getLbuSetId() {
		return lbuSetId;
	}



	public void setLbuSetId(String lbuSetId) {
		this.lbuSetId = lbuSetId;
	}



	public String getAddressPGUID() {
		return addressPGUID;
	}



	public void setAddressPGUID(String addressPGUID) {
		this.addressPGUID = addressPGUID;
	}
	
	

	public String getPobPGUID() {
		return pobPGUID;
	}



	public void setPobPGUID(String pobPGUID) {
		this.pobPGUID = pobPGUID;
	}



	/**
	 * locationId is a combination of addressId and addressSeq
	 * @param addressId
	 * @param addressSeq
	 * @return
	 */
	public String generateLocationId(String addressId, String addressSeq)
	{
		if(addressSeq.toString().length() < 2)
		{
			return addressId + "S0" + addressSeq.toString();
		} else {
			return addressId + "S" + addressSeq.toString();
		}
			
	}
	
	

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
