package com.wam.model.db.gbsd.entity;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import javax.persistence.*;

@Entity
@Table(name="SYS_HISTORY")
public class SysHistory implements Serializable{

	private Integer historyId;
	private String processType;
	private String processSubType;
	private String entityId;
	private String sendStatus;
	private String replyStatus;
	private String errorCode;
	private String errorMessage;
	private String transId;
	private String message;
	private String replyMessage;
	private String createdBy;
	private String createdDateTime;
	private String updatedBy;
	private String updatedDatetime;
	private Integer messageId;
	private String entitySeq0;
	private String entitySeq1;
	private String entitySeq2;
	private String entitySeq3;
	private String entitySeq4;
	private String entitySeq5;
	private String dataSourceName;
	
	
	public Integer getHistoryId() {
		return historyId;
	}



	public void setHistoryId(Integer historyId) {
		this.historyId = historyId;
	}



	public String getProcessType() {
		return processType;
	}



	public void setProcessType(String processType) {
		this.processType = processType;
	}



	public String getProcessSubType() {
		return processSubType;
	}



	public void setProcessSubType(String processSubType) {
		this.processSubType = processSubType;
	}



	public String getEntityId() {
		return entityId;
	}



	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}



	public String getSendStatus() {
		return sendStatus;
	}



	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}



	public String getReplyStatus() {
		return replyStatus;
	}



	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}



	public String getErrorCode() {
		return errorCode;
	}



	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}



	public String getErrorMessage() {
		return errorMessage;
	}



	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}



	public String getTransId() {
		return transId;
	}



	public void setTransId(String transId) {
		this.transId = transId;
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public String getCreatedDateTime() {
		return createdDateTime;
	}



	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public String getUpdatedDatetime() {
		return updatedDatetime;
	}



	public void setUpdatedDatetime(String updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}



	public Integer getMessageId() {
		return messageId;
	}



	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}



	public String getEntitySeq0() {
		return entitySeq0;
	}



	public void setEntitySeq0(String entitySeq0) {
		this.entitySeq0 = entitySeq0;
	}



	public String getEntitySeq1() {
		return entitySeq1;
	}



	public void setEntitySeq1(String entitySeq1) {
		this.entitySeq1 = entitySeq1;
	}



	public String getEntitySeq2() {
		return entitySeq2;
	}



	public void setEntitySeq2(String entitySeq2) {
		this.entitySeq2 = entitySeq2;
	}



	public String getEntitySeq3() {
		return entitySeq3;
	}



	public void setEntitySeq3(String entitySeq3) {
		this.entitySeq3 = entitySeq3;
	}



	public String getEntitySeq4() {
		return entitySeq4;
	}



	public void setEntitySeq4(String entitySeq4) {
		this.entitySeq4 = entitySeq4;
	}



	public String getEntitySeq5() {
		return entitySeq5;
	}



	public void setEntitySeq5(String entitySeq5) {
		this.entitySeq5 = entitySeq5;
	}

	

	public String getDataSourceName() {
		return dataSourceName;
	}



	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	

	public String getReplyMessage() {
		return replyMessage;
	}



	public void setReplyMessage(String replyMessage) {
		this.replyMessage = replyMessage;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
