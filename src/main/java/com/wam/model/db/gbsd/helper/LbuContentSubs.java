package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wam.model.db.gbsd.entity.LbuContentSub;


public class LbuContentSubs implements Serializable{
	
	private ArrayList<LbuContentSub> list = new ArrayList<LbuContentSub>();
	

	public ArrayList<LbuContentSub> getList() {
		return list;
	}


	public void setList(ArrayList<LbuContentSub> list) {
		this.list = list;
	}


	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
