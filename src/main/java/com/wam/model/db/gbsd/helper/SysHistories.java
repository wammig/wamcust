package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.SysHistory;

public class SysHistories implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<SysHistory> list = new ArrayList<SysHistory>();
	private Boolean isChanged = false;
	
	
	public ArrayList<SysHistory> getList() {
		return list;
	}



	public void setList(ArrayList<SysHistory> list) {
		this.list = list;
	}

	


	public Boolean getIsChanged() {
		return isChanged;
	}



	public void setIsChanged(Boolean isChanged) {
		this.isChanged = isChanged;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
