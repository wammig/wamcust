package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuContentSubs;



public class Orders implements Serializable{
	
	private LbuLocations lbuLocations = new LbuLocations();
	private LbuContentSubs lbuContentSubs = new LbuContentSubs();
	private LbuAccount lbuAccount = new LbuAccount();
	
	
	
	public LbuLocations getLbuLocations() {
		return lbuLocations;
	}



	public void setLbuLocations(LbuLocations lbuLocations) {
		this.lbuLocations = lbuLocations;
	}



	public LbuContentSubs getLbuContentSubs() {
		return lbuContentSubs;
	}



	public void setLbuContentSubs(LbuContentSubs lbuContentSubs) {
		this.lbuContentSubs = lbuContentSubs;
	}



	public LbuAccount getLbuAccount() {
		return lbuAccount;
	}



	public void setLbuAccount(LbuAccount lbuAccount) {
		this.lbuAccount = lbuAccount;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
