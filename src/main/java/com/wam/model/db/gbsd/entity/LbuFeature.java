package com.wam.model.db.gbsd.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name="LBU_FEATURE")
public class LbuFeature {
	
	private String assemblyId;
	private String description;
	private String status;
	
	
	public String getAssemblyId() {
		return assemblyId;
	}



	public void setAssemblyId(String assemblyId) {
		this.assemblyId = assemblyId;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
