package com.wam.model.db.gbsd.helper;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LbuLocation;

public class LbuLocationAdmin extends LbuLocation{

	private String accountName1;
	
	
	public String getAccountName1() {
		return accountName1;
	}



	public void setAccountName1(String accountName1) {
		this.accountName1 = accountName1;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
