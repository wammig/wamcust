package com.wam.model.db.gbsd.validation;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuLocation;

@Service("LocationFormValidator")
public class LocationFormValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> paramClass) {
		return LbuAccount.class.equals(paramClass);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		LbuLocation locationForm = (LbuLocation)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuAccountId", "Size");	
		if(locationForm.getLbuAccountId().length() < 5 || locationForm.getLbuAccountId().length() > 15) 
		{
			errors.rejectValue("lbuAccountId", "Size");
		}
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuLocationId", "Size");	
		if(locationForm.getLbuLocationId().length() < 5 || locationForm.getLbuLocationId().length() > 25) 
		{
			errors.rejectValue("lbuLocationId", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "addressSeq", "Size");	
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "addressLine1", "Size");	
		if(locationForm.getAddressLine1().length() > 40) 
		{
			errors.rejectValue("addressLine1", "Size");
		}
		
		if(locationForm.getAddressLine2() != null)
		{
			if(locationForm.getAddressLine2().length() > 40) 
			{
				errors.rejectValue("addressLine2", "Size");
			}
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cityName", "Size");	
		if(locationForm.getCityName().length() > 50) 
		{
			errors.rejectValue("cityName", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "auditUserId", "Size");	
		if(locationForm.getAuditUserId().length() > 50) 
		{
			errors.rejectValue("auditUserId", "Size");
		}
		
		if(locationForm.getCountryName() != null)
		{
			if(locationForm.getCountryName().length() > 50) 
			{
				errors.rejectValue("countryName", "Size");
			}
		}
		
		if(locationForm.getZipCode() != null)
		{
			if(locationForm.getZipCode().length() > 25) 
			{
				errors.rejectValue("zipCode", "Size");
			}
		}
		
		if(locationForm.getStateOrProvinceCode() != null)
		{
			if(locationForm.getStateOrProvinceCode().length() > 6) 
			{
				errors.rejectValue("stateOrProvinceCode", "Size");
			}
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "countryCode", "Size");	
		if(locationForm.getCountryCode().length() > 3) 
		{
			errors.rejectValue("countryCode", "Size");
		}
		
	}
}
