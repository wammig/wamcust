package com.wam.model.db.gbsd.form;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
//import org.hibernate.validator.NotEmpty;
//import org.hibernate.validator.NotNull;
//import org.hibernate.validator.Size;

public class AccountForm {

//	@Size(min=7 , max=15)
	private String lbuAccountId;
	
//	@Size(max=15)
//	@NotNull
	private String gbsAccountId;
	
//	@Size(max=15)
	private String glpAccountId;
	
//	@Size(max=40)
//	@NotNull
	private String accountName1;
	
//	@Size(max=40)
	private String accountName2;
	
//	@Size(max=50)
//	@NotNull
	private String auditUserId;
	
//	@Size(max=50)
	private String salesRepId;
	
//	@Size(max=25)
	private String extOrderNo;
	
//	@Size(max=10)
	private Integer maxUsers = 0;
	
//	@Size(max=15)
//	@NotNull
	private String billableStatus;
	
//	@Size(max=25)
//	@NotNull
	private String accountType;
	
//	@Size(max=40)
//	@NotNull
	private String addressLine1;
	
//	@Size(max=40)
	private String addressLine2;
	
//	@Size(max=50)
//	@NotNull
	private String cityName;
	
//	@Size(max=50)
	private String countryName;
	
//	@Size(max=25)
//	@NotNull
	private String zipCode;
	
//	@Size(max=4)
	private String stateOrProvinceCode;
	
//	@Size(max=3)
//	@NotNull
	private String countryCode;
	
//	@NotNull
	private String updatedDatetime;
	
//	@NotNull
	private String createdDatetime;
	
	private String dataMigration;
	
//	@Size(max=2)
//	@NotNull
	private String lbuSetId;
	
//	@Size(max=50)
	private String customerPguid;
	
//	@Size(max=50)
	private String pobpguid;
	
	
	public String getLbuAccountId() {
		return lbuAccountId;
	}



	public void setLbuAccountId(String lbuAccountId) {
		this.lbuAccountId = lbuAccountId;
	}



	public String getGbsAccountId() {
		return gbsAccountId;
	}



	public void setGbsAccountId(String gbsAccountId) {
		this.gbsAccountId = gbsAccountId;
	}



	public String getGlpAccountId() {
		return glpAccountId;
	}



	public void setGlpAccountId(String glpAccountId) {
		this.glpAccountId = glpAccountId;
	}



	public String getAccountName1() {
		return accountName1;
	}



	public void setAccountName1(String accountName1) {
		this.accountName1 = accountName1;
	}



	public String getAccountName2() {
		return accountName2;
	}



	public void setAccountName2(String accountName2) {
		this.accountName2 = accountName2;
	}



	public String getAuditUserId() {
		return auditUserId;
	}



	public void setAuditUserId(String auditUserId) {
		this.auditUserId = auditUserId;
	}



	public String getSalesRepId() {
		return salesRepId;
	}



	public void setSalesRepId(String salesRepId) {
		this.salesRepId = salesRepId;
	}



	public String getExtOrderNo() {
		return extOrderNo;
	}



	public void setExtOrderNo(String extOrderNo) {
		this.extOrderNo = extOrderNo;
	}



	public Integer getMaxUsers() {
		return maxUsers;
	}



	public void setMaxUsers(Integer maxUsers) {
		this.maxUsers = maxUsers;
	}



	public String getBillableStatus() {
		return billableStatus;
	}



	public void setBillableStatus(String billableStatus) {
		this.billableStatus = billableStatus;
	}



	public String getAccountType() {
		return accountType;
	}



	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}



	public String getAddressLine1() {
		return addressLine1;
	}



	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}



	public String getAddressLine2() {
		return addressLine2;
	}



	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}



	public String getCityName() {
		return cityName;
	}



	public void setCityName(String cityName) {
		this.cityName = cityName;
	}



	public String getCountryName() {
		return countryName;
	}



	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}



	public String getZipCode() {
		return zipCode;
	}



	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}



	public String getStateOrProvinceCode() {
		return stateOrProvinceCode;
	}



	public void setStateOrProvinceCode(String stateOrProvinceCode) {
		this.stateOrProvinceCode = stateOrProvinceCode;
	}



	public String getCountryCode() {
		return countryCode;
	}



	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}



	public String getUpdatedDatetime() {
		return updatedDatetime;
	}



	public void setUpdatedDatetime(String updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}



	public String getCreatedDatetime() {
		return createdDatetime;
	}



	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}



	public String getDataMigration() {
		return dataMigration;
	}



	public void setDataMigration(String dataMigration) {
		this.dataMigration = dataMigration;
	}



	public String getLbuSetId() {
		return lbuSetId;
	}



	public void setLbuSetId(String lbuSetId) {
		this.lbuSetId = lbuSetId;
	}
	
	
	
	public String getCustomerPguid() {
		return customerPguid;
	}



	public void setCustomerPguid(String customerPguid) {
		this.customerPguid = customerPguid;
	}



	public String getPobpguid() {
		return pobpguid;
	}



	public void setPobpguid(String pobpguid) {
		this.pobpguid = pobpguid;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
