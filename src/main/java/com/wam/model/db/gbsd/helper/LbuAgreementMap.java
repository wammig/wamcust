package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LbuAgreement;

public class LbuAgreementMap {

	private Map<String, LbuAgreement> map = new HashMap<String, LbuAgreement>();
	
	
	
	public Map<String, LbuAgreement> getMap() {
		return map;
	}



	public void setMap(Map<String, LbuAgreement> map) {
		this.map = map;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
