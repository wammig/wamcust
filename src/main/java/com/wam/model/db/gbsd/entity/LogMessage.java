package com.wam.model.db.gbsd.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name="LOG_MESSAGE")
public class LogMessage {
	private Integer messageId;
	private String processType;
	private String processSubType;
	private Integer actionId;
	private String status;
	private String message;
	private String transId;
	private String groupId;
	private String application;
	private String createdBy;
	private String createdDatetime;
	private String updatedBy;
	private String updatedDatetime;
	
	
	
	public Integer getMessageId() {
		return messageId;
	}



	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}



	public String getProcessType() {
		return processType;
	}



	public void setProcessType(String processType) {
		this.processType = processType;
	}



	public String getProcessSubType() {
		return processSubType;
	}



	public void setProcessSubType(String processSubType) {
		this.processSubType = processSubType;
	}



	public Integer getActionId() {
		return actionId;
	}



	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}



	public String getTransId() {
		return transId;
	}



	public void setTransId(String transId) {
		this.transId = transId;
	}



	public String getGroupId() {
		return groupId;
	}



	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}



	public String getApplication() {
		return application;
	}



	public void setApplication(String application) {
		this.application = application;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public String getCreatedDatetime() {
		return createdDatetime;
	}



	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public String getUpdatedDatetime() {
		return updatedDatetime;
	}



	public void setUpdatedDatetime(String updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
