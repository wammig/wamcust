package com.wam.model.db.gbsd.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.service.AccountWamService;

@Service("ContentSubFormValidator")
public class ContentSubFormValidator implements Validator{

	public static String regexUpdatedDate = "[0-9]{2}-[0-9]{2}-[19|20]{2}[0-9]{2}";
	
	@Override
	public boolean supports(Class<?> paramClass) {
		return LbuContentSub.class.equals(paramClass);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		LbuContentSub contentSubForm = (LbuContentSub)obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contractNum", "Size");	
		if( contentSubForm.getContractNum().length() > 25) 
		{
			errors.rejectValue("contractNum", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contractLineNum", "Size");	
		if( contentSubForm.getContractLineNum().toString().length() > 5) 
		{
			errors.rejectValue("contractLineNum", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contractLineNumOrigin", "Size");	
		if( contentSubForm.getContractLineNumOrigin().toString().length() > 5) 
		{
			errors.rejectValue("contractLineNumOrigin", "Size");
		}
		
		if( contentSubForm.getContentSubName().toString().length() > 50) 
		{
			errors.rejectValue("contentSubName", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuAccountId", "Size");	
		if( contentSubForm.getLbuAccountId().toString().length() > 15) 
		{
			errors.rejectValue("lbuAccountId", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuContentSubId", "Size");	
		if( contentSubForm.getLbuContentSubId().toString().length() > 25) 
		{
			errors.rejectValue("lbuContentSubId", "Size");
		}
		
		Pattern datePattern = Pattern.compile(this.regexUpdatedDate);
		Matcher dateMatcher;
		
		dateMatcher = datePattern.matcher(contentSubForm.getBeginDate());
		if( ! dateMatcher.find() ) 
		{
			errors.rejectValue("beginDate", "Size");
		}
		
		dateMatcher = datePattern.matcher(contentSubForm.getEndDate());
		if( ! dateMatcher.find() ) 
		{
			errors.rejectValue("endDate", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "auditUserId", "Size");	
		if( contentSubForm.getAuditUserId().toString().length() > 50 ) 
		{
			errors.rejectValue("auditUserId", "Size");
		}
		
		if(contentSubForm.getMaxUsers() != null)
		{
			if( !(contentSubForm.getMaxUsers() instanceof Integer) ) 
			{
				errors.rejectValue("maxUsers", "typeMismatch");
			}
			
			if(contentSubForm.getMaxUsers().toString().length() > 10)
			{
				errors.rejectValue("maxUsers", "Size");
			}
		}
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "monthlyCommitment", "Size");	
		if( ! (contentSubForm.getMonthlyCommitment() instanceof Float) || contentSubForm.getMonthlyCommitment().toString().length() > 10) 
		{
			errors.rejectValue("monthlyCommitment", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "monthlyCap", "Size");	
		if( ! (contentSubForm.getMonthlyCap() instanceof Float) || contentSubForm.getMonthlyCap().toString().length() > 10) 
		{
			errors.rejectValue("monthlyCap", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuSourcePackageId", "Size");	
		if( contentSubForm.getLbuSourcePackageId().toString().length() > 25) 
		{
			errors.rejectValue("lbuSourcePackageId", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuSetId", "Size");	
		if(contentSubForm.getLbuSetId().toString().length() != 2) 
		{
			errors.rejectValue("lbuSetId", "Size");
		}
		
		if(contentSubForm.getLbuLocationId() != null)
		{
			if( contentSubForm.getLbuLocationId().toString().length() > 25) 
			{
				errors.rejectValue("lbuLocationId", "Size");
			}
		}
		
		
		if( contentSubForm.getAddressSeq() != null) 
		{
			if(! (contentSubForm.getAddressSeq() instanceof Integer))
			{
				errors.rejectValue("addressSeq", "Size");
			}

			if(contentSubForm.getAddressSeq().toString().length() > 5)
			{
				errors.rejectValue("addressSeq", "Size");
			}
		}
	}
}
