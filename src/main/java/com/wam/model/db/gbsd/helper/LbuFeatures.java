package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LbuFeature;

public class LbuFeatures implements Serializable{

	private ArrayList<LbuFeature> list = new ArrayList<LbuFeature>();

	
	
	public ArrayList<LbuFeature> getList() {
		return list;
	}



	public void setList(ArrayList<LbuFeature> list) {
		this.list = list;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
