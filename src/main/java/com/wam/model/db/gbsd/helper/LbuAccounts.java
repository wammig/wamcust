package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LbuAccount;

public class LbuAccounts implements Serializable{

	private ArrayList<LbuAccount> list = new ArrayList<LbuAccount>();
	
	
	
	public ArrayList<LbuAccount> getList() {
		return list;
	}



	public void setList(ArrayList<LbuAccount> list) {
		this.list = list;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
