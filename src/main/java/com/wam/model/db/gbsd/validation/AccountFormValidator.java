package com.wam.model.db.gbsd.validation;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.wam.model.db.gbsd.entity.LbuAccount;

@Service("AccountFormValidator")
public class AccountFormValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> paramClass) {
		return LbuAccount.class.equals(paramClass);
	}
	
	
	@Override
	public void validate(Object obj, Errors errors) {
		LbuAccount accountForm = (LbuAccount)obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuAccountId", "Size");	
		if(accountForm.getLbuAccountId().length() < 5 || accountForm.getLbuAccountId().length() > 15) 
		{
			errors.rejectValue("lbuAccountId", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accountName1", "Size");	
		if(accountForm.getAccountName1().length() < 1 || accountForm.getAccountName1().length() > 150)
		{
			errors.rejectValue("accountName1", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "auditUserId", "Size");	
		if(accountForm.getAuditUserId().length() < 1 || accountForm.getAuditUserId().length() > 50)
		{
			errors.rejectValue("auditUserId", "Size");
		}
		
		if( (!(accountForm.getMaxUsers() instanceof Integer) && accountForm.getMaxUsers() != null) || accountForm.getMaxUsers().toString().length() > 10)
		{
			errors.rejectValue("maxUsers", "typeMismatch");
		}
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "addressLine1", "Size");	
		if(accountForm.getAddressLine1().length() < 1 || accountForm.getAddressLine1().length() > 50)
		{
			errors.rejectValue("addressLine1", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cityName", "Size");	
		if(accountForm.getCityName().length() < 1 || accountForm.getCityName().length() > 50)
		{
			errors.rejectValue("cityName", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zipCode", "Size");	
		if(accountForm.getZipCode().length() < 4 || accountForm.getZipCode().length() > 25)
		{
			errors.rejectValue("zipCode", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "countryCode", "Size");	
		if(accountForm.getCountryCode().length() != 3)
		{
			errors.rejectValue("countryCode", "Size");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lbuSetId", "Size");	
		if(accountForm.getLbuSetId().length() != 2)
		{
			errors.rejectValue("lbuSetId", "Size");
		}
		
	}
}
