package com.wam.model.db.gbsd.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name="LBU_SOURCEPACKAGE")
public class LbuSourcePackage implements Serializable{

	private String lbuSourcePackageId;
	private String sourcePackageName;
	private String updatedDateTime;
	private String createdDateTime;
	private String lbuSetId;
	private String assemblyId;
	
	
	
	public String getLbuSourcePackageId() {
		return lbuSourcePackageId;
	}



	public void setLbuSourcePackageId(String lbuSourcePackageId) {
		this.lbuSourcePackageId = lbuSourcePackageId;
	}



	public String getSourcePackageName() {
		return sourcePackageName;
	}



	public void setSourcePackageName(String sourcePackageName) {
		this.sourcePackageName = sourcePackageName;
	}



	public String getUpdatedDateTime() {
		return updatedDateTime;
	}



	public void setUpdatedDateTime(String updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}



	public String getCreatedDateTime() {
		return createdDateTime;
	}



	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}



	public String getLbuSetId() {
		return lbuSetId;
	}



	public void setLbuSetId(String lbuSetId) {
		this.lbuSetId = lbuSetId;
	}



	public String getAssemblyId() {
		return assemblyId;
	}



	public void setAssemblyId(String assemblyId) {
		this.assemblyId = assemblyId;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
