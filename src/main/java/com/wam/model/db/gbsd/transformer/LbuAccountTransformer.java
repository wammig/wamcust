package com.wam.model.db.gbsd.transformer;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LbuAccount;

import javax.persistence.*;

@Entity
public class LbuAccountTransformer extends LbuAccount implements Serializable{
	
	/**
	 * additional fields for the table
	 */
	private String processType;
	private String processSubtype;
	
	
	public String getProcessType() {
		return processType;
	}



	public void setProcessType(String processType) {
		this.processType = processType;
	}



	public String getProcessSubtype() {
		return processSubtype;
	}



	public void setProcessSubtype(String processSubtype) {
		this.processSubtype = processSubtype;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
