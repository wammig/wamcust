package com.wam.model.db.gbsd.entity;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.utility.helper.StringHelper;

import javax.persistence.*;

@Entity
@Table(name="LBU_CONTENTSUB")
public class LbuContentSub implements Serializable{

	private String contractNum;
	private Integer contractLineNum;
	private Integer contractLineNumOrigin;
	private String contentSubName;
	private String lbuAccountId;
	private String lbuContentSubId;
	private String beginDate;
	private String endDate;
	private String auditUserId;
	private Integer maxUsers;
	private Float monthlyCommitment;
	private Float monthlyCap;
	private String lbuSourcePackageId;
	private String updatedDatetime;
	private String createdDatetime;
	private String lbuSetId;
	private String lbuLocationId;
	private Integer addressSeq;
	private String agreementPguid;
	public String assetAgreementId;
	private String noReNew;
	
	
	/**
	 * additional fields
	 * 
	 */
	private Boolean isChanged;

	private Integer messageId;
	
	public Boolean getIsChanged() {
		return isChanged;
	}

	public void setIsChanged(Boolean isChanged) {
		this.isChanged = isChanged;
	}

	
	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public String getContractNum() {
		return contractNum;
	}



	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}



	public Integer getContractLineNum() {
		return contractLineNum;
	}



	public void setContractLineNum(Integer contractLineNum) {
		this.contractLineNum = contractLineNum;
	}



	public Integer getContractLineNumOrigin() {
		return contractLineNumOrigin;
	}



	public void setContractLineNumOrigin(Integer contractLineNumOrigin) {
		this.contractLineNumOrigin = contractLineNumOrigin;
	}



	public String getContentSubName() {
		return contentSubName;
	}



	public void setContentSubName(String contentSubName) {
		this.contentSubName = contentSubName;
	}

	public String getLbuAccountId() {
		return lbuAccountId;
	}



	public void setLbuAccountId(String lbuAccountId) {
		this.lbuAccountId = lbuAccountId;
	}


	/**
	 * return combination of contractNum and contractLineNum
	 * @return
	 */
	public String getLbuContentSubId() {
		return this.contractNum + "L" + StringHelper.getLastnCharacters("00" + this.contractLineNum.toString(), 3) + "S00";
	}



	public void setLbuContentSubId(String lbuContentSubId) {
		this.lbuContentSubId = lbuContentSubId;
	}


	public String getBeginDate() {
		return beginDate;
	}



	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}



	public String getEndDate() {
		return endDate;
	}



	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}



	public String getAuditUserId() {
		return auditUserId;
	}



	public void setAuditUserId(String auditUserId) {
		this.auditUserId = auditUserId;
	}


	public Integer getMaxUsers() {
		return maxUsers;
	}



	public void setMaxUsers(Integer maxUsers) {
		this.maxUsers = maxUsers;
	}


	public Float getMonthlyCommitment() {
		return monthlyCommitment;
	}



	public void setMonthlyCommitment(Float monthlyCommitment) {
		this.monthlyCommitment = monthlyCommitment;
	}



	public Float getMonthlyCap() {
		return monthlyCap;
	}



	public void setMonthlyCap(Float monthlyCap) {
		this.monthlyCap = monthlyCap;
	}



	public String getLbuSourcePackageId() {
		return lbuSourcePackageId;
	}



	public void setLbuSourcePackageId(String lbuSourcePackageId) {
		this.lbuSourcePackageId = lbuSourcePackageId;
	}



	public String getUpdatedDatetime() {
		return updatedDatetime;
	}



	public void setUpdatedDatetime(String updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}



	public String getCreatedDatetime() {
		return createdDatetime;
	}



	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}



	public String getLbuSetId() {
		return lbuSetId;
	}



	public void setLbuSetId(String lbuSetId) {
		this.lbuSetId = lbuSetId;
	}



	public String getLbuLocationId() {
		return lbuLocationId;
	}



	public void setLbuLocationId(String lbuLocationId) {
		this.lbuLocationId = lbuLocationId;
	}

	


	public Integer getAddressSeq() {
		return addressSeq;
	}



	public void setAddressSeq(Integer addressSeq) {
		this.addressSeq = addressSeq;
	}
	
	public String getAgreementPguid()
	{
		return agreementPguid;
	}
	
	public void setAgreementPguid(String agreementPguid)
	{
		this.agreementPguid = agreementPguid;
	}

	
    /**
     * return combination of contractNum and contractLineNumOrigin
     * something like 001, 002, 011, 105
     * @return
     */
	public String getAssetAgreementId() {
		String line = "L" + StringHelper.getLastnCharacters("00" + this.contractLineNumOrigin.toString(), 3);
		
		return this.contractNum + line;
	}
	
	/**
	 * AssetAgreementIdFromDb from database
	 * @return
	 */
	public String getAssetAgreementIdFromDb()
	{
		return assetAgreementId;
	}

	public void setAssetAgreementId(String assetAgreementId) {
		this.assetAgreementId = assetAgreementId;
	}



	public String getNoReNew() {
		return noReNew;
	}



	public void setNoReNew(String noReNew) {
		this.noReNew = noReNew;
	}
	
	/**
	 * this method returns the combination identifier for the contentSub
	 * The primary key composed by 3 fields
	 * @return
	 */
	public String getCombinationIdentifier(boolean space) {
		if(space) {
			return this.getContractNum() + " " + this.getContractLineNum() + " " + this.getLbuSourcePackageId();
		} else {
			return this.getContractNum() + this.getContractLineNum() + this.getLbuSourcePackageId();
		}
		
	}

	/**
	 * test contentSub
	 * @param args
	 */
//	public static void main(String[] args)
//	{
//		String line = "L" + StringHelper.getLastnCharacters("00" + "17", 3);
//		System.out.println(line);
//	}


	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
