package com.wam.model.db.gbsd.helper;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LbuLocation;

import javax.persistence.*;

public class LbuLocationAdmins implements Serializable{

	private ArrayList<LbuLocationAdmin> list = new ArrayList<LbuLocationAdmin>();
	
	public ArrayList<LbuLocationAdmin> getList() {
		return list;
	}



	public void setList(ArrayList<LbuLocationAdmin> list) {
		this.list = list;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
