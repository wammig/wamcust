package com.wam.model.db.gbsd.helper;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * LbuAgreementGroup contains the agreementId with a list of assetAgreement linked to it.
 * @author QINC1
 *
 */
public class LbuAgreementGroup extends LbuAgreements{
	private String agreementId;
	
	
	
	public String getAgreementId() {
		return agreementId;
	}



	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
