package com.wam.model.db.gbsd.helper;

import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AgreementsWithContentSubs {

	private ArrayList<AgreementWithContentSubs> list;
	
	
	
	public ArrayList<AgreementWithContentSubs> getList() {
		return list;
	}



	public void setList(ArrayList<AgreementWithContentSubs> list) {
		this.list = list;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
