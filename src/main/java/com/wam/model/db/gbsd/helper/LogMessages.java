package com.wam.model.db.gbsd.helper;

import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.db.gbsd.entity.LogMessage;

public class LogMessages {

	private ArrayList<LogMessage> list = new ArrayList<LogMessage>();
	
	
	
	public ArrayList<LogMessage> getList() {
		return list;
	}



	public void setList(ArrayList<LogMessage> list) {
		this.list = list;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
