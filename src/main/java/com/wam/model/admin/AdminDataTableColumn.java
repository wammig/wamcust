package com.wam.model.admin;

public class AdminDataTableColumn {

	public String data;
	
	public String name;
	
	public String searchable;
	
	public String orderable;
	
	public AdminDataTableSearch search;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSearchable() {
		return searchable;
	}

	public void setSearchable(String searchable) {
		this.searchable = searchable;
	}

	public String getOrderable() {
		return orderable;
	}

	public void setOrderable(String orderable) {
		this.orderable = orderable;
	}

	public AdminDataTableSearch getSearch() {
		return search;
	}

	public void setSearch(AdminDataTableSearch search) {
		this.search = search;
	}
	
	
	
}
