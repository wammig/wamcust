package com.wam.model.admin;

public class AdminDataTableSearch {

	public String value ;
	
	public String regex ;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}
	
	
	
}
