package com.wam.model.statistic;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.wam.utility.definition.Definition;

public class RequestStatistic {
	public static int requestTimes = 0;
	public static int requestTimesAccount = 0;
	public static int requestTimesLocation = 0;
	public static int requestTimesAgreement = 0;
	
	public static String firstTime;
	public static String lastTime;
	
	public static synchronized void add()
	{
		++requestTimes;
		changeTime();
	}
	
	public static synchronized void addWithType(String type)
	{
		if(type == Definition.ACCOUNT)
		{
			++requestTimesAccount;
		}
		
		if(type == Definition.LOCATION)
		{
			 ++requestTimesLocation;
		}
		
		if(type == Definition.AGREEMENT)
		{
			 ++requestTimesAgreement;
		}
		
		add();
	}
	
	public static synchronized void changeTime()
	{
		SimpleDateFormat formatter = new SimpleDateFormat(Definition.DATEFORMAT_MMDDYYYYHHMMSS);
		if(firstTime == null)
		{
			Date dateTimeBegin = new Date();
			firstTime = formatter.format(dateTimeBegin).toString();
		}
		
		Date dateTimeEnd = new Date();
		lastTime = formatter.format(dateTimeEnd).toString();
	}
	
	
	public static synchronized void clearAll()
	{
		requestTimes = 0;
		requestTimesAccount = 0;
		requestTimesLocation = 0;
		requestTimesAgreement = 0;
		
		firstTime = null;
		lastTime = null;
	}
	
	public static String display()
	{
		String message = "Request times: " + requestTimes + " First: " + firstTime + " last: " + lastTime; 
		//System.out.println(message);
		
		return message;
	}
}
