package com.wam.model.mvc.response.helper;

public class WsMessage {

	private String url;
	private String method;
	private String messageOut;
	private String messageIn;
	private String code;
	private String id;
	
	public WsMessage(String url, String method, String messageOut, String messageIn, String code, String id)
	{
		this.url = url;
		this.method = method;
		this.messageOut = messageOut;
		this.messageIn = messageIn;
		this.code = code;
		this.id = id;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMessageOut() {
		return messageOut;
	}
	public void setMessageOut(String messageOut) {
		this.messageOut = messageOut;
	}
	public String getMessageIn() {
		return messageIn;
	}
	public void setMessageIn(String messageIn) {
		this.messageIn = messageIn;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
}
