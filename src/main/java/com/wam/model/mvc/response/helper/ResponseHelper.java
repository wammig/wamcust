package com.wam.model.mvc.response.helper;

import java.util.ArrayList;
import java.util.List;

public class ResponseHelper {

	public static List<ResponseMessage> messageList = new ArrayList<ResponseMessage>();

	public static Integer listPosition = 0;
	
	public static List<ResponseMessage> getMessageList() {
		return ResponseHelper.messageList;
	}

	public void setMessageList(List<ResponseMessage> messageList) {
		ResponseHelper.messageList = messageList;
	}
	
	public static void cleanMessageList()
	{
		ResponseHelper.messageList = new ArrayList<ResponseMessage>();
		ResponseHelper.listPosition = 0;
	}
	
}
