package com.wam.model.mvc.response.helper;

import java.util.ArrayList;
import java.util.List;

public class WsMessageHelper {

	public static List<WsMessage> messageList = new ArrayList<WsMessage>();
	
	public static Integer listPosition = 0;
	
	public static List<WsMessage> getMessageList() {
		return WsMessageHelper.messageList;
	}

	public void setMessageList(List<WsMessage> messageList) {
		WsMessageHelper.messageList = messageList;
	}
	
	public static void cleanMessageList()
	{
		WsMessageHelper.messageList = new ArrayList<WsMessage>();
		WsMessageHelper.listPosition = 0;
	}
}
