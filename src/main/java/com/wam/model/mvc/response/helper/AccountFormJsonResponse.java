package com.wam.model.mvc.response.helper;

import java.util.Map;

import com.wam.model.db.gbsd.entity.LbuAccount;

public class AccountFormJsonResponse {

	private LbuAccount accountForm;
	private boolean validated;
	private Map<String, String> errorMessages;
	
	public LbuAccount getAccountForm() {
		return accountForm;
	}
	public void setAccountForm(LbuAccount accountForm) {
		this.accountForm = accountForm;
	}
	public boolean isValidated() {
		return validated;
	}
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	public Map<String, String> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	
}
