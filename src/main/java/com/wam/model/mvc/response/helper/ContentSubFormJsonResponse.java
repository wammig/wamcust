package com.wam.model.mvc.response.helper;

import java.util.Map;

import com.wam.model.db.gbsd.entity.LbuContentSub;

public class ContentSubFormJsonResponse {

	private LbuContentSub lbuContentSubForm;
	private boolean validated;
	private Map<String, String> errorMessages;
	private boolean isAgreementChanged;
	
	public LbuContentSub getContentSubForm() {
		return lbuContentSubForm;
	}
	public void setContentSubForm(LbuContentSub lbuContentSubForm) {
		this.lbuContentSubForm = lbuContentSubForm;
	}
	public boolean isValidated() {
		return validated;
	}
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	public Map<String, String> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public LbuContentSub getLbuContentSubForm() {
		return lbuContentSubForm;
	}
	public void setLbuContentSubForm(LbuContentSub lbuContentSubForm) {
		this.lbuContentSubForm = lbuContentSubForm;
	}
	public boolean isAgreementChanged() {
		return isAgreementChanged;
	}
	public void setAgreementChanged(boolean isAgreementChanged) {
		this.isAgreementChanged = isAgreementChanged;
	}
	
}
