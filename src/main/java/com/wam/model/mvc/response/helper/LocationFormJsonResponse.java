package com.wam.model.mvc.response.helper;

import java.util.Map;

import com.wam.model.db.gbsd.entity.LbuLocation;

public class LocationFormJsonResponse {

	private LbuLocation locationForm;
	private boolean validated;
	private Map<String, String> errorMessages;
	
	public LbuLocation getLocationForm() {
		return locationForm;
	}
	public void setLocationForm(LbuLocation locationForm) {
		this.locationForm = locationForm;
	}
	public boolean isValidated() {
		return validated;
	}
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	public Map<String, String> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	
}
