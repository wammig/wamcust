package com.wam.model.mvc.response.helper;

import java.util.Map;

import com.wam.model.db.gbsd.entity.LogMessage;

public class MessageFormJsonResponse {

	private LogMessage messageForm;
	private boolean validated;
	private Map<String, String> errorMessages;
	
	public LogMessage getMessageForm() {
		return messageForm;
	}
	public void setMessageForm(LogMessage messageForm) {
		this.messageForm = messageForm;
	}
	public boolean isValidated() {
		return validated;
	}
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	public Map<String, String> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	
}
