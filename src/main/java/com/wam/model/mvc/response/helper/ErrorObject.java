package com.wam.model.mvc.response.helper;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.http.HttpStatus;

/**
 * ErrorObject for constructing a message object before throwing message
 * @author QINC1
 *
 */
public class ErrorObject {

	private String resultStatus;
	private HttpStatus httpStatus;
	private Integer code;
	private String message;
	
	
	
	public String getResultStatus() {
		return resultStatus;
	}



	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	


	public HttpStatus getHttpStatus() {
		return httpStatus;
	}



	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}



	public Integer getCode() {
		return code;
	}



	public void setCode(Integer code) {
		this.code = code;
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}



	@Override
	public String toString()
	{
		return "status: " + resultStatus + " code: " + httpStatus.toString() + " message: " + message;
	}
}
