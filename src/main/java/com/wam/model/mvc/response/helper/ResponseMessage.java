package com.wam.model.mvc.response.helper;

public class ResponseMessage {
	
	public final static String SUCCESS = "SUCCESS";
	public final static String FAILED = "FAILED";
	public final static String WAIT = "WAIT";
	
	private String message;
	private String status;
	
	public ResponseMessage(String message, String status)
	{
		this.message = message;
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
