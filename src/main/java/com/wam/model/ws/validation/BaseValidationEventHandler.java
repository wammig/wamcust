package com.wam.model.ws.validation;


import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.util.ValidationEventCollector;
/**
 * BaseValidationEventHandler
 * JAXB
 * @author QINC1
 *
 */
public class BaseValidationEventHandler extends ValidationEventCollector implements ValidationEventHandler {
	
	@Override
	public boolean handleEvent(ValidationEvent event) 
	{
		super.handleEvent(event);

		//System.out.println(event.getMessage());
		

			try {
				throw new Exception(event.getMessage());
			} catch (Exception e) {

			}

		/*
		 	System.out.println("EVENT");
	        System.out.println("	SEVERITY:  " + event.getSeverity());
	        System.out.println("	MESSAGE:  " + event.getMessage());
	        System.out.println("	LINKED EXCEPTION:  " + event.getLinkedException());
	        System.out.println("	LOCATOR: ");
	        System.out.println("    LINE NUMBER:  " + event.getLocator().getLineNumber());
	        System.out.println("    COLUMN NUMBER:  " + event.getLocator().getColumnNumber());
	        System.out.println("    OFFSET:  " + event.getLocator().getOffset());
	        System.out.println("    OBJECT:  " + event.getLocator().getObject());
	        System.out.println("    NODE:  " + event.getLocator().getNode());
	        System.out.println("    URL:  " + event.getLocator().getURL());
	     */
	    return true;
	}
	
}
