package com.wam.model.ws.in.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "PlaceOfBusiness")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class PlaceOfBusinessWrapper {
	
	
	public String addressPGUID;
	
	
	public String orgCustomerPGUID;
	
	
	public String pOBPGUID;
	
	
	@XmlElement(name = "AddressPGUID", namespace="http://services.lexisnexis.com/xmlschema/common/1")
	public String getAddressPGUID() {
		return addressPGUID;
	}



	public void setAddressPGUID(String addressPGUID) {
		this.addressPGUID = addressPGUID;
	}


	@XmlElement(name = "OrgCustomerPGUID", namespace="http://services.lexisnexis.com/xmlschema/common/1")
	public String getOrgCustomerPGUID() {
		return orgCustomerPGUID;
	}



	public void setOrgCustomerPGUID(String orgCustomerPGUID) {
		this.orgCustomerPGUID = orgCustomerPGUID;
	}


	@XmlElement(name = "POBPGUID", namespace="http://services.lexisnexis.com/xmlschema/common/1")
	public String getpOBPGUID() {
		return pOBPGUID;
	}



	public void setpOBPGUID(String pOBPGUID) {
		this.pOBPGUID = pOBPGUID;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
