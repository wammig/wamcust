package com.wam.model.ws.in.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "DataArea")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class DataAreaWrapper {

	
	public OrgCustomerBOWrapper orgCustomerBO;
	
	
	@XmlElement(name = "OrgCustomerBO")
	public OrgCustomerBOWrapper getOrgCustomerBO() {
		return orgCustomerBO;
	}



	public void setOrgCustomerBO(OrgCustomerBOWrapper orgCustomerBO) {
		this.orgCustomerBO = orgCustomerBO;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
