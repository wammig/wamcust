package com.wam.model.ws.in.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "OrgCustomer", namespace="http://services.lexisnexis.com/xmlschema/orgcustomer/1")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class OrgCustomerWrapper {


	public DataAreaWrapper dataArea;
	
	@XmlElement(name = "DataArea")
	public DataAreaWrapper getDataArea() {
		return dataArea;
	}

	public void setDataArea(DataAreaWrapper dataArea) {
		this.dataArea = dataArea;
	}	

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
