package com.wam.model.ws.in.creation.customer;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


import com.wam.model.ws.in.creation.customer.PlaceOfBusinessWrapper;

@XmlRootElement(name = "ListOfPlaceOfBusiness")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class ListOfPlaceOfBusinessWrapper {
	
	
	ArrayList<PlaceOfBusinessWrapper> list;
	
	@XmlElement(name = "PlaceOfBusiness")
	public ArrayList<PlaceOfBusinessWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<PlaceOfBusinessWrapper> list)
    {
        this.list = list;
    }
}
