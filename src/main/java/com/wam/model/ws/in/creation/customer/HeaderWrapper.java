package com.wam.model.ws.in.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlAccessorType (XmlAccessType.PROPERTY)
@XmlType(propOrder={"statusCode", "statusMessage"})
public class HeaderWrapper {

	
	public String statusCode;
	
	
	public String statusMessage;
	
	
	@XmlElement(name = "ns0:StatusCode")
	public String getStatusCode() {
		return statusCode;
	}


	
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}


	@XmlElement(name = "ns0:StatusMessage")
	public String getStatusMessage() {
		return statusMessage;
	}



	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
