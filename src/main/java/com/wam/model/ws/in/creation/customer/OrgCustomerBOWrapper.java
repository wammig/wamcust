package com.wam.model.ws.in.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "OrgCustomerBO")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class OrgCustomerBOWrapper {

	
	public String orgCustomerPGUID;
	
	
	public ListOfPlaceOfBusinessWrapper listOfPlaceOfBusiness;
	
	
	@XmlElement(name = "OrgCustomerPGUID")
	public String getOrgCustomerPGUID() {
		return orgCustomerPGUID;
	}



	public void setOrgCustomerPGUID(String orgCustomerPGUID) {
		this.orgCustomerPGUID = orgCustomerPGUID;
	}


	@XmlElement(name = "ListOfPlaceOfBusiness")
	public ListOfPlaceOfBusinessWrapper getListOfPlaceOfBusiness() {
		return listOfPlaceOfBusiness;
	}



	public void setListOfPlaceOfBusiness(ListOfPlaceOfBusinessWrapper listOfPlaceOfBusiness) {
		this.listOfPlaceOfBusiness = listOfPlaceOfBusiness;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
