package com.wam.model.ws.in.sync.order.reply;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.in.sync.order.reply.HubException;;

//@XmlRootElement(name = "hubExceptionList")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class HubExceptionList {

	ArrayList<HubException> list;

	@XmlElement(name = "hubException", required = false, nillable=true, defaultValue="null")
	public ArrayList<HubException> getList() {
		return list;
	}

	public void setList(ArrayList<HubException> list) {
		this.list = list;
	}
}
