package com.wam.model.ws.in.sync.order.reply;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


@XmlRootElement(name = "SyncOrderReply")
@XmlAccessorType (XmlAccessType.PROPERTY)
public class SyncOrderReply {

	public String messageType;
	public String lbuId;
	public String lbuAccountId;
	public String lbuContentSubId;
	public String agreementPguid;
	public String messageId;
	public String actionType;
	public String messageStatus;
	public String messageException;
	public String messageCreatedTimestamp;
	public HubExceptionList hubExceptionList;
	
	@XmlElement(name = "messageType")
	public String getMessageType() {
		return messageType;
	}



	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}


	@XmlElement(name = "lbuId")
	public String getLbuId() {
		return lbuId;
	}



	public void setLbuId(String lbuId) {
		this.lbuId = lbuId;
	}


	@XmlElement(name = "lbuAccountId")
	public String getLbuAccountId() {
		return lbuAccountId;
	}



	public void setLbuAccountId(String lbuAccountId) {
		this.lbuAccountId = lbuAccountId;
	}


	@XmlElement(name = "lbuContentSubId")
	public String getLbuContentSubId() {
		return lbuContentSubId;
	}



	public void setLbuContentSubId(String lbuContentSubId) {
		this.lbuContentSubId = lbuContentSubId;
	}


	@XmlElement(name = "agreementPguid")
	public String getAgreementPguid() {
		return agreementPguid;
	}



	public void setAgreementPguid(String agreementPguid) {
		this.agreementPguid = agreementPguid;
	}
	
	
	@XmlElement(name = "messageId")
	public String getMessageId() {
		return messageId;
	}



	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}



	@XmlElement(name = "actionType")
	public String getActionType() {
		return actionType;
	}



	public void setActionType(String actionType) {
		this.actionType = actionType;
	}


	@XmlElement(name = "messageStatus")
	public String getMessageStatus() {
		return messageStatus;
	}



	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}


	@XmlElement(name = "messageException")
	public String getMessageException() {
		return messageException;
	}



	public void setMessageException(String messageException) {
		this.messageException = messageException;
	}


	@XmlElement(name = "messageCreatedTimestamp")
	public String getMessageCreatedTimestamp() {
		return messageCreatedTimestamp;
	}



	public void setMessageCreatedTimestamp(String messageCreatedTimestamp) {
		this.messageCreatedTimestamp = messageCreatedTimestamp;
	}


	@XmlElement(name = "hubExceptionList", required = false, nillable=true, defaultValue="null")
	public HubExceptionList getHubExceptionList() {
		return hubExceptionList;
	}



	public void setHubExceptionList(HubExceptionList hubExceptionList) {
		this.hubExceptionList = hubExceptionList;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
