package com.wam.model.ws.in.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "DataArea")
public class DataArea {

	@JacksonXmlElementWrapper(localName = "ListOfPlaceOfBusinessBO", useWrapping = false)
	public ListOfPlaceOfBusinessBO ListOfPlaceOfBusinessBO;
	
	
	
	public ListOfPlaceOfBusinessBO getListOfPlaceOfBusinessBO() {
		return ListOfPlaceOfBusinessBO;
	}



	public void setListOfPlaceOfBusinessBO(ListOfPlaceOfBusinessBO listOfPlaceOfBusinessBO) {
		ListOfPlaceOfBusinessBO = listOfPlaceOfBusinessBO;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
