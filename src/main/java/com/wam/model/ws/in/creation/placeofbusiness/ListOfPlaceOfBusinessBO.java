package com.wam.model.ws.in.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.wam.model.ws.in.creation.placeofbusiness.PlaceOfBusiness;


@JacksonXmlRootElement(localName = "ListOfPlaceOfBusinessBO")
public class ListOfPlaceOfBusinessBO {
	
	@JacksonXmlElementWrapper(localName = "PlaceOfBusiness", useWrapping = false)
	public PlaceOfBusiness PlaceOfBusiness;
	
	
	public PlaceOfBusiness getPlaceOfBusiness() 
	{
		return this.PlaceOfBusiness;
	}
	
	public void setPlaceOfBusiness(PlaceOfBusiness PlaceOfBusiness)
	{
		this.PlaceOfBusiness = PlaceOfBusiness;
	}
	
	/*
	@JacksonXmlElementWrapper(localName = "PlaceOfBusiness", useWrapping = false)
	public PlaceOfBusiness[] PlaceOfBusiness;
	
	
	public ListOfPlaceOfBusinessBO()
	{
		
	}
	
	public ListOfPlaceOfBusinessBO(PlaceOfBusiness[] PlaceOfBusiness)
	{
		this.PlaceOfBusiness = PlaceOfBusiness;
	}
	
	
	
	public PlaceOfBusiness[] getPlaceOfBusiness() 
	{
		return this.PlaceOfBusiness;
	}
	
	public void setPlaceOfBusiness(PlaceOfBusiness[] PlaceOfBusiness)
	{
		this.PlaceOfBusiness = PlaceOfBusiness;
	}
	
	@Override 
	public String toString()
	{
		return Arrays.toString(this.PlaceOfBusiness);
	}
	*/
	/*
	ArrayList<PlaceOfBusinessWrapper> list;
	
	@XmlElement(name = "PlaceOfBusiness")
	public ArrayList<PlaceOfBusinessWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<PlaceOfBusinessWrapper> list)
    {
        this.list = list;
    }
    */
}
