package com.wam.model.ws.in.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "PlaceOfBusiness")
public class PlaceOfBusiness {
	
	@JacksonXmlProperty(localName = "AddressPGUID")
	public String AddressPGUID;
	
	@JacksonXmlProperty(localName = "POBPGUID")
	public String POBPGUID;


	public PlaceOfBusiness()
	{
		
	}
	
	public PlaceOfBusiness(String AddressPGUID,String POBPGUID)
	{
		this.AddressPGUID = AddressPGUID;
		this.POBPGUID = POBPGUID;
	}
	
	public String getAddressPGUID() {
		return AddressPGUID;
	}



	public void setAddressPGUID(String addressPGUID) {
		this.AddressPGUID = addressPGUID;
	}


	public String getpOBPGUID() {
		return POBPGUID;
	}



	public void setpOBPGUID(String pOBPGUID) {
		this.POBPGUID = pOBPGUID;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
