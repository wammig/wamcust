/**
 * this is a very impotant configuration for JaxB when translate xml to java object.
 * If you have some namespaces in the header
 */

@XmlSchema( 
    namespace = "http://services.lexisnexis.com/xmlschema/orgcustomer/1",
    elementFormDefault = XmlNsForm.QUALIFIED,
    xmlns = { @XmlNs(namespaceURI = "http://services.lexisnexis.com/xmlschema/common/1", prefix = "ns2"),
    			@XmlNs(namespaceURI = "http://services.lexisnexis.com/xmlschema/commontracking/1", prefix = "ns1"),
    			@XmlNs(namespaceURI = "http://services.lexisnexis.com/xmlschema/header/1", prefix = "ns0")}) 
package com.wam.model.ws.in.creation.customer;
 
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNs;
