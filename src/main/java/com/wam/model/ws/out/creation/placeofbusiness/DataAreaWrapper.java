package com.wam.model.ws.out.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.ListOfPlaceOfBusinessWrapperBO;


@XmlRootElement(name = "ns6:DataArea")
@XmlAccessorType (XmlAccessType.FIELD)
public class DataAreaWrapper {
	
	@XmlElement(name = "ns7:ListOfPlaceOfBusinessBO")
	public ListOfPlaceOfBusinessWrapperBO listOfPlaceOfBusinessBO;
	
	
	public ListOfPlaceOfBusinessWrapperBO getListOfPlaceOfBusinessBO() {
		return listOfPlaceOfBusinessBO;
	}



	public void setListOfPlaceOfBusinessBO(ListOfPlaceOfBusinessWrapperBO listOfPlaceOfBusinessBO) {
		this.listOfPlaceOfBusinessBO = listOfPlaceOfBusinessBO;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
