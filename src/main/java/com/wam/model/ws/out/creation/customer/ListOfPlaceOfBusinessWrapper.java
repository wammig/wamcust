package com.wam.model.ws.out.creation.customer;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Example:
 * 
 * 	<ns6:ListOfPlaceOfBusiness>
        <ns6:PlaceOfBusiness>
                <ns13:AddressPGUID>urn:ecm:I1424ZN4JZQ</ns13:AddressPGUID>
                <ns13:Address1>7718 BLUE WATER LN</ns13:Address1>
                <ns13:Address2/>
                <ns13:Address3/>
                <ns13:City>CASTLE ROCK</ns13:City>
                <ns13:CountrySubdivisionDescription>CO</ns13:CountrySubdivisionDescription>
                <ns13:Country>United States</ns13:Country>
                <ns13:CountryPGUID>USA</ns13:CountryPGUID>
                <ns13:CountrySubdivisionPGUID>urn:ecm:00000098D</ns13:CountrySubdivisionPGUID>
                <ns13:CountrySubdivisionCode>CO</ns13:CountrySubdivisionCode>
                <ns13:County>DOUGLAS</ns13:County>
                <ns13:TaxGeocode>2026</ns13:TaxGeocode>
                <ns13:PostalCode>80108</ns13:PostalCode>
                <ns13:ValidAddressFlag>Y</ns13:ValidAddressFlag>
                <ns13:GeographicLocationFlag>Y</ns13:GeographicLocationFlag>
                <ns13:POBPGUID/>
                <ns13:PrimaryAddressIndicator>Y</ns13:PrimaryAddressIndicator>
                <ns13:ListOfCommunication>
                    <ns13:Communication>
                        <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                        <ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                        <ns13:CommValue>+1 (158) 745-2136</ns13:CommValue>
                    </ns13:Communication>
                </ns13:ListOfCommunication>
                <ns13:ListOfTax>
                                <ns13:Tax/>
                </ns13:ListOfTax>
                <ns13:ListOfPOBCredentialingCountry/>
        </ns6:PlaceOfBusiness>
	</ns6:ListOfPlaceOfBusiness>
 * 
 * @author QINC1
 *
 */
@XmlRootElement(name = "ns6:ListOfPlaceOfBusiness")
@XmlAccessorType (XmlAccessType.FIELD)
public class ListOfPlaceOfBusinessWrapper {
	
	@XmlElement(name = "ns6:PlaceOfBusiness", type = PlaceOfBusinessWrapper.class)
	ArrayList<PlaceOfBusinessWrapper> list;

	public ArrayList<PlaceOfBusinessWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<PlaceOfBusinessWrapper> list)
    {
        this.list = list;
    }
	
}
