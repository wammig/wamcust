package com.wam.model.ws.out.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


@XmlRootElement(name = "ns11:Header")
@XmlAccessorType (XmlAccessType.FIELD)
public class HeaderWrapper {

	@XmlElement(name = "ns12:commonTrackingMessage")
	public CommonTrackingMessageWrapper commonTrackingMessage;
	
	@XmlElement(name = "ns11:ServiceConsumerAssetId")
	public String serviceConsumerAssetId;
	
	@XmlElement(name = "ns11:ModifiedBy")
	public String modifiedBy;
	
	@XmlElement(name = "ns11:SourceAppName")
	public String sourceAppName;
	
	
	
	
	public String getServiceConsumerAssetId() {
		return serviceConsumerAssetId;
	}



	public void setServiceConsumerAssetId(String serviceConsumerAssetId) {
		this.serviceConsumerAssetId = serviceConsumerAssetId;
	}



	public String getModifiedBy() {
		return modifiedBy;
	}



	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}



	public String getSourceAppName() {
		return sourceAppName;
	}



	public void setSourceAppName(String sourceAppName) {
		this.sourceAppName = sourceAppName;
	}



	public CommonTrackingMessageWrapper getCommonTrackingMessage() {
		return commonTrackingMessage;
	}



	public void setCommonTrackingMessage(CommonTrackingMessageWrapper commonTrackingMessage) {
		this.commonTrackingMessage = commonTrackingMessage;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
