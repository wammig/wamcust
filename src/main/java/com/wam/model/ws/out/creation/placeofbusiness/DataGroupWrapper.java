package com.wam.model.ws.out.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "ns12:dataGroup")
@XmlAccessorType (XmlAccessType.FIELD)
public class DataGroupWrapper {
	
	@XmlElement(name = "ns12:userPermId")
	public String userPermId;
	
		
	public String getUserPermId() {
		return userPermId;
	}


	public void setUserPermId(String userPermId) {
		this.userPermId = userPermId;
	}


	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
