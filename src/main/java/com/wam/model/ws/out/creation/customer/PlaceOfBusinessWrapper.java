package com.wam.model.ws.out.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Example:
 * 
 * 
 * 		<ns6:PlaceOfBusiness>
                <ns13:AddressPGUID>urn:ecm:I1424ZN4JZQ</ns13:AddressPGUID>
                <ns13:Address1>7718 BLUE WATER LN</ns13:Address1>
                <ns13:Address2/>
                <ns13:Address3/>
                <ns13:City>CASTLE ROCK</ns13:City>
                <ns13:CountrySubdivisionDescription>CO</ns13:CountrySubdivisionDescription>
                <ns13:Country>United States</ns13:Country>
                <ns13:CountryPGUID>USA</ns13:CountryPGUID>
                <ns13:CountrySubdivisionPGUID>urn:ecm:00000098D</ns13:CountrySubdivisionPGUID>
                <ns13:CountrySubdivisionCode>CO</ns13:CountrySubdivisionCode>
                <ns13:County>DOUGLAS</ns13:County>
                <ns13:TaxGeocode>2026</ns13:TaxGeocode>
                <ns13:PostalCode>80108</ns13:PostalCode>
                <ns13:ValidAddressFlag>Y</ns13:ValidAddressFlag>
                <ns13:GeographicLocationFlag>Y</ns13:GeographicLocationFlag>
                <ns13:POBPGUID/>
                <ns13:PrimaryAddressIndicator>Y</ns13:PrimaryAddressIndicator>
                <ns13:ListOfCommunication>
                    <ns13:Communication>
                        <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                        <ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                        <ns13:CommValue>+1 (158) 745-2136</ns13:CommValue>
                    </ns13:Communication>
                </ns13:ListOfCommunication>
                <ns13:ListOfTax>
                                <ns13:Tax/>
                </ns13:ListOfTax>
                <ns13:ListOfPOBCredentialingCountry/>
		</ns6:PlaceOfBusiness>
 * 
 * @todo add ListOfTax and ListOfPOBCredentialingCountry
 * 
 *  <ns13:ListOfTax>
        <ns13:Tax/>
    </ns13:ListOfTax>
    <ns13:ListOfPOBCredentialingCountry/>
 * 
 * @author QINC1
 *
 */

@XmlRootElement(name = "ns6:PlaceOfBusiness")
@XmlAccessorType (XmlAccessType.FIELD)
public class PlaceOfBusinessWrapper {

	@XmlElement(name = "ns13:AddressPGUID")
	public String addressPGUID;
	
	@XmlElement(name = "ns13:Address1")
	public String address1;
	
	@XmlElement(name = "ns13:Address2" , nillable = true)
	public String address2;
	
	@XmlElement(name = "ns13:Address3" , nillable = true)
	public String address3;
	
	@XmlElement(name = "ns13:City")
	public String city;
	
	@XmlElement(name = "ns13:CountrySubdivisionDescription")
	public String countrySubdivisionDescription;
	
	@XmlElement(name = "ns13:Country")
	public String country;
	
	@XmlElement(name = "ns13:CountryPGUID")
	public String countryPGUID;
	
	@XmlElement(name = "ns13:CountrySubdivisionPGUID")
	public String countrySubdivisionPGUID;
	
	@XmlElement(name = "ns13:CountrySubdivisionCode")
	public String countrySubdivisionCode;
	
	@XmlElement(name = "ns13:County")
	public String county;
	
	@XmlElement(name = "ns13:TaxGeocode")
	public String taxGeocode;
	
	@XmlElement(name = "ns13:PostalCode")
	public String postalCode;
	
	@XmlElement(name = "ns13:ValidAddressFlag")
	public String validAddressFlag;
	
	@XmlElement(name = "ns13:GeographicLocationFlag")
	public String geographicLocationFlag;
	
	@XmlElement(name = "ns13:POBPGUID", nillable = true)
	public String pobPGUID;
	
	@XmlElement(name = "ns13:PrimaryAddressIndicator")
	public String primaryAddressIndicator;
	
	@XmlElement(name = "ns13:ListOfCommunication")
	public ListOfCommunicationWrapper listOfCommunication; 
	
	public String getAddressPGUID() {
		return addressPGUID;
	}

	public void setAddressPGUID(String addressPGUID) {
		this.addressPGUID = addressPGUID;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountrySubdivisionDescription() {
		return countrySubdivisionDescription;
	}

	public void setCountrySubdivisionDescription(String countrySubdivisionDescription) {
		this.countrySubdivisionDescription = countrySubdivisionDescription;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryPGUID() {
		return countryPGUID;
	}

	public void setCountryPGUID(String countryPGUID) {
		this.countryPGUID = countryPGUID;
	}

	public String getCountrySubdivisionPGUID() {
		return countrySubdivisionPGUID;
	}

	public void setCountrySubdivisionPGUID(String countrySubdivisionPGUID) {
		this.countrySubdivisionPGUID = countrySubdivisionPGUID;
	}

	public String getCountrySubdivisionCode() {
		return countrySubdivisionCode;
	}

	public void setCountrySubdivisionCode(String countrySubdivisionCode) {
		this.countrySubdivisionCode = countrySubdivisionCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getTaxGeocode() {
		return taxGeocode;
	}

	public void setTaxGeocode(String taxGeocode) {
		this.taxGeocode = taxGeocode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getValidAddressFlag() {
		return validAddressFlag;
	}

	public void setValidAddressFlag(String validAddressFlag) {
		this.validAddressFlag = validAddressFlag;
	}

	public String getGeographicLocationFlag() {
		return geographicLocationFlag;
	}

	public void setGeographicLocationFlag(String geographicLocationFlag) {
		this.geographicLocationFlag = geographicLocationFlag;
	}

	public String getPobPGUID() {
		return pobPGUID;
	}

	public void setPobPGUID(String pobPGUID) {
		this.pobPGUID = pobPGUID;
	}

	public String getPrimaryAddressIndicator() {
		return primaryAddressIndicator;
	}

	public void setPrimaryAddressIndicator(String primaryAddressIndicator) {
		this.primaryAddressIndicator = primaryAddressIndicator;
	}

	public ListOfCommunicationWrapper getListOfCommunication() {
		return listOfCommunication;
	}

	public void setListOfCommunication(ListOfCommunicationWrapper listOfCommunication) {
		this.listOfCommunication = listOfCommunication;
	}

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
