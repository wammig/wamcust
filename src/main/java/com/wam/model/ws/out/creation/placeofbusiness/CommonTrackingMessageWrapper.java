package com.wam.model.ws.out.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.DataGroupWrapper;

@XmlRootElement(name = "ns12:commonTrackingMessage")
@XmlAccessorType (XmlAccessType.FIELD)
public class CommonTrackingMessageWrapper {

	@XmlElement(name = "ns12:dataGroup")
	public DataGroupWrapper dataGroup;
	
	
	
	public DataGroupWrapper getDataGroup() {
		return dataGroup;
	}



	public void setDataGroup(DataGroupWrapper dataGroup) {
		this.dataGroup = dataGroup;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
