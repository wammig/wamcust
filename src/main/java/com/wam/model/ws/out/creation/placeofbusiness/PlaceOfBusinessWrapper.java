package com.wam.model.ws.out.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.ListOfCommunicationWrapper;

/**
 * Example:
 * 
 * 
 * <ns7:PlaceOfBusiness>
 * 			<ns13:AddressPGUID>urn:ecm:I2424ZTMZQW</ns13:AddressPGUID>
            <ns13:POBPGUID>urn:ecm:I2424ZTMZQX</ns13:POBPGUID>
            <ns13:Address1>14 avenue de France</ns13:Address1>
            <ns13:Address2/>
            <ns13:Address3/>
            <ns13:City>tarn</ns13:City>
            <ns13:Country>France</ns13:Country>
            <ns13:CountryPGUID>FRA</ns13:CountryPGUID>
            <ns13:CountrySubdivisionPGUID/>
            <ns13:TaxGeocode>195</ns13:TaxGeocode>
            <ns13:PostalCode>06190</ns13:PostalCode>
            <ns13:ValidAddressFlag>U</ns13:ValidAddressFlag>
            <ns13:GeographicLocationFlag>Y</ns13:GeographicLocationFlag>
            <ns13:OrgCustomerPGUID>urn:ecm:I2424ZPNYXC</ns13:OrgCustomerPGUID>
            <ns13:PrimaryAddressIndicator>N</ns13:PrimaryAddressIndicator>
            <ns13:CredentialingFlag>N</ns13:CredentialingFlag>
            <ns13:RequestedPRLevel>NONE</ns13:RequestedPRLevel>
            <ns13:ListOfCommunication>
                    <ns13:Communication>
                            <ns13:CommTypePGUID>urn:ecm:0000000J4</ns13:CommTypePGUID>
                            <ns13:CommSubTypePGUID>urn:ecm:0000000JB</ns13:CommSubTypePGUID>
                            <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                            <ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                            <ns13:CommValue>+33 4 93 78 06 79</ns13:CommValue>
                    </ns13:Communication>
            </ns13:ListOfCommunication>
    </ns7:PlaceOfBusiness>
 * 
 * 
 * 
 * @author QINC1
 *
 */

@XmlRootElement(name = "ns7:PlaceOfBusiness")
@XmlAccessorType (XmlAccessType.FIELD)
public class PlaceOfBusinessWrapper {

	@XmlElement(name = "ns13:AddressPGUID")
	public String addressPGUID;
	
	@XmlElement(name = "ns13:POBPGUID")
	public String pobPGUID;
	
	@XmlElement(name = "ns13:Address1")
	public String address1;
	
	@XmlElement(name = "ns13:Address2" , nillable = true)
	public String address2;
	
	@XmlElement(name = "ns13:Address3" , nillable = true)
	public String address3;
	
	@XmlElement(name = "ns13:City")
	public String city;
	
	@XmlElement(name = "ns13:CountrySubdivisionDescription")
	public String countrySubdivisionDescription;
	
	@XmlElement(name = "ns13:Country")
	public String country;
	
	@XmlElement(name = "ns13:CountryPGUID")
	public String countryPGUID;
	
	@XmlElement(name = "ns13:CountrySubdivisionPGUID", nillable = true)
	public String countrySubdivisionPGUID;
	
	@XmlElement(name = "ns13:CountrySubdivisionCode")
	public String countrySubdivisionCode;
	
	@XmlElement(name = "ns13:County")
	public String county;
	
	@XmlElement(name = "ns13:TaxGeocode")
	public String taxGeocode;
	
	@XmlElement(name = "ns13:PostalCode")
	public String postalCode;
	
	@XmlElement(name = "ns13:ValidAddressFlag")
	public String validAddressFlag;
	
	@XmlElement(name = "ns13:GeographicLocationFlag")
	public String geographicLocationFlag;
	
	@XmlElement(name = "ns13:OrgCustomerPGUID")
	public String orgCustomerPGUID;
	
	@XmlElement(name = "ns13:PrimaryAddressIndicator")
	public String primaryAddressIndicator;
	

	@XmlElement(name = "ns13:CredentialingFlag")
	public String credentialingFlag;
	
	@XmlElement(name = "ns13:RequestedPRLevel")
	public String requestedPRLevel;
	
	@XmlElement(name = "ns13:ListOfCommunication")
	public ListOfCommunicationWrapper listOfCommunication; 
	
	
	public String getOrgCustomerPGUID() {
		return orgCustomerPGUID;
	}

	public void setOrgCustomerPGUID(String orgCustomerPGUID) {
		this.orgCustomerPGUID = orgCustomerPGUID;
	}

	public String getCredentialingFlag() {
		return credentialingFlag;
	}

	public void setCredentialingFlag(String credentialingFlag) {
		this.credentialingFlag = credentialingFlag;
	}

	public String getRequestedPRLevel() {
		return requestedPRLevel;
	}

	public void setRequestedPRLevel(String requestedPRLevel) {
		this.requestedPRLevel = requestedPRLevel;
	}

	public String getAddressPGUID() {
		return addressPGUID;
	}

	public void setAddressPGUID(String addressPGUID) {
		this.addressPGUID = addressPGUID;
	}
	
	public String getPobPGUID() {
		return pobPGUID;
	}

	public void setPobPGUID(String pobPGUID) {
		this.pobPGUID = pobPGUID;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountrySubdivisionDescription() {
		return countrySubdivisionDescription;
	}

	public void setCountrySubdivisionDescription(String countrySubdivisionDescription) {
		this.countrySubdivisionDescription = countrySubdivisionDescription;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryPGUID() {
		return countryPGUID;
	}

	public void setCountryPGUID(String countryPGUID) {
		this.countryPGUID = countryPGUID;
	}

	public String getCountrySubdivisionPGUID() {
		return countrySubdivisionPGUID;
	}

	public void setCountrySubdivisionPGUID(String countrySubdivisionPGUID) {
		this.countrySubdivisionPGUID = countrySubdivisionPGUID;
	}

	public String getCountrySubdivisionCode() {
		return countrySubdivisionCode;
	}

	public void setCountrySubdivisionCode(String countrySubdivisionCode) {
		this.countrySubdivisionCode = countrySubdivisionCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getTaxGeocode() {
		return taxGeocode;
	}

	public void setTaxGeocode(String taxGeocode) {
		this.taxGeocode = taxGeocode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getValidAddressFlag() {
		return validAddressFlag;
	}

	public void setValidAddressFlag(String validAddressFlag) {
		this.validAddressFlag = validAddressFlag;
	}

	public String getGeographicLocationFlag() {
		return geographicLocationFlag;
	}

	public void setGeographicLocationFlag(String geographicLocationFlag) {
		this.geographicLocationFlag = geographicLocationFlag;
	}

	public String getPrimaryAddressIndicator() {
		return primaryAddressIndicator;
	}

	public void setPrimaryAddressIndicator(String primaryAddressIndicator) {
		this.primaryAddressIndicator = primaryAddressIndicator;
	}

	public ListOfCommunicationWrapper getListOfCommunication() {
		return listOfCommunication;
	}

	public void setListOfCommunication(ListOfCommunicationWrapper listOfCommunication) {
		this.listOfCommunication = listOfCommunication;
	}

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
