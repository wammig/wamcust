package com.wam.model.ws.out.creation.customer;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * Example:
 * 
 * 	<ns6:ListOfCommunication>
        <ns13:Communication>
            <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
           	<ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
            <ns13:CommValue>+1 (158) 745-2137</ns13:CommValue>
        </ns13:Communication>
	</ns6:ListOfCommunication>
 * 
 * @author QINC1
 *
 */

@XmlRootElement(name = "ns6:ListOfCommunication")
@XmlAccessorType (XmlAccessType.FIELD)
public class ListOfCommunicationWrapper {
	
	@XmlElement(name = "ns13:Communication", type = CommunicationWrapper.class)
	ArrayList<CommunicationWrapper> list;

	public ArrayList<CommunicationWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<CommunicationWrapper> list)
    {
        this.list = list;
    }
	
}
