package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "DataArea")
@XmlAccessorType (XmlAccessType.FIELD)
public class DataAreaWrapper {

	@XmlElement(name = "OrderBO")
	public OrderBOWrapper orderBO;

	public OrderBOWrapper getOrderBO() {
		return orderBO;
	}

	public void setOrderBO(OrderBOWrapper orderBO) {
		this.orderBO = orderBO;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
