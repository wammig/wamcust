package com.wam.model.ws.out.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 
 * Example:
 * 
 * 
 * 	<ns13:Communication>
        <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
       	<ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
        <ns13:CommValue>+1 (158) 745-2137</ns13:CommValue>
	</ns13:Communication>
 * 
 * 
 * @author QINC1
 *
 */


@XmlRootElement(name = "ns13:Communication")
@XmlAccessorType (XmlAccessType.FIELD)
public class CommunicationWrapper {

	@XmlElement(name = "ns13:CommTypeDesc")
	public String commTypeDesc;
	
	@XmlElement(name = "ns13:CommSubTypeDesc")
	public String commSubTypeDesc;
	
	@XmlElement(name = "ns13:CommValue")
	public String commValue;
	
	public String getCommTypeDesc() {
		return commTypeDesc;
	}

	public void setCommTypeDesc(String commTypeDesc) {
		this.commTypeDesc = commTypeDesc;
	}

	public String getCommSubTypeDesc() {
		return commSubTypeDesc;
	}

	public void setCommSubTypeDesc(String commSubTypeDesc) {
		this.commSubTypeDesc = commSubTypeDesc;
	}

	public String getCommValue() {
		return commValue;
	}

	public void setCommValue(String commValue) {
		this.commValue = commValue;
	}

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
