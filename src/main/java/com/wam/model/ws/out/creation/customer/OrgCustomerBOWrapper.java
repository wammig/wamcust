package com.wam.model.ws.out.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Example:
 * 
 * 
 * 
 * <ns6:OrgCustomerBO>
        <ns6:TrustLifeCyclePGUID>urn:ecm:00000169F</ns6:TrustLifeCyclePGUID>
        <ns6:TrustLifeCycleDesc>TargetedProspect</ns6:TrustLifeCycleDesc>
        <ns6:BusinessUnitPGUID>urn:ecm:0000003O8</ns6:BusinessUnitPGUID>
        <ns6:BusinessUnitDescription>USA</ns6:BusinessUnitDescription>
        <ns6:AgencyType>Customer</ns6:AgencyType>
        <ns6:OrgCustomerName>Testing1</ns6:OrgCustomerName>
        <ns6:ListOfOCNameVariant/>
        <ns6:ListOfSegment>
                <ns13:Segment>
                        <ns13:SegmentTypePGUID>urn:ecm:0000009MJ</ns13:SegmentTypePGUID>
                        <ns13:SegmentType>Pricing Segment</ns13:SegmentType>
                        <ns13:SegmentPGUID>urn:ecm:0000009MP</ns13:SegmentPGUID>
                        <ns13:SegmentDescription>Government</ns13:SegmentDescription>
                </ns13:Segment>
        </ns6:ListOfSegment>
        <ns6:ListOfCommunication>
                <ns13:Communication>
                        <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                       	<ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                        <ns13:CommValue>+1 (158) 745-2137</ns13:CommValue>
                </ns13:Communication>
        </ns6:ListOfCommunication>
        <ns6:CustomerClassTypePGUID>urn:ecm:0000000BG</ns6:CustomerClassTypePGUID>
        <ns6:CustomerClassTypeDesc>Government</ns6:CustomerClassTypeDesc>
        <ns6:CustomerSubClassTypePGUID>urn:ecm:00000137U</ns6:CustomerSubClassTypePGUID>
        <ns6:CustomerSubClassTypeDesc>Federal Gov US</ns6:CustomerSubClassTypeDesc>
        <ns6:SecondarySubClassTypePGUID>urn:ecm:00000138V</ns6:SecondarySubClassTypePGUID>
        <ns6:SecondarySubClassTypeDesc>Federal Gov US</ns6:SecondarySubClassTypeDesc>
        <ns6:IndustryClassTypePGUID>urn:ecm:00000139S</ns6:IndustryClassTypePGUID>
        <ns6:IndustryClassTypeDesc>Courts</ns6:IndustryClassTypeDesc>
        <ns6:IndustrySubClassTypePGUID>urn:ecm:00000144W</ns6:IndustrySubClassTypePGUID>
        <ns6:IndustrySubClassTypeDesc>US Supreme Court</ns6:IndustrySubClassTypeDesc>
        <ns6:NoOfAttorneys>7</ns6:NoOfAttorneys>
        <ns6:ListOfPlaceOfBusiness>
            <ns6:PlaceOfBusiness>
                    <ns13:AddressPGUID>urn:ecm:I1424ZN4JZQ</ns13:AddressPGUID>
                    <ns13:Address1>7718 BLUE WATER LN</ns13:Address1>
                    <ns13:Address2/>
                    <ns13:Address3/>
                    <ns13:City>CASTLE ROCK</ns13:City>
                    <ns13:CountrySubdivisionDescription>CO</ns13:CountrySubdivisionDescription>
                    <ns13:Country>United States</ns13:Country>
                    <ns13:CountryPGUID>USA</ns13:CountryPGUID>
                    <ns13:CountrySubdivisionPGUID>urn:ecm:00000098D</ns13:CountrySubdivisionPGUID>
                    <ns13:CountrySubdivisionCode>CO</ns13:CountrySubdivisionCode>
                    <ns13:County>DOUGLAS</ns13:County>
                    <ns13:TaxGeocode>2026</ns13:TaxGeocode>
                    <ns13:PostalCode>80108</ns13:PostalCode>
                    <ns13:ValidAddressFlag>Y</ns13:ValidAddressFlag>
                    <ns13:GeographicLocationFlag>Y</ns13:GeographicLocationFlag>
                    <ns13:POBPGUID xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"/>
                    <ns13:PrimaryAddressIndicator>Y</ns13:PrimaryAddressIndicator>
                    <ns13:ListOfCommunication>
                                    <ns13:Communication>
                                                    <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                                                    <ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                                                    <ns13:CommValue>+1 (158) 745-2136</ns13:CommValue>
                                    </ns13:Communication>
                    </ns13:ListOfCommunication>
                    <ns13:ListOfTax>
                                    <ns13:Tax/>
                    </ns13:ListOfTax>
                    <ns13:ListOfPOBCredentialingCountry/>
            </ns6:PlaceOfBusiness>
        </ns6:ListOfPlaceOfBusiness>
        <ns6:ListOfDomains/>
        <ns6:ListOfOCRelationship/>
        <ns6:IPLockoutPGUID>urn:ecm:000001049</ns6:IPLockoutPGUID>
        <ns6:CustomerEmulationIndicator>N</ns6:CustomerEmulationIndicator>
    </ns6:OrgCustomerBO>
 * 
 * 
 * 
 * @author QINC1
 *
 */

@XmlRootElement(name = "ns6:OrgCustomerBO")
@XmlAccessorType (XmlAccessType.FIELD)
public class OrgCustomerBOWrapper {
	@XmlElement(name = "ns6:OrgCustomerPGUID", nillable = true)
	public String orgCustomerPGUID;
	
	@XmlElement(name = "ns6:TrustLifeCyclePGUID")
	public String trustLifeCyclePGUID;
	
	@XmlElement(name = "ns6:TrustLifeCycleDesc")
	public String trustLifeCycleDesc;
	
	@XmlElement(name = "ns6:BusinessUnitPGUID")
	public String businessUnitPGUID;
	
	@XmlElement(name = "ns6:BusinessUnitDescription")
	public String businessUnitDescription;
	
	@XmlElement(name = "ns6:AgencyType")
	public String agencyType;
	
	@XmlElement(name = "ns6:OrgCustomerName")
	public String orgCustomerName;
	
	@XmlElement(name = "ns6:ListOfSegment")
	public ListOfSegmentWrapper listOfSegment; 
	
	@XmlElement(name = "ns6:ListOfCommunication")
	public ListOfCommunicationWrapper listOfCommunication;
	
	@XmlElement(name = "ns6:CustomerClassTypePGUID")
	public String customerClassTypePGUID;
	
	@XmlElement(name = "ns6:CustomerClassTypeDesc")
	public String customerClassTypeDesc;
	
	@XmlElement(name = "ns6:CustomerSubClassTypePGUID")
	public String customerSubClassTypePGUID;
	
	@XmlElement(name = "ns6:CustomerSubClassTypeDesc")
	public String customerSubClassTypeDesc;
	
	@XmlElement(name = "ns6:SecondarySubClassTypePGUID")
	public String secondarySubClassTypePGUID;
	
	@XmlElement(name = "ns6:SecondarySubClassTypeDesc")
	public String secondarySubClassTypeDesc;
	
	@XmlElement(name = "ns6:IndustryClassTypePGUID")
	public String industryClassTypePGUID;
	
	@XmlElement(name = "ns6:IndustryClassTypeDesc")
	public String industryClassTypeDesc;
	
	@XmlElement(name = "ns6:IndustrySubClassTypePGUID")
	public String industrySubClassTypePGUID;
	
	@XmlElement(name = "ns6:IndustrySubClassTypeDesc")
	public String industrySubClassTypeDesc;
	
	@XmlElement(name = "ns6:NoOfAttorneys")
	public String noOfAttorneys;
	
	@XmlElement(name = "ns6:ListOfPlaceOfBusiness")
	public ListOfPlaceOfBusinessWrapper listOfPlaceOfBusiness;
	
	@XmlElement(name = "ns6:IPLockoutPGUID")
	public String iPLockoutPGUID;
	
	@XmlElement(name = "ns6:CustomerEmulationIndicator")
	public String customerEmulationIndicator;
	
	
	
	
	
	public String getOrgCustomerPGUID() {
		return orgCustomerPGUID;
	}




	public void setOrgCustomerPGUID(String orgCustomerPGUID) {
		this.orgCustomerPGUID = orgCustomerPGUID;
	}




	public String getTrustLifeCyclePGUID() {
		return trustLifeCyclePGUID;
	}




	public void setTrustLifeCyclePGUID(String trustLifeCyclePGUID) {
		this.trustLifeCyclePGUID = trustLifeCyclePGUID;
	}




	public String getTrustLifeCycleDesc() {
		return trustLifeCycleDesc;
	}




	public void setTrustLifeCycleDesc(String trustLifeCycleDesc) {
		this.trustLifeCycleDesc = trustLifeCycleDesc;
	}




	public String getBusinessUnitPGUID() {
		return businessUnitPGUID;
	}




	public void setBusinessUnitPGUID(String businessUnitPGUID) {
		this.businessUnitPGUID = businessUnitPGUID;
	}




	public String getBusinessUnitDescription() {
		return businessUnitDescription;
	}




	public void setBusinessUnitDescription(String businessUnitDescription) {
		this.businessUnitDescription = businessUnitDescription;
	}




	public String getAgencyType() {
		return agencyType;
	}




	public void setAgencyType(String agencyType) {
		this.agencyType = agencyType;
	}




	public String getOrgCustomerName() {
		return orgCustomerName;
	}




	public void setOrgCustomerName(String orgCustomerName) {
		this.orgCustomerName = orgCustomerName;
	}




	public ListOfSegmentWrapper getListOfSegment() {
		return listOfSegment;
	}




	public void setListOfSegment(ListOfSegmentWrapper listOfSegment) {
		this.listOfSegment = listOfSegment;
	}




	public ListOfCommunicationWrapper getListOfCommunication() {
		return listOfCommunication;
	}




	public void setListOfCommunication(ListOfCommunicationWrapper listOfCommunication) {
		this.listOfCommunication = listOfCommunication;
	}




	public String getCustomerClassTypePGUID() {
		return customerClassTypePGUID;
	}




	public void setCustomerClassTypePGUID(String customerClassTypePGUID) {
		this.customerClassTypePGUID = customerClassTypePGUID;
	}




	public String getCustomerClassTypeDesc() {
		return customerClassTypeDesc;
	}




	public void setCustomerClassTypeDesc(String customerClassTypeDesc) {
		this.customerClassTypeDesc = customerClassTypeDesc;
	}




	public String getCustomerSubClassTypePGUID() {
		return customerSubClassTypePGUID;
	}




	public void setCustomerSubClassTypePGUID(String customerSubClassTypePGUID) {
		this.customerSubClassTypePGUID = customerSubClassTypePGUID;
	}




	public String getCustomerSubClassTypeDesc() {
		return customerSubClassTypeDesc;
	}




	public void setCustomerSubClassTypeDesc(String customerSubClassTypeDesc) {
		this.customerSubClassTypeDesc = customerSubClassTypeDesc;
	}




	public String getSecondarySubClassTypePGUID() {
		return secondarySubClassTypePGUID;
	}




	public void setSecondarySubClassTypePGUID(String secondarySubClassTypePGUID) {
		this.secondarySubClassTypePGUID = secondarySubClassTypePGUID;
	}




	public String getSecondarySubClassTypeDesc() {
		return secondarySubClassTypeDesc;
	}




	public void setSecondarySubClassTypeDesc(String secondarySubClassTypeDesc) {
		this.secondarySubClassTypeDesc = secondarySubClassTypeDesc;
	}




	public String getIndustryClassTypePGUID() {
		return industryClassTypePGUID;
	}




	public void setIndustryClassTypePGUID(String industryClassTypePGUID) {
		this.industryClassTypePGUID = industryClassTypePGUID;
	}




	public String getIndustryClassTypeDesc() {
		return industryClassTypeDesc;
	}




	public void setIndustryClassTypeDesc(String industryClassTypeDesc) {
		this.industryClassTypeDesc = industryClassTypeDesc;
	}




	public String getIndustrySubClassTypePGUID() {
		return industrySubClassTypePGUID;
	}




	public void setIndustrySubClassTypePGUID(String industrySubClassTypePGUID) {
		this.industrySubClassTypePGUID = industrySubClassTypePGUID;
	}




	public String getIndustrySubClassTypeDesc() {
		return industrySubClassTypeDesc;
	}




	public void setIndustrySubClassTypeDesc(String industrySubClassTypeDesc) {
		this.industrySubClassTypeDesc = industrySubClassTypeDesc;
	}




	public String getNoOfAttorneys() {
		return noOfAttorneys;
	}




	public void setNoOfAttorneys(String noOfAttorneys) {
		this.noOfAttorneys = noOfAttorneys;
	}




	public ListOfPlaceOfBusinessWrapper getListOfPlaceOfBusiness() {
		return listOfPlaceOfBusiness;
	}




	public void setListOfPlaceOfBusiness(ListOfPlaceOfBusinessWrapper listOfPlaceOfBusiness) {
		this.listOfPlaceOfBusiness = listOfPlaceOfBusiness;
	}




	public String getiPLockoutPGUID() {
		return iPLockoutPGUID;
	}




	public void setiPLockoutPGUID(String iPLockoutPGUID) {
		this.iPLockoutPGUID = iPLockoutPGUID;
	}




	public String getCustomerEmulationIndicator() {
		return customerEmulationIndicator;
	}




	public void setCustomerEmulationIndicator(String customerEmulationIndicator) {
		this.customerEmulationIndicator = customerEmulationIndicator;
	}




	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
