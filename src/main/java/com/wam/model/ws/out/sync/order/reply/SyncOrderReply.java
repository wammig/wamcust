package com.wam.model.ws.out.sync.order.reply;

import static java.lang.System.out;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.http.HttpStatus;

import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.service.AccountWamService;
import com.wam.utility.definition.Definition;

public class SyncOrderReply {

	private String regexCode = "(code){1}(:)?(\\s)?[0-9]{3,6}";
	private String regexCodeTrim = "(code){1}(:)?(\\s)?";
	
	private String regexMessage = "(message){1}(:)?(\\s)?([\\w\\s\\(\\)\\:\\\"\\,\\<\\>\\{\\}\\'�-�-.]+)";
	private String regexMessageTrim = "(message){1}((:)|(\\s))";
	
	private String status;
	private String code;
	private String message;
	
	/**
	 * handle a normal message object
	 * @param message
	 */
	public SyncOrderReply(ResponseMessage message)
	{
		if(message.getStatus() == ResponseMessage.FAILED)
		{
			this.setStatus(Definition.FAILURE);
			this.setCode(HttpStatus.BAD_REQUEST.toString());
		}
		if(message.getStatus() == ResponseMessage.SUCCESS)
		{
			this.setStatus(Definition.SUCCESS);
			this.setCode(HttpStatus.OK.toString());
		}
		this.setMessage(message.getMessage());
	}
	
	/**
	 * handle text message
	 */
	public SyncOrderReply(String text) 
	{
		handleText(text);
	}
	
	public void handleText(String text)
	{
		if(text == null)
		{
			this.setStatus(Definition.FAILURE);
			this.setCode(HttpStatus.BAD_REQUEST.toString());
			this.setMessage(Definition.EMPTY);
			return;
		}
		/**
		 * first we check it the error message come from JAXB
		 */
		if((text.contains("<") && text.contains(">")) || text.contains("lineNumber") || text.contains("Exception"))
		{
			this.setStatus(Definition.FAILURE);
			this.setCode(HttpStatus.BAD_REQUEST.toString());
			this.setMessage(text);
		} else {
		/**
		 * otherwise fetch the code and message	from another layer
		 */
			Pattern codePattern = Pattern.compile(this.regexCode);
			Matcher codeMatcher = codePattern.matcher(text);
			//out.println(text);
			if(codeMatcher.find())
			{
				this.code = codeMatcher.group(0).toString().replaceAll(this.regexCodeTrim, "");
				this.status = Definition.FAILURE;
			} else {
				this.code = "0";
				this.status = Definition.SUCCESS;
			}
			
			Pattern messagePattern = Pattern.compile(this.regexMessage);
			Matcher messageMatcher = messagePattern.matcher(text);
			if(messageMatcher.find())
			{
				this.message = messageMatcher.group(0).toString().replaceAll(this.regexMessageTrim, "");
			} else {
				this.message = text;
			}
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
