package com.wam.model.ws.out.creation.placeofbusiness;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.CommunicationWrapper;


/**
 * Example:
 * 
 * 	<ns13:ListOfCommunication>
            <ns13:Communication>
                    <ns13:CommTypePGUID>urn:ecm:0000000J4</ns13:CommTypePGUID>
                    <ns13:CommSubTypePGUID>urn:ecm:0000000JB</ns13:CommSubTypePGUID>
                    <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                    <ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                    <ns13:CommValue>+33 4 93 78 06 79</ns13:CommValue>
            </ns13:Communication>
    </ns13:ListOfCommunication>
 * 
 * @author QINC1
 *
 */

@XmlRootElement(name = "ns13:ListOfCommunication")
@XmlAccessorType (XmlAccessType.FIELD)
public class ListOfCommunicationWrapper {
	
	@XmlElement(name = "ns13:Communication", type = CommunicationWrapper.class)
	ArrayList<CommunicationWrapper> list;

	public ArrayList<CommunicationWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<CommunicationWrapper> arrayList)
    {
        this.list = arrayList;
    }
	
}
