package com.wam.model.ws.out.sync.order.cemea;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.sync.order.cemea.OrderLineItemWrapper;

/**
 * 
 * @author QINC1
 * example:
 * 
 * <ListOfOrderLineItems>
      <OrderLineItem>
         <AssetIntegrationID>1000921037</AssetIntegrationID>
         <ProductPGUID>urn:product:9001450</ProductPGUID>
         <SellingType>Subscription</SellingType>
         <LicenseCount>1</LicenseCount>
      </OrderLineItem>
      <OrderLineItem>
         <AssetIntegrationID>1000921038</AssetIntegrationID>
         <ProductPGUID>urn:product:9001451</ProductPGUID>
         <SellingType>Subscription</SellingType>
         <LicenseCount>1</LicenseCount>
      </OrderLineItem>
      <OrderLineItem>
         <AssetIntegrationID>1000921039</AssetIntegrationID>
         <ProductPGUID>urn:product:9001452</ProductPGUID>
         <SellingType>Subscription</SellingType>
         <LicenseCount>1</LicenseCount>
      </OrderLineItem>
   </ListOfOrderLineItems>
	               
 */
@XmlRootElement(name = "ListOfOrderLineItems")
@XmlAccessorType (XmlAccessType.FIELD)
public class ListOfOrderLineItemsWrapper {

	@XmlElement(name = "OrderLineItem", type = OrderLineItemWrapper.class)
	ArrayList<OrderLineItemWrapper> list;

	public ArrayList<OrderLineItemWrapper> getList() {
		return list;
	}

	public void setList(ArrayList<OrderLineItemWrapper> list) {
		this.list = list;
	}
	
}
