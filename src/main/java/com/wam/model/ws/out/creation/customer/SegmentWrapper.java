package com.wam.model.ws.out.creation.customer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * Example:
 * 
 * 
 * 	<ns13:Segment>
       	<ns13:SegmentTypePGUID>urn:ecm:0000009MJ</ns13:SegmentTypePGUID>
        <ns13:SegmentType>Pricing Segment</ns13:SegmentType>
        <ns13:SegmentPGUID>urn:ecm:0000009MP</ns13:SegmentPGUID>
        <ns13:SegmentDescription>Government</ns13:SegmentDescription>
	</ns13:Segment>
 * 
 * 
 * @author QINC1
 *
 */


@XmlRootElement(name = "ns13:Segment")
@XmlAccessorType (XmlAccessType.FIELD)
public class SegmentWrapper {
	@XmlElement(name = "ns13:SegmentTypePGUID")
	public String segmentTypePGUID;
	
	@XmlElement(name = "ns13:SegmentType")
	public String segmentType;
	
	@XmlElement(name = "ns13:SegmentPGUID")
	public String segmentPGUID;
	
	@XmlElement(name = "ns13:SegmentDescription")
	public String segmentDescription;
	
	
	public String getSegmentTypePGUID() {
		return segmentTypePGUID;
	}



	public void setSegmentTypePGUID(String segmentTypePGUID) {
		this.segmentTypePGUID = segmentTypePGUID;
	}



	public String getSegmentType() {
		return segmentType;
	}



	public void setSegmentType(String segmentType) {
		this.segmentType = segmentType;
	}



	public String getSegmentPGUID() {
		return segmentPGUID;
	}



	public void setSegmentPGUID(String segmentPGUID) {
		this.segmentPGUID = segmentPGUID;
	}



	public String getSegmentDescription() {
		return segmentDescription;
	}



	public void setSegmentDescription(String segmentDescription) {
		this.segmentDescription = segmentDescription;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
