package com.wam.model.ws.out.creation.customer;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Example:
 * 
 * <ns6:ListOfSegment>
        <ns13:Segment>
            <ns13:SegmentTypePGUID>urn:ecm:0000009MJ</ns13:SegmentTypePGUID>
            <ns13:SegmentType>Pricing Segment</ns13:SegmentType>
            <ns13:SegmentPGUID>urn:ecm:0000009MP</ns13:SegmentPGUID>
            <ns13:SegmentDescription>Government</ns13:SegmentDescription>
        </ns13:Segment>
    </ns6:ListOfSegment>
 * 
 * 
 * @author QINC1
 *
 */
@XmlRootElement(name = "ns6:ListOfSegment")
@XmlAccessorType (XmlAccessType.FIELD)
public class ListOfSegmentWrapper {
	
	@XmlElement(name = "ns13:Segment", type = SegmentWrapper.class)
	ArrayList<SegmentWrapper> list;
	
	public ArrayList<SegmentWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<SegmentWrapper> list)
    {
        this.list = list;
    }
}
