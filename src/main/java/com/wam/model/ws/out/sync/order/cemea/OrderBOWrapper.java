package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 
 * @author QINC1
 * example:
 * 
 * <OrderBO>
      <OrgCustomerID>urn:ecm:D1424TJT7NC</OrgCustomerID>
      <AgreementPGUID>urn:NCRM:Ag:1000838292</AgreementPGUID>
      <AgreementStartDate>07/24/2018 14:00:00</AgreementStartDate>
	  <AgreementEndDate>07/24/2019 13:59:59</AgreementEndDate>
      <AgreementID/>
      <MessageID/>                                                 
    	<ListOfOrderLineItems>
          <OrderLineItem>
             <AssetIntegrationID>1000921037</AssetIntegrationID>
             <ProductPGUID>urn:product:9001450</ProductPGUID>
             <SellingType>Subscription</SellingType>
             <LicenseCount>1</LicenseCount>
          </OrderLineItem>
       </ListOfOrderLineItems>
    </OrderBO>
 */

@XmlRootElement(name = "OrderBO")
@XmlAccessorType (XmlAccessType.FIELD)
public class OrderBOWrapper {

	@XmlElement(name = "OrgCustomerID")
	public String orgCustomerID;
	
	@XmlElement(name = "AgreementPGUID")
	public String agreementPGUID;
	
	@XmlElement(name = "AgreementStartDate")
	public String agreementStartDate;
	
	@XmlElement(name = "AgreementEndDate")
	public String agreementEndDate;
	
	@XmlElement(name = "AgreementID", nillable = true)
	public String agreementID;
	
	@XmlElement(name = "MessageID", nillable = true)
	public String messageID;
	
	@XmlElement(name = "ListOfOrderLineItems")
	public ListOfOrderLineItemsWrapper listOfOrderLineItems; 
	
	
	public String getOrgCustomerID() {
		return orgCustomerID;
	}



	public void setOrgCustomerID(String orgCustomerID) {
		this.orgCustomerID = orgCustomerID;
	}



	public String getAgreementPGUID() {
		return agreementPGUID;
	}



	public void setAgreementPGUID(String agreementPGUID) {
		this.agreementPGUID = agreementPGUID;
	}



	public String getAgreementStartDate() {
		return agreementStartDate;
	}



	public void setAgreementStartDate(String agreementStartDate) {
		this.agreementStartDate = agreementStartDate;
	}



	public String getAgreementEndDate() {
		return agreementEndDate;
	}



	public void setAgreementEndDate(String agreementEndDate) {
		this.agreementEndDate = agreementEndDate;
	}



	public String getAgreementID() {
		return agreementID;
	}



	public void setAgreementID(String agreementID) {
		this.agreementID = agreementID;
	}



	public String getMessageID() {
		return messageID;
	}



	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}



	public ListOfOrderLineItemsWrapper getListOfOrderLineItems() {
		return listOfOrderLineItems;
	}



	public void setListOfOrderLineItems(ListOfOrderLineItemsWrapper listOfOrderLineItems) {
		this.listOfOrderLineItems = listOfOrderLineItems;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
