package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "MigrationActions")
@XmlAccessorType (XmlAccessType.FIELD)
public class MigrationActionsWrapper {

	@XmlElement(name = "SendVCEmail")
	public String sendVCEmail;
	
	@XmlElement(name = "SendWelcomeKit")
	public String SendWelcomeKit;

	
	public String getSendVCEmail() {
		return sendVCEmail;
	}



	public void setSendVCEmail(String sendVCEmail) {
		this.sendVCEmail = sendVCEmail;
	}



	public String getSendWelcomeKit() {
		return SendWelcomeKit;
	}



	public void setSendWelcomeKit(String sendWelcomeKit) {
		SendWelcomeKit = sendWelcomeKit;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
