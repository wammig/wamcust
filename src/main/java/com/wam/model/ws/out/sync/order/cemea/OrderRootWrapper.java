package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "Order")
@XmlAccessorType (XmlAccessType.FIELD)
public class OrderRootWrapper {

	@XmlAttribute(name = "xmlns")
	public String xmlns;
	
	@XmlAttribute(name = "xmlns:SOAP-ENV")
	public String soapEnv;
	
	@XmlAttribute(name = "xmlns:xsi")
	public String xsi;
	
	@XmlAttribute(name = "xmlns:xsd")
	public String xsd;
	
	@XmlElement(name = "Header")
	public HeaderWrapper header;
	
	@XmlElement(name = "DataArea")
	public DataAreaWrapper dataArea;
	
	
	
	public String getXmlns() {
		return xmlns;
	}



	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}



	public String getSoapEnv() {
		return soapEnv;
	}



	public void setSoapEnv(String soapEnv) {
		this.soapEnv = soapEnv;
	}



	public String getXsi() {
		return xsi;
	}



	public void setXsi(String xsi) {
		this.xsi = xsi;
	}



	public String getXsd() {
		return xsd;
	}



	public void setXsd(String xsd) {
		this.xsd = xsd;
	}



	public HeaderWrapper getHeader() {
		return header;
	}



	public void setHeader(HeaderWrapper header) {
		this.header = header;
	}



	public DataAreaWrapper getDataArea() {
		return dataArea;
	}



	public void setDataArea(DataAreaWrapper dataArea) {
		this.dataArea = dataArea;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
