package com.wam.model.ws.out.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.CommonTrackingMessageWrapper;

@XmlRootElement(name = "ns11:Header")
@XmlAccessorType (XmlAccessType.FIELD)
public class HeaderWrapper {

	@XmlElement(name = "ns12:commonTrackingMessage")
	public CommonTrackingMessageWrapper commonTrackingMessage;
	
	@XmlElement(name = "ns11:SourceAppName")
	public String sourceAppName;
	
	@XmlElement(name = "ns11:Priority")
	public String Priority;
	
	
	
	
	
	public String getSourceAppName() {
		return sourceAppName;
	}



	public void setSourceAppName(String sourceAppName) {
		this.sourceAppName = sourceAppName;
	}



	public String getPriority() {
		return Priority;
	}



	public void setPriority(String priority) {
		Priority = priority;
	}



	public CommonTrackingMessageWrapper getCommonTrackingMessage() {
		return commonTrackingMessage;
	}



	public void setCommonTrackingMessage(CommonTrackingMessageWrapper commonTrackingMessage) {
		this.commonTrackingMessage = commonTrackingMessage;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
