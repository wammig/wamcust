package com.wam.model.ws.out.creation.placeofbusiness;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.DataAreaWrapper;
import com.wam.model.ws.out.creation.placeofbusiness.HeaderWrapper;

@XmlRootElement(name = "ns7:PlaceOfBusiness")
@XmlAccessorType (XmlAccessType.FIELD)
public class PlaceOfBusinessRootWrapper {

	@XmlAttribute(name = "xmlns:ns7")
	public String ns7;
	
	@XmlAttribute(name = "xmlns:ns13")
	public String ns13;
	
	@XmlAttribute(name = "xmlns:ns12")
	public String ns12;
	
	@XmlAttribute(name = "xmlns:ns11")
	public String ns11;
	
	@XmlElement(name = "ns11:Header")
	public HeaderWrapper header;
	
	@XmlElement(name = "ns7:DataArea")
	public DataAreaWrapper dataArea;
	
	
	
	public String getNs7() {
		return ns7;
	}



	public void setNs7(String ns7) {
		this.ns7 = ns7;
	}



	public String getNs13() {
		return ns13;
	}



	public void setNs13(String ns13) {
		this.ns13 = ns13;
	}



	public String getNs12() {
		return ns12;
	}



	public void setNs12(String ns12) {
		this.ns12 = ns12;
	}



	public String getNs11() {
		return ns11;
	}



	public void setNs11(String ns11) {
		this.ns11 = ns11;
	}



	public HeaderWrapper getHeader() {
		return header;
	}



	public void setHeader(HeaderWrapper header) {
		this.header = header;
	}



	public DataAreaWrapper getDataArea() {
		return dataArea;
	}



	public void setDataArea(DataAreaWrapper dataArea) {
		this.dataArea = dataArea;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
