package com.wam.model.ws.out.creation.placeofbusiness;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.wam.model.ws.out.creation.placeofbusiness.PlaceOfBusinessWrapper;

/**
 * Example:
 * 
 * 
 * 
 * <ns7:ListOfPlaceOfBusinessBO>
        <ns7:PlaceOfBusiness>
            <ns13:Address1>14 avenue de France</ns13:Address1>
            <ns13:Address2/>
            <ns13:Address3/>
            <ns13:City>tarn</ns13:City>
            <ns13:Country>France</ns13:Country>
            <ns13:CountryPGUID>FRA</ns13:CountryPGUID>
            <ns13:CountrySubdivisionPGUID/>
            <ns13:TaxGeocode>195</ns13:TaxGeocode>
            <ns13:PostalCode>06190</ns13:PostalCode>
            <ns13:ValidAddressFlag>U</ns13:ValidAddressFlag>
            <ns13:GeographicLocationFlag>Y</ns13:GeographicLocationFlag>
            <ns13:OrgCustomerPGUID>urn:ecm:I2424ZPNYXC</ns13:OrgCustomerPGUID>
            <ns13:PrimaryAddressIndicator>N</ns13:PrimaryAddressIndicator>
            <ns13:CredentialingFlag>N</ns13:CredentialingFlag>
            <ns13:RequestedPRLevel>NONE</ns13:RequestedPRLevel>
            <ns13:ListOfCommunication>
                <ns13:Communication>
                        <ns13:CommTypePGUID>urn:ecm:0000000J4</ns13:CommTypePGUID>
                        <ns13:CommSubTypePGUID>urn:ecm:0000000JB</ns13:CommSubTypePGUID>
                        <ns13:CommTypeDesc>Phone</ns13:CommTypeDesc>
                        <ns13:CommSubTypeDesc>Main</ns13:CommSubTypeDesc>
                        <ns13:CommValue>+33 4 93 78 06 79</ns13:CommValue>
                </ns13:Communication>
            </ns13:ListOfCommunication>
        </ns7:PlaceOfBusiness>
	</ns7:ListOfPlaceOfBusinessBO>
 * 
 * 
 * 
 * 
 * @author QINC1
 *
 */
@XmlRootElement(name = "ns7:ListOfPlaceOfBusinessBO")
@XmlAccessorType (XmlAccessType.FIELD)
public class ListOfPlaceOfBusinessWrapperBO {
	
	@XmlElement(name = "ns7:PlaceOfBusiness", type = PlaceOfBusinessWrapper.class)
	ArrayList<PlaceOfBusinessWrapper> list;

	public ArrayList<PlaceOfBusinessWrapper> getList()
	{
		return list;
	}
	
    public void setList(ArrayList<PlaceOfBusinessWrapper> list)
    {
        this.list = list;
    }
	
}
