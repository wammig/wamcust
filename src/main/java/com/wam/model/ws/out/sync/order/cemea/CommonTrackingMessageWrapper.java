package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "commonTrackingMessage")
@XmlAccessorType (XmlAccessType.FIELD)
public class CommonTrackingMessageWrapper 
{
	@XmlAttribute(name = "xmlns")
	public String xmlns;
	
	@XmlElement(name = "dataGroup")
	public DataGroupWrapper dataGroup;
	
	
	
	public String getXmlns() {
		return xmlns;
	}



	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}



	public DataGroupWrapper getDataGroup() {
		return dataGroup;
	}



	public void setDataGroup(DataGroupWrapper dataGroup) {
		this.dataGroup = dataGroup;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
}
