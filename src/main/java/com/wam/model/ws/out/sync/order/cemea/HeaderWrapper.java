package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "Header")
@XmlAccessorType (XmlAccessType.FIELD)
public class HeaderWrapper {

	@XmlAttribute(name = "xmlns")
	public String xmlns;
	
	@XmlElement(name = "commonTrackingMessage")
	public CommonTrackingMessageWrapper commonTrackingMessage;
	
	@XmlElement(name = "MigrationFlag")
	public String migrationFlag;
	
	@XmlElement(name = "MigrationActions")
	public MigrationActionsWrapper migrationActions;
	
	@XmlElement(name = "CreatedDate")
	public String createdDate;
	
	@XmlElement(name = "LastUpdatedDate")
	public String lastUpdatedDate;
	
	
	
	public String getXmlns() {
		return xmlns;
	}



	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}



	public CommonTrackingMessageWrapper getCommonTrackingMessage() {
		return commonTrackingMessage;
	}



	public void setCommonTrackingMessage(CommonTrackingMessageWrapper commonTrackingMessage) {
		this.commonTrackingMessage = commonTrackingMessage;
	}



	public String getMigrationFlag() {
		return migrationFlag;
	}



	public void setMigrationFlag(String migrationFlag) {
		this.migrationFlag = migrationFlag;
	}



	public MigrationActionsWrapper getMigrationActions() {
		return migrationActions;
	}



	public void setMigrationActions(MigrationActionsWrapper migrationActions) {
		this.migrationActions = migrationActions;
	}



	public String getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}



	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}



	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
