package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 
 * @author QINC1
 * example:
 * 
 * <dataGroup>
      <sourceApp>PPLST</sourceApp>
   </dataGroup>
 * 
 */

@XmlRootElement(name = "dataGroup")
@XmlAccessorType (XmlAccessType.FIELD)
public class DataGroupWrapper {

	@XmlElement(name = "sourceApp")
	public String sourceApp;

	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
