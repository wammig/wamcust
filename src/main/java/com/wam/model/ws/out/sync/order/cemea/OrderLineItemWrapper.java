package com.wam.model.ws.out.sync.order.cemea;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 
 * @author QINC1
 * example
 * 
 * <OrderLineItem>
     <AssetIntegrationID>1000921037</AssetIntegrationID>
     <ProductPGUID>urn:product:9001450</ProductPGUID>
     <SellingType>Subscription</SellingType>
     <LicenseCount>1</LicenseCount>
   </OrderLineItem>
 */

@XmlRootElement(name = "OrderLineItem")
@XmlAccessorType (XmlAccessType.FIELD)
public class OrderLineItemWrapper {

	@XmlElement(name = "AssetIntegrationID")
	public String assetIntegrationID;
	
	@XmlElement(name = "ProductPGUID")
	public String productPGUID;
	
	@XmlElement(name = "SellingType")
	public String sellingType;
	
	@XmlElement(name = "LicenseCount")
	public String licenseCount;
	
	@XmlElement(name = "StartDate")
	public String startDate;
	
	@XmlElement(name = "EndDate")
	public String endDate;
	
	
	public String getAssetIntegrationID() {
		return assetIntegrationID;
	}



	public void setAssetIntegrationID(String assetIntegrationID) {
		this.assetIntegrationID = assetIntegrationID;
	}



	public String getProductPGUID() {
		return productPGUID;
	}



	public void setProductPGUID(String productPGUID) {
		this.productPGUID = productPGUID;
	}



	public String getSellingType() {
		return sellingType;
	}



	public void setSellingType(String sellingType) {
		this.sellingType = sellingType;
	}



	public String getLicenseCount() {
		return licenseCount;
	}



	public void setLicenseCount(String licenseCount) {
		this.licenseCount = licenseCount;
	}

	

	public String getStartDate() {
		return startDate;
	}



	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}



	public String getEndDate() {
		return endDate;
	}



	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}



	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
