package com.wam.model.ws.out.creation.customer;

import java.io.Serializable;

import org.springframework.context.annotation.PropertySource;

/**
 * 
 * this class transform the parameters to a xml String before calling the ws orgCustomer (creation customer)
 * 
 * @author QINC1
 *
 */

public class CreationCustomer extends OrgCustomerWrapper implements Serializable{

	public OrgCustomerWrapper orgCustomerWrapper;
	
	
	public OrgCustomerWrapper getOrgCustomerWrapper() {
		return orgCustomerWrapper;
	}


	public void setOrgCustomerWrapper(OrgCustomerWrapper orgCustomerWrapper) {
		this.orgCustomerWrapper = orgCustomerWrapper;
	}


	@Override
	public String toString()
	{
		return orgCustomerWrapper.toString();
	}
}
