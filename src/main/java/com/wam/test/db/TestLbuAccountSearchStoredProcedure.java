package com.wam.test.db;

import static java.lang.System.out;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.stored.procedure.LbuAccountSearchStoredProcedure;
import com.wam.stored.procedure.LbuAccountSelectByIdStoredProcedure;

public class TestLbuAccountSearchStoredProcedure extends BaseTest{
	
	public static void main(String args[]) throws Exception {
		TestLbuAccountSearchStoredProcedure testLbuAccountSearchStoredProcedure = new TestLbuAccountSearchStoredProcedure();
		LbuAccounts lbuAccounts = (LbuAccounts)testLbuAccountSearchStoredProcedure.TestLbuAccountSearch();
		out.println(lbuAccounts.toString());
	}
	
	public TestLbuAccountSearchStoredProcedure()
	{
		super();
	}
	
	public LbuAccounts TestLbuAccountSearch() throws Exception
	{
		Map<String, String> map = getTestCase();
		
	    // if the parameters not null
	    if(map.get("LBU_ACCOUNT_ID") != null)
	    {
	    	LbuAccounts lbuAccounts = (LbuAccounts)searchLbuAccount(
	    			map.get("LBU_SET_ID").toString(),
	    			null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);
	        
	        return lbuAccounts;
	    } else {
	    	out.println(" No test case found ");
	    }
		
	    return null;
	}
	
	public Map<String,String> getTestCase()
	{
		Connection conn = null;
	    String sql = " select * from LBU_ACCOUNT WHERE ROWNUM <= 1 ORDER BY LBU_ACCOUNT_ID DESC ";
	    String accountId = null;
	    String setId = null;
	    Map<String, String> map = new HashMap<String, String>();
	    
	    try {
	    	conn = this.dataSource.getConnection();
	    	PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet resultSet = ps.executeQuery();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (resultSet.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        String columnValue = resultSet.getString(i);
			        map.put(rsmd.getColumnName(i).toString(), columnValue);
			    }
			}
			ps.close();
	    	
	    } catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	    
	    return map;
	}
	
	public LbuAccounts searchLbuAccount(
			String lbuSetId, 
			String lbuAccountId, 
			String gbsAccountId,
			String glpAccountId, 
			String accountName1,
			String accountName2,
			String salesRepId,
			String accountType,
			String zipCode,
			String countryCode,
			String billableStatus,
			String addressLine1
			) throws Exception
	{
		LbuAccountSearchStoredProcedure sp = new LbuAccountSearchStoredProcedure(dataSource);
		Map outMap = sp.execute(
				 lbuSetId, 
				 lbuAccountId, 
				 gbsAccountId,
				 glpAccountId, 
				 accountName1,
				 accountName2,
				 salesRepId,
				 accountType,
				 zipCode,
				 countryCode,
				 billableStatus,
				 addressLine1
				);
		
		LbuAccounts lbuAccounts = (LbuAccounts)sp.transformAccountCursorToObject(outMap);
		
		return lbuAccounts;
	}

}
