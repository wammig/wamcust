package com.wam.test.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.wam.service.WorkFlowManagerService;
import com.wam.stored.packages.LogMessagePackage;

import static java.lang.System.out;

import java.net.URISyntaxException;

@Service("BaseTest")
public class BaseTest {
	public String contextConfigurationFile;
	
	public static DataSource dataSource;
	
	public static ApplicationContext context;
	public static Integer increment = 1;
	
	public BaseTest()
	{
		//out.println("BaseTest " + increment++);
		
		setContextWeb();
	}
	
	public static void setContextWeb()
	{
		if(BaseTest.context == null)
		{
			BaseTest.context = new ClassPathXmlApplicationContext(new String[] {"context-web.xml"});
		}

		if(BaseTest.dataSource == null)
		{
			BaseTest.dataSource = (DataSource)context.getBean("dataSource");
		}
	}
	
	public static void setContextBatch()
	{

		BaseTest.context = new ClassPathXmlApplicationContext(new String[] {"context-batch.xml"});
		BaseTest.dataSource = (DataSource)context.getBean("dataSource");
	}
	
	public static void setContextBatchE2e()
	{

		BaseTest.context = new ClassPathXmlApplicationContext(new String[] {"context-batch-e2e.xml"});
		BaseTest.dataSource = (DataSource)context.getBean("dataSource");
	}
	
	public static void setContextBatchCert()
	{

		BaseTest.context = new ClassPathXmlApplicationContext(new String[] {"context-batch-cert.xml"});
		BaseTest.dataSource = (DataSource)context.getBean("dataSource");
	}
	
	public void setDataSource(DataSource dataSource)
	{
		BaseTest.dataSource = dataSource;
	}
	
	public DataSource getDataSource()
	{
		return BaseTest.dataSource;
	}
	
	public String getMethodName(final int depth)
	{
		//String name = new Object(){}.getClass().getEnclosingMethod().getName();
		 final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		 
		 return ste[ste.length - 1 - depth].getMethodName();
	}
	
	public final static String getConstantFilePath() {

		String propertyFilePath = null;

		String jarway = BaseTest.class.getProtectionDomain().getCodeSource()
				.getLocation().getPath();
		System.out
				.println("ConstantsI-getConstantFilePath() jarway :" + jarway);

		java.net.URL jarway2 = BaseTest.class.getProtectionDomain()
				.getCodeSource().getLocation();
		System.out.println("ConstantsI-getConstantFilePath() jarway2 :"
				+ jarway2);

		java.io.File jarfile;
		String appstartinipath = null;
		try {
			jarfile = new java.io.File(jarway2.toURI());

			String jarway3 = jarfile.getParent();
			System.out.println("ConstantsI-getConstantFilePath() jarway3 :"
					+ jarway3);

			appstartinipath = jarway3 + "\\" + "props.ini";
			java.io.File appstartini = new java.io.File(appstartinipath);

			/*
			if (appstartini.exists())
				propertyFilePath = (PropertyAccess.getPropertyValue(
						appstartinipath, keyfilepath)).trim();
			else
				propertyFilePath = defaultfilepath;
				
				*/

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			java.io.File file = new java.io.File(".");
			String absolutePath = file.getAbsolutePath();
			System.out.println();
			System.out
					.println("ConstantsI-getConstantFilePath() absolutePath :"
							+ absolutePath);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		//System.out.println("ConstantsI-getConstantFilePath() " + keyfilepath+ ":" + propertyFilePath);

		return propertyFilePath;
	}
	
	/**
	 * 
	 * @return
	 */
	public Map<String,String> getTestCaseAccount()
	{
		Connection conn = null;
	    String sql = " select * from LBU_ACCOUNT WHERE ROWNUM = 1 ORDER BY LBU_ACCOUNT_ID DESC ";

	    Map<String, String> map = new HashMap<String, String>();
	    
	    try {
	    	conn = BaseTest.dataSource.getConnection();
	    	PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet resultSet = ps.executeQuery();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (resultSet.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        String columnValue = resultSet.getString(i);
			        map.put(rsmd.getColumnName(i).toString(), columnValue);
			    }
			}
			ps.close();
	    	
	    } catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	    
	    return map;
	}
}
