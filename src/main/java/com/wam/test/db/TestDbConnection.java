package com.wam.test.db;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

public class TestDbConnection extends BaseTest{
	
	public static void main(String args[]) throws Exception {  
		TestDbConnection testDbConnection = new TestDbConnection();
		testDbConnection.TestDbConnectionJDBC();
	}
	 
	public TestDbConnection()
	{
		super();
	}

	public void TestDbConnectionJDBC()
	{
	    Connection conn = null;
	    String sql = " select * from LBU_ACCOUNT where ZIP_CODE IS NOT NULL ORDER BY LBU_ACCOUNT_ID DESC ";
		
		try {
	    	conn = this.dataSource.getConnection();
	    	PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet resultSet = ps.executeQuery();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (resultSet.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        if (i > 1) System.out.print(",  ");
			        String columnValue = resultSet.getString(i);
			        System.out.print(columnValue + " " + rsmd.getColumnName(i));
			    }
			    System.out.println("");
			}
			ps.close();
	    	
	    } catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
		
	}
	
}
