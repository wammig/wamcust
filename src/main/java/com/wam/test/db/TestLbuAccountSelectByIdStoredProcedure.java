package com.wam.test.db;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.stored.packages.LbuAccountPackage;
import com.wam.stored.procedure.LbuAccountSelectByIdStoredProcedure;

public class TestLbuAccountSelectByIdStoredProcedure extends BaseTest{
	
	public static void main(String args[]) throws Exception {
		TestLbuAccountSelectByIdStoredProcedure testLbuAccountSelectByIdStoredProcedure = new TestLbuAccountSelectByIdStoredProcedure();
		LbuAccount lbuAccount = (LbuAccount)testLbuAccountSelectByIdStoredProcedure.TestLbuAccountSelectById();
		out.println(lbuAccount.toString());
	}
	
	public TestLbuAccountSelectByIdStoredProcedure()
	{
		super();
	}
	
	public LbuAccount TestLbuAccountSelectById() throws Exception
	{
		Map<String, String> map = getTestCase();
	    // if the parameters not null
	    if(map.get("accountId") != null && map.get("setId") != null)
	    {
	    	LbuAccount lbuAccount = (LbuAccount)selectLbuAccountById(map.get("accountId").toString(), map.get("setId").toString());
	    	
	    	return lbuAccount;
	    	
	    } else {
	    	out.println(" No test case found ");
	    }
		
	    return null;
	}
	
	public Map<String,String> getTestCase()
	{
		Connection conn = null;
	    String sql = " select * from LBU_ACCOUNT WHERE ROWNUM <= 1 ORDER BY LBU_ACCOUNT_ID DESC ";
	    String accountId = null;
	    String setId = null;
	    
	    
	    try {
	    	conn = this.dataSource.getConnection();
	    	PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet resultSet = ps.executeQuery();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			while (resultSet.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        String columnValue = resultSet.getString(i);
			        if(rsmd.getColumnName(i).toString().equals("LBU_ACCOUNT_ID"))
			        {
			        	accountId = columnValue;
			        }
			        if(rsmd.getColumnName(i).toString().equals("LBU_SET_ID"))
			        {
			        	setId = columnValue;
			        }  
			    }
			}
			ps.close();
	    	
	    } catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	    
	    Map<String, String> map = new HashMap<String, String>();
	    map.put("accountId", accountId);
	    map.put("setId", setId);
	    
	    return map;
	}
	
	/**
	 * call the stocked procedure defined in oracle to select the account by id
	 * @param lbuAccount
	 * @throws Exception 
	 */
	public LbuAccount selectLbuAccountById(String lbuAccountId,String lbuSetId) throws Exception
	{
		LbuAccountSelectByIdStoredProcedure sp = new LbuAccountSelectByIdStoredProcedure(dataSource);
		
		Map outMap = sp.execute(
				lbuSetId,
				lbuAccountId
				);
		
		// check the returned data from oracle stocked procedure
		if(outMap.containsKey("ERROR_CODE") != true || outMap.containsKey("ERROR_DESC") != true || outMap.containsKey("P_ACCOUNT_CURSOR") != true)
		{
			throw new Exception("Oracle stocked procedure error.");
		}
		
		// transform BigDecimal to Int
		if( ((BigDecimal)outMap.get("ERROR_CODE")).intValue() != 0)
		{
			throw new Exception("code: " + outMap.get("ERROR_CODE").toString() + " message: " + outMap.get("ERROR_DESC"));
		}
		
		LbuAccount lbuAccountDb = sp.transformAccountCursorToObject(outMap);
		
		return lbuAccountDb;
	}
	
}
