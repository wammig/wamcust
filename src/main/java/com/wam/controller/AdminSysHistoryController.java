package com.wam.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.SysHistory;
import com.wam.service.WamService;
import com.wam.stored.packages.HistoryPackage;

@Controller
class AdminSysHistoryController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	HistoryPackage historyPackage;
	
	@Autowired
	@Qualifier("searchHistoryFilterList")
	private SearchFilterList searchFilterList;
	
	public static final String SYSHISTORY_SHOW = "admin/syshistory/show";
	public static final String SYSHISTORY_LIST = "admin/syshistory/list";

	@RequestMapping(value = "admin/syshistory/list", 
			method = RequestMethod.GET)
	public String syshistoryList(
			ModelMap model,
			HttpServletRequest request
			) throws Exception 
	{	
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String filterList = mapper.writeValueAsString(searchFilterList.getList());
			model.addAttribute("searchFilterList", filterList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SYSHISTORY_LIST;
	}
	
	@RequestMapping(value = "admin/syshistory/{historyId}", 
			method = RequestMethod.GET)
	public String historyShow(
			ModelMap model,
			@PathVariable(value="historyId", required = true) String historyId
			) throws Exception
	{
		SysHistory sysHistory = historyPackage.findSysHistorySearchLast(Integer.parseInt(historyId), null, null, null, null, null, null, null, null, null, null, null, null, null);
		model.addAttribute("sysHistory", sysHistory);
		
		return SYSHISTORY_SHOW;
	}
}
