package com.wam.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.service.AdminService;
import com.wam.service.WamService;

@Controller
class AdminLogMessageController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	@Autowired
	@Qualifier("logMessageFilterList")
	private SearchFilterList searchFilterList;
	
	public static final String LOGMESSAGE_SHOW = "admin/logmessage/show";
	public static final String LOGMESSAGE_LIST = "admin/logmessage/list";
	public static final String MESSAGE_SHOW =  "admin/logmessage/show";

	@RequestMapping(value = "admin/logmessage/list", 
			method = RequestMethod.GET)
	public String logMessageList(
			ModelMap model,
			HttpServletRequest request
			) throws Exception 
	{	
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String filterList = mapper.writeValueAsString(searchFilterList.getList());
			model.addAttribute("searchFilterList", filterList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return LOGMESSAGE_LIST;
	}
	
	@RequestMapping(value = "admin/logmessage/{messageId}", 
			method = RequestMethod.GET)
	public String accountShow(
			ModelMap model,
			@PathVariable(value="messageId", required = true) String messageId
			) throws Exception 
	{		
		LogMessage logMessage = adminService.findLogMessageById(messageId);

		model.addAttribute("mapActionIdList", AdminService.mapActionIdList);
		model.addAttribute("mapMessageStatusList", AdminService.mapMessageStatusList);
		
		model.addAttribute("logMessage", logMessage);
		
		return MESSAGE_SHOW;
		
	}
}
