package com.wam.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyErrorController implements ErrorController  {
 
	public static final String VIEW_404 = "error/404";
	
    @RequestMapping("/error")
    public String handleError(ModelMap model, HttpServletRequest httpRequest) {
    	model.addAttribute("noLayout", true);
		
//    	 int httpErrorCode = getErrorCode(httpRequest);
//    	 String errorMsg = "";
//    	 
//         switch (httpErrorCode) {
//             case 400: {
//                 errorMsg = "Http Error Code: 400. Bad Request";
//                 break;
//             }
//             case 401: {
//                 errorMsg = "Http Error Code: 401. Unauthorized";
//                 break;
//             }
//             case 404: {
//                 errorMsg = "Http Error Code: 404. Resource not found";
//                 break;
//             }
//             case 500: {
//                 errorMsg = "Http Error Code: 500. Internal Server Error";
//                 break;
//             }
//         }
//         
//         System.out.println(errorMsg);
    	
    	
    	return VIEW_404;
    }
    
    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest
          .getAttribute("javax.servlet.error.status_code");
    }
 
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
