package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.service.AdminService;
import com.wam.service.WamService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;

import static java.lang.System.out;

@Controller
public class AdminContentSubController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	public static final String CONTENTSUB_SHOW = "admin/contentSub/show";
	
	@RequestMapping(value = "admin/contentSub/{accountId}/{sourcePackageId}/{contractNum}/{contractLineNum}", 
			method = RequestMethod.GET)
	public String contentSubShow(
			ModelMap model,
			@PathVariable(value="accountId", required = true) String accountId,
			@PathVariable(value="sourcePackageId", required = true) String sourcePackageId,
			@PathVariable(value="contractNum", required = true) String contractNum,
			@PathVariable(value="contractLineNum", required = true) String contractLineNum,
			@RequestParam(value="send", required = false) String send
			) throws Exception
	{
		LbuContentSub lbuContentSub = (LbuContentSub)wamService.contentSubWamService.LbuContentSubSelectById(accountId, contractNum, Integer.parseInt(contractLineNum), sourcePackageId);
		model.addAttribute("lbuContentSub", lbuContentSub);
		model.addAttribute("mapContentSubTypeList", AdminService.mapContentSubTypeList);
		model.addAttribute("mapPromoIndList", AdminService.mapPromoIndList);
		model.addAttribute("mapContentSubNoReNewList", AdminService.mapContentSubNoReNewList);
		

		if("true".equals(send))
		{
			model.addAttribute("send", true);
		}
		
		return CONTENTSUB_SHOW;
	}
			
}
