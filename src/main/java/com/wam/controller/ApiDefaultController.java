package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.ldap.AdminUser;
import com.wam.service.AdminService;
import com.wam.service.WamService;
import com.wam.stored.packages.LbuAccountPackage;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;

import static java.lang.System.out;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@Controller
@PropertySource("classpath:errors.properties")
public class ApiDefaultController {
	
	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("LbuAccountPackage")
	LbuAccountPackage lbuAccountPackage;
	
	@RequestMapping(value = "/api/index", 
			method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> apiAdmin(ModelMap model) throws Exception 
	{
		Map<String, Object> map = lbuAccountPackage.adminDBStatic();
		
		return map;
	}
}
