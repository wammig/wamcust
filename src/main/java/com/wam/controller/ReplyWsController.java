package com.wam.controller;

import static java.lang.System.out;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.model.ws.in.sync.order.reply.SyncOrderReply;
import com.wam.service.WamService;
import com.wam.utility.definition.Definition;

@Controller
public class ReplyWsController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	private static final String VIEW_SYNC_ORDER_REPLY = "wam/syncOrderReply";
	
	/**
	 * receive post params of raw xml
	 * @param model
	 * @param request
	 * @return
	 * @throws JsonProcessingException 
	 */
	@RequestMapping(value = "/syncOrderReply", 
			method = { 
						RequestMethod.GET, 
						RequestMethod.POST 
					 }, 
			//consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE}, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public ResponseEntity syncOrderReply(ModelMap model, HttpServletRequest request, @RequestHeader(value = HttpHeaders.CONTENT_LENGTH, required = true) Long contentLength) throws JsonProcessingException
	{
		try {
			/*
			if(request.getMethod() != "POST")
			{
				return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body("METHOD_NOT_ALLOWED");
			}
			*/
			ResponseHelper.cleanMessageList();
			wamService.SyncOrderReplyTransform(request.getInputStream());
			/**
			 *  reform response
			 *  fetch the 1st message 
			 */
			ResponseMessage responseMessage = (ResponseMessage)ResponseHelper.getMessageList().get(0);
			com.wam.model.ws.out.sync.order.reply.SyncOrderReply syncOrderReply = new com.wam.model.ws.out.sync.order.reply.SyncOrderReply(responseMessage);
			
			/**
			 * to remove the transfer-encoding chucked
			 * we need to fix the response body length
			 */
			Integer morelength = 0;
			if(request.getRequestURI().contains(".xml"))
			{
				morelength = 46;
			} 
			
			HttpStatus httpStatus = HttpStatus.OK;
			HttpHeaders headers = new HttpHeaders();
			headers.set(HttpHeaders.CONTENT_LENGTH, String.valueOf(new ObjectMapper().writeValueAsString(syncOrderReply).length() + morelength));
  
			return ResponseEntity.status(httpStatus).headers(headers).body(syncOrderReply);
		}
		catch(Exception ex)
		{
			/**
			 *  reform error response
			 */
			ResponseMessage responseMessage = (ResponseMessage)ResponseHelper.getMessageList().get(0);
			com.wam.model.ws.out.sync.order.reply.SyncOrderReply syncOrderReply = new com.wam.model.ws.out.sync.order.reply.SyncOrderReply(responseMessage.getMessage());

			/**
			 * for general problems
			 * return BAD_REQUEST
			 */
			HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
			
			/**
			 * if code is 10010 defined in db
			 * it means LBU_CONTENT_SUB_ID not found in db
			 * return NO_CONTENT
			 */
			if(Integer.parseInt(syncOrderReply.getCode()) == Integer.parseInt(Definition.ORACLE_SP_CODE_10010))
			{
				httpStatus = HttpStatus.NO_CONTENT;
			}
			
			/**
			 * to remove the transfer-encoding chucked
			 * we need to fix the response body length
			 */
			Integer morelength = 10;
			if(request.getRequestURI().contains(".xml"))
			{
				morelength = 60;
			} 
			
			HttpHeaders headers = new HttpHeaders();
			headers.set(HttpHeaders.CONTENT_LENGTH, String.valueOf(new ObjectMapper().writeValueAsString(syncOrderReply).length() + morelength));
			    
			return ResponseEntity.status(httpStatus).headers(headers).body(syncOrderReply);
		}
		
	}
	
	/**
	 * this is a traditional way in spring to transform xml to java object
	 * But without error messages when errors occurs
	 * So it's not in use 
	 * @param model
	 * @param request
	 * @param syncOrderReply
	 * @return
	 * @throws Exception 
	 */
	/*
	@RequestMapping(value = "/syncOrderReply", method = RequestMethod.POST, consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE})
	public String syncOrderReply(ModelMap model, HttpServletRequest request, @RequestBody SyncOrderReply syncOrderReply)
	{
		try {
			return VIEW_SYNC_ORDER_REPLY;
		}
		catch(Exception ex)
		{
			out.println(ex.getMessage());
			model.addAttribute("message", ex.getMessage());
			return IndexController.VIEW_ERROR;
		}
	}
	*/
	
	/*
	@ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleException(MethodArgumentNotValidException exception) {
		
		out.println("BAD_REQUEST");
        return null;
    }
	*/
	
	@RequestMapping(value = "/order/reply/history/{transId}", 
			method = { 
						RequestMethod.GET
					 }, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public ResponseEntity<Map> getSyncOrderReplyFromHistory(
			ModelMap model,
			@PathVariable(value="transId", required = true) String transId) throws Exception
	{
		ResponseHelper.cleanMessageList();
		SyncOrderReply syncOrderReply = wamService.getSyncOrderReplyFromHistory(transId);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Definition.MESSAGE, ResponseHelper.getMessageList().get(0).getMessage());
		map.put(Definition.STATUS, ResponseHelper.getMessageList().get(0).getStatus());
		map.put(Definition.OBJECT, syncOrderReply);
		
		return ResponseEntity.ok(map);
	}
}
