package com.wam.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;

import com.wam.model.ldap.AdminUser;
import com.wam.service.WamService;

@Controller
@RequestMapping("/security")
public class AdminSecurityController {
	
	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	public static final String LOGIN = "admin/security/login";
	
	
	@RequestMapping(value = "/login", 
			method = RequestMethod.GET)
	public String accountList(
			ModelMap model,
			HttpServletRequest request
			) throws Exception
	{		
		model.addAttribute("noLayout", true);
		
		return LOGIN;
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(@ModelAttribute AdminUser adminUser, HttpServletRequest request, SessionStatus status) {

	    status.setComplete();

	    HttpSession session = request.getSession();
	    session.removeAttribute("adminUser");
	    session.invalidate();
	    
	    return "redirect:/security/login";
	}
	
}
