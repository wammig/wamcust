package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.service.EmailService;
import com.wam.service.TestService;
import com.wam.service.WamService;
import com.wam.test.db.TestLbuAccountSearchStoredProcedure;
import com.wam.test.db.TestLbuAccountSelectByIdStoredProcedure;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;

import javax.mail.SendFailedException;
import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.System.out;

@RequestMapping(value = "/test")
@Controller
@PropertySource("classpath:workflow-manager.properties")
public class TestController {
	
	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("TestService")
	private TestService testService;
	
	 @Autowired
	 @Qualifier("EmailService")
	 public EmailService emailService;

	
	/**
	 * url example:
	 * /test/account/search.xml
	 * /test/account/search.json
	 * /test/account/search
	 * 
	 * test the case we have defined in TestLbuAccountSearchStoredProcedure
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/search", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public List<LbuAccount> accountSearch(ModelMap model) throws Exception 
	{
		TestLbuAccountSearchStoredProcedure testLbuAccountSearchStoredProcedure = new TestLbuAccountSearchStoredProcedure();
		LbuAccounts lbuAccounts = (LbuAccounts)testLbuAccountSearchStoredProcedure.TestLbuAccountSearch();
		
		return lbuAccounts.getList();  
	}
	
	
	/**
	 * url example:
	 * /test/account/select.xml
	 * /test/account/select.json
	 * /test/account/select
	 * 
	 * test the case we have defined in TestLbuAccountSelectByIdStoredProcedure
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/account/select", 
			method = RequestMethod.GET, 
			produces={"application/xml","application/json"})
	@ResponseBody
	public LbuAccount accountSelect(ModelMap model) throws Exception 
	{
		TestLbuAccountSelectByIdStoredProcedure testLbuAccountSelectByIdStoredProcedure = new TestLbuAccountSelectByIdStoredProcedure();
		LbuAccount lbuAccount= (LbuAccount)testLbuAccountSelectByIdStoredProcedure.TestLbuAccountSelectById();
		return lbuAccount;  
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	/*
	@RequestMapping(value = "/agreement/send/ws", 
			method = RequestMethod.GET)
	@ResponseBody
	public String agreementSendWs(ModelMap model) throws Exception 
	{
		try {
			testService.agreementSendWs();
			
			return "success";
		} catch(Exception ex) {
			return ex.getMessage();
		}
	
	}
	*/
	
	/**
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	/*
	 @RequestMapping(value = "/workflow/email", method = RequestMethod.GET)
     public String WorkFlowEmail(ModelMap model) throws Exception {
        try {
            emailService.prepareMailParametersBatch();
        }
        catch(SendFailedException ex)
        {
            out.println(ex.getInvalidAddresses().toString());
        }
        catch(Exception ex)
        {
            out.println(ex.getMessage());
        }
 
        return TransformController.VIEW_WAM_WORKFLOW_MANAGER;
     }
     */

}
