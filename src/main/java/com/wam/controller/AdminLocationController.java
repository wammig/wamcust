package com.wam.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.service.AdminService;
import com.wam.service.WamService;

@Controller
public class AdminLocationController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("searchLocationFilterList")
	private SearchFilterList searchLocationFilterList;
	
	public static final String LOCATION_SHOW = "admin/location/show";
	public static final String LOCATION_LIST = "admin/location/list";
	
	@RequestMapping(value = "admin/location/list", 
			method = RequestMethod.GET)
	public String locationList(
			ModelMap model,
			HttpServletRequest request
			) throws Exception 
	{	
		LbuLocations lbuLocations = new LbuLocations();
		lbuLocations.setList(new ArrayList<LbuLocation>());
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String lbuLocationsJson = mapper.writeValueAsString(lbuLocations.getList());
			String filterList = mapper.writeValueAsString(searchLocationFilterList.getList());
			model.addAttribute("searchFilterList", filterList);
			model.addAttribute("lbuLocationsJson", lbuLocationsJson);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return LOCATION_LIST;
	}
	
	
	
	@RequestMapping(value = "admin/location/{lbuSetId}/{lbuAccountId}/{lbuLocationId}", 
			method = RequestMethod.GET)
	public String locationShow(
			ModelMap model,
			@PathVariable(value="lbuSetId", required = true) String lbuSetId,
			@PathVariable(value="lbuAccountId", required = true) String lbuAccountId,
			@PathVariable(value="lbuLocationId", required = true) String lbuLocationId
			) throws Exception
	{
		LbuLocation lbuLocation = (LbuLocation)wamService.locationWamService.selectLbuLocation(lbuSetId, lbuAccountId, lbuLocationId);
		//System.out.println(lbuLocation);
		model.addAttribute("lbuLocation", lbuLocation);
		
		return LOCATION_SHOW;
	}
	
}
