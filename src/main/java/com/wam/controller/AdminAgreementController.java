package com.wam.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.service.AccountWamService;
import com.wam.service.WamService;

@Controller
public class AdminAgreementController{

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("searchAgreementFilterList")
	private SearchFilterList searchFilterList;
	
	public final static Logger logger = LoggerFactory.getLogger(AdminAgreementController.class);
	
	public static final String AGREEMENT_ASSET_SHOW = "admin/agreement/asset/show";
	public static final String AGREEMENT_SHOW = "admin/agreement/show";
	public static final String AGREEMENT_LIST = "admin/agreement/list";
	

	@RequestMapping(value = "admin/agreement/list", 
			method = RequestMethod.GET)
	public String agreementList(
			ModelMap model,
			HttpServletRequest request
			) throws Exception 
	{	
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String filterList = mapper.writeValueAsString(searchFilterList.getList());
			model.addAttribute("searchFilterList", filterList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return AGREEMENT_LIST;
	}
	
	@RequestMapping(value = "admin/agreement/asset/{assetAgreementId}", 
			method = RequestMethod.GET)
	public String agreementAssetShow(
			ModelMap model,
			@PathVariable(value="assetAgreementId", required = true) String assetAgreementId,
			HttpServletResponse httpServletResponse
			) throws Exception
	{
		try {
			LbuAgreement lbuAgreement = wamService.agreementService.selectLbuAgreement(assetAgreementId);
			LbuContentSubs lbuContentSubs = wamService.contentSubWamService.lbuContentSubSearch(null, null, null, null, null, null, null, null, null, null, null, null, null, assetAgreementId);
			LbuAccount lbuAccount = wamService.accountWamService.getLbuAccountByCustomerPguid(lbuAgreement.getCustomerPguid());
			model.addAttribute("lbuAgreement", lbuAgreement);
			model.addAttribute("lbuContentSubsList", lbuContentSubs.getList());
			model.addAttribute("lbuAccount", lbuAccount);
			
			return AGREEMENT_ASSET_SHOW;
		} catch(Exception e) {
			logger.error(assetAgreementId + e.getMessage());
		
			return "redirect:/error";
		}

	}
	
	@RequestMapping(value = "admin/agreement/{agreementId}", 
			method = RequestMethod.GET)
	public String agreementShow(
			ModelMap model,
			@PathVariable(value="agreementId", required = true) String agreementId,
			HttpServletResponse httpServletResponse
			) throws Exception
	{
		LbuAgreements lbuAgreements = wamService.agreementService.lbuAgreementPackage.LbuAgreementSelectById(agreementId);
		model.addAttribute("lbuAgreements", lbuAgreements.getList());
		
		if(!lbuAgreements.getList().isEmpty())
		{	
			// fetch the first asset agreement from the list
			LbuAgreement firstLbuAgreement = lbuAgreements.getList().get(0);
			
			LbuAccount lbuAccount = wamService.accountWamService.getLbuAccountByCustomerPguid(firstLbuAgreement.getCustomerPguid());
			model.addAttribute("lbuAccount", lbuAccount);
			model.addAttribute("firstLbuAgreement", firstLbuAgreement);
		}
	
		
		return AGREEMENT_SHOW;
	}
	
}
