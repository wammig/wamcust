package com.wam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuAgreement;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.helper.LbuAgreementGroup;
import com.wam.model.db.gbsd.helper.LbuAgreements;
import com.wam.service.AdminService;
import com.wam.service.WamService;
import com.wam.utility.definition.Definition;

import static java.lang.System.out;

@Controller
public class ApiAgreementController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	
	@RequestMapping(value = "/api/agreement/list", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public AdminDataTables agreementList(
			ModelMap model,
			@RequestParam(value="agreementId", required = false) String agreementId,
			@RequestParam(value="assetAgreementId", required = false) String assetAgreementId,
			@RequestParam(value="productId", required = false) String productId,
			@RequestParam(value="customerPguid", required = false) String customerPguid,
			@RequestParam(value="start", required = false) Integer fromNumber,
			@RequestParam(value="length", required = false) Integer numberPerPage,
			@RequestParam(value="columns[0][search][value]", required = false) String searchValue0,
			@RequestParam(value="columns[1][search][value]", required = false) String searchValue1,
			@RequestParam(value="columns[2][search][value]", required = false) String searchValue2,
			@RequestParam(value="columns[3][search][value]", required = false) String searchValue3,
			@RequestParam(value="search[value]", required = false) String searchValue
			) throws Exception
	{
		if(searchValue != "")
		{
			agreementId = searchValue;
			assetAgreementId = searchValue;
			productId = searchValue;
			customerPguid = searchValue;
			
		} else {
			
			if(searchValue0 != "")
			{
				agreementId = searchValue0;
			}
			
			if(searchValue1 != "")
			{
				assetAgreementId = searchValue1;
			}
			
			if(searchValue2 != "")
			{
				productId = searchValue2;
			}
			
			if(searchValue3 != "")
			{
				customerPguid = searchValue3;
			}
			
		}
		
		AdminDataTables adminDataTables = (AdminDataTables)adminService.wamService.agreementService.lbuAgreementPackage.LbuAgreementSearch(
				agreementId, 
				assetAgreementId, 
				productId, 
				customerPguid, 
				fromNumber, 
				numberPerPage
				)
;
		
		return adminDataTables;	
	}
	
	
	@RequestMapping(value = "/api/agreement/listByAccountId/{accountId}/{setId}", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public Map<String, LbuAgreements> agreementsListByAccountId(
			ModelMap model,
			@PathVariable(value="accountId", required = true) long accountId,
			@PathVariable(value="setId", required = true) String setId
			) throws Exception 
	{
		LbuAccount lbuAccount = (LbuAccount)wamService.accountWamService.selectLbuAccountById(String.valueOf(accountId), setId);
		Map<String, LbuAgreements> map= wamService.agreementService.getLbuAgreementSelectByCustomerByType(lbuAccount);
		
		return map;
	}
	
	@RequestMapping(value = "/api/agreementGroup/listByAccountId/{accountId}/{setId}", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public Map<String, LbuAgreementGroup> agreementsListGroupByAccountId(
			ModelMap model,
			@PathVariable(value="accountId", required = true) long accountId,
			@PathVariable(value="setId", required = true) String setId
			) throws Exception 
	{
		LbuAccount lbuAccount = (LbuAccount)wamService.accountWamService.selectLbuAccountById(String.valueOf(accountId), setId);
		Map<String, LbuAgreements> map= wamService.agreementService.getLbuAgreementSelectByCustomerByType(lbuAccount);
		
		Map<String, LbuAgreementGroup> mapGroup = new HashMap<String, LbuAgreementGroup>();
		
		map.entrySet().forEach(entry -> {
			if(Definition.PRODUCT.toLowerCase().equals(entry.getKey()))
				entry.getValue().getList().forEach(e -> {
				if(!mapGroup.containsKey(e.getAgreementId()))
				{
					LbuAgreementGroup lbuAgreementGroup = new LbuAgreementGroup();
					lbuAgreementGroup.setAgreementId(e.getAgreementId());
					lbuAgreementGroup.setList(new ArrayList<LbuAgreement>());
					mapGroup.put(e.getAgreementId(), lbuAgreementGroup);
				}
				mapGroup.get(e.getAgreementId()).getList().add(e);
			});
		});
		
		return mapGroup;
	}
	
	
	/**
	 * send the AssetAgreements which we have selected
	 * 
	 */
	@RequestMapping(value = "/api/send/agreements", 
			method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity sendAgreements(@RequestParam(value="agreements[]") List<String> agreements
	) throws Exception
	{
		try {
			wamService.agreementService.sendAgreementsToWs(agreements);
			
			String result = "";
			result = agreements.stream()
			.map(v -> v.toString())
			.collect( Collectors.joining( " , " ) )
			;
			result += " sent ";
			
			return ResponseEntity.status(HttpStatus.OK).body(result);
		} catch (Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/api/send/agreementByContractNum/{contractNum}", 
			method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity sendAgreementByContractNum(@PathVariable(value="contractNum") String contractNum) throws Exception
	{
		try {
			LbuAgreements lbuAgreements = wamService.agreementService.lbuAgreementPackage.LbuAgreementSelectById(contractNum);
			
			List<String> assetAgreementIds = lbuAgreements.getList()
			.stream()
			.map(v -> v.getAssetAgreementId())
			.collect(Collectors.toList())
			;
			
			wamService.agreementService.sendAgreementsToWs(assetAgreementIds);
			
			String result = "";
			result = assetAgreementIds.stream()
			.map(v -> v.toString())
			.collect( Collectors.joining( " , " ) )
			;
			result += " sent ";
			
			return ResponseEntity.status(HttpStatus.OK).body(result);
		} catch (Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}
	}
	
}
