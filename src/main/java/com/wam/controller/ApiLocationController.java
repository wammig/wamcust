package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.entity.LbuLocation;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.mvc.response.helper.AccountFormJsonResponse;
import com.wam.model.mvc.response.helper.LocationFormJsonResponse;
import com.wam.service.AdminService;
import com.wam.service.WamService;

@Controller
public class ApiLocationController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	@RequestMapping(value = "/api/location/list", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public AdminDataTables locationList(
			ModelMap model,
			@RequestParam(value="lbuAccountId", required = false) String lbuAccountId,
			@RequestParam(value="lbuLocationId", required = false) String lbuLocationId,
			@RequestParam(value="accountName1", required = false) String accountName1,
			@RequestParam(value="cityName", required = false) String cityName,
			@RequestParam(value="zipCode", required = false) String zipCode,
			@RequestParam(value="countryCode", required = false) String countryCode,
			@RequestParam(value="addressLine1", required = false) String addressLine1,
			@RequestParam(value="addressLine2", required = false) String addressLine2,
			@RequestParam(value="start", required = false) Integer fromNumber,
			@RequestParam(value="length", required = false) Integer numberPerPage,
			@RequestParam(value="columns[0][search][value]", required = false) String searchValue0,
			@RequestParam(value="columns[1][search][value]", required = false) String searchValue1,
			@RequestParam(value="columns[2][search][value]", required = false) String searchValue2,
			@RequestParam(value="columns[3][search][value]", required = false) String searchValue3,
			@RequestParam(value="columns[4][search][value]", required = false) String searchValue4,
			@RequestParam(value="columns[5][search][value]", required = false) String searchValue5,
			@RequestParam(value="columns[6][search][value]", required = false) String searchValue6,
			@RequestParam(value="columns[7][search][value]", required = false) String searchValue7,
			@RequestParam(value="search[value]", required = false) String searchValue
			) throws Exception
	{
		if(searchValue != "")
		{
			lbuAccountId = searchValue;
			lbuLocationId = searchValue;
			addressLine1 = searchValue;
			addressLine2 = searchValue;
			accountName1 = searchValue;
			cityName = searchValue;
			zipCode = searchValue;
			countryCode = searchValue;
			
		} else {
			
			if(searchValue0 != "")
			{
				lbuAccountId = searchValue0;
			}
			
			if(searchValue1 != "")
			{
				lbuLocationId = searchValue1;
			}
			
			if(searchValue2 != "")
			{
				accountName1 = searchValue2;
			}
			
			if(searchValue3 != "")
			{
				cityName = searchValue3;
			}
			
			if(searchValue4 != "")
			{
				zipCode = searchValue4;
			}
			
			if(searchValue5 != "")
			{
				countryCode = searchValue5;
			}
			
			if(searchValue6 != "")
			{
				addressLine1 = searchValue6;
			}
			
			if(searchValue7 != "")
			{
				addressLine2 = searchValue7;
			}
			
		}
		
		AdminDataTables adminDataTables = (AdminDataTables)adminService.wamService.locationWamService.lbuLocationSearch2(
				lbuAccountId, 
				lbuLocationId, 
				accountName1,
				cityName,
				zipCode, 
				countryCode, 
				addressLine1, 
				addressLine2, 
				fromNumber, 
				numberPerPage
				);
		
		return adminDataTables;	
	}
	
	
	@RequestMapping(value = "/api/location/listByAccountId/{accountId}/{setId}", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public List<LbuLocation> locationListByAccountId(
			ModelMap model,
			@PathVariable(value="accountId", required = true) long accountId,
			@PathVariable(value="setId", required = true) String setId
			) throws Exception 
	{
		LbuLocations lbuLocations = (LbuLocations)wamService.locationWamService.lbuLocationSearch
				(
					setId, 
					String.valueOf(accountId), 
					null,
					null, 
					null, 
					null, 
					null, 
					null, 
					null
				);


		return lbuLocations.getList();
	}
	
	@RequestMapping(value = "/admin/location/update", produces = { MediaType.APPLICATION_JSON_VALUE }, 
			method = RequestMethod.POST)
	@ResponseBody
	public LocationFormJsonResponse updateAccount(@ModelAttribute("locationForm") @Validated LbuLocation location,
	         BindingResult result) throws Exception {
		
		return adminService.updateLocation(location, result);
	}
}
