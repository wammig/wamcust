package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.ldap.AdminUser;
import com.wam.service.WamService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;

import static java.lang.System.out;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@PropertySource("classpath:errors.properties")
public class AdminDefaultController {
	
	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
//	 @Autowired
//	 private HttpSession session;

	public static final String HOME = "admin/default/index";

	
	@RequestMapping(value = "admin", 
			method = RequestMethod.GET)
	public String index(ModelMap model,HttpServletRequest request) throws Exception 
	{
//		AdminUser adminUser = (AdminUser) request.getSession().getAttribute("adminUser");
//		out.println(adminUser);
		
		return HOME;
	}
	
	
}
