package com.wam.controller;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wam.model.admin.AdminDataTables;
import com.wam.service.AdminService;
import com.wam.service.WamService;

@Controller
public class ApiSysHistoryController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	@RequestMapping(value = "/api/syshistory/list", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public AdminDataTables sysHistoryList(
			ModelMap model,
			@RequestParam(value="historyId", required = false) String historyId,
			@RequestParam(value="entityId", required = false) String entityId,
			@RequestParam(value="transId", required = false) String transId,
			@RequestParam(value="messageId", required = false) String messageId,
			@RequestParam(value="start", required = false) Integer fromNumber,
			@RequestParam(value="length", required = false) Integer numberPerPage,
			@RequestParam(value="columns[0][search][value]", required = false) String searchValue0,
			@RequestParam(value="columns[1][search][value]", required = false) String searchValue1,
			@RequestParam(value="columns[2][search][value]", required = false) String searchValue2,
			@RequestParam(value="columns[3][search][value]", required = false) String searchValue3,
			@RequestParam(value="search[value]", required = false) String searchValue
			) throws Exception
	{
		
		if(searchValue != "")
		{
			historyId = searchValue;
			entityId = searchValue;
			transId = searchValue;
			messageId = searchValue;
			
		} else {
			
			if(searchValue0 != "")
			{
				historyId = searchValue0;
			}
			
			if(searchValue1 != "")
			{
				entityId = searchValue1;
			}
			
			if(searchValue2 != "")
			{
				transId = searchValue2;
			}
			
			if(searchValue3 != "")
			{
				messageId = searchValue3;
			}
			
		}
		
		AdminDataTables adminDataTables = (AdminDataTables)adminService.wamService
				.accountWamService.getHistoryPackage()
				.findSysHistorySearch2(historyId, entityId, transId, messageId, fromNumber, numberPerPage)
				;
		
		return adminDataTables;
	}
	
}
