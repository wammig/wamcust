package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.System.out;

import com.wam.model.admin.AdminDataTableColumn;
import com.wam.model.admin.AdminDataTableSearch;
import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.mvc.response.helper.AccountFormJsonResponse;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.service.AdminService;
import com.wam.service.WamService;
import com.wam.utility.definition.Definition;


@Controller
public class ApiAccountController {

	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	/**
	 * 
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/api/account/list", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public List<LbuAccount> accountList(
			ModelMap model,
			@RequestParam(value="lbuSetId", required = false) String lbuSetId,
			@RequestParam(value="lbuAccountId", required = false) String lbuAccountId,
			@RequestParam(value="gbsAccountId", required = false) String gbsAccountId,
			@RequestParam(value="glpAccountId", required = false) String glpAccountId,
			@RequestParam(value="accountName1", required = false) String accountName1,
			@RequestParam(value="accountName2", required = false) String accountName2,
			@RequestParam(value="salesRepId", required = false) String salesRepId,
			@RequestParam(value="accountType", required = false) String accountType,
			@RequestParam(value="zipCode", required = false) String zipCode,
			@RequestParam(value="countryCode", required = false) String countryCode,
			@RequestParam(value="billableStatus", required = false) String billableStatus,
			@RequestParam(value="addressLine1", required = false) String addressLine1
			) throws Exception 
	{	
		LbuAccounts lbuAccounts = (LbuAccounts)adminService.wamService.accountWamService.searchLbuAccount(
					lbuSetId, 
					lbuAccountId, 
					gbsAccountId, 
					glpAccountId, 
					accountName1, 
					accountName2, 
					salesRepId, 
					accountType, 
					zipCode, 
					countryCode, 
					billableStatus, 
					addressLine1
				);
		
		return lbuAccounts.getList();  
	}
	
	@RequestMapping(value = "/api/account/list2", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public AdminDataTables accountList2(
			ModelMap model,
			@RequestParam(value="lbuSetId", required = false) String lbuSetId,
			@RequestParam(value="lbuAccountId", required = false) String lbuAccountId,
			@RequestParam(value="gbsAccountId", required = false) String gbsAccountId,
			@RequestParam(value="accountName1", required = false) String accountName1,
			@RequestParam(value="accountName2", required = false) String accountName2,
			@RequestParam(value="city", required = false) String city,
			@RequestParam(value="zipCode", required = false) String zipCode,
			@RequestParam(value="countryCode", required = false) String countryCode,
			@RequestParam(value="billableStatus", required = false) String billableStatus,
			@RequestParam(value="addressLine1", required = false) String addressLine1,
			@RequestParam(value="customerPguid", required = false) String customerPguid,
			@RequestParam(value="start", required = false) Integer fromNumber,
			@RequestParam(value="length", required = false) Integer numberPerPage,
			@RequestParam(value="columns[0][search][value]", required = false) String searchValue0,
			@RequestParam(value="columns[1][search][value]", required = false) String searchValue1,
			@RequestParam(value="columns[2][search][value]", required = false) String searchValue2,
			@RequestParam(value="columns[3][search][value]", required = false) String searchValue3,
			@RequestParam(value="columns[4][search][value]", required = false) String searchValue4,
			@RequestParam(value="columns[5][search][value]", required = false) String searchValue5,
			@RequestParam(value="columns[6][search][value]", required = false) String searchValue6,
			@RequestParam(value="search[value]", required = false) String searchValue
			) throws Exception 
	{	
		
		if(searchValue != "")
		{
			lbuAccountId = searchValue;
			accountName1 = searchValue;
			addressLine1 = searchValue;
			city = searchValue;
			zipCode = searchValue;
			countryCode = searchValue;
			lbuSetId = searchValue;
			customerPguid = searchValue;
			
		} else {
			
			if(searchValue0 != "")
			{
				lbuAccountId = searchValue0;
			}
			
			if(searchValue1 != "")
			{
				accountName1 = searchValue1;
			}
			
			if(searchValue2 != "")
			{
				addressLine1 = searchValue2;
			}
			
			if(searchValue3 != "")
			{
				city = searchValue3;
			}
			
			if(searchValue4 != "")
			{
				countryCode = searchValue4;
			}
			
			if(searchValue5 != "")
			{
				zipCode = searchValue5;
			}
			
			if(searchValue6 != "")
			{
				customerPguid = searchValue6;
			}

		}
		
		
		AdminDataTables adminLbuAccounts = (AdminDataTables)adminService.wamService.accountWamService.searchLbuAccount2(
					lbuSetId, 
					lbuAccountId, 
					gbsAccountId, 
					accountName1, 
					accountName2, 
					city,
					zipCode, 
					countryCode, 
					billableStatus, 
					addressLine1,
					customerPguid,
					fromNumber,
					numberPerPage
				);
		
		return adminLbuAccounts;  
	}
	
	/**
	 * sendCustomerToEcmWs
	 * call Ecm from backoffice admin
	 * 
	 * @param model
	 * @param accountId
	 * @param setId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/api/send/ecm/account/{accountId}/{setId}/{action}", 
			method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity sendCustomerToEcmWs(
			ModelMap model,
			@PathVariable(value="accountId", required = true) long accountId,
			@PathVariable(value="setId", required = true) String setId,
			@PathVariable(value="action", required = true) String action
			) throws Exception
	{
		try {
			adminService.wamService.accountWamService.sendCustomerToEcmWs(String.valueOf(accountId), setId, action);
			int last = ResponseHelper.getMessageList().size() - 1;
			String result= ResponseHelper.getMessageList().get(last).getMessage().toString();
			
			return ResponseEntity.status(HttpStatus.OK).body(result);
		} catch (Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
		}
	}
	
	/**
	 * 
	 * @param account
	 * @param result
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/admin/account/update", produces = { MediaType.APPLICATION_JSON_VALUE }, 
			method = RequestMethod.POST)
	@ResponseBody
	public AccountFormJsonResponse updateAccount(@ModelAttribute("accountForm") @Validated LbuAccount account,
	         BindingResult result) throws Exception {
		
		return adminService.updateAccount(account, result);
	}
}
