package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.System.out;

import java.io.IOException;
import java.net.SocketException;

import com.wam.model.db.gbsd.helper.LogMessages;
import com.wam.model.mvc.response.helper.ResponseHelper;
import com.wam.model.mvc.response.helper.ResponseMessage;
import com.wam.model.mvc.response.helper.WsMessage;
import com.wam.model.mvc.response.helper.WsMessageHelper;
import com.wam.service.BaseService;
import com.wam.service.WamService;
import com.wam.utility.definition.Definition;

@PropertySource("classpath:workflow-manager.properties")
@Controller
public class TransformController {

	@Autowired
	private Environment env;
	
	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	public static final String VIEW_INDEX = "wam/index";
	public static final String VIEW_WAM_TRANSFORMER_WS = "wam/wamTransformerWs";
	public static final String VIEW_WAM_TRANSFORMER_LOADER = "wam/wamTransformerLoader";
	public static final String VIEW_WAM_WORKFLOW_MANAGER = "wam/workflowManager";
	
	public static final String VIEW_404 = "error/404";
	
	private static final Logger logger = LoggerFactory.getLogger(TransformController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {
		
		return "index";
	}
	
	@RequestMapping(value = "/transform", method = RequestMethod.GET)
	public String wamTransformerLoader(
			ModelMap model,
			@RequestParam(value="prc", required = false) String prc,
			@RequestParam(value="prs", required = false) String prs,
			@RequestParam(value="aid", required = false) String aid,
			@RequestParam(value="acn", required = false) String acn,
			@RequestParam(value="uid", required = false) String uid,
			@RequestParam(value="bst", required = false) String bst,
			@RequestParam(value="act", required = false) String act,
			@RequestParam(value="adr", required = false) String adr,
			@RequestParam(value="dte", required = false) String dte,
			@RequestParam(value="udte", required = false) String udte,
			@RequestParam(value="lbu", required = false) String lbu,
			@RequestParam(value="addr", required = false) String addr,
			@RequestParam(value="cnum", required = false) String cnum,
			@RequestParam(value="csub", required = false) String csub
			
			) throws Exception {
			
		model.addAttribute("noLayout", true);
		
		return VIEW_WAM_TRANSFORMER_LOADER;
	}
	
	/**
	 * wamTransformer
	 * transform request parameter to attribute of the model
	 * @throws Exception 
	 */
	@RequestMapping(value = "/transform/ajax", method = RequestMethod.GET)
	public String wamTransformerWs(
			ModelMap model,
			@RequestParam(value="prc", required = false) String prc,
			@RequestParam(value="prs", required = false) String prs,
			@RequestParam(value="aid", required = false) String aid,
			@RequestParam(value="acn", required = false) String acn,
			@RequestParam(value="uid", required = false) String uid,
			@RequestParam(value="bst", required = false) String bst,
			@RequestParam(value="act", required = false) String act,
			@RequestParam(value="adr", required = false) String adr,
			@RequestParam(value="dte", required = false) String dte,
			@RequestParam(value="udte", required = false) String udte,
			@RequestParam(value="lbu", required = false) String lbu,
			@RequestParam(value="addr", required = false) String addr,
			@RequestParam(value="cnum", required = false) String cnum,
			@RequestParam(value="csub", required = false) String csub
			
			) throws Exception {
			
		try {
			ResponseHelper.cleanMessageList();
			WsMessageHelper.cleanMessageList();
			
			// check table name
			if(WamService.mapDbTableService.get(prc) == null) {
				String messages = wamService.generateErrorMessageDbTableServiceType();
				throw new Exception(messages);
			};
			
			// check action
			// it's no more in use
			/*
			if(WamService.mapAction.get(prs) == null) {
				String messages = wamService.generateErrorMessageActionType();
				throw new Exception(messages);
			}
			*/
			
			Map mapParam = new HashMap();
			mapParam.put("prc", prc);
			mapParam.put("aid", aid);
			mapParam.put("acn", acn);
			mapParam.put("uid", uid);
			mapParam.put("bst", bst);
			mapParam.put("act", act);
			mapParam.put("adr", adr);
			mapParam.put("dte", dte);
			mapParam.put("udte", udte);
			mapParam.put("lbu", lbu);
			mapParam.put("addr", addr);
			mapParam.put("cnum", cnum);
			mapParam.put("csub", csub);
			
			Map<?, ?> mapWamTransform = wamService.wamTransform(mapParam);
			List<ResponseMessage> messageList = ResponseHelper.getMessageList();
			model.addAttribute("messages", messageList);
			
			List<WsMessage> wsMessageList = WsMessageHelper.getMessageList();
			model.addAttribute("isITParis", true); // BaseService.isITParis()
			model.addAttribute("wsMessages", wsMessageList);
			if(mapWamTransform.containsKey(Definition.GENERATED_TRANS_ID_PRODUCT))
			{
				model.addAttribute("transIdProduct", mapWamTransform.get(Definition.GENERATED_TRANS_ID_PRODUCT).toString());	
			}
			if(mapWamTransform.containsKey(Definition.IS_REAL_SEND_AGREEMENT))
			{
				model.addAttribute("isRealSendAgreement", mapWamTransform.get(Definition.IS_REAL_SEND_AGREEMENT).toString());	
			}
			model.addAttribute("noLayout", true);
		}
		catch(SocketException ex)
		{
			List<ResponseMessage> messageList = ResponseHelper.getMessageList();
			
			messageList.add(new ResponseMessage(ex.getMessage(), ResponseMessage.FAILED));
			model.addAttribute("messages", messageList);
			model.addAttribute("noLayout", true);
		}
		catch(IOException ex)
		{
			List<ResponseMessage> messageList = ResponseHelper.getMessageList();
			messageList.add(new ResponseMessage(ex.getMessage(), ResponseMessage.FAILED));
			model.addAttribute("messages", messageList);
			model.addAttribute("noLayout", true);
		}
		catch(Exception ex)
		{
			List<ResponseMessage> messageList = ResponseHelper.getMessageList();
			
			if(null != ex.getMessage()) {
				messageList.add(new ResponseMessage(ex.getMessage(), ResponseMessage.FAILED));
			}
			model.addAttribute("messages", messageList);
			model.addAttribute("noLayout", true);
		}
		
		return VIEW_WAM_TRANSFORMER_WS;
	}

}