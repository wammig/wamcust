package com.wam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.mvc.response.helper.AccountFormJsonResponse;
import com.wam.service.AdminService;

@Controller
@RequestMapping("/security")
public class ApiSecurityController {

	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	@RequestMapping(value = "/api/login", produces = { MediaType.APPLICATION_JSON_VALUE }, 
			method = RequestMethod.GET)
	@ResponseBody
	public Map<?,?> login(
			@RequestParam(value="username") String username,
			@RequestParam(value="password") String password
			) throws Exception {
		
		return adminService.getLdapEntryWithUsernamePassword(username, password);
	}
	
}
