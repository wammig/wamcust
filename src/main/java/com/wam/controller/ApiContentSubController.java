package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LbuContentSub;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuContentSubs;
import com.wam.model.mvc.response.helper.AccountFormJsonResponse;
import com.wam.model.mvc.response.helper.ContentSubFormJsonResponse;
import com.wam.service.AdminService;
import com.wam.service.WamService;

@Controller
public class ApiContentSubController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	
	@RequestMapping(value = "/api/contentSub/listByAccountId/{accountId}/{setId}", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public List<LbuContentSub> contentSubListByAccountId(
			ModelMap model,
			@PathVariable(value="accountId", required = true) long accountId,
			@PathVariable(value="setId", required = true) String setId
			) throws Exception 
	{
		LbuContentSubs lbuContentSubs = (LbuContentSubs)wamService.contentSubWamService.lbuContentSubSearch(
				setId, 
				String.valueOf(accountId),
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null
				);

		return lbuContentSubs.getList();
	}
	
	@RequestMapping(value = "/api/contentSub/update", produces = { MediaType.APPLICATION_JSON_VALUE }, 
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity updateAccount(@ModelAttribute("contentSubForm") @Validated LbuContentSub contentSub,
	         BindingResult result) throws Exception {
		
		try {
			HttpStatus httpStatus = HttpStatus.OK;	
			
			return ResponseEntity.status(httpStatus).body(adminService.updateContentSub(contentSub, result));
		} catch(Exception ex) {
			HttpStatus httpStatus = HttpStatus.BAD_REQUEST;	
			
			return ResponseEntity.status(httpStatus).body(ex.getMessage());
		}
		
	}
}
