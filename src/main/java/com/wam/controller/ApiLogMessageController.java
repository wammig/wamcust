package com.wam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wam.model.admin.AdminDataTables;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.entity.LogMessage;
import com.wam.model.mvc.response.helper.AccountFormJsonResponse;
import com.wam.model.mvc.response.helper.MessageFormJsonResponse;
import com.wam.service.AdminService;
import com.wam.service.WamService;
import com.wam.stored.packages.LogMessagePackage;

@Controller
public class ApiLogMessageController {

	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("LogMessagePackage")
	LogMessagePackage logMessagePackage;
	
	@Autowired
	@Qualifier("AdminService")
	private AdminService adminService;
	
	@RequestMapping(value = "/api/logmessage/list", 
			method = RequestMethod.GET, 
			produces={"application/json","application/xml"})
	@ResponseBody
	public AdminDataTables logMessageList(
			ModelMap model,
			@RequestParam(value="messageId", required = false) String messageId,
			@RequestParam(value="processType", required = false) String processType,
			@RequestParam(value="message", required = false) String message,
			@RequestParam(value="transId", required = false) String transId,
			@RequestParam(value="actionId", required = false) String actionId,
			@RequestParam(value="status", required = false) String status,
			@RequestParam(value="start", required = false) Integer fromNumber,
			@RequestParam(value="length", required = false) Integer numberPerPage,
			@RequestParam(value="columns[0][search][value]", required = false) String searchValue0,
			@RequestParam(value="columns[1][search][value]", required = false) String searchValue1,
			@RequestParam(value="columns[2][search][value]", required = false) String searchValue2,
			@RequestParam(value="columns[3][search][value]", required = false) String searchValue3,
			@RequestParam(value="columns[4][search][value]", required = false) String searchValue4,
			@RequestParam(value="columns[5][search][value]", required = false) String searchValue5,
			@RequestParam(value="search[value]", required = false) String searchValue
			) throws Exception
	{
		
		if(searchValue != "")
		{
			messageId = searchValue;
			processType = searchValue;
			message = searchValue;
			transId = searchValue;
			actionId = searchValue;
			status = searchValue;
			
		} else {
			
			if(searchValue0 != "")
			{
				messageId = searchValue0;
			}
			
			if(searchValue1 != "")
			{
				processType = searchValue1;
			}
			
			if(searchValue2 != "")
			{
				message = searchValue2;
			}
			
			if(searchValue3 != "")
			{
				transId = searchValue3;
			}
			
			if(searchValue4 != "")
			{
				actionId = searchValue4;
			}
			
			if(searchValue5 != "")
			{
				status = searchValue5;
			}
			
		}
		
		
		
		AdminDataTables adminDataTables = (AdminDataTables)logMessagePackage
				.LogMessageSearch2(messageId, processType, message, transId, actionId, status, fromNumber, numberPerPage)
				;
		
		return adminDataTables;
	}
	
	
	@RequestMapping(value = "/admin/logmessage/update", produces = { MediaType.APPLICATION_JSON_VALUE }, 
			method = RequestMethod.POST)
	@ResponseBody
	public MessageFormJsonResponse updateMessage(@ModelAttribute("messageForm") @Validated LogMessage message,
	         BindingResult result) throws Exception {
		
		return adminService.updateMessage(message, result);
	}
	
}
