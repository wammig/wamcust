package com.wam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wam.model.config.admin.SearchFilterList;
import com.wam.model.db.gbsd.entity.LbuAccount;
import com.wam.model.db.gbsd.helper.LbuAccounts;
import com.wam.model.db.gbsd.helper.LbuLocations;
import com.wam.model.ldap.AdminUser;
import com.wam.service.WamService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.apache.ignite.internal.processors.security.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static java.lang.System.out;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

@Controller
@PropertySource("classpath:errors.properties")
public class AdminAccountController {
	
	@Autowired
	@Qualifier("WamService")
	private WamService wamService;
	
	@Autowired
	@Qualifier("searchAccountFilterList")
	private SearchFilterList searchAccountFilterList;
	
	public static final String ACCOUNT_LIST = "admin/account/list";
	public static final String ACCOUNT_LIST2 = "admin/account/list2";
	public static final String ACCOUNT_SHOW = "admin/account/show";
	
	/*
	@RequestMapping(value = "admin/account/list", 
			method = RequestMethod.GET)
	public String accountList(
			ModelMap model,
			@RequestParam(value="lbuSetId", required = false) String lbuSetId,
			@RequestParam(value="lbuAccountId", required = false) String lbuAccountId,
			@RequestParam(value="gbsAccountId", required = false) String gbsAccountId,
			@RequestParam(value="glpAccountId", required = false) String glpAccountId,
			@RequestParam(value="accountName1", required = false) String accountName1,
			@RequestParam(value="accountName2", required = false) String accountName2,
			@RequestParam(value="salesRepId", required = false) String salesRepId,
			@RequestParam(value="accountType", required = false) String accountType,
			@RequestParam(value="zipCode", required = false) String zipCode,
			@RequestParam(value="countryCode", required = false) String countryCode,
			@RequestParam(value="billableStatus", required = false) String billableStatus,
			@RequestParam(value="addressLine1", required = false) String addressLine1,
			HttpServletRequest request
			) throws Exception 
	{	
		
		LbuAccounts lbuAccounts = (LbuAccounts)wamService.accountWamService.searchLbuAccount(
				lbuSetId, 
				lbuAccountId, 
				gbsAccountId, 
				glpAccountId, 
				accountName1, 
				accountName2, 
				salesRepId, 
				accountType, 
				zipCode, 
				countryCode, 
				billableStatus, 
				addressLine1
			);
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String lbuAccountsJson = mapper.writeValueAsString(lbuAccounts.getList());
			String filterList = mapper.writeValueAsString(searchFilterList.getList());
			model.addAttribute("searchFilterList", filterList);
			model.addAttribute("lbuAccountsJson", lbuAccountsJson);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ACCOUNT_LIST;
	}
	*/
	
	@RequestMapping(value = "admin/account/list", 
			method = RequestMethod.GET)
    public Object redirectWithUsingRedirectView(
      RedirectAttributes attributes) {
        return "redirect:" + "list2";
    }
	
	@RequestMapping(value = "admin/account/list2", 
			method = RequestMethod.GET)
	public String accountList2(
			ModelMap model,
			HttpServletRequest request
			) throws Exception 
	{	
		LbuAccounts lbuAccounts = new LbuAccounts();
		lbuAccounts.setList(new ArrayList());
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String lbuAccountsJson = mapper.writeValueAsString(lbuAccounts.getList());
			String filterList = mapper.writeValueAsString(searchAccountFilterList.getList());
			model.addAttribute("searchFilterList", filterList);
			model.addAttribute("lbuAccountsJson", lbuAccountsJson);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ACCOUNT_LIST2;
	}
	
	@RequestMapping(value = "admin/account/{accountId}/{setId}", 
			method = RequestMethod.GET)
	public String accountShow(
			ModelMap model,
			@PathVariable(value="accountId", required = true) long accountId,
			@PathVariable(value="setId", required = true) String setId
			) throws Exception 
	{		
		LbuAccount lbuAccount = (LbuAccount)wamService.accountWamService.selectLbuAccountById(String.valueOf(accountId), setId);
		LbuLocations lbuLocations = wamService.locationWamService.lbuLocationSearch(setId, String.valueOf(accountId), null, null, null, null, null, null, null);
		
		model.addAttribute("lbuAccount", lbuAccount);
		model.addAttribute("lbuLocationsList", lbuLocations.getList());
		
		return ACCOUNT_SHOW;
		
	}
	
	
}
