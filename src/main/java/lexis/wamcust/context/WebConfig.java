package lexis.wamcust.context;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.wam.model.ldap.AdminUser;
import com.wam.utility.definition.Definition;

@EnableWebMvc
@Configuration
@ComponentScan
public class WebConfig implements WebMvcConfigurer {
    // All web configuration will go here
	
	
	@Bean
	public ViewResolver internalResourceViewResolver() {
	    InternalResourceViewResolver bean = new InternalResourceViewResolver();
	    bean.setViewClass(JstlView.class);
	    bean.setPrefix("/WEB-INF/jsp/");
	    bean.setSuffix(".jsp");
	    
	    /**
	     * activate session in jsp
	     */
	    bean.setExposeContextBeansAsAttributes(true);
	    
	    return bean;
	}
	
	 @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
          .addResourceHandler("/front/**")
          .addResourceLocations("/WEB-INF/static/"); 
    }
	 
	 @Bean
	 @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	 public AdminUser adminUser() {
	     return new AdminUser();
	 } 
	 
	 @Bean
	 @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	 public Map<String, String> roles() {
	     Map<String, String> roles = new HashMap<String, String>();
	     roles.put(Definition.ROLE_ALL, Definition.ROLE_ALL);
	     roles.put(Definition.ROLE_SEND, Definition.ROLE_SEND);
	     roles.put(Definition.ROLE_READ_ONLY, Definition.ROLE_READ_ONLY);
	     
		return roles;
	 } 
	 

	 
	 

}
