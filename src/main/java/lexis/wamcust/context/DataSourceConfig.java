package lexis.wamcust.context;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DataSourceConfig {

	@Primary
	@Bean(name = "dataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource firstDataSource() {
		DataSource dataSource = DataSourceBuilder.create().build();

		return dataSource;
	}
//	
//	@Bean(name = "dataSourceMig")
//	@ConfigurationProperties(prefix = "spring.datasource2")
//	public DataSource secondDataSource() {
//		DataSource dataSource = DataSourceBuilder.create().build();
//		return dataSource;
//	}
	
}
