package lexis.wamcust;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.ApplicationContext;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.wam.service.WorkFlowManagerService;


@SpringBootApplication
//@EnableScheduling
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
@ComponentScan({"lexis.wamcust", "com.wam"})
@ImportResource({ "classpath*:workflow-mail-context.xml", "classpath*:order-beans-context.xml", 
	"classpath*:ecm-beans-context.xml", "classpath*:admin-beans-account-context.xml", 
	"classpath*:admin-beans-location-context.xml", "classpath*:admin-beans-agreement-context.xml",
	"classpath*:admin-beans-syshistory-context.xml", "classpath*:admin-beans-logmessage-context.xml"})
public class WamcustApplication{

	public final static Logger logger = LoggerFactory.getLogger(WamcustApplication.class);
	
	/**
	 * pass args to ScheduledTasks
	 */
	public static String[] ARGS = {}; 
	
	public static void main(String[] args) throws Exception {
			
		ApplicationContext ctx = SpringApplication.run(WamcustApplication.class, args);
		
		ARGS = args;
		
		/**
		 * comment this call when web mode
		 * it's just for batch mode
		 */
		if(args.length > 0)
		{	
			String argsString = Arrays.asList(args).toString();
			
			if(argsString.contains("batch"))
			{
				triggerBatch(ctx, args);
			}
		}
		
	}
	
	public static void triggerBatch(ApplicationContext ctx, String[] args) throws Exception
	{
		WorkFlowManagerService.main(args);
		
        // ...determine it's time to stop...
        int exitCode = SpringApplication.exit(ctx, new ExitCodeGenerator() {
            @Override
            public int getExitCode() {
                // no errors
                return 0;
            }
        });
        System.exit(exitCode);
	}
	
	
	/**
	 * comment this method when batch mode
	 */
//	@Bean
//    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//        return args -> {
//
//            System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//            String[] beanNames = ctx.getBeanDefinitionNames();
//            Arrays.sort(beanNames);
//            for (String beanName : beanNames) {
//                System.out.println(beanName);
//            }
//
//        	
//
//        };
//    }

}

