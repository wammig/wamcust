

$(document).ready(function () {
	$('#modify').click(function(){
		$('#logMessageTable #actionId').addClass('hidden');
		$('#logMessageTable #actionIdInput').removeClass('hidden');
		
		$('#logMessageTable #status').addClass('hidden');
		$('#logMessageTable #statusInput').removeClass('hidden');
		
		$('#cancel').removeClass('hidden');
		$('#save').removeClass('hidden');
		$('#modify').addClass('hidden');
	});
	
	$('#cancel').click(function(){

		$('#logMessageTable #actionId').removeClass('hidden');
		$('#logMessageTable #actionIdInput').addClass('hidden');
		
		$('#logMessageTable #status').removeClass('hidden');
		$('#logMessageTable #statusInput').addClass('hidden');
		
		$('#cancel').addClass('hidden');
		$('#save').addClass('hidden');
		$('#modify').removeClass('hidden');
		$('#logMessageTable .errorMessage').addClass('hidden');
		$('#logMessageTable .input').removeClass('error');
		
		recoverOriginalDataAccount();
	});
	
	$('#save').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		
		$.post({
	         url : $('form#messageForm').attr('action'),
	         data : $('form#messageForm').serialize(),
	         success : function(data) {
	        	 
	        	 $('.errorMessage').addClass('hidden');
	        	 $('.input').removeClass('error');
	        	 $(".ibox-content").toggleClass('sk-loading');
	        	 
	        	 if(!data.validated && typeof data.errorMessages != undefined) {
	        		 
	        		 $.each(data.errorMessages,function(key,value){
	       	            $('span#'+key+'ErrorMessage').removeClass('hidden').find('.error').text(value);
	       	            $('#'+key+'Input').addClass('error');
	                 });
	        	 } else {
	        		 accountForm = data.accountForm;

	        		 swal({
	        		      title: "Message updated", 
	        		      text: "Message " + data.messageForm.messageId + " updated", 
	        		      type: "success",
	        		      showCancelButton: false
	        		 }, function() {
	        		    	 $(location).attr('href', urlWindow);
	        		 });
	        	 }
	        	 
	        	 
	         }
		});
	});
});

function recoverOriginalDataAccount()
{
	$("input.input, select.select").each(function(key, element) {
		var key = $(element).attr('name');
		
		if(typeof messageForm [key] != undefined) {
			$(this).val(messageForm [key]);
		}
	});
}