
try{
	var logMessageListTable;
	$(document).ready(function(){
		logMessageListTable = $('#logMessageListTable').DataTable( {
			  "processing": true,
		      "serverSide": true,
		      "deferLoading": 0,
			  "ajax": baseContextUrl + 'api/logmessage/list.json',
		    columns: [
	            { data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/logmessage/' + data.messageId + '">' + data.messageId + '</a>'
	            }, title: 'messageId' },
	            { data: 'processType', title: 'entityId'},       
	            { data: function(data, type, row) {
	            	if(data.message.indexOf("http://") != -1)
	            	{
	            		return "<a target='_blank' href='" + data.message + "'>" + data.message + "</a>";
	            	}	
	            	return data.message;
	            }, title: 'message' },
	            { data: 'transId', title: 'transId' },
	            { data: 'actionId', title: 'actionId' },
	            { data: 'status', title: 'status' },
	            { data: 'createdDatetime', title: 'createdDatetime' },
	            { data: 'updatedDatetime', title: 'updatedDatetime' }
	        ],
	        ordering: false,
	        pageLength: 25,
	        responsive: true,
	        initComplete: function(settings, json) {

	        }
		} );
		
		
		
		/**
		 * remove the default search bar
		 */
		$('#logMessageListTable_filter').find('input[type=search]').first().first().hide();
		
		$('#logMessageListTable_length').parent().addClass('col-sm-4');
		$('#logMessageListTable_filter').addClass('pull-left');
		
		/**
		 * search select component
		 */
		 
		 var select = $('<select id="searchByField" class="form-control input-sm"></select>')
	        .appendTo(
	        		'div#logMessageListTable_filter'
	        )
	        .on('keyup change', function(){
	        	
	        	if($('#searchInput'+$(this).val()).val() == '') {
	        		localStorage.removeItem('searchByFieldForLogMessage');
	        	}
	        	
	        	$('.searchInput').addClass('hidden');
	        	$('#searchInput'+$(this).val()).removeClass('hidden');
	        })
	        ;
		 
		 var index_filter = 0;
		 searchFilterList.forEach(function(element){
			    if(element.label.indexOf('lobal') !== -1)
			    {
			    	$(select).append("<option value=''>" +element.label+ "</option>");
			    } else {
			    	 $(select).append("<option value='" + index_filter++ + "'>" +element.label+ "</option>");
			    }
		 });
		 
		 
		 /**
		  * search input component
		  */
		 
		 var searchComponent = 
			 '<input type="text" id="searchInput" class="form-control input-sm searchInput" />'+
			 '<input type="text" id="searchInput0" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput1" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput2" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput3" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput4" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput5" class="form-control input-sm searchInput hidden" />'
			 ;
		 
		 var search = $(searchComponent)
		 .appendTo(
	        		'div#logMessageListTable_filter'
	     )
	     .on( 'keyup', function () {
	    	 
	    	    $(select).prop( "disabled", true );
	    	 	localStorage.setItem("searchInputForLogMessage", $('#searchInput'+$('#searchByField').val()).val());
	    	 	localStorage.setItem("searchByFieldForLogMessage", $('#searchByField').val());
				if($('#searchInput' + $('#searchByField').val()).val() == '') {
					$(select).prop( "disabled", false );
					localStorage.removeItem('searchInputForLogMessage');
				}
				
				
				/**
				 * if the text input is more than 3 letters
				 */

	    	 	if($('#searchByField').val() == "") {
	        		globalSearch('logMessageListTable', $('#searchInput'+$('#searchByField').val()).val());
	        	} else {
	        		filterColumnSearch('logMessageListTable', $('#searchByField').val(), $('#searchInput'+$('#searchByField').val()).val());
	        	}

	    	 	
				controlPaging();
	    	 
	     })
		 ;
		 
		 controlPaging();
		 
		 /**
		  * keep the select when go back
		  */
		 if(localStorage.key("searchByFieldForLogMessage")) {
			 if(localStorage.getItem("searchByFieldForLogMessage") != null) {
				 $(select).val(localStorage.getItem("searchByFieldForLogMessage")).change();
			 }
		 }
		 
		 /**
		  * keep the search input when go back
		  */
		 if(localStorage.key("searchInputForLogMessage")) {
			 if(localStorage.getItem("searchInputForLogMessage") != null && localStorage.getItem("searchInputForLogMessage") != "") {
				 $(select).prop( "disabled", true );

				 $('#searchInput'+$('#searchByField').val()).attr('value', localStorage.getItem("searchInputForLogMessage"));
				 $('#searchInput'+$('#searchByField').val()).keyup(); 
			 } 
		 }
		 
		 
		
	});

}
catch(e)
{
	
}

function controlPaging()
{
	if($('#searchInput'+$('#searchByField').val()).val() != "")
	{
		$('#logMessageListTable').removeClass('hidden');
		$('#logMessageListTable_info').removeClass('hidden');
		$('#logMessageListTable_paginate').removeClass('hidden');
		$('#logMessageListTable_length').removeClass('hidden');
	} else {
		$('#logMessageListTable').addClass('hidden');
		$('#logMessageListTable_info').addClass('hidden');
		$('#logMessageListTable_paginate').addClass('hidden');
		$('#logMessageListTable_length').addClass('hidden');
	}	
}

/**
 * launch a global search
 * @param table
 * @param input
 * @returns
 */
function globalSearch ( table, input ) {
	
	$('#' + table).DataTable().search(
    		input, false, false
    ).draw();
}

/**
 * search by one column
 * @param table
 * @param i
 * @param input
 * @returns
 */

function filterColumnSearch ( table, i, input ) {
    
	$('#' + table).DataTable().column( i ).search(
    		input, false, false
    ).draw();
}
