
try{
	var sysHistoryListTable;
	$(document).ready(function(){
		sysHistoryListTable = $('#sysHistoryListTable').DataTable( {
			 "processing": true,
		      "serverSide": true,
		      "deferLoading": 0,
		     //"stateSave": true,
			    "ajax": baseContextUrl + 'api/syshistory/list.json',
		    columns: [
	            { data: function(data, type, row) {
	            	return "<a href='"+ baseContextUrl + 'admin/syshistory/' + data.historyId +"'>"+ data.historyId +"</a>";
	            }, title: 'historyId' },
	            { data: function(data, type, row) {
            		
            		var url = null;
            		
            		if(data.processType == "ACCOUNT")
            		{
            			url = baseContextUrl + 'admin/account/'+ data.entitySeq1 + '/' + data.entitySeq0;
            		}	
            		
            		if(data.processType == "LOCATION")
            		{
            			url = baseContextUrl + 'admin/location/'+ data.entitySeq0 + '/' + data.entitySeq1 + '/' + data.entitySeq2;
            		}	
            		
            		if(data.processType == "AGREEMENT")
            		{
            			url = baseContextUrl + 'admin/agreement/' + data.entitySeq2;
            		}
            			
            		if(url !== null)
            		{
            			return "<a href='"+url+"'>"+data.entityId+"</a>";
            		}	
            		else{
            			return data.entityId;
            		}
	            	
	            }, title: 'entityId'},
	            
	            { data: 'transId', title: 'transId' },
	            { data: 'messageId', title: 'messageId' },
	            { data: 'processType', title: 'processType' },
	            { data: 'sendStatus', title: 'sendStatus' },
	            { data: 'replyStatus', title: 'replyStatus' },
	            { data: 'createdDateTime', title: 'createdDateTime' },
	            { data: 'updatedDatetime', title: 'updatedDatetime' }
	        ],
	        ordering: false,
	        pageLength: 25,
	        responsive: true,
	        initComplete: function(settings, json) {

	        }
		} );
		
		
		
		/**
		 * remove the default search bar
		 */
		$('#sysHistoryListTable_filter').find('input[type=search]').first().first().hide();
		
		$('#sysHistoryListTable_length').parent().addClass('col-sm-4');
		$('#sysHistoryListTable_filter').addClass('pull-left');
		
		/**
		 * search select component
		 */
		 
		 var select = $('<select id="searchByField" class="form-control input-sm"></select>')
	        .appendTo(
	        		'div#sysHistoryListTable_filter'
	        )
	        .on('keyup change', function(){
	        	
	        	if($('#searchInput'+$(this).val()).val() == '') {
	        		localStorage.removeItem('searchByFieldForSysHistory');
	        	}
	        	
	        	$('.searchInput').addClass('hidden');
	        	$('#searchInput'+$(this).val()).removeClass('hidden');
	        })
	        ;
		 
		 var index_filter = 0;
		 searchFilterList.forEach(function(element){
			    if(element.label.indexOf('lobal') !== -1)
			    {
			    	$(select).append("<option value=''>" +element.label+ "</option>");
			    } else {
			    	 $(select).append("<option value='" + index_filter++ + "'>" +element.label+ "</option>");
			    }
		 });
		 
		 
		 /**
		  * search input component
		  */
		 
		 var searchComponent = 
			 '<input type="text" id="searchInput" class="form-control input-sm searchInput" />'+
			 '<input type="text" id="searchInput0" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput1" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput2" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput3" class="form-control input-sm searchInput hidden" />'
			 ;
		 
		 var search = $(searchComponent)
		 .appendTo(
	        		'div#sysHistoryListTable_filter'
	     )
	     .on( 'keyup', function () {
	    	 
	    	    $(select).prop( "disabled", true );
	    	 	localStorage.setItem("searchInputForSysHistory", $('#searchInput'+$('#searchByField').val()).val());
	    	 	localStorage.setItem("searchByFieldForSysHistory", $('#searchByField').val());
				if($('#searchInput' + $('#searchByField').val()).val() == '') {
					$(select).prop( "disabled", false );
					localStorage.removeItem('searchInputForSysHistory');
				}
				
				
				/**
				 * if the text input is more than 3 letters
				 */

	    	 	if($('#searchByField').val() == "") {
	    	 		console.log("globalSearch");
	        		globalSearch('sysHistoryListTable', $('#searchInput'+$('#searchByField').val()).val());
	        	} else {
	        		console.log("filterColumnSearch");
	        		console.log("select " + $('#searchByField').val());
	        		console.log("searchInput " + $('#searchInput'+$('#searchByField').val()).val());
	        		
	        		filterColumnSearch('sysHistoryListTable', $('#searchByField').val(), $('#searchInput'+$('#searchByField').val()).val());
	        	}

	    	 	
				controlPaging();
	    	 
	     })
		 ;
		 
		 controlPaging();
		 
		 /**
		  * keep the select when go back
		  */
		 if(localStorage.key("searchByFieldForSysHistory")) {
			 if(localStorage.getItem("searchByFieldForSysHistory") != null) {
				 $(select).val(localStorage.getItem("searchByFieldForSysHistory")).change();
			 }
		 }
		 
		 /**
		  * keep the search input when go back
		  */
		 if(localStorage.key("searchInputForSysHistory")) {
			 if(localStorage.getItem("searchInputForSysHistory") != null && localStorage.getItem("searchInputForSysHistory") != "") {
				 $(select).prop( "disabled", true );

				 $('#searchInput'+$('#searchByField').val()).attr('value', localStorage.getItem("searchInputForSysHistory"));
				 $('#searchInput'+$('#searchByField').val()).keyup(); 
			 } 
		 }
		 
		 
		
	});

}
catch(e)
{
	
}

function controlPaging()
{
	if($('#searchInput'+$('#searchByField').val()).val() != "")
	{
		$('#sysHistoryListTable').removeClass('hidden');
		$('#sysHistoryListTable_info').removeClass('hidden');
		$('#sysHistoryListTable_paginate').removeClass('hidden');
		$('#sysHistoryListTable_length').removeClass('hidden');
	} else {
		$('#sysHistoryListTable').addClass('hidden');
		$('#sysHistoryListTable_info').addClass('hidden');
		$('#sysHistoryListTable_paginate').addClass('hidden');
		$('#sysHistoryListTable_length').addClass('hidden');
	}	
}

/**
 * launch a global search
 * @param table
 * @param input
 * @returns
 */
function globalSearch ( table, input ) {
	
	$('#' + table).DataTable().search(
    		input, false, false
    ).draw();
}

/**
 * search by one column
 * @param table
 * @param i
 * @param input
 * @returns
 */

function filterColumnSearch ( table, i, input ) {
    
	$('#' + table).DataTable().column( i ).search(
    		input, false, false
    ).draw();
}
