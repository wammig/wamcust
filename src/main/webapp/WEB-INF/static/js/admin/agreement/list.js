
try{
	var agreementListTable;
	$(document).ready(function(){
		agreementListTable = $('#agreementListTable').DataTable( {
			 "processing": true,
		      "serverSide": true,
		      "deferLoading": 0,
		     //"stateSave": true,
			    "ajax": baseContextUrl + 'api/agreement/list.json',
		    columns: [
	            { data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/agreement/' + data.agreementId + '">' + data.agreementId + '</a>'
	            }, title: 'agreementId' },
	            { data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/agreement/asset/' + data.assetAgreementId + '">' + data.assetAgreementId + '</a>'
	            }, title: 'assetAgreementId'},
	            { data: 'productId', title: 'productId' },
	            { data: 'customerPguid', title: 'customerPguid' },
	            { data: 'licenseCount', title: 'licenseCount' },
	            { data: 'beginDate', title: 'beginDate' },
	            { data: 'endDate', title: 'endDate' }
	        ],
	        ordering: false,
	        pageLength: 25,
	        responsive: true,
	        initComplete: function(settings, json) {

	        }
		} );
		
		
		
		/**
		 * remove the default search bar
		 */
		$('#agreementListTable_filter').find('input[type=search]').first().first().hide();
		
		$('#agreementListTable_length').parent().addClass('col-sm-4');
		$('#agreementListTable_filter').addClass('pull-left');
		
		/**
		 * search select component
		 */
		 
		 var select = $('<select id="searchByField" class="form-control input-sm"></select>')
	        .appendTo(
	        		'div#agreementListTable_filter'
	        )
	        .on('keyup change', function(){
	        	
	        	if($('#searchInput'+$(this).val()).val() == '') {
	        		localStorage.removeItem('searchByFieldForAgreement');
	        	}
	        	
	        	$('.searchInput').addClass('hidden');
	        	$('#searchInput'+$(this).val()).removeClass('hidden');
	        })
	        ;
		 
		 var index_filter = 0;
		 searchFilterList.forEach(function(element){
			    if(element.label.indexOf('lobal') !== -1)
			    {
			    	$(select).append("<option value=''>" +element.label+ "</option>");
			    } else {
			    	 $(select).append("<option value='" + index_filter++ + "'>" +element.label+ "</option>");
			    }
		 });
		 
		 
		 /**
		  * search input component
		  */
		 
		 var searchComponent = 
			 '<input type="text" id="searchInput" class="form-control input-sm searchInput" />'+
			 '<input type="text" id="searchInput0" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput1" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput2" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput3" class="form-control input-sm searchInput hidden" />'
			 ;
		 
		 var search = $(searchComponent)
		 .appendTo(
	        		'div#agreementListTable_filter'
	     )
	     .on( 'keyup', function () {
	    	 
	    	 
	    	 	
	    	    $(select).prop( "disabled", true );
	    	 	localStorage.setItem("searchInputForAgreement", $('#searchInput'+$('#searchByField').val()).val());
	    	 	localStorage.setItem("searchByFieldForAgreement", $(select).val());
				if($('#searchInput' + $('#searchByField').val()).val() == '') {
					$(select).prop( "disabled", false );
					localStorage.removeItem('searchInputForAgreement');
				}
				
				
				/**
				 * if the text input is not empty 
				 */
				//if($('#searchInput'+$('#searchByField').val()).val() != "")
		    	//{
		    	 	if($('#searchByField').val() == "") {
		        		globalSearch('agreementListTable', $('#searchInput'+$('#searchByField').val()).val());
		        	} else {
		        		filterColumnSearch('agreementListTable', $(select).val(), $('#searchInput'+$('#searchByField').val()).val());
		        	}
		    	 //}
	    	 	
				controlPaging();
	    	 
	     })
		 ;
		 
		 controlPaging();
		 
		 /**
		  * keep the select when go back
		  */
		 if(localStorage.key("searchByFieldForAgreement")) {
			 if(localStorage.getItem("searchByFieldForAgreement") != null) {
				 $(select).val(localStorage.getItem("searchByFieldForAgreement")).change();
			 }
		 }
		 
		 /**
		  * keep the search input when go back
		  */
		 if(localStorage.key("searchInputForAgreement")) {
			 if(localStorage.getItem("searchInputForAgreement") != null && localStorage.getItem("searchInputForAgreement") != "") {
				 $(select).prop( "disabled", true );

				 $('#searchInput'+$('#searchByField').val()).attr('value', localStorage.getItem("searchInputForAgreement"));
				 $('#searchInput'+$('#searchByField').val()).keyup(); 
			 } 
		 }
		 
		 
		
	});

}
catch(e)
{
	
}

function controlPaging()
{
	if($('#searchInput'+$('#searchByField').val()).val() != "")
	{
		$('#agreementListTable').removeClass('hidden');
		$('#agreementListTable_info').removeClass('hidden');
		$('#agreementListTable_paginate').removeClass('hidden');
		$('#agreementListTable_length').removeClass('hidden');
	} else {
		$('#agreementListTable').addClass('hidden');
		$('#agreementListTable_info').addClass('hidden');
		$('#agreementListTable_paginate').addClass('hidden');
		$('#agreementListTable_length').addClass('hidden');
	}	
}

/**
 * launch a global search
 * @param table
 * @param input
 * @returns
 */
var old_column_index ;
function globalSearch ( table, input ) {
	
	$('#' + table).DataTable().search(
    		input, false, false
    ).draw();
}

/**
 * search by one column
 * @param table
 * @param i
 * @param input
 * @returns
 */

function filterColumnSearch ( table, i, input ) {
	
    old_column_index = i;
    
	$('#' + table).DataTable().column( i ).search(
    		input, false, false
    ).draw();
}
