
function sendAgreementsRequest(agreementsArray)
{
	$.ajax({
		  method: "GET"	,	
		  url: urlSendAgreements,
		  data : {
			  agreements: agreementsArray
		  },
		  statusCode: {
			  400: function(data) {
				  swal({
			            title: "Sent agreements failed!",
			            text: data.responseText,
			            type: "error"
			        });
				  $(".ibox-content").toggleClass('sk-loading');
			  }
		  }
		}).done(function(data) {
			swal({
		        title: "Agreements Sent!",
		        text: data,
		        type: "success"
		    }, function(){
		    	$(location).attr('href', urlWindow);
		    });
			$(".ibox-content").toggleClass('sk-loading');

		});
}
