
/**
 * location list
 */
$('#showLocation').iCheck({
    checkboxClass: 'icheckbox_square-green'
})
.iCheck('uncheck')
.on('ifChecked', function(event){
	
	if(typeof locationListTable == 'undefined')
	{
		createLocations();
	}	
	$("#locationListTable_wrapper").show(500);
	
})
.on('ifUnchecked', function(event){
	
	$("#locationListTable_wrapper").hide(500);
	
});


/**
 * agreement list
 */
$('#showAgreement').iCheck({
    checkboxClass: 'icheckbox_square-green'
})
.iCheck('uncheck')
.on('ifChecked', function(event){
	
	if(typeof agreementProductGroupListTables == 'undefined')
	{
		createAgreementsGroup();
	}	
	$("#agreementGroupListTables").show(500);
	
	displaySendAgreements();
	
})
.on('ifUnchecked', function(event){
	
	$("#agreementGroupListTables").hide(500);
	hideSendAgreements();
	
});



function displaySendAgreements()
{
	setTimeout(function(){

		if($('#agreementGroupListTables .checked').length > 0)
		{
			$("#sendAgreements").show(500);
		} else {
			hideSendAgreements();
		}
	},
	100);
}

function hideSendAgreements()
{
	$("#sendAgreements").hide(500);
}

$(document).ready(function () {
	
	$('#sendAgreements').hide();

	$('#sendAgreements').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		sendAgreements();
	});
	
	$('#sendAccountToEcm').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		sendAccountToEcm();
	});
});


var agreementProductGroupListTables;
var agreementFeatureListTable;


var agreementProductGroupListDatas;
var agreementFeatureListData;

function createAgreementsGroup()
{
	$.ajax({
		method: "GET",
		url: urlAgreementGroupListByAccountId,
	})
    .done(function(data) {
    	for(var groupElement in data){
    		
    		if(typeof agreementProductGroupListDatas == 'undefined')
    		{
    			agreementProductGroupListDatas = [];
    		}	
    		
    		agreementProductGroupListDatas[groupElement] = data[groupElement].list;
    		
    		var agreementGroupListTableId = 'agreementGroupListTable' + groupElement;
    		$('#agreementGroupListTables').append('<table class="table table-striped table-bordered table-hover dataTables" id="'+agreementGroupListTableId+'"></table>');
    		
    		if(typeof agreementProductGroupListTables == 'undefined')
    		{
    			agreementProductGroupListTables = [];
    		}	
    		agreementProductGroupListTables[groupElement] =  $('#'+agreementGroupListTableId).DataTable( {
				data: agreementProductGroupListDatas[groupElement],
				"dom": '<"toolbarProduct">frtip',
				"columnDefs": [
				    { "orderable": false, "targets": 0 }
				  ],
				"order": [[ 2, "desc" ]],  
				"initComplete": function(settings, json) {
					
					setTimeout(function(){
						$('input.i-checks').iCheck({
					        checkboxClass: 'icheckbox_square-green'
					    })
					    .on('ifChecked', function(){
							displaySendAgreements();
					    })
					    .on('ifUnchecked', function(){
							displaySendAgreements();
					    });
					},100);
					
				},
				searching: false,
				paging: false,
			    columns: [
			    	{ data: function(row){
			    		return '<a href="' + baseContextUrl + 'admin/agreement/asset/' + row.assetAgreementId  +'" >' + row.assetAgreementId + '</a>'
			    		+ '<input type="hidden" id="' + row.assetAgreementId + '" />'
			    		;
			    	}, title: 'assetAgreementId' },
			    	{ data: 'agreementId', title: 'agreementId' },
		            { data: 'productId', title: 'productId' },
		            { data: 'beginDate', title: 'beginDate' },
		            { data: 'endDate', title: 'endDate' },
		            { data: 'licenseCount', title: 'licenseCount' },
		            { data: 'customerPguid', title: 'customerPguid' }
		        ],
		        pageLength: 25,
		        responsive: true
    		})
    		;
    		
    		
    		var toolbarProduct = '<ul class="todo-list m-t ui-sortable" id="sendAgreement'+ groupElement +' '+ groupElement +' ">'+
		    '<li>'+
		    '<input type="checkbox" class="i-checks sendAgreement" id="' +groupElement+ '">'+
		    '<span class="m-l-xs" id="sendAgreement'+ groupElement +'Span">Agreement ' + groupElement + '</span>'+
		    '</li>'+
		  '</ul>';
    		$("#agreementGroupListTable"+groupElement+"_wrapper > div.toolbarProduct").html(toolbarProduct);
    	}
    })
	;
}



var locationListTable;

function createLocations()
{
	locationListTable = $('#locationListTable').DataTable( {
		ajax: {
			url: urlLocationListByAccountId,
			dataSrc: ''
		},
		searching: false,
		paging: false,
	    columns: [
	    	{ data: function(row){
	    		return '<a href="' + baseContextUrl + 'admin/location/' + row.lbuSetId + '/' + row.lbuAccountId + '/' + row.lbuLocationId +'" >' + row.lbuLocationId + '</a>';
	    	}, title: 'lbuLocationId' },
	    	{ data: 'lbuAccountId', title: 'lbuAccountId' },
            { data: 'lbuAddressId', title: 'lbuAddressId' },
            { data: 'addressSeq', title: 'addressSeq' },
            { data: 'addressLine1', title: 'addressLine1' },
            { data: 'cityName', title: 'cityName' },
            { data: 'countryName', title: 'countryName' },
            { data: 'zipCode', title: 'zipCode' }
        ],
        pageLength: 25,
        responsive: true
	} );
}

function sendAccountToEcm()
{
    
	$.ajax({
	  method: "GET"	,	
	  url: urlSendAccountToEcm,
	  statusCode: {
		  400: function(data) {
			  swal({
		            title: "Sent to Ecm failed!",
		            text: data.responseText,
		            type: "error"
		        });
			  $(".ibox-content").toggleClass('sk-loading');
		  }
	  }
	}).done(function(data) {
		swal({
            title: "Sent to Ecm!",
            text: data,
            type: "success"
        }, function(){
        	$(location).attr('href', urlWindow);
        });
		$(".ibox-content").toggleClass('sk-loading');

	});
}

function sendAgreements()
{
	var agreementsArray = [];
	var selectedAgreements = $('#agreementGroupListTables .icheckbox_square-green.checked');
	var selectedAgreementsLength = selectedAgreements.length;
	
	$(selectedAgreements).each(function(index){
		var agreementId = $(selectedAgreements[index]).find('input').attr('id');
	
		var assetAgreements = $('#agreementGroupListTable'+agreementId).find('input[type=hidden]');
		
		$(assetAgreements).each(function(index,element){
			console.log($(element).attr('id'));
			var assetAgreementId = $(element).attr('id')
			
			if(typeof assetAgreementId !== 'undefined')
			{
				if( agreementsArray.indexOf(assetAgreementId) == -1 )
				{
					agreementsArray.push(assetAgreementId);
				}
			}	
		});
		
		
	})
	;
	

	sendAgreementsRequest(agreementsArray);
		
}