

$(document).ready(function () {
	$('#modify').click(function(){
		$('#accountTable .span').addClass('hidden');
		$('#accountTable input.input').removeClass('hidden');
		$('#sendAccountToEcm').addClass('hidden');
		$('#cancel').removeClass('hidden');
		$('#saveAccount').removeClass('hidden');
		$('#modify').addClass('hidden');
	});
	
	$('#cancel').click(function(){
		$('#accountTable .span').removeClass('hidden');
		$('#accountTable input.input').addClass('hidden');
		$('#sendAccountToEcm').removeClass('hidden');
		$('#cancel').addClass('hidden');
		$('#saveAccount').addClass('hidden');
		$('#modify').removeClass('hidden');
		$('#accountTable .errorMessage').addClass('hidden');
		$('#accountTable .input').removeClass('error');
		
		recoverOriginalDataAccount();
	});
	
	$('#saveAccount').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		
		$.post({
	         url : $('form#accountForm').attr('action'),
	         data : $('form#accountForm').serialize(),
	         success : function(data) {
	        	 
	        	 $('.errorMessage').addClass('hidden');
	        	 $('.input').removeClass('error');
	        	 $(".ibox-content").toggleClass('sk-loading');
	        	 
	        	 if(!data.validated && typeof data.errorMessages != undefined) {
	        		 
	        		 $.each(data.errorMessages,function(key,value){
	       	            $('span#'+key+'ErrorMessage').removeClass('hidden').find('.error').text(value);
	       	            $('#'+key+'Input').addClass('error');
	                 });
	        	 } else {
	        		 accountForm = data.accountForm;

	        		 swal({
	        		      title: "Account updated", 
	        		      text: "Account " + data.accountForm.lbuAccountId + " updated", 
	        		      type: "success",
	        		      showCancelButton: false
	        		 }, function() {
	        		    	 $(location).attr('href', urlWindow);
	        		 });
	        	 }
	        	 
	        	 
	         }
		});
	});
});

function recoverOriginalDataAccount()
{
	$("input.input, select.select").each(function(key, element) {
		var key = $(element).attr('name');
		
		if(typeof accountForm[key] != undefined) {
			$(this).val(accountForm[key]);
		}
	});
}