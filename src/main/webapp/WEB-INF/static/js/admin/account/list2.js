
try{
	var accountListTable;
	$(document).ready(function(){
		 accountListTable = $('#userListTable').DataTable( {
			 "processing": true,
		      "serverSide": true,
		      "deferLoading": 0,
		     //"stateSave": true,
			    "ajax": baseContextUrl + 'api/account/list2.json',
		    columns: [
	            { data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/account/' + data.lbuAccountId + '/' + data.lbuSetId + '">' + data.lbuAccountId + '</a>'
	            }, title: 'lbuAccountId'},
	            { data: 'accountName1', title: 'accountName1' },
	            { data: 'addressLine1', title: 'addressLine1' },
	            { data: 'cityName', title: 'cityName' },
	            { data: 'countryName', title: 'countryName' },
	            { data: 'zipCode', title: 'zipCode' },
	            { data: 'updatedDatetime', title: 'updatedDatetime' },
	            { data: 'createdDatetime', title: 'createdDatetime' },
	            { data: 'lbuSetId', title: 'lbuSetId' },
	            { data: 'customerPguid', title: 'customerPguid' }
	        ],
	        ordering: false,
	        pageLength: 25,
	        responsive: true,
	        initComplete: function(settings, json) {

	        }
		} );
		
		
		
		/**
		 * remove the default search bar
		 */
		$('#userListTable_filter').find('input[type=search]').first().first().hide();
		
		$('#userListTable_length').parent().addClass('col-sm-4');
		$('#userListTable_filter').addClass('pull-left');
		
		/**
		 * search select component
		 */
		 
		 var select = $('<select id="searchByField" class="form-control input-sm"></select>')
	        .appendTo(
	        		'div#userListTable_filter'
	        )
	        .on('keyup change', function(){
	        	
	        	if($('#searchInput'+$(this).val()).val() == '') {
	        		localStorage.removeItem('searchByFieldForAccount');
	        	}
	        	
	        	$('.searchInput').addClass('hidden');
	        	$('#searchInput'+$(this).val()).removeClass('hidden');
	        })
	        ;
		 
		 var index_filter = 0;
		 searchFilterList.forEach(function(element){
			    if(element.label.indexOf('lobal') !== -1)
			    {
			    	$(select).append("<option value=''>" +element.label+ "</option>");
			    } else {
			    	 $(select).append("<option value='" + index_filter++ + "'>" +element.label+ "</option>");
			    }
		 });
		 
		 
		 /**
		  * search input component
		  */
		 
		 var searchComponent = 
			 '<input type="text" id="searchInput" class="form-control input-sm searchInput" />'+
			 '<input type="text" id="searchInput0" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput1" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput2" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput3" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput4" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput5" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput6" class="form-control input-sm searchInput hidden" />'
			 ;
		 
		 var search = $(searchComponent)
		 .appendTo(
	        		'div#userListTable_filter'
	     )
	     .on( 'keyup', function () {
	    	 
	    	 
	    	 	
	    	    $(select).prop( "disabled", true );
	    	 	localStorage.setItem("searchInputForAccount", $('#searchInput'+$('#searchByField').val()).val());
	    	 	localStorage.setItem("searchByFieldForAccount", $(select).val());
				if($('#searchInput' + $('#searchByField').val()).val() == '') {
					$(select).prop( "disabled", false );
					localStorage.removeItem('searchInputForAccount');
				}
				
				
				/**
				 * if the text input is not empty 
				 */
				//if($('#searchInput'+$('#searchByField').val()).val() != "")
		    	//{
		    	 	if($('#searchByField').val() == "") {
		        		globalSearch('userListTable', $('#searchInput'+$('#searchByField').val()).val());
		        	} else {
		        		filterColumnSearch('userListTable', $(select).val(), $('#searchInput'+$('#searchByField').val()).val());
		        	}
		    	 //}
	    	 	
				controlPaging();
	    	 
	     })
		 ;
		 
		 controlPaging();
		 
		 /**
		  * keep the select when go back
		  */
		 if(localStorage.key("searchByFieldForAccount")) {
			 if(localStorage.getItem("searchByFieldForAccount") != null) {
				 $(select).val(localStorage.getItem("searchByFieldForAccount")).change();
			 }
		 }
		 
		 /**
		  * keep the search input when go back
		  */
		 if(localStorage.key("searchInputForAccount")) {
			 if(localStorage.getItem("searchInputForAccount") != null && localStorage.getItem("searchInputForAccount") != "") {
				 $(select).prop( "disabled", true );
				 $('#searchInput'+$('#searchByField').val()).attr('value', localStorage.getItem("searchInputForAccount"));
				 $('#searchInput'+$('#searchByField').val()).keyup(); 
			 } 
		 }
		 
		 
		
	});

}
catch(e)
{
	
}

function controlPaging()
{
	if($('#searchInput'+$('#searchByField').val()).val() != "")
	{
		$('#userListTable').removeClass('hidden');
		$('#userListTable_info').removeClass('hidden');
		$('#userListTable_paginate').removeClass('hidden');
		$('#userListTable_length').removeClass('hidden');
	} else {
		$('#userListTable').addClass('hidden');
		$('#userListTable_info').addClass('hidden');
		$('#userListTable_paginate').addClass('hidden');
		$('#userListTable_length').addClass('hidden');
	}	
}

/**
 * launch a global search
 * @param table
 * @param input
 * @returns
 */
var old_column_index ;
function globalSearch ( table, input ) {
	
	$('#' + table).DataTable().search(
    		input, false, false
    ).draw();
}

/**
 * search by one column
 * @param table
 * @param i
 * @param input
 * @returns
 */

function filterColumnSearch ( table, i, input ) {
	
    old_column_index = i;
    
	$('#' + table).DataTable().column( i ).search(
    		input, false, false
    ).draw();
}
