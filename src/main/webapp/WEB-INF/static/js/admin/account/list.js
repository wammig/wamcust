
try{
	$(document).ready(function(){
		var accountListTable = $('#userListTable').DataTable( {
		    data: accountList,
		    columns: [
	            { data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/account/' + data.lbuAccountId + '/' + data.lbuSetId + '">' + data.lbuAccountId + '</a>'
	            }, title: 'lbuAccountId'},
	            { data: 'accountName1', title: 'accountName1' },
	            { data: 'accountType', title: 'accountType' },
	            { data: 'addressLine1', title: 'addressLine1' },
	            { data: 'cityName', title: 'cityName' },
	            { data: 'countryName', title: 'countryName' },
	            { data: 'zipCode', title: 'zipCode' },
	            { data: 'updatedDatetime', title: 'updatedDatetime' },
	            { data: 'createdDatetime', title: 'createdDatetime' },
	            { data: 'lbuSetId', title: 'lbuSetId' }
	        ],
	        ordering: false,
	        pageLength: 25,
	        responsive: true
		} );
		
		/**
		 * remove the default search bar
		 */
		$('#userListTable_filter').find('input[type=search]').first().first().hide();
		
		/**
		 * search select component
		 */
		 var select = $('<select id="searchByField" class="form-control input-sm"></select>')
	        .appendTo(
	        		'div#userListTable_filter'
	        )
	        .on('keyup change', function(){
	        	if($(search).val() == '') {
	        		localStorage.removeItem('searchByField');
	        	}
	        })
	        ;
		 
		 searchFilterList.forEach(function(element){
			 if(element.hasOwnProperty('key')) {
				 $(select).append("<option value='" +element.key+ "'>" +element.label+ "</option>");
			 }
		 });
		 
		 
		 /**
		  * search input component
		  */
		 var search = $('<input type="text" id="searchInput" class="form-control input-sm" />')
		 .appendTo(
	        		'div#userListTable_filter'
	     )
	     .on( 'keyup click', function () {
	    	 
	    	 /**
			  * control the select list to avoid the bug in dataTable search
			  */

	    	 	$(select).prop( "disabled", true );
	    	 	localStorage.setItem("searchInput", $(this).val());
	    	 	localStorage.setItem("searchByField", $(select).val());
				if($(search).val() == '') {
					$(select).prop( "disabled", false );
					localStorage.removeItem('searchInput');
				}
				
				
	    	 	if($('#searchByField').val() == "100") {
	        		globalSearch('userListTable', $(this).val());
	        	} else {
	        		filterColumnSearch('userListTable', $(select).val(), $(this).val());
	        	}
	     })
		 ;
		 
		 /**
		  * keep the select when go back
		  */
		 if(localStorage.key("searchByField")) {
			 if(localStorage.getItem("searchByField") != null) {
				 $(select).val(localStorage.getItem("searchByField")).change();
			 }
		 }
		 
		 /**
		  * keep the search input when go back
		  */
		 if(localStorage.key("searchInput")) {
			 if(localStorage.getItem("searchInput") != null) {
				 $(select).prop( "disabled", true );
				 $(search).attr('value', localStorage.getItem("searchInput"));
				 $('input#searchInput').keyup();
				 
			 }
		 }

		
	});

}
catch(e)
{
	
}

/**
 * launch a global search
 * @param table
 * @param input
 * @returns
 */
function globalSearch ( table, input ) {
	$('#' + table).DataTable().search(
    		input, false, false
    ).draw();
}

/**
 * search by one column
 * @param table
 * @param i
 * @param input
 * @returns
 */
function filterColumnSearch ( table, i, input ) {
	$('#' + table).DataTable().column( i ).search(
    		input, false, false
    ).draw();
}
