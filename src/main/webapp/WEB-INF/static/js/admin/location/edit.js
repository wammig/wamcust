$(document).ready(function () {
	/*
	$(".touchSpanInt").TouchSpin({
        min: 0,
        max: 99999,
        step: 1,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
	
	$('.bootstrap-touchspin').addClass('hidden');
	*/
	$('#modify').click(function(){
		var that = $(this);
		var parentDiv = $(that).parent().parent();
		
		$('#locationTable .span').addClass('hidden');
		$('#locationTable input.input').removeClass('hidden');
		$('#cancel').removeClass('hidden');
		$('#save').removeClass('hidden');
		$('#modify').addClass('hidden');
		
		//$('.bootstrap-touchspin').removeClass('hidden');
	});
	
	$('#cancel').click(function(){
		var that = $(this);
		var parentDiv = $(that).parent().parent();
		
		$('#locationTable .span').removeClass('hidden');
		$('#locationTable input.input').addClass('hidden');
		$('#cancel').addClass('hidden');
		$('#save').addClass('hidden');
		$('#modify').removeClass('hidden');
		
		$(parentDiv).find('.errorMessage').addClass('hidden');
		$(parentDiv).find('.input').removeClass('error');
		
		//$('.bootstrap-touchspin').addClass('hidden');
		
		recoverOriginalDataLocation();
	});
	
	if(typeof $(location).attr('hash') != 'undefined')
	{
		if($(location).attr('hash').indexOf('modify') != -1)
		{
			$('#modify').click();
		}	
	}
	
	$('#save').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		
		$.post({
	         url : $('#locationForm').attr('action'),
	         data : $('form#locationForm').serialize(),
	         success : function(data) {
	        	 
	        	 $('.errorMessage').addClass('hidden');
	        	 $('.input').removeClass('error');
	        	 $(".ibox-content").toggleClass('sk-loading');
	        	 
	        	 if(!data.validated && typeof data.errorMessages != undefined) {
	        		 
	        		 $.each(data.errorMessages,function(key,value){
	       	            $('span#'+key+'ErrorMessage').removeClass('hidden').find('.error').text(value);
	       	            $('#'+key+'Input').addClass('error');
	                 });
	        	 } else {
	        		 locationForm = data.locationForm;
	        		 console.log(urlWindow);
	        		 
	        		 swal({
	        		      title: "Location updated", 
	        		      text: "Location " + data.locationForm.lbuLocationId + " updated", 
	        		      type: "success",
	        		      showCancelButton: false
	        		 }, function() {
	        		    	$(location).attr('href', urlWindow);
	        		 });
	        	 }
	        	 
	        	 
	         },
	         error: function(data){
	        	 console.log(data.status);
	        	 console.log(data.statusText);
	        	 //console.log(data);
	        	 
	        	 $(".ibox-content").toggleClass('sk-loading');
	        	 /*
	        	 swal({
	       		      title: "Location Failed", 
	       		      text: "data.statusText", 
	       		      type: "failed",
	       		      showCancelButton: false
	       		 }, function() {
	       		    	 //$(location).attr('href', urlWindow);
	       		 });
	       		 */
	         }
		});
	});
	
});

function recoverOriginalDataLocation()
{
	$("input.input, select.select").each(function(key, element) {
		var key = $(element).attr('name');
		
		if(typeof locationsForm != 'undefined') {
			if(typeof locationsForm[key] != 'undefined') {
				console.log(locationsForm[key]);
				$(this).val(locationsForm[key]);
			}
		}
	});
}