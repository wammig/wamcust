
try{
	var locationListTable;
	$(document).ready(function(){
		 locationListTable = $('#locationListTable').DataTable( {
			 "processing": true,
		      "serverSide": true,
		      "deferLoading": 0,
		     //"stateSave": true,
			    "ajax": baseContextUrl + 'api/location/list.json',
		    columns: [
		    	{ data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/account/' + data.lbuAccountId + '/' + data.lbuSetId + '">' + data.lbuAccountId + '</a>'
	            }, title: 'lbuAccountId'},
	            { data: function(data, type, row) {
	            	return '<a href="' + baseContextUrl + 'admin/location/' + data.lbuSetId + '/' + data.lbuAccountId + '/' + data.lbuLocationId + '">' + data.lbuLocationId + '</a>'
	            }, title: 'lbuLocationId'},
	            { data: 'accountName1', title: 'accountName1' },
	            { data: 'cityName', title: 'cityName' },
	            { data: 'zipCode', title: 'zipCode' },
	            { data: 'countryName', title: 'countryName' },
	            { data: 'addressLine1', title: 'addressLine1' },
	            { data: 'addressLine2', title: 'addressLine2' },
	            { data: 'lbuSetId', title: 'lbuSetId' }
	        ],
	        ordering: false,
	        pageLength: 25,
	        responsive: true,
	        initComplete: function(settings, json) {

	        }
		} );
		
		
		
		/**
		 * remove the default search bar
		 */
		$('#locationListTable_filter').find('input[type=search]').first().first().hide();
		
		$('#locationListTable_length').parent().addClass('col-sm-4');
		$('#locationListTable_filter').addClass('pull-left');
		
		/**
		 * search select component
		 */
		 
		 var select = $('<select id="searchByField" class="form-control input-sm"></select>')
	        .appendTo(
	        		'div#locationListTable_filter'
	        )
	        .on('keyup change', function(){
	        	
	        	if($('#searchInput'+$(this).val()).val() == '') {
	        		localStorage.removeItem('searchByFieldForLocation');
	        	}
	        	
	        	$('.searchInput').addClass('hidden');
	        	$('#searchInput'+$(this).val()).removeClass('hidden');
	        })
	        ;
		 
		 var index_filter = 0;
		 searchFilterList.forEach(function(element){
			    if(element.label.indexOf('lobal') !== -1)
			    {
			    	$(select).append("<option value=''>" +element.label+ "</option>");
			    } else {
			    	 $(select).append("<option value='" + index_filter++ + "'>" +element.label+ "</option>");
			    }
		 });
		 
		 
		 /**
		  * search input component
		  */
		 
		 var searchComponent = 
			 '<input type="text" id="searchInput" class="form-control input-sm searchInput" />'+
			 '<input type="text" id="searchInput0" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput1" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput2" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput3" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput4" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput5" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput6" class="form-control input-sm searchInput hidden" />'+
			 '<input type="text" id="searchInput7" class="form-control input-sm searchInput hidden" />'
			 ;
		 
		 var search = $(searchComponent)
		 .appendTo(
	        		'div#locationListTable_filter'
	     )
	     .on( 'keyup', function () {
	    	 
	    	 
	    	 	
	    	    $(select).prop( "disabled", true );
	    	 	localStorage.setItem("searchInputForLocation", $('#searchInput'+$('#searchByField').val()).val());
	    	 	localStorage.setItem("searchByFieldForLocation", $(select).val());
				if($('#searchInput' + $('#searchByField').val()).val() == '') {
					$(select).prop( "disabled", false );
					localStorage.removeItem('searchInputForLocation');
				}
				
				
				/**
				 * if the text input is not empty 
				 */
				//if($('#searchInput'+$('#searchByField').val()).val() != "")
		    	//{
		    	 	if($('#searchByField').val() == "") {
		        		globalSearch('locationListTable', $('#searchInput'+$('#searchByField').val()).val());
		        	} else {
		        		filterColumnSearch('locationListTable', $(select).val(), $('#searchInput'+$('#searchByField').val()).val());
		        	}
		    	 //}
	    	 	
				controlPaging();
	    	 
	     })
		 ;
		 
		 controlPaging();
		 
		 /**
		  * keep the select when go back
		  */
		 if(localStorage.key("searchByFieldForLocation")) {
			 if(localStorage.getItem("searchByFieldForLocation") != null) {
				 $(select).val(localStorage.getItem("searchByFieldForLocation")).change();
			 }
		 }
		 
		 /**
		  * keep the search input when go back
		  */
		 if(localStorage.key("searchInputForLocation")) {
			 if(localStorage.getItem("searchInputForLocation") != null && localStorage.getItem("searchInputForLocation") != "") {
				 $(select).prop( "disabled", true );

				 $('#searchInput'+$('#searchByField').val()).attr('value', localStorage.getItem("searchInputForLocation"));
				 $('#searchInput'+$('#searchByField').val()).keyup(); 
			 } 
		 }
		 
		 
		
	});

}
catch(e)
{
	
}

function controlPaging()
{
	if($('#searchInput'+$('#searchByField').val()).val() != "")
	{
		$('#locationListTable').removeClass('hidden');
		$('#locationListTable_info').removeClass('hidden');
		$('#locationListTable_paginate').removeClass('hidden');
		$('#locationListTable_length').removeClass('hidden');
	} else {
		$('#locationListTable').addClass('hidden');
		$('#locationListTable_info').addClass('hidden');
		$('#locationListTable_paginate').addClass('hidden');
		$('#locationListTable_length').addClass('hidden');
	}	
}

/**
 * launch a global search
 * @param table
 * @param input
 * @returns
 */
var old_column_index ;
function globalSearch ( table, input ) {
	
	$('#' + table).DataTable().search(
    		input, false, false
    ).draw();
}

/**
 * search by one column
 * @param table
 * @param i
 * @param input
 * @returns
 */

function filterColumnSearch ( table, i, input ) {
	
    old_column_index = i;
    
	$('#' + table).DataTable().column( i ).search(
    		input, false, false
    ).draw();
}
