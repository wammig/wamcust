

$(document).ready(function(){
	$('body').addClass('gray-bg');
	
	$('#login').click(function(){
		if($('#username').val().length == 0)
		{
			$('#username').parent().addClass("has-error");
			
			 swal({
	   		      title: "Windows ID empty", 
	   		      text: "Please input your Windows ID", 
	   		      type: "error",
	   		      showCancelButton: false
	   		 });
			
			return;
		} else {
			$('#username').parent().removeClass("has-error");
		}
		
		if($('#password').val().length == 0)
		{
			$('#password').parent().addClass("has-error");
			
			swal({
	   		      title: "Password empty", 
	   		      text: "Please input your Windows Password", 
	   		      type: "error",
	   		      showCancelButton: false
	   		 });
			
			return;
		} else {
			$('#password').parent().removeClass("has-error");
		}
		
		$('#login-progress').removeClass('hidden');
		$('#login').addClass('hidden');
		
		var password = $('#password').val();
		
		/**
		 * avoid % for the last charactor
		 */
		if(password.charAt(password.length - 1) == '%')
		{
			password = password + '25';
		}	
		
		$.ajax({
	        url : urlLoginApi,
	        data: 'username='+$('#username').val()+'&password='+password,
	        success : function(data) {
	        	
	        	$('#login-progress').addClass('hidden');
	        	$('#login').removeClass('hidden');
	        	
	        	if(data.STATUS == true)
	        	{
	        		swal({
		  	   		      title: "Success", 
		  	   		      text: data.MESSAGE, 
		  	   		      type: "success",
		  	   		      showCancelButton: false
		  	   		 });
	        		
	        		$(location).attr('href', urlHomeApi);
	        		
	        	} else {
	        		swal({
		  	   		      title: "Failed", 
		  	   		      text: data.MESSAGE, 
		  	   		      type: "error",
		  	   		      showCancelButton: false
		  	   		 });
	        		
	        		if(data.ERROR == "username")
	        		{
	        			$('#username').parent().addClass("has-error");
	        		} else {
	        			$('#username').parent().removeClass("has-error");
	        		}	
	        		
	        		if(data.ERROR == "password")
	        		{
	        			$('#password').parent().addClass("has-error");
	        		} else {
	        			$('#password').parent().removeClass("has-error");
	        		}
	        	}
	        }
		});
			
	});
});