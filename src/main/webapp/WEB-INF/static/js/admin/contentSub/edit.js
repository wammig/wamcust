var modifyState = false;

$(document).ready(function () {
	$(".touchSpanFloat").TouchSpin({
        min: 0,
        max: 10000000,
        step: 1,
        decimals: 2,
        forcestepdivisibility: 'none',
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
	
	$(".touchSpanInt").TouchSpin({
        min: 0,
        max: 1000,
        step: 1,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });

	
	$('.bootstrap-touchspin').addClass('hidden');
	
	$('input.dateTime').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        calendarWeeks: true,
        forceParse: true,
        autoclose: true,
        format: "mm-dd-yyyy"

    });

	
	$('#modify').click(function(){
		modifyState = true;
		$('.span').addClass('hidden');
		$('input.input, select.select').removeClass('hidden');
		$('#sendContentSubToEcm').addClass('hidden');
		$('#cancel').removeClass('hidden');
		$('#save').removeClass('hidden');
		$('#modify').addClass('hidden');
		$('#back').addClass('hidden');
		
		$('.bootstrap-touchspin').removeClass('hidden');
	});
	
	$('#cancel').click(function(){
		modifyState = false;
		$('.span').removeClass('hidden');
		$('input.input, select.select').addClass('hidden');
		$('#sendContentSubToEcm').removeClass('hidden');
		$('#cancel').addClass('hidden');
		$('#save').addClass('hidden');
		$('#modify').removeClass('hidden');
		$('#back').removeClass('hidden');
		$('.errorMessage').addClass('hidden');
		$('.input').removeClass('error');
		
		$('.bootstrap-touchspin').addClass('hidden');
		
		recoverOriginalData();
	});	
	
	$('#send').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		
		$.get({
			 url : urlAgreementByContractNum,
	         statusCode: {
	   		    400: function(data) {
	   			  swal({
	   		            title: "Sent failed!",
	   		            text: data.responseText,
	   		            type: "error"
	   		        });
	   			  $(".ibox-content").toggleClass('sk-loading');
	   		    }
	   	  	 },
	         success : function(data) {

	        		 swal({
	        		      title: "Agreement modifications has been sent!", 
	        		      text: "Agreement " + contractNum + " updated", 
	        		      type: "success",
	        		      showCancelButton: false
	        		 }, function() {
	        		    	 $(location).attr('href', urlWindow);
	        		 });
	        	 }
	        	 
	        	 
	         
		});
		
	});
	
	$('#save').click(function(){
		$(".ibox-content").toggleClass('sk-loading');
		
		$.post({
	         url : $('#contentSubForm').attr('action'),
	         data : $('form#contentSubForm').serialize(),
	         statusCode: {
	   		    400: function(data) {
	   			  swal({
	   		            title: "Update order failed!",
	   		            text: data.responseText,
	   		            type: "error"
	   		        });
	   			  $(".ibox-content").toggleClass('sk-loading');
	   		    }
	   	  	 },
	         success : function(data) {
	        	 
	        	 $('.errorMessage').addClass('hidden');
	        	 $('.input').removeClass('error');
	        	 $(".ibox-content").toggleClass('sk-loading');
	        	 
	        	 if(!data.validated && typeof data.errorMessages != undefined) {
	        		 
	        		 $.each(data.errorMessages,function(key,value){
	       	            $('span#'+key+'ErrorMessage').removeClass('hidden').find('.error').text(value);
	       	            $('#'+key+'Input').addClass('error');
	                 });
	        	 } else {
	        		 contentSubForm = data.contentSubForm;

	        		 swal({
	        		      title: "Order updated", 
	        		      text: "Order " + data.contentSubForm.lbuContentSubId + " updated! " + ((data.agreementChanged == true) ? "Don't forget to click on the bouton 'Send Agreements' to Send modifications of Agreements" : ""), 
	        		      type: "success",
	        		      showCancelButton: false
	        		 }, function() {
	        			 if(data.agreementChanged == true) {
	        		    	 $(location).attr('href', urlWindowSend);
	        			 } else {
	        				 $(location).attr('href', urlWindow);
	        			 }
	        		 });
	        	 }
	        	 
	        	 
	         }
		});
	});
});



function recoverOriginalData()
{
	$("input.input, select.select").each(function(key, element) {
		var key = $(element).attr('name');
		
		if(typeof contentSubForm[key] != undefined) {
			$(this).val(contentSubForm[key]);
		}
	});
}