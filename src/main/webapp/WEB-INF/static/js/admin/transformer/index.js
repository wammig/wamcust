var searchParams = window.location.search;

var andCo = {
		origin : "&",
		replacement : "---et-commercial-and-commercial---"
};

var andAid = {
		origin : "&aid",
		replacement : "**etco**aid"
		
};

var andAcn = {
		origin : "&acn",
		replacement : "**etco**acn"
		
};

var andUid = {
		origin : "&uid",
		replacement : "**etco**uid"
		
};

var andDte = {
		origin : "&dte",
		replacement : "**etco**dte"
		
};

var andLbu = {
		origin : "&lbu",
		replacement : "**etco**lbu"
		
};

var andAddr = {
		origin : "&addr",
		replacement : "**etco**addr"
		
};

var andCnum = {
		origin : "&cnum",
		replacement : "**etco**cnum"
		
};

var andCsub = {
		origin : "&csub",
		replacement : "**etco**csub"
		
};

function filterCharacter()
{
	if(searchParams.indexOf(andAid.origin) !== -1)
	{
		searchParams = searchParams.replace(andAid.origin, andAid.replacement);
	}
	
	if(searchParams.indexOf(andAcn.origin) !== -1)
	{
		searchParams = searchParams.replace(andAcn.origin, andAcn.replacement);
	}
	
	if(searchParams.indexOf(andUid.origin) !== -1)
	{
		searchParams = searchParams.replace(andUid.origin, andUid.replacement);
	}
	
	if(searchParams.indexOf(andDte.origin) !== -1)
	{
		searchParams = searchParams.replace(andDte.origin, andDte.replacement);
	}
	
	if(searchParams.indexOf(andLbu.origin) !== -1)
	{
		searchParams = searchParams.replace(andLbu.origin, andLbu.replacement);
	}
	
	if(searchParams.indexOf(andAddr.origin) !== -1)
	{
		searchParams = searchParams.replace(andAddr.origin, andAddr.replacement);
	}
	
	if(searchParams.indexOf(andCnum.origin) !== -1)
	{
		searchParams = searchParams.replace(andCnum.origin, andCnum.replacement);
	}
	
	if(searchParams.indexOf(andCsub.origin) !== -1)
	{
		searchParams = searchParams.replace(andCsub.origin, andCsub.replacement);
	}
	
	/**
	 * check &
	 */
	if(searchParams.indexOf(andCo.origin) !== -1)
	{
		searchParams = searchParams.replace(/&/g, andCo.replacement);
	}
	
	
	/**
	 * replace back
	 */

	if(searchParams.indexOf("**etco**") !== -1)
	{
		searchParams = searchParams.replace(/\*\*etco\*\*/g, "&");
	}
	
	return searchParams;
}


if(typeof $ !== "function")
{
	// it enters here if IE 5,6,7

	filterCharacter();
	
	var loadResult = document.getElementById('load-result');
	loadResult.innerHTML = "<h1 class='pull-left'>Loading...</h1>";
	document.location.href = urlWindowAjax + searchParams;
	
} else {
	
	$(function () {

		filterCharacter();
		
		setTimeout(function(){
			$('#loading-content').removeClass('hidden');
		}, 1000);
		
		$.ajax({
	        url : urlWindowAjax + searchParams,
	        success : function(data) {
	        	
	        	var newDoc = document.open("text/html", "replace");
	            newDoc.write(data);
	            newDoc.close();
	 
	        }
		});
		
	});
	
}	
