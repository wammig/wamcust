function actionCheckSynchronisationAgreements()
{
	if(typeof urlOrderReplyHistory != 'undefined')
	{

		$.ajax({
	        url : urlOrderReplyHistory,
	        success : function(data) {
	        	
	        	var type = 'warning';
	        	var title = 'Waiting';
	        	var text = data.MESSAGE;
	        	if(data.STATUS == 'SUCCESS')
	        	{
	        		type = 'success';
	        		title = 'Finished';
	        		
	        		$('#loadingCheckSynchronisationAgreements').addClass('hidden');
	        	}	
	        	if(data.STATUS == 'FAILED')
	        	{
	        		type = 'error';
	        		title = 'Failed';
	        		
	        		$('#loadingCheckSynchronisationAgreements').addClass('hidden');
	        	}	
	        	
	        	showPopupAgreement(title, text, type);
	        }
		});
	 
		
	}	
	
	
}




function showPopupAgreement(title, text, type)
{
	$(".ibox-content").toggleClass('sk-loading');
	swal({
        title: title,
        text: text,
        type: type
    }, function(){
    	$(".ibox-content").toggleClass('sk-loading');
    });
}
