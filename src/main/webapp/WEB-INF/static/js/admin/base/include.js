function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", filename);
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
    }
    if (typeof fileref!="undefined") {
    	document.getElementsByTagName("head")[0].appendChild(fileref);
    }  
}


function checkMenu(url)
{
	if(url.indexOf('account') !== -1)
	{
		$('#accountListMenu').addClass('landing_link');
	}	
	if(url.indexOf('location') !== -1)
	{
		$('#locationListMenu').addClass('landing_link');
	}	
	if(url.indexOf('agreement') !== -1)
	{
		$('#agreementListMenu').addClass('landing_link');
	}
	if(url.indexOf('syshistory') !== -1)
	{
		$('#sysHistoryListMenu').addClass('landing_link');
	}	
	if(url.indexOf('logmessage') !== -1)
	{
		$('#logMessageListMenu').addClass('landing_link');
	}	
}