
try{
	$(document).ready(function(){
		
		if(typeof urlhomeApi != 'undefined')
		{
			$.ajax({
		        url : urlhomeApi,
		        success : function(data) {
		        	
		        	if($('#P_ACCOUNT_TOTAL').length == 1)
		        	{
		        		$('#P_ACCOUNT_TOTAL').text(data.P_ACCOUNT_TOTAL);
		        	}	
		        	if($('#P_AGREEMENT_TOTAL').length == 1)
		        	{
		        		$('#P_AGREEMENT_TOTAL').text(data.P_AGREEMENT_TOTAL);
		        	}
		        	if($('#P_CONTENTSUB_TOTAL').length == 1)
		        	{
		        		$('#P_CONTENTSUB_TOTAL').text(data.P_CONTENTSUB_TOTAL);
		        	}
		        	if($('#P_LOCATION_TOTAL').length == 1)
		        	{
		        		$('#P_LOCATION_TOTAL').text(data.P_LOCATION_TOTAL);
		        	}
		        	if($('#P_LOGMESSAGE_TOTAL').length == 1)
		        	{
		        		$('#P_LOGMESSAGE_TOTAL').text(data.P_LOGMESSAGE_TOTAL);
		        	}
		        	if($('#P_SYSHISTORY_TOTAL').length == 1)
		        	{
		        		$('#P_SYSHISTORY_TOTAL').text(data.P_SYSHISTORY_TOTAL);
		        	}
		        	
		        	if($('#P_SYS_HISTORY_AGREE_S_TOTAL').length == 1)
		        	{
		        		$('#P_SYS_HISTORY_AGREE_S_TOTAL').text(data.P_SYS_HISTORY_AGREE_S_TOTAL);
		        	}
		        	if($('#P_SYS_HISTORY_AGREE_F_TOTAL').length == 1)
		        	{
		        		$('#P_SYS_HISTORY_AGREE_F_TOTAL').text(data.P_SYS_HISTORY_AGREE_F_TOTAL);
		        	}
		        	if($('#P_SYS_HISTORY_ACCOUNT_S_TOTAL').length == 1)
		        	{
		        		$('#P_SYS_HISTORY_ACCOUNT_S_TOTAL').text(data.P_SYS_HISTORY_ACCOUNT_S_TOTAL);
		        	}
		        	if($('#P_SYS_HISTORY_ACCOUNT_F_TOTAL').length == 1)
		        	{
		        		$('#P_SYS_HISTORY_ACCOUNT_F_TOTAL').text(data.P_SYS_HISTORY_ACCOUNT_F_TOTAL);
		        	}
		        	
		        	if($('#P_LOGMESSAGE_C_TOTAL').length == 1)
		        	{
		        		$('#P_LOGMESSAGE_C_TOTAL').text(data.P_LOGMESSAGE_C_TOTAL);
		        	}
		        	if($('#P_LOGMESSAGE_B_TOTAL').length == 1)
		        	{
		        		$('#P_LOGMESSAGE_B_TOTAL').text(data.P_LOGMESSAGE_B_TOTAL);
		        	}
		        	if($('#P_LOGMESSAGE_E_TOTAL').length == 1)
		        	{
		        		$('#P_LOGMESSAGE_E_TOTAL').text(data.P_LOGMESSAGE_E_TOTAL);
		        	}
		        	if($('#P_LOGMESSAGE_R_TOTAL').length == 1)
		        	{
		        		$('#P_LOGMESSAGE_R_TOTAL').text(data.P_LOGMESSAGE_R_TOTAL);
		        	}
		        }
			});
		}
		
		$('#account_zone').click(function(){
			$(location).attr('href',urlAccountList);
		});
		
		$('#pob_zone').click(function(){
			$(location).attr('href',urlPobList);
		});
		$('#agreement_zone').click(function(){
			$(location).attr('href',urlAgreementList);
		});
		$('#history_zone').click(function(){
			$(location).attr('href',urlHistoryList);
		});
		$('#message_zone').click(function(){
			$(location).attr('href',urlMessageList);
		});
		
		$('#history_agree_s_zone').click(function(){
			$(location).attr('href',urlHistoryList);
		});
		
		$('#history_agree_f_zone').click(function(){
			$(location).attr('href',urlHistoryList);
		});
		
		$('#history_account_s_zone').click(function(){
			$(location).attr('href',urlHistoryList);
		});
		
		$('#history_account_f_zone').click(function(){
			$(location).attr('href',urlHistoryList);
		});
		
		$('#history_message_c_zone').click(function(){
			$(location).attr('href',urlMessageList);
		});
		
		$('#history_message_e_zone').click(function(){
			$(location).attr('href',urlMessageList);
		});
		
		$('#history_message_b_zone').click(function(){
			$(location).attr('href',urlMessageList);
		});
		
		
	} );
}
catch(e)
{
	
}