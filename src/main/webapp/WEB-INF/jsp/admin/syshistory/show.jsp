<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
        <link href="<c:url value="/front/inspinia/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title">
					           <h5>SysHistory</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
										<div class="table-responsive col-lg-12" id="sysHistoryTable">
				                            <table class="table table-striped table-hover">
				                                <tbody>
				                                	<tr>
				                                        <td class="col-md-3"><label class="control-label">historyId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.historyId}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">processType:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.processType}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">processSubType:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.processSubType}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entityId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entityId}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">sendStatus:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.sendStatus}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">replyStatus:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.replyStatus}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">errorCode:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.errorCode}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">errorMessage:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.errorMessage}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">transId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.transId}</span>
				                                        </td>
				                                    </tr>
				                                    
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">createdBy:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.createdBy}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">createdDateTime:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.createdDateTime}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">updatedBy:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.updatedBy}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">updatedDatetime:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.updatedDatetime}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">messageId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.messageId}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entitySeq0:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entitySeq0}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entitySeq1:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entitySeq1}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entitySeq2:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entitySeq2}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entitySeq3:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entitySeq3}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entitySeq4:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entitySeq4}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">entitySeq5:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.entitySeq5}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">dataSourceName:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${sysHistory.dataSourceName}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">message:</label></td>
				                                        <td class="col-md-9">
				                                        	<c:if test="${ not empty sysHistory.message }">
					                                        	<textarea class="form-control" rows="10">
					                                        		${sysHistory.message}
					                                        	</textarea>	
				                                        	</c:if>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">replyMessage:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">
					                                        	<c:if test="${ not empty sysHistory.replyMessage }">
					                                        		<textarea class="form-control" rows="10">${sysHistory.replyMessage}</textarea>
					                                        	</c:if>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                </tbody>
			                                </table>
		                                </div>
 
                                </div>
                                <!-- end row -->
                                
 
                                
                                <div class="row">
                                	<div class="col-lg-12">
	                                	<a href="${baseContextUrl}admin/syshistory/list" class="btn btn-w-m btn-outline btn-primary pull-right">Back to the SysHistory list</a>
                                	</div>
                                </div>
                                
		                    </div>
						   <!-- end content -->	
	       			</div>
	            </div>
	        </div>
	    </div>

	</jsp:body>
</t:adminlayout>