<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
        <link href="<c:url value="/front/inspinia/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" />" rel="stylesheet">
    	<link href="<c:url value="/front/inspinia/css/plugins/datapicker/datepicker3.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/jasny/jasny-bootstrap.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/datapicker/bootstrap-datepicker.js" />"></script>
    	<script src="<c:url value="/front/js/admin/contentSub/edit.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
                	
                		  <!-- title part -->
					      <div class="ibox-title">
					           <h5>ContentSub</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       <!-- content -->
						   <div class="ibox-content">
						   
						   		<!-- row -->
								<div class="row">
									<form action="${baseContextUrl}api/contentSub/update" method="post" name="contentSubForm" id="contentSubForm">
									<div class="table-responsive col-lg-6" id="contentSubTable">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                	<tr>
			                                        <td class="col-md-3"><label class="control-label">contractNum:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="contractNumSpan" class="span">${lbuContentSub.contractNum}</span>
			                                        	<input id="contractNumInput" name="contractNum" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.contractNum}" />
			                                        	<span id="contractNumErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="contractNumError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">contractLineNum:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="contractLineNumSpan" class="span">${lbuContentSub.contractLineNum}</span>
			                                        	<input id="contractLineNumInput" name="contractLineNum" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.contractLineNum}" />
			                                        	<span id="contractLineNumErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="contractLineNumError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">contractLineNumOrigin:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="contractLineNumOriginSpan" class="span">${lbuContentSub.contractLineNumOrigin}</span>
			                                        	<input id="contractLineNumOriginInput" name="contractLineNumOrigin" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.contractLineNumOrigin}" />
			                                        	<span id="contractLineNumOriginErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="contractLineNumOriginError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">contentSubName:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="contentSubNameSpan" class="span">${lbuContentSub.contentSubName}</span>
			                                        	<input id="contentSubNameInput" name="contentSubName" type="text" class="form-control input-sm input hidden" value="${lbuContentSub.contentSubName}" />
			                                        	<span id="contentSubNameErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="contentSubNameError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">lbuAccountId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="lbuAccountIdDescSpan" class="span">${lbuContentSub.lbuAccountId}</span>
			                                        	<input id="lbuAccountIdDescInput" name="lbuAccountId" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.lbuAccountId}" />
			                                        	<span id="lbuAccountIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuAccountIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">lbuContentSubId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="lbuContentSubIdSpan" class="span">${lbuContentSub.lbuContentSubId}</span>
			                                        	<input id="lbuContentSubIdInput" name="lbuContentSubId" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.lbuContentSubId}" />
			                                        	<span id="lbuContentSubIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuContentSubIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">beginDate:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="beginDateSpan" class="span">${lbuContentSub.beginDate}</span>
			                                        	<input id="beginDateInput" name="beginDate" type="text" class="form-control dateTime input-sm input hidden" placeholder="MM-DD-YYYY"  data-mask="99-99-9999" value="${lbuContentSub.beginDate}" />
			                                        	<span id="beginDateErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="beginDateError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">endDate:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="endDateSpan" class="span">${lbuContentSub.endDate}</span>
			                                        	<input id="endDateInput" name="endDate" type="text" class="form-control dateTime input-sm input hidden" placeholder="MM-DD-YYYY"  data-mask="99-99-9999" value="${lbuContentSub.endDate}" />
			                                        	<span id="endDateErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="endDateError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">auditUserId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="auditUserIdSpan" class="span">${lbuContentSub.auditUserId}</span>
			                                        	<input id="auditUserIdInput" name="auditUserId" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.auditUserId}" />
			                                        	<span id="auditUserIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="auditUserIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                </tbody>
		                                </table>
	                                </div>
	                                
	                                <div class="table-responsive col-lg-6" id="addressTable">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">maxUsers:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="maxUsersSpan" class="span">${lbuContentSub.maxUsers}</span>
			                                        	<input id="maxUsersInput" name="maxUsers" type="text" class="form-control touchSpanInt input-sm input hidden" value="${lbuContentSub.maxUsers}" />
			                                        	<span id="maxUsersErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="maxUsersError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr class="hidden">
			                                        <td class="col-md-3"><label class="control-label">monthlyCommitment:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="monthlyCommitmentSpan" class="span">${lbuContentSub.monthlyCommitment}</span>
			                                        	<input id="monthlyCommitmentInput" name="monthlyCommitment" type="text" class="touchSpanFloat form-control input-sm input hidden" value="${lbuContentSub.monthlyCommitment}" />
			                                        	<span id="monthlyCommitmentErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="monthlyCommitmentError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr class="hidden">
			                                        <td class="col-md-3"><label class="control-label">monthlyCap:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="monthlyCapSpan" class="span">${lbuContentSub.monthlyCap}</span>
			                                        	<input id="monthlyCapInput" name="monthlyCap" type="text" class="touchSpanFloat form-control input-sm input hidden" value="${lbuContentSub.monthlyCap}" />
			                                        	<span id="monthlyCapErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="monthlyCapError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">lbuSourcePackageId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="lbuSourcePackageIdSpan" class="span">${lbuContentSub.lbuSourcePackageId}</span>
			                                        	<input id="lbuSourcePackageIdInput" name="lbuSourcePackageId" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.lbuSourcePackageId}" />
			                                        	<span id="lbuSourcePackageIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuSourcePackageIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">updatedDatetime:</label></td>
			                                        <td class="col-md-9">
			                                        	${lbuContentSub.updatedDatetime}
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">createdDatetime:</label></td>
			                                        <td class="col-md-9">
														${lbuContentSub.createdDatetime}
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">lbuSetId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="lbuSetIdSpan" class="span">${lbuContentSub.lbuSetId}</span>
			                                        	<input id="lbuSetIdInput" name="lbuSetId" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.lbuSetId}" />
			                                        	<span id="lbuSetIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuSetIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">lbuLocationId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="lbuLocationIdSpan" class="span">${lbuContentSub.lbuLocationId}</span>
			                                        	<input id="lbuLocationIdInput" name="lbuLocationId" type="text" class="form-control input-sm input hidden" readonly value="${lbuContentSub.lbuLocationId}" />
			                                        	<span id="lbuLocationIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuLocationIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">addressSeq:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="addressSeqSpan" class="span">${lbuContentSub.addressSeq}</span>
			                                        	<input id="addressSeqInput" name="addressSeq" type="text" readonly class="form-control input-sm input hidden" value="${lbuContentSub.addressSeq}" />
			                                        	<span id="addressSeqErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="addressSeqError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">noReNew:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="noReNewSpan" class="span">${lbuContentSub.noReNew}</span>
			                                        	<select id="noReNewInput" name="noReNew" class="form-control select hidden" value="${lbuContentSub.noReNew}">
				                                        	
				                                        	<c:forEach items="${mapContentSubNoReNewList}" var="entry">
				                                        		<option value="${entry.key}" <c:if test="${entry.key == lbuContentSub.noReNew}"> selected </c:if> >${entry.value}</option>
				                                        	</c:forEach>
			                                        	</select>
			                                        	<span id="noReNewErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="noReNewIndError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                </tbody>
	                                	</table>
	                                </div>	
	                                </form>
                                </div>
                                
                                
                                <div class="row">
	                                	<div class="col-sm-6">

	                                	</div>
		                                
		                                <div class="col-sm-6">  
		                                	<div class="col-sm-offset-2 pull-right">
		                                		
		                                		<button type="button" id="cancel" class="btn btn-w-m btn-outline btn-warning  hidden"
		                                		<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
		                                		>
			                                		Cancel
			                                	</button>
			                                	<button type="button" id="save" class="btn btn-w-m btn-success  hidden"
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
		                                			Save
			                                	</button>   
			                                	  
			                                	<button type="button" id="modify" class="btn btn-w-m btn-danger" 
			                                	<c:if test="${  adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
			                                		Modify
			                                	</button>
			                                	
			                                	
			                                	  
				                                	<button type="button" id="send" class="btn btn-w-m btn-primary 
				                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL and  adminUser.role ne roles.ROLE_SEND }">disabled</c:if>
				                                	<c:if test="${not send }">hidden</c:if>">
				                                		Send Agreements
				                                		<span class="loading rhomb"></span>
				                                	</button>

			                                	<a id="back" href="${baseContextUrl}admin/agreement/asset/${lbuContentSub.assetAgreementIdFromDb}" 
			                                	class="btn btn-w-m btn-outline btn-primary <c:if test="${ send }">hidden</c:if>">Back to the Asset Agreement</a>
		                                	</div>
	                                    </div>
                                </div>
						   
						   </div>
						   <!-- end content -->	
                	
                	</div>
	            </div>
	        </div>
	    </div>
    	<script>
    		var urlWindow = '${requestScope['javax.servlet.forward.request_uri']}';
    		var urlWindowSend = urlWindow + '?send=true';
    		var urlAgreementByContractNum = '${baseContextUrl}' + '/api/send/agreementByContractNum/${lbuContentSub.contractNum}' ;
    		var contractNum = '${lbuContentSub.contractNum}';
    		var contentSubForm = {
    				'contractNum' : '${lbuContentSub.contractNum}',
    				'contractLineNum' : '${lbuContentSub.contractLineNum}',
    				'contractLineNumOrigin' : '${lbuContentSub.contractLineNumOrigin}',
    				'contentSubName' : '${lbuContentSub.contentSubName}',
    				'lbuAccountId' : '${lbuContentSub.lbuAccountId}',
    				'lbuContentSubId' : '${lbuContentSub.lbuContentSubId}',
    				'beginDate' : '${lbuContentSub.beginDate}',
    				'endDate' : '${lbuContentSub.endDate}',
    				'auditUserId' : '${lbuContentSub.auditUserId}',
    				'maxUsers' : '${lbuContentSub.maxUsers}',
    				'monthlyCommitment' : '${lbuContentSub.monthlyCommitment}',
    				'monthlyCap' : '${lbuContentSub.monthlyCap}',
    				'lbuSourcePackageId' : '${lbuContentSub.lbuSourcePackageId}',
    				'updatedDatetime' : '${lbuContentSub.updatedDatetime}',
    				'createdDatetime' : '${lbuContentSub.createdDatetime}',
    				'lbuSetId' : '${lbuContentSub.lbuSetId}',
    				'lbuLocationId' : '${lbuContentSub.lbuLocationId}',
    				'addressSeq' : '${lbuContentSub.addressSeq}',
    				'noReNew' : '${lbuContentSub.noReNew}'
    		};
    	</script>
    </jsp:body>
</t:adminlayout>