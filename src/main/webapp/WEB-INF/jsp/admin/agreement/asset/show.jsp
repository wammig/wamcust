<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/jasny/jasny-bootstrap.min.js" />"></script>
    	
    	<script src="<c:url value="/front/js/admin/agreement/send.js" />"></script>
    	<script src="<c:url value="/front/js/admin/agreement/asset/show.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
                	
                		  <!-- title part -->
					      <div class="ibox-title">
					           <h5>Asset Agreement</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
								
									<div class="table-responsive col-lg-12" id="assetAgreementTable">
			                            <table class="table table-striped table-hover">
			                            	<tbody>
			                            		<tr>
			                                        <td class="col-md-3"><label class="control-label">AgreementId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="agreementIdSpan" class="span">
			                                        		<a href="${baseContextUrl}admin/agreement/${lbuAgreement.agreementId}">${lbuAgreement.agreementId}</a>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">AssetAgreementId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="assetAgreementIdSpan" class="span">
			                                        		<a href="${baseContextUrl}admin/agreement/asset/${lbuAgreement.assetAgreementId}">${lbuAgreement.assetAgreementId}</a>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">ProductId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="productIdSpan" class="span">${lbuAgreement.productId}</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">licenseCount</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="licenseCountSpan" class="span">${lbuAgreement.licenseCount}</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">CustomerPguid:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="customerPguidSpan" class="span">${lbuAgreement.customerPguid}</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">BeginDate:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="beginDateSpan" class="span">${lbuAgreement.beginDate}</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">endDate:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="endDateSpan" class="span">${lbuAgreement.endDate}</span>
			                                        </td>
			                                    </tr>
			                            	</tbody>
			                            </table>
		                            </div>
								
								</div>	
								
								<div class="row">
                                	<div class="col-lg-12">
                                		<div class="table-responsive">
			                            	<table class="table table-striped table-bordered table-hover dataTables" id="contentSubsTable">
										        <thead>
										            <tr>
										                <th>CONTENT SUB ID</th>
										                <th>CONTRACT NUM</th>
										                <th>CONTRACT LINE NUM</th>
										                <th>CONTRACT LINE NUM ORIGIN</th>
										                <th>CONTENT SUB NAME</th>
										                <th>AGREEMENT PGUID</th>	
										                <th>ASSET AGREEMENT ID</th>
										                <th>NO RENEW</th>									                
										                <th>BEGIN_DATE</th>
										                <th>END_DATE</th>
										            </tr> 
										            <c:forEach items="${lbuContentSubsList}" var="lbuContentSub">
										            	<tr>
										            		<td><a href="${baseContextUrl}admin/contentSub/${lbuContentSub.lbuAccountId}/${lbuContentSub.lbuSourcePackageId}/${lbuContentSub.contractNum}/${lbuContentSub.contractLineNum}">${lbuContentSub.lbuContentSubId}</a></td>
											            	<td><a href="${baseContextUrl}admin/agreement/${lbuContentSub.contractNum}">${lbuContentSub.contractNum}</a></td>
											            	<td>${lbuContentSub.contractLineNum}</td>
											            	<td>${lbuContentSub.contractLineNumOrigin}</td>
											            	<td>${lbuContentSub.contentSubName}</td>
											            	<td>${lbuContentSub.agreementPguid}</td>
											            	<td>${lbuContentSub.assetAgreementId}</td>
											            	<td>${lbuContentSub.noReNew}</td>
											            	<td>${lbuContentSub.beginDate}</td>
											            	<td>${lbuContentSub.endDate}</td>
										            	</tr>
										            </c:forEach>
										        </thead>
										     </table>  
									     </div> 
                                	</div>
	                            </div>    	
								
								<div class="row">
									<div class="col-sm-6">
									</div>
									<div class="col-sm-6">
	                                	<div class="col-sm-offset-2 pull-right">
		                                	<a href="${baseContextUrl}admin/account/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}" class="btn btn-w-m btn-outline btn-primary">Back to the Account</a>
	                                	</div>
	                                </div>	
                                </div>
								
						   </div>
						   <!-- end content -->	
				    </div>
	            </div>
	        </div>
	    </div>
    	<script>
    		var urlWindow = '${requestScope['javax.servlet.forward.request_uri']}';
    		var assetAgreementId = '${lbuAgreement.assetAgreementId}';
    		var urlSendAgreements = baseContextUrl + 'api/send/agreements';
    	</script>
    </jsp:body>
</t:adminlayout>