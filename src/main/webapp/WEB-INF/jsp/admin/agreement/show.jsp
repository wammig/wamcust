<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/jasny/jasny-bootstrap.min.js" />"></script>
    	
    	<script src="<c:url value="/front/js/admin/agreement/send.js" />"></script>
    	<script src="<c:url value="/front/js/admin/agreement/show.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
                	
                		  <!-- title part -->
					      <div class="ibox-title">
					           <h5>Agreement</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
								
									<div class="table-responsive col-lg-12" id="assetAgreementTable">
			                            <table class="table table-striped table-hover">
			                            	<tbody>
			                            		<tr>
			                                        <td class="col-md-3"><label class="control-label">AgreementId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="agreementIdSpan" class="span">
			                                        		<a href="${baseContextUrl}admin/agreement/${firstLbuAgreement.agreementId}">${firstLbuAgreement.agreementId}</a>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td class="col-md-3"><label class="control-label">CustomerPguid:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="customerPguidSpan" class="span">${firstLbuAgreement.customerPguid}</span>
			                                        </td>
			                                    </tr>

			                            	</tbody>
			                            </table>
		                            </div>
								
								</div>	
								  	
								<div class="row">
                                	<div class="col-lg-12">
                                		<div class="table-responsive">
			                            	<table class="table table-striped table-bordered table-hover dataTables" id="assetListTable">
										        <thead>  
									        		<th>AgreementId</th>
									        		<th>AssetAgreementId</th>
									        		<th>ProductId</th>
									        		<th>BeginDate</th>
									        		<th>EndDate</th>
									        		<th>LicenseCount</th>
									        		<th>CustomerPguid</th>
												</thead>
												<c:forEach items="${lbuAgreements}" var="lbuAgreement">
										            	<tr>
										            		<td><a href="${baseContextUrl}admin/agreement/${lbuAgreement.agreementId}">${lbuAgreement.agreementId}</a></td>
										            		<td><a href="${baseContextUrl}admin/agreement/asset/${lbuAgreement.assetAgreementId}">${lbuAgreement.assetAgreementId}</a></td>
										            		<td>${lbuAgreement.productId}</td>
										            		<td>${lbuAgreement.beginDate}</td>
										            		<td>${lbuAgreement.endDate}</td>
										            		<td>${lbuAgreement.licenseCount}</td>
										            		<td>${lbuAgreement.customerPguid}</td>
										            	</tr>
										        </c:forEach>    	
										     </table>  
									     </div> 
                                	</div>
	                            </div>    	
	                            
								<div class="row">
									<div class="col-sm-6">
									</div>
									<div class="col-sm-6">
	                                	<div class="col-sm-offset-2 pull-right">
		                                	<a href="${baseContextUrl}admin/account/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}" class="btn btn-w-m btn-outline btn-primary">Back to the Account</a>
	                                		
	                                		<button type="button" id="sendAgreement" class="btn btn-primary" 
	                                		<c:if test="${ adminUser.role ne roles.ROLE_ALL and adminUser.role ne roles.ROLE_SEND }">disabled</c:if> >
		                                		Send agreement
		                                	</button>
	                                	</div>
	                                </div>	
                                </div>
								
						   </div>
						   <!-- end content -->	
				    </div>
	            </div>
	        </div>
	    </div>
    	<script>
    		var urlWindow = '${requestScope['javax.servlet.forward.request_uri']}';
    		var agreementsArray = [];
    		var urlSendAgreements = baseContextUrl + 'api/send/agreements';
    		
    		<c:forEach items="${lbuAgreements}" var="lbuAgreement">
    			agreementsArray.push('${lbuAgreement.assetAgreementId}');
    		</c:forEach>  
    	</script>
    </jsp:body>
</t:adminlayout>