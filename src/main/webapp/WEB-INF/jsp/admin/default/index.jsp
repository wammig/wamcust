<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/js/admin/default/index.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            		<div class="col-lg-4" id="account_zone">
            			<div class="widget navy-bg p-lg text-center">
                        <div class="m-b-md">
                            <i class="fa fa-users fa-4x"></i>
                            <h1 class="m-xs" id="P_ACCOUNT_TOTAL"></h1>
                            <h3 class="font-bold no-margins">
                                LbuAccount
                            </h3>
                        </div>   
                    	</div>
                    </div>
					<div class="col-lg-4" id="pob_zone">
            			<div class="widget yellow-bg p-lg text-center">
	                        <div class="m-b-md">
	                            <i class="fa fa-globe fa-4x"></i>
	                            <h1 class="m-xs" id="P_LOCATION_TOTAL"></h1>
	                            <h3 class="font-bold no-margins">
	                                LbuLocation
	                            </h3>
	                        </div>
                    	</div>
                    </div>
                    <div class="col-lg-4" id="agreement_zone">
            			<div class="widget lazur-bg p-lg text-center">
	                        <div class="m-b-md">
	                            <i class="fa fa-database fa-4x"></i>
	                            <h1 class="m-xs" id="P_AGREEMENT_TOTAL"></h1>
	                            <h3 class="font-bold no-margins">
	                                Agreement
	                            </h3>
	                        </div>
                    	</div>
                    </div>
              </div>
              <div class="row">    
                    <div class="col-lg-4" id="contentsub_zone">
            			<div class="widget p-lg text-center">
	                        <div class="m-b-md">
	                            <i class="fa fa-file-text fa-4x"></i>
	                            <h1 class="m-xs" id="P_CONTENTSUB_TOTAL"></h1>
	                            <h3 class="font-bold no-margins">
	                                Contract
	                            </h3>
	                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4" id="history_zone">
            			<div class="widget p-lg text-center">
	                        <div class="m-b-md">
	                            <i class="fa fa-bell fa-4x"></i>
	                            <h1 class="m-xs" id="P_SYSHISTORY_TOTAL"></h1>
	                            <h3 class="font-bold no-margins">
	                                SysHistory
	                            </h3>
	                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4" id="message_zone">
            			<div class="widget p-lg text-center">
	                        <div class="m-b-md">
	                            <i class="fa fa-twitch fa-4x"></i>
	                            <h1 class="m-xs" id="P_LOGMESSAGE_TOTAL"></h1>
	                            <h3 class="font-bold no-margins">
	                                LogMessage
	                            </h3>
	                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-2">
                    
                    	<div class="widget style1 navy-bg" id="history_agree_s_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-database fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Success</span>
	                                <h2 id="P_SYS_HISTORY_AGREE_S_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="widget style1 red-bg" id="history_agree_f_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-database fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Failure</span>
	                                <h2 id="P_SYS_HISTORY_AGREE_F_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
                        
                    </div>
                    
                    
                    <div class="col-lg-2">
                    
                    	<div class="widget style1 navy-bg" id="history_account_s_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-users fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Success</span>
	                                <h2 id="P_SYS_HISTORY_ACCOUNT_S_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="widget style1 red-bg" id="history_account_f_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-users fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Failure</span>
	                                <h2 id="P_SYS_HISTORY_ACCOUNT_F_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
                        
                    </div>
                    
                    <div class="col-lg-2">
                    
                        <div class="widget style1 navy-bg" id="history_message_c_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-twitch fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Complete</span>
	                                <h2 id="P_LOGMESSAGE_C_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
	                    
	                    <div class="widget style1 red-bg" id="history_message_e_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-twitch fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Error</span>
	                                <h2 id="P_LOGMESSAGE_E_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
                        
                    </div>
                    
                    <div class="col-lg-2">
                    
                    
                    	<div class="widget style1 yellow-bg" id="history_message_b_zone">
                        	<div class="row">
	                            <div class="col-xs-3">
	                                <i class="fa fa-twitch fa-5x"></i>
	                            </div>
	                            <div class="col-xs-9 text-right">
	                            	<span>Not Finished</span>
	                                <h2 id="P_LOGMESSAGE_B_TOTAL"></h2>
	                            </div>
	                        </div>
	                    </div>
	                   
                       
                        
                    </div>
                    
                    
                    
	        </div>
	    </div>

        <script> 
        	var urlhomeApi = baseContextUrl + 'api/index.json';
        	var urlAccountList = baseContextUrl + 'admin/account/list2';
        	var urlPobList = baseContextUrl + 'admin/location/list';
        	var urlAgreementList = baseContextUrl + 'admin/agreement/list';
        	var urlHistoryList = baseContextUrl + 'admin/syshistory/list';
        	var urlMessageList = baseContextUrl + 'admin/logmessage/list';
        </script>
    </jsp:body>
</t:adminlayout>