<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
        <link href="<c:url value="/front/inspinia/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/js/admin/location/edit.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title">
					           <h5>POB</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
									<form action="${baseContextUrl}admin/location/update" method="post" name="locationForm" id="locationForm">
										<div class="table-responsive col-lg-6" id="locationTable">
				                            <table class="table table-striped table-hover">
				                                <tbody>
				                                	<tr>
				                                        <td class="col-md-3"><label class="control-label">lbuAccountId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="lbuAccountIdSpan" class="span">
				                                        		<a href="${baseContextUrl}admin/account/${lbuLocation.lbuAccountId}/${lbuLocation.lbuSetId}">${lbuLocation.lbuAccountId}</a>
				                                        	</span>
				                                        	<input id="lbuAccountIdInput" name="lbuAccountId" type="text" class="form-control input-sm input hidden" readonly value="${lbuLocation.lbuAccountId}" />
				                                        	<span id="lbuAccountIdErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="lbuAccountIdError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">lbuLocationId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="lbuLocationIdSpan" class="span">
				                                        	<a href="${baseContextUrl}admin/location/${lbuLocation.lbuSetId}/${lbuLocation.lbuAccountId}/${lbuLocation.lbuLocationId}">${lbuLocation.lbuLocationId}</a>
				                                        	</span>
				                                        	<input id="lbuLocationIdInput" name="lbuLocationId" type="text" class="form-control input-sm input hidden" readonly value="${lbuLocation.lbuLocationId}" />
				                                        	<span id="lbuLocationIdErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="lbuLocationIdError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">addressSeq:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="addressSeqSpan" class="span">${lbuLocation.addressSeq}</span>
				                                        	<input id="addressSeqInput" name="addressSeq" type="text" class="form-control touchSpanInt input-sm input hidden" readonly value="${lbuLocation.addressSeq}" />
				                                        	<span id="addressSeqErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="addressSeqError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">lbuSetId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="lbuSetIdSpan" class="span">${lbuLocation.lbuSetId}</span>
				                                        	<input id="lbuSetIdInput" name="lbuSetId" type="text" class="form-control input-sm input hidden" readonly value="${lbuLocation.lbuSetId}" />
				                                        	<span id="lbuSetIdErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="lbuSetIdError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">addressPGUID:</label></td>
				                                        <td class="col-md-9">${lbuLocation.addressPGUID}</td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">pobPGUID:</label></td>
				                                        <td class="col-md-9">${lbuLocation.pobPGUID}</td>
				                                    </tr>
				                                </tbody>
			                                </table>
		                                </div>
		                                
		                                <div class="table-responsive col-lg-6" id="locationTable">
				                            <table class="table table-striped table-hover">
				                                <tbody>
				                                	<tr>
				                                        <td class="col-md-3"><label class="control-label">addressLine1:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="addressLine1Span" class="span">${lbuLocation.addressLine1}</span>
				                                        	<input id="addressLine1Input" name="addressLine1" type="text" class="form-control input-sm input hidden" value="${lbuLocation.addressLine1}" />
				                                        	<span id="addressLine1ErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="addressLine1Error" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">addressLine2:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="addressLine2Span" class="span">${lbuLocation.addressLine2}</span>
				                                        	<input id="addressLine2Input" name="addressLine2" type="text" class="form-control input-sm input hidden" value="${lbuLocation.addressLine2}" />
				                                        	<span id="addressLine2ErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="addressLine2Error" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">cityName:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="cityNameSpan" class="span">${lbuLocation.cityName}</span>
				                                        	<input id="cityNameInput" name="cityName" type="text" class="form-control input-sm input hidden" value="${lbuLocation.cityName}" />
				                                        	<span id="cityNameErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="cityNameError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">auditUserId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="auditUserIdSpan" class="span">${lbuLocation.auditUserId}</span>
				                                        	<input id="auditUserIdInput" name="auditUserId" type="text" class="form-control input-sm input hidden" value="${lbuLocation.auditUserId}" />
				                                        	<span id="auditUserIdErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="auditUserIdError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">countryName:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="countryNameSpan" class="span">${lbuLocation.countryName}</span>
				                                        	<input id="countryNameInput" name="countryName" type="text" class="form-control input-sm input hidden" value="${lbuLocation.countryName}" />
				                                        	<span id="countryNameErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="countryNameError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">zipCode:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="zipCodeSpan" class="span">${lbuLocation.zipCode}</span>
				                                        	<input id="zipCodeInput" name="zipCode" type="text" class="form-control input-sm input hidden" value="${lbuLocation.zipCode}" />
				                                        	<span id="zipCodeErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="zipCodeError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">stateOrProvinceCode:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="stateOrProvinceCodeSpan" class="span">${lbuLocation.stateOrProvinceCode}</span>
				                                        	<input id="stateOrProvinceCodeInput" name="stateOrProvinceCode" type="text" class="form-control input-sm input hidden" value="${lbuLocation.stateOrProvinceCode}" />
				                                        	<span id="stateOrProvinceCodeErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="stateOrProvinceCodeError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">countryCode:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="countryCodeSpan" class="span">${lbuLocation.countryCode}</span>
				                                        	<input id="countryCodeInput" name="countryCode" type="text" class="form-control input-sm input hidden" value="${lbuLocation.countryCode}" />
				                                        	<span id="countryCodeErrorMessage" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="countryCodeError" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                </tbody>
			                                </table>
		                                </div>
	                                </form>
                                </div>
                                <!-- end row -->
                                
                                <div class="row">
	                                	<div class="col-sm-6">
	                                	</div>
		                                
		                                <div class="col-sm-6">  
		                                	<div class="col-sm-offset-2 pull-right">
		                                		<button type="button" id="cancel" class="btn btn-w-m btn-outline btn-warning  hidden"
		                                		<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
		                                		>
			                                		Cancel
			                                	</button>
			                                	<button type="button" id="save" class="btn btn-w-m btn-success  hidden"
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
		                                			Save
			                                	</button>     
			                                	
			                                	<button type="button" id="modify" class="btn btn-w-m btn-danger" 
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
			                                		Modify Pob
			                                	</button>
		                                	</div>
	                                    </div>
                                </div>
                                
                                <div class="row">
                                	<div class="col-lg-12">
	                                	<a href="${baseContextUrl}admin/location/list" class="btn btn-w-m btn-outline btn-primary pull-right">Back to the Pob list</a>
                                	</div>
                                </div>
                                
		                    </div>
						   <!-- end content -->	
	       			</div>
	            </div>
	        </div>
	    </div>

        <script> 

        	<c:choose>
	        	<c:when test="${empty lbuAccount.addressPGUID}">
	        	  var ecmAction = 'CREATE';
	        	</c:when>
	        	<c:otherwise>
		       	  var ecmAction = 'UPDATE';
		       	</c:otherwise>
	       	</c:choose>
        	var urlSendLocationToEcm = baseContextUrl + 'api/send/ecm/location/' + ecmAction;
            

        	
        	var locationsForm = {
        			'lbuLocationId': '${lbuLocation.lbuLocationId}',
        			'lbuAccountId': '${lbuLocation.lbuAccountId}',
   					'addressLine1' : '${lbuLocation.addressLine1}',
   					'addressLine2' : '${lbuLocation.addressLine2}',
   					'auditUserId': '${lbuLocation.auditUserId}',
   					'cityName' : '${lbuLocation.cityName}',
   					'zipCode' : '${lbuLocation.zipCode}',
   					'stateOrProvinceCode' : '${lbuLocation.stateOrProvinceCode}',
   					'countryName' : '${lbuLocation.countryName}',
   					'countryCode' : '${lbuLocation.countryCode}',
   					'lbuSetId': '${lbuLocation.lbuSetId}',
   					'addressPGUID': '${lbuLocation.addressPGUID}',
   					'pobPGUID': '${lbuLocation.pobPGUID}'
        	};
			
        </script>
    </jsp:body>
</t:adminlayout>