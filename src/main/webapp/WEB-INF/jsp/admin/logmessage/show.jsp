<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
        <link href="<c:url value="/front/inspinia/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js" />"></script>
   		<script src="<c:url value="/front/js/admin/logmessage/edit.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title">
					           <h5>LogMessage</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
									<form action="${baseContextUrl}admin/logmessage/update" method="post" name="messageForm" id="messageForm">
										<div class="table-responsive col-lg-12" id="logMessageTable">
				                            <table class="table table-striped table-hover">
				                                <tbody>
				                                	<tr>
				                                        <td class="col-md-3"><label class="control-label">messageId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.messageId}</span>
				                                        	<input type="hidden" name="messageId" id="messageId" value="${logMessage.messageId}"  />
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">processType:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.processType}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">processSubType:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.processSubType}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">actionId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span" id="actionId">${logMessage.actionId}</span>
				                                        	<select id="actionIdInput" name="actionId" class="form-control select hidden" value="${logMessage.actionId}">
				                                        	
					                                        	<c:forEach items="${mapActionIdList}" var="entry">
					                                        		<option value="${entry.key}" <c:if test="${entry.key == logMessage.actionId}"> selected </c:if> >${entry.value}</option>
					                                        	</c:forEach>
				                                        	</select>
			                                        	<span id="noReNewErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="noReNewIndError" class="error"></label>
			                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">status:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span" id="status">${logMessage.status}</span>
				                                        	
				                                        	<select id="statusInput" name="status" class="form-control select hidden" value="${logMessage.status}">
				                                        	
					                                        	<c:forEach items="${mapMessageStatusList}" var="entry">
					                                        		<option value="${entry.key}" <c:if test="${entry.key == logMessage.status}"> selected </c:if> >${entry.value}</option>
					                                        	</c:forEach>
				                                        	</select>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">message:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.message}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">transId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.transId}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">groupId:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.groupId}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">application:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.application}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">createdBy:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.createdBy}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">createdDatetime:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.createdDatetime}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">updatedBy:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.updatedBy}</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td class="col-md-3"><label class="control-label">updatedDatetime:</label></td>
				                                        <td class="col-md-9">
				                                        	<span class="span">${logMessage.updatedDatetime}</span>
				                                        </td>
				                                    </tr>
				                                </tbody>
			                                </table>
		                                </div>
									</form>
                                </div>
                                <!-- end row -->
                                
 
                                
                                <div class="row">
                                	<div class="col-sm-6">

	                                </div>
                                	<div class="col-lg-6">
                                		<div class="col-sm-offset-2 pull-right">
                                				<button type="button" id="cancel" class="btn btn-w-m btn-outline btn-warning  hidden"
		                                		<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
		                                		>
			                                		Cancel
			                                	</button>
			                                	<button type="button" id="save" class="btn btn-w-m btn-success  hidden"
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
		                                			Save
			                                	</button>   
			                                	  
			                                	<button type="button" id="modify" class="btn btn-w-m btn-danger" 
			                                	<c:if test="${  adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
			                                		Modify
			                                	</button>
	                                		<a href="${baseContextUrl}admin/logmessage/list" class="btn btn-w-m btn-outline btn-primary" id="messageList">Back to the Message list</a>
                                		</div>
                                	</div>
                                </div>
                                
		                    </div>
						   <!-- end content -->	
	       			</div>
	            </div>
	        </div>
	    </div>
		<script>
		var messageForm = {
				'messageId' : '${logMessage.messageId}',
				'status' : '${logMessage.status}',
				'actionId' : '${logMessage.actionId}',
		};
		</script>
	</jsp:body>
</t:adminlayout>