<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/js/admin/account/show.js" />"></script>
    	<script src="<c:url value="/front/js/admin/account/edit.js" />"></script>
    	<script src="<c:url value="/front/js/admin/location/edit.js" />"></script>
    	<script src="<c:url value="/front/js/admin/agreement/send.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>

    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title">
					           <h5>account</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
									<form action="${baseContextUrl}/admin/account/update" method="post" name="accountForm" id="accountForm">
									<div class="table-responsive col-lg-6" id="accountTable">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                	<tr>
			                                        <td class="col-md-3"><label class="control-label">lbuAccountId:</label></td>
			                                        <td class="col-md-9">
			                                        	<span id="lbuAccountIdSpan" class="span">
			                                        		<a href="${baseContextUrl}admin/account/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}">${lbuAccount.lbuAccountId}</a>
			                                        	</span>
			                                        	<input id="lbuAccountIdInput" name="lbuAccountId" type="text" class="form-control input-sm input hidden" readonly value="${lbuAccount.lbuAccountId}" />
			                                        	<span id="lbuAccountIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuAccountIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">accountName1:</label></td>
			                                        <td>
			                                        	<span id="accountName1Span" class="span">${lbuAccount.accountName1}</span>
			                                        	<input id="accountName1Input" name="accountName1" type="text" class="form-control input-sm input hidden" value="${lbuAccount.accountName1}" />
			                                        	<span id="accountName1ErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="accountName1Error" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">accountName2:</label></td>
			                                        <td>
			                                        	<span id="accountName2Span" class="span"> ${lbuAccount.accountName2}</span>
			                                        	<input id="accountName2Input" name="accountName2" type="text" class="form-control input-sm input hidden" value="${lbuAccount.accountName2}" />
			                                        	<span id="accountName2ErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="accountName2Error" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">auditUserId:</label></td>
			                                        <td>
			                                       		<span id="auditUserIdSpan" class="span"> ${lbuAccount.auditUserId}</span>
			                                        	<input id="auditUserIdInput" name="auditUserId" type="text" class="form-control input-sm input hidden" readonly value="${lbuAccount.auditUserId}" />
			                                        	<span id="auditUserIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="auditUserIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                   
			                                    <tr>
			                                        <td><label class="control-label">updatedDatetime:</label></td>
			                                        <td>${lbuAccount.updatedDatetime}</td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">createdDatetime:</label></td>
			                                        <td>${lbuAccount.createdDatetime}</td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">lbuSetId:</label></td>
			                                        <td>
			                                        	<span id="lbuSetIdSpan" class="span"> ${lbuAccount.lbuSetId}</span>
			                                        	<input id="lbuSetIdInput" name="lbuSetId" type="text" class="form-control input-sm input hidden" readonly value="${lbuAccount.lbuSetId}" />
			                                        	<span id="lbuSetIdErrorMessage" class="help-block m-b-none errorMessage hidden">
			                                        		<label id="lbuSetIdError" class="error"></label>
			                                        	</span>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">customerPguid:</label></td>
			                                        <td>${lbuAccount.customerPguid}</td>
			                                    </tr>
			                                    <tr>
			                                        <td><label class="control-label">pobpguid:</label></td>
			                                        <td>${lbuAccount.pobpguid}</td>
			                                    </tr>

			                                </tbody>
		                                </table>
		                                
	                                </div>
	                                </form>
	                                
	                                <div class="table-responsive col-lg-6" id="addressTables">
	                                	
	                                	<c:forEach items="${lbuLocationsList}" var="lbuLocation">
	                                		<form action="${baseContextUrl}/admin/location/update" method="post" name="locationForm${lbuLocation.lbuLocationId}" id="accountForm${lbuLocation.lbuLocationId}">
				                            <table class="table table-striped table-hover tableAddress" id="tableAddress${lbuLocation.lbuLocationId}">
				                                <tbody>
				                                	 <tr>
				                                        <td class="col-md-3"><label class="control-label">addressLine1:</label></td>
				                                        <td class="col-md-9">
				                                        	<span id="addressLine1Span${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.addressLine1}</span>
				                                        	<input id="addressLine1Input${lbuLocation.lbuLocationId}" name="addressLine1" type="text" class="form-control input-sm input hidden" value="${lbuLocation.addressLine1}" />
				                                        	<span id="addressLine1ErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="addressLine1Error${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td><label class="control-label">addressLine2:</label></td>
				                                        <td>
				                                       		<span id="addressLine2Span${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.addressLine2}</span>
				                                        	<input id="addressLine2Input${lbuLocation.lbuLocationId}" name="addressLine2" type="text" class="form-control input-sm input hidden" value="${lbuLocation.addressLine2}" />
				                                        	<span id="addressLine2ErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="addressLine2Error${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td><label class="control-label">cityName:</label></td>
				                                        <td>
				                                        	<span id="cityNameSpan${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.cityName}</span>
				                                        	<input id="cityNameInput${lbuLocation.lbuLocationId}" name="cityName" type="text" class="form-control input-sm input hidden" value="${lbuLocation.cityName}" />
				                                        	<span id="cityNameErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="cityNameError${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td><label class="control-label">countryName:</label></td>
				                                        <td>
				                                        	<span id="countryNameSpan${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.countryName}</span>
				                                        	<input id="countryNameInput${lbuLocation.lbuLocationId}" name="countryName" type="text" class="form-control input-sm input hidden" value="${lbuLocation.countryName}" />
				                                        	<span id="countryNameErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="countryNameError${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td><label class="control-label">zipCode:</label></td>
				                                        <td>
				                                        	<span id="zipCodeSpan${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.zipCode}</span>
				                                        	<input id="zipCodeInput${lbuLocation.lbuLocationId}" name="zipCode" type="text" class="form-control input-sm input hidden" value="${lbuLocation.zipCode}" />
				                                        	<span id="zipCodeErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="zipCodeError${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td><label class="control-label">stateOrProvinceCode:</label></td>
				                                        <td>
				                                        	<span id="stateOrProvinceCodeSpan${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.stateOrProvinceCode}</span>
				                                        	<input id="stateOrProvinceCodeInput${lbuLocation.lbuLocationId}" name="stateOrProvinceCode" type="text" class="form-control input-sm input hidden" value="${lbuLocation.stateOrProvinceCode}" />
				                                        	<span id="stateOrProvinceCodeErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="stateOrProvinceCodeError${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td><label class="control-label">countryCode:</label></td>
				                                        <td>
				                                        	<span id="countryCodeSpan${lbuLocation.lbuLocationId}" class="span"> ${lbuLocation.countryCode}</span>
				                                        	<input id="countryCodeInput${lbuLocation.lbuLocationId}" name="countryCode" type="text" class="form-control input-sm input hidden" value="${lbuLocation.countryCode}" />
				                                        	<span id="countryCodeErrorMessage${lbuLocation.lbuLocationId}" class="help-block m-b-none errorMessage hidden">
				                                        		<label id="countryCodeError${lbuLocation.lbuLocationId}" class="error"></label>
				                                        	</span>
				                                        </td>
				                                    </tr>
				                                    <tr>
				                                        <td>
				                                        </td>
				                                        <td>
															<a href="${baseContextUrl}/admin/location/${lbuLocation.lbuSetId}/${lbuLocation.lbuAccountId}/${lbuLocation.lbuLocationId}#modify" class="modifyLocation btn btn-w-m btn-danger pull-right"
															<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
															>
						                                		Modify Pob
						                                	</a>
				                                        </td>
				                                    </tr>
				                                </tbody>
			                                </table>
			                                <input type="hidden" id="lbuLocationId${lbuLocation.lbuLocationId}" class="lbuLocationId" value="${lbuLocation.lbuLocationId}" />
			                                </form>
			                                
			                                <div class="col-sm-offset-2 pull-right">
				                                <button type="button" id="${lbuLocation.lbuLocationId}" class="cancelLocation btn btn-w-m btn-outline btn-warning  hidden">
			                                		Cancel
			                                	</button>
			                                	<button type="button" id="${lbuLocation.lbuLocationId}" class="saveLocation btn btn-w-m btn-success  hidden">
		                                			Save Location
			                                	</button> 
				                                <button type="button" id="${lbuLocation.lbuLocationId}" class="modifyLocation btn btn-w-m btn-danger hidden">
			                                		Modify Location
			                                	</button>
		                                	</div>
		                                </c:forEach>
	                                </div>
	                                
	                                
                                </div>
                                <!-- end row -->
                                
                                <div class="row">
	                                	<div class="col-sm-6">
	                                		<label id="showLocationLabel">
		                                		<input id="showLocation" type="checkbox"> 
			                                	<span id="showLocationSpan">Show places of business</span>
		                                	</label>
		                                	<label id="showAgreementLabel">
		                                		<input id="showAgreement" type="checkbox"> 
			                                	<span id="showAgreementSpan">Show agreements</span>
		                                	</label> 
	                                	</div>
		                                
		                                <div class="col-sm-6">  
		                                	<div class="col-sm-offset-2 pull-right">
		                                		<button type="button" id="cancel" class="btn btn-w-m btn-outline btn-warning  hidden"
		                                		<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
		                                		>
			                                		Cancel
			                                	</button>
			                                	<button type="button" id="saveAccount" class="btn btn-w-m btn-success  hidden"
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
		                                			Save
			                                	</button>    
			                                	
			                                	
			                                	
			                                	<button type="button" id="sendAccountToEcm" class="btn btn-w-m btn-primary "
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL and  adminUser.role ne roles.ROLE_SEND }">disabled</c:if>
			                                	>
			                                		Send account to Ecm
			                                	</button>

			                                	<button type="button" id="modify" class="btn btn-w-m btn-danger "
			                                	<c:if test="${ adminUser.role ne roles.ROLE_ALL }">disabled</c:if>
			                                	>
			                                		Modify Account
			                                	</button>
			 
		                                	</div>
	                                    </div>
                                </div>
                                
                                <div class="row">
                                	<div class="table-responsive col-lg-12">
		                                <table class="table table-striped table-bordered table-hover dataTables" id="locationListTable">
								    	</table>
							    	</div>
                                </div>
                                
                                
                                <div class="row">
                                	<div class="table-responsive col-lg-12" id="agreementGroupListTables">
                                	</div>
                                </div>	
                                
                                <div class="row">
                                	<div class="table-responsive col-lg-12" id="sendAgreementsBouton">
                                		<button type="button" id="sendAgreements" class="btn btn-primary pull-right"
                                		<c:if test="${ adminUser.role ne roles.ROLE_ALL and adminUser.role ne roles.ROLE_SEND }">disabled</c:if>
                                		>
	                                		Send agreements
	                                	</button>
                                	</div>
                                </div>
                                
                                <!--div class="row">
                                	<div class="table-responsive col-lg-12">
		                                <table class="table table-striped table-bordered table-hover dataTables" id="contentSubListTable">
								    	</table>
							    	</div>
                                </div-->
                                
                                <div class="row">
                                	<div class="col-lg-12">
	                                	<a href="${baseContextUrl}/admin/account/list" class="btn btn-w-m btn-outline btn-primary pull-right">Back to the Account list</a>
                                	</div>
                                </div>
                                
		                    </div>
						   <!-- end content -->	
	       			</div>
	            </div>
	        </div>
	    </div>

        <script> 
        	var urlContentSubListByAccountId= baseContextUrl + '/api/contentSub/listByAccountId/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}.json';
        	var urlAgreementListByAccountId= baseContextUrl + '/api/agreement/listByAccountId/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}.json';
        	var urlAgreementGroupListByAccountId = baseContextUrl + '/api/agreementGroup/listByAccountId/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}.json';
        	var urlLocationListByAccountId = baseContextUrl + '/api/location/listByAccountId/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}.json';
        	var urlSendAgreements = baseContextUrl + '/api/send/agreements';
        	
        	<c:choose>
	        	<c:when test="${empty lbuAccount.customerPguid}">
	        	  var ecmAction = 'CREATE';
	        	</c:when>
	        	<c:otherwise>
		       	  var ecmAction = 'UPDATE';
		       	</c:otherwise>
	       	</c:choose>
        	var urlSendAccountToEcm = baseContextUrl + '/api/send/ecm/account/${lbuAccount.lbuAccountId}/${lbuAccount.lbuSetId}/' + ecmAction;
            
        	var accountForm = {
        			'lbuAccountId' : '${lbuAccount.lbuAccountId}',
        			'gbsAccountId' : '${lbuAccount.gbsAccountId}',
        			'glpAccountId' : '${lbuAccount.glpAccountId}',
        			'accountName1' : '${lbuAccount.accountName1}',
        			'accountName2' : '${lbuAccount.accountName2}',
        			'auditUserId' : '${lbuAccount.auditUserId}',
        			'salesRepId' : '${lbuAccount.salesRepId}',
        			'extOrderNo' : '${lbuAccount.extOrderNo}',
        			'maxUsers' : '${lbuAccount.maxUsers}',
        			'billableStatus' : '${lbuAccount.billableStatus}',
        			'accountType' : '${lbuAccount.accountType}',
        			'addressLine1' : '${lbuAccount.addressLine1}',
        			'addressLine2' : '${lbuAccount.addressLine2}',
        			'cityName' : '${lbuAccount.cityName}',
        			'countryName' : '${lbuAccount.countryName}',
        			'zipCode' : '${lbuAccount.zipCode}',
        			'stateOrProvinceCode' : '${lbuAccount.stateOrProvinceCode}',
        			'countryCode' : '${lbuAccount.countryCode}',
        			'updatedDatetime' : '${lbuAccount.updatedDatetime}',
        			'createdDatetime' : '${lbuAccount.createdDatetime}',
        			'dataMigration' : '${lbuAccount.dataMigration}',
        			'lbuSetId' : '${lbuAccount.lbuSetId}',
        			'customerPguid' : '${lbuAccount.customerPguid}',
        			'pobpguid' : '${lbuAccount.pobpguid}'
        	};
        	/*
        	var locationsForm = {
        			<c:forEach items="${lbuLocationsList}" var="lbuLocation">
	        				'${lbuLocation.lbuLocationId}' : {
	        					'addressLine1' : '${lbuLocation.addressLine1}',
	        					'addressLine2' : '${lbuLocation.addressLine2}',
	        					'cityName' : '${lbuLocation.cityName}',
	        					'zipCode' : '${lbuLocation.zipCode}',
	        					'stateOrProvinceCode' : '${lbuLocation.stateOrProvinceCode}',
	        					'countryName' : '${lbuLocation.countryName}',
	        					'countryCode' : '${lbuLocation.countryCode}'
	        				}
        			</c:forEach>
        	};
			*/
        </script>
    </jsp:body>
</t:adminlayout>