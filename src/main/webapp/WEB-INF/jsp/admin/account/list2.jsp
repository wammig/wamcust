<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>

<t:adminlayout>
	<jsp:attribute name="extra_css">
        <link href="<c:url value="/front/inspinia/css/plugins/dataTables/datatables.min.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/dataTables/datatables.min.js" />"></script>
        <script src="<c:url value="/front/js/admin/account/list2.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title">
					           <h5>List of account</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       </div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content">
		                        <div class="table-responsive">
		                            <table class="table table-striped table-bordered table-hover dataTables" id="userListTable" style="width: 100%">

								    </table>
		                        </div>
		                    </div>
						   <!-- content -->	
	       			</div>
	            </div>
	        </div>
	    </div>
    
        <script>
            var searchFilterList = ${searchFilterList};
        	var accountList = ${lbuAccountsJson};
        </script>
    </jsp:body>
</t:adminlayout>