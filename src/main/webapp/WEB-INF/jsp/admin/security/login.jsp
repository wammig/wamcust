<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/js/admin/security/login.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	
    	<div class="middle-box text-center loginscreen animated fadeInDown">
        	<div class="row ibox-content">
            		
            <h3>Bowam</h3>


            <form class="m-t" role="form" action="index.html">
                <div class="form-group">
                    <input type="text" class="form-control" id="username" placeholder="Windows ID" required="true">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" placeholder="Windows Password" required="true">
                </div>
                <div class="progress progress-striped active hidden" id="login-progress">
                     <div style="width: 100%" class="progress-bar progress-bar-success">
                         <span class="sr-only">Loading...</span>
                     </div>
                </div>
                <button type="button" class="btn btn-primary full-width m-b" id="login">Login</button>
            </form>
                    
	        </div>
	    </div>

        <script> 
        	var urlLoginApi = baseContextUrl + 'security/api/login';
        	var urlHomeApi = baseContextUrl + 'admin';
        </script>
    </jsp:body>
</t:adminlayout>