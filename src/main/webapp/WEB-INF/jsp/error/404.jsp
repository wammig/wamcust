<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	<body class="gray-bg">
	    	<div class="middle-box text-center animated fadeInDown">
		        <h1>404</h1>
		        <h3 class="font-bold">Page Not Found</h3>
	    	</div>
	
	        <script> 
	        	var urlWindow = '${requestScope['javax.servlet.forward.request_uri']}';
	        </script>
        </body>
    </jsp:body>
</t:adminlayout>