<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
		<link href="<c:url value="/front/css/style.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script>	
    	  if(browser() == 'IE' || browser() == 'Firefox')
    	  {
    		  loadjscssfile("/wam/front/js/admin/transformer/agreement.js", "js");
    		  loadjscssfile("/wam/front/js/admin/transformer/index.js", "js");
    	  }	  
    	</script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title">
					           <h5>WAM transform result</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       	</div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content">
						   		<!-- row -->
								<div class="row">
									<c:forEach items="${messages}" var="messageObject">
									    <div class="alert <c:if test="${messageObject.status == 'SUCCESS'}">alert-success</c:if> <c:if test="${messageObject.status == 'FAILED'}">alert-danger</c:if>">
									    	${messageObject.message}
									    </div>
									</c:forEach>
									
									<c:if test="${isRealSendAgreement}" >
	                                	<input type="hidden" id="transIdProduct" value="${transIdProduct}" />
	                                	<div class="h1 m-t-xs text-navy">                        	
		                                	<button type="button" id="checkSynchronisationAgreements" class="btn btn-w-m btn-primary pull-right" onclick="actionCheckSynchronisationAgreements()">
		                                		Check Synchronisation status of agreements
		                                	</button>
		                                	<span id="loadingCheckSynchronisationAgreements" class="loading bullet pull-right"></span>
	                                	</div>
									</c:if>
                                </div>
                                
                                <!-- end row -->                           
                                
		                    </div>
						   <!-- end content -->	
	       			</div>
	            </div>
	        </div>
	    </div>
	    
	    <c:if test="${not empty wsMessages && isITParis }">
		    <div class="wrapper wrapper-content animated fadeInRight">
	        	<div class="row">
	            	<div class="col-lg-12">
	                	<div class="ibox float-e-margins">
		    				
		    				<!-- title part -->
						    	<div class="ibox-title">
						           <h5>ECM requests</h5>
						           <div class="ibox-tools">
						               <a class="collapse-link">
						                   <i class="fa fa-chevron-up"></i>
						               </a>
						           </div>
						       	</div>
						       <!-- title part -->
						       
						       <!-- content -->
							   <div class="ibox-content">
							   		<!-- row -->
									<div class="row">
									
										<div class="panel-body">
											<div class="panel-group" id="accordion">
												<c:forEach items="${wsMessages}" var="wsMessageObject">
													<div class="panel panel-success">
														<div class="panel-heading">
															<h5 class="panel-title">
																<a data-toggle="collapse" data-parent="#accordion" href="#${wsMessageObject.id}" aria-expanded="true" class="">${ wsMessageObject.url } <span class="label label-warning">${ wsMessageObject.method }</span> </a>
															</h5>
														</div>
														<div id="#${wsMessageObject.id}" class="panel-collapse collapse in" aria-expanded="true">
															<div class="panel-body">
																<div class="row">
																	<span class="label label-warning">Data sent:</span>
																	<textarea class="form-control" rows="50">${ wsMessageObject.messageOut }</textarea>
																</div>
																
																<br>
																<div class="row">
																	<span class="label label-warning">Response:</span>				
																	<textarea class="form-control" rows="10">${ wsMessageObject.messageIn }</textarea>
																</div>
															</div>
														</div>
													</div>
												</c:forEach>
											</div>
										</div>
									
									</div>
									<!-- end row -->  
								</div>
		    					<!-- end content -->	
						</div>
		            </div>
		        </div>
		    </div>
		</c:if>			
					
        <script> 
        	var urlWindow = '${requestScope['javax.servlet.forward.request_uri']}';
        	var urlWindowAjax = urlWindow + '/ajax';
        	<c:if test="${not empty transIdProduct}" >
        	var urlOrderReplyHistory = baseContextUrl + '/order/reply/history/${transIdProduct}.json'; 
        	</c:if>
        </script>
    </jsp:body>
</t:adminlayout>