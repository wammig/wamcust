<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/layout/admin" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:adminlayout>
	<jsp:attribute name="extra_css">
		<link href="<c:url value="/front/css/style.css" />" rel="stylesheet">
    </jsp:attribute>
    <jsp:attribute name="extra_js">
    	<script src="<c:url value="/front/inspinia/js/plugins/iCheck/icheck.min.js" />"></script>
    	<script src="<c:url value="/front/inspinia/js/plugins/sweetalert/sweetalert.min.js" />"></script>
    	<script src="<c:url value="/front/js/admin/transformer/agreement.js" />"></script>
    	<script src="<c:url value="/front/js/admin/transformer/index.js" />"></script>
    </jsp:attribute>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
    </jsp:attribute>
    <jsp:body>
    	
    	<div class="wrapper wrapper-content animated fadeInRight">
        	<div class="row">
            	<div class="col-lg-12">
                	<div class="ibox float-e-margins">
    	
    	
					    	<!-- title part -->
					    	<div class="ibox-title" id="load-result">
					           <h5>WAM transform result</h5>
					           <div class="ibox-tools">
					               <a class="collapse-link">
					                   <i class="fa fa-chevron-up"></i>
					               </a>
					           </div>
					       	</div>
					       <!-- title part -->
					       
					       
						   <!-- content -->
						   <div class="ibox-content text-center hidden" id="loading-content">
						   		<div class="h1 m-t-xs text-navy">
	                                <span class="loading bullet"></span>
	                                <button class="btn btn-primary">Loading</button>
	                            </div>                         
	                                
		                    </div>
						   <!-- end content -->	
	       			</div>
	            </div>
	        </div>
	    </div>
	    
		
					
        <script> 
        	var urlWindow = '${requestScope['javax.servlet.forward.request_uri']}';
        	var urlWindowAjax = urlWindow + '/ajax';
        </script>
    </jsp:body>
</t:adminlayout>