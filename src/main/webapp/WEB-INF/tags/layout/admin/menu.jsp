<nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
            	<li class="nav-header">
                    <div class="dropdown profile-element"> 
                   		<span>
                   			<i class="fa fa-user-circle fa-3x" aria-hidden="true"></i>
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
	                        <span class="clear"> 
		                        <span class="block m-t-xs"> 
		                        	<strong class="font-bold">
		                        		<span class="text-muted text-xs block"><c:out value="${adminUser.windowsId}"/> <b class="caret"></b></span> 
		                        	</strong>
		                        </span> 
	                        </span> 
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="#">${adminUser.role}</a></li>
                            <li class="divider"></li>
                            <li><a href="${baseContextUrl}security/logout">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        LN
                    </div>
                </li>
                <li id="accountListMenu">
                    <a href="${baseContextUrl}admin/account/list2"><i class="fa fa-users"></i> <span class="nav-label">Account List </span><span class="label label-info pull-right"></span></a>
                </li>
                <li id="locationListMenu">
                    <a href="${baseContextUrl}admin/location/list"><i class="fa fa-globe"></i> <span class="nav-label">Pob List</span> <span class="label label-warning pull-right"></span></a>
                </li>
                <li id="agreementListMenu">
                    <a href="${baseContextUrl}/admin/agreement/list"><i class="fa fa-database"></i> <span class="nav-label">Agreement List</span> <span class="label label-warning pull-right"></span></a>
                </li>
                <li id="sysHistoryListMenu">
                    <a href="${baseContextUrl}/admin/syshistory/list"><i class="fa fa-bell"></i> <span class="nav-label">SysHistory List</span> <span class="label label-warning pull-right"></span></a>
                </li>
                <li id="logMessageListMenu">
                    <a href="${baseContextUrl}/admin/logmessage/list"><i class="fa fa-twitch"></i> <span class="nav-label">LogMessage List</span> <span class="label label-warning pull-right"></span></a>
                </li>
            </ul>

        </div>
    </nav>