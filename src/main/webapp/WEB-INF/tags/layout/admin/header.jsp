<div class="row border-bottom">
      <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	      <div class="navbar-header">
	          <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
	      </div>
	      
	      <ul class="nav navbar-top-links navbar-right">
	      	  <li>
    	  		      <c:if test="${adminUser.role == roles.ROLE_ALL}">
			          	<span class="label label-primary">${adminUser.role}</span>
			          </c:if>
			          <c:if test="${adminUser.role == roles.ROLE_SEND}">
			          	<span class="label label-warning">${adminUser.role}</span>
			          </c:if>
			          <c:if test="${adminUser.role == roles.ROLE_READ_ONLY}">
			          	<span class="label label-danger">${adminUser.role}</span>
			          </c:if>
	      	  </li>		
	          <li>
	              <span class="m-r-sm text-muted welcome-message">Wam Back Office</span>
	          </li>
	
	          <li>
	              <a href="${baseContextUrl}security/logout">
	                  <i class="fa fa-sign-out"></i> Log out
	              </a>
	          </li>
	      </ul>

      </nav>
 </div>