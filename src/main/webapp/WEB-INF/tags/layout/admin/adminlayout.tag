<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@tag description="Layout" pageEncoding="UTF-8"%>
<%@attribute name="extra_css" fragment="true" %>
<%@attribute name="extra_js" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=""> <!--<![endif]-->

<c:set var="scheme" value="${pageContext.request.scheme}"/>
<c:set var="port" value="${pageContext.request.serverPort}"/>
<c:set var="contextPath" value="${pageContext.request.contextPath}" scope="application" />
<c:set var="baseContextUrl" value="${scheme}://${pageContext.request.serverName}:${port}${contextPath}/" scope="application" />

<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Bowam</title>
        
        <script>
	    	var baseURL = '${scheme}://${pageContext.request.serverName}:${port}';
	    	var baseContextUrl = '${baseContextUrl}';
	    	var urlWindow = baseURL + '${requestScope['javax.servlet.forward.request_uri']}';
	    </script>
        
		<link href="<c:url value="/front/inspinia/css/bootstrap.min.css" />" rel="stylesheet">
		<link href="<c:url value="/front/inspinia/font-awesome/css/font-awesome.css" />" rel="stylesheet">
		<link href="<c:url value="/front/inspinia/css/animate.css" />" rel="stylesheet">
		<link href="<c:url value="/front/inspinia/css/style.css" />" rel="stylesheet">
		<link href="<c:url value="/front/inspinia/css/plugins/sweetalert/sweetalert.css" />" rel="stylesheet">
		<link href="<c:url value="/front/inspinia/css/plugins/iCheck/custom.css" />" rel="stylesheet">
	 	<link href="<c:url value="/front/inspinia/css/plugins/textSpinners/spinners.css" />" rel="stylesheet">
		<!-- link href="<c:url value="/front/inspinia/css/plugins/toastr/toastr.min.css" />" rel="stylesheet"-->
	    <!--[if lt IE 9]>
			<script src="/front/inspinia/js/jquery-1.4.min.js"></script>
		<![endif]-->
		<jsp:invoke fragment="extra_css"/>
		<style>
			::-ms-clear {
			    display: none;
			}
		</style>
		
	</head>	
  <body>
    <div id="header">
      
      <jsp:invoke fragment="header"/>
    </div>
    <div id="body">
    		<c:if test="${ empty noLayout}">
    			<%@include file="menu.jsp" %>
    	
    			<div id="page-wrapper" class="gray-bg">
    		</c:if>
    	
    		<c:if test="${ empty noLayout}">
	        	<%@include file="header.jsp" %>
	        	<%@include file="path.jsp" %> 
	        </c:if>
	        
	        	<jsp:doBody/>
	        
	        <c:if test="${ empty noLayout}">
	        	<%@include file="footer.jsp" %>
	        
        		</div>
        	</c:if>
    </div>
    
    <div id="footer">
      <jsp:invoke fragment="footer"/>
    </div>
    
    <script src="<c:url value="/front/inspinia/js/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/front/inspinia/js/bootstrap.min.js" />"></script>
    <script src="<c:url value="/front/inspinia/js/plugins/metisMenu/jquery.metisMenu.js" />"></script>
    <script src="<c:url value="/front/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js" />"></script>
    <script src="<c:url value="/front/inspinia/js/inspinia.js" />"></script>
    <script src="<c:url value="/front/inspinia/js/plugins/pace/pace.min.js" />"></script>
    <!-- script src="<c:url value="/front/inspinia/js/plugins/toastr/toastr.min.js" />"></script -->
    <script src="<c:url value="/front/js/admin/base/navigator.js" />"></script>
    <script src="<c:url value="/front/js/admin/base/include.js" />"></script>
    <script>
    	checkMenu(urlWindow);
    </script>
    <jsp:invoke fragment="extra_js"/>
  </body>
</html>