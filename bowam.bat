@echo on

cls

Set APP_HOME="C:\myLexis\wamcust"

java -jar %APP_HOME%/target/wamcust-0.0.1-SNAPSHOT.jar --spring.profiles.active=bowam --spring.config.location=classpath:/application-bowam.properties,file:///C:/myLexis/conf/db.properties
